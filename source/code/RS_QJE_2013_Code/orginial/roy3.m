%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Roy Model Optimal Tax Simulation
%
% EXECUTION FILE
% roy3.m
%
% Casey Rothschild & Florian Scheuer, June 2012
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clear all
close all
clc

delete roy3;
diary roy3;

global x num_w ggamma yy_theta yy_phi gam w dw ff gg f f_phi f_theta

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SETUP

% SET FORMATS
format long;
format compact;
warning off;

% HOW TO COMPUTE WAGE DISTRIBUTION
wd_comp=1; % 1=completely numerical approach, 2=half-analytical approach using independence of theta and phi

% LOAD DATA AND UNDERLYING PARAMETERS
load('logndist.mat'); % load skill distribution and other parameters
options=optimset('TolCon',1e-13,'TolFun',1e-13,'TolX',1e-13,'Display','off','MaxFunEvals',10000000,'MaxIter',200);

theta = grid_t';
phi = grid_p';

pmf_theta=max(0,pdf_t'-pdf_t(end)); %so that there is no jump at upper
%bound (ONLY DO ONE)
%pmf_theta=pdf_p';
cdf_theta=cumsum(pmf_theta);

aux=find(pmf_theta==0);
theta(aux)=[];
pmf_theta(aux)=[];
cdf_theta(aux)=[];
num_theta=length(theta);

%pmf_phi=max(0,pdf_p'-pdf_p(end)); %so that there is no jump at upper bound (ONLY DO ONE)
pmf_phi=pdf_p';
cdf_phi=cumsum(pmf_phi);

aux=find(pmf_phi==0);
phi(aux)=[];
pmf_phi(aux)=[];
cdf_phi(aux)=[];
num_phi=length(phi);

bin_width_theta = theta(2) - theta(1);
bin_width_phi = phi(2) - phi(1);
theta_min = min(theta);
theta_max = max(theta);
phi_min = min(phi);
phi_max = max(phi);

% using independence here
pmf=pmf_theta*pmf_phi';

%% FIX x
%xgrid=(.9:.01:1.1);
%xgrid=1; %W<7.25 truncation case, c-d, bottom at 1

%xgrid=.875; %W<1 truncation case, cobb-douglass, bottom truncated at 1

r=1.3;
%xgrid = (.7:.001:.88);
% xgrid = (0.2:.05:2)
xgrid = .875; %W<1 truncation case, cobb-douglass, bottom
% truncated at 1, 1.3 in swf 
%xgrid=.870; %W<1 truncation case, cobb-douglass, bottom truncated at 1, 1.5 in swf, top rate about -1.2%
%xgrid = .863; %W<1 truncation case, cobb-douglass, bottom truncated at 1, 1.8 in swf  -- top rate: about -1%
%xgrid = .858; %W<1 truncation case, cobb-douglass, bottom truncated at 1, 2 in swf  -- top rate: about -1%
%xgrid = .837; %W<1 truncation case, cobb-douglass, bottom truncated at 1, 3 in swf
%xgrid = .826; %W<1 truncation case, cobb-douglass, bottom truncated at 1, 4 in swf
%xgrid = .818; %W<1 truncation case, cobb-douglass, bottom truncated at 1, 5 in swf
%xgrid = .806; %W<1 truncation case, cobb-douglass, bottom truncated at 1, 10 in swf
%xgrid = .803; %W<1 truncation case, cobb-douglass, bottom truncated at 1, 15 in swf
%xgrid = .799; %W<1 truncation case, cobb-douglass, bottom truncated at 1, 1000 in swf (near Rawlsian) -- get bunching at bottom
%xgrid=(.54:.001:.58); 
%xgrid=.556;
%xgrid=1.034; %W<0.5 truncation, bottom at .1, cobb-douglass



%xgrid=(94.6:.1:94.8); %W<5 truncation case, rho = -0.5, alpha =.2
%xgrid=94.7; %W<5 truncation case, with bottom truncated at .1


% COMPUTE WELFARE FOR EACH x
welfaregrid=zeros(size(xgrid));
for j=1:size(xgrid,2)
x=xgrid(j);

% TECHNOLOGY & PREFERENCES
%alpha=.2;
%alpha=0.026593390690445; % now contained in the lognormal.mat loaded at
%the top of the file
%rho=0; % now contained in the lognormal.mat loaded at the top
if rho==1;
    yy_phi=1-alpha;
    yy_theta=alpha;
elseif rho==0;
    yy_phi=(1-alpha)*x.^alpha;
    yy_theta=alpha*x.^(alpha-1);
else
    yy_phi=(1-alpha)*(alpha*x^rho+1-alpha)^((1-rho)/rho);
    yy_theta=alpha/(1-alpha)*x^(rho-1)*yy_phi;
end

ggamma=yy_phi/(x*yy_theta);
%elast=.5; %now constained in the lognormal.mat loaded at the top
gam=1+1./elast;

%% COMPUTE WAGE DISTRIBUTION
w_min=max(yy_theta*theta_min,yy_phi*phi_min);
w_max=max(yy_theta*theta_max,yy_phi*phi_max);
num_w=num_theta;
dw=(w_max-w_min)/(num_w-1);
w=(w_min:dw:w_max)';

if wd_comp==1;

% WAGE AND OCCUPATIONAL CHOICE MATRIX
% WW=max(yy_theta*theta*ones(1,num_phi),yy_phi*ones(num_theta,1)*phi');
% OO_theta=(yy_theta*theta*ones(1,num_phi)>=yy_phi*ones(num_theta,1)*phi');
% OO_phi=ones(num_theta,num_phi)-OO_theta;
% 
% f_theta=zeros(num_w,1);
% f_phi=zeros(num_w,1);
% for i=1:num_w
%    f_theta(i)=sum(sum(OO_theta.*pmf.*(WW>=w(i)-dw/2).*(WW<w(i)+dw/2))); 
%    f_phi(i)=sum(sum(OO_phi.*pmf.*(WW>=w(i)-dw/2).*(WW<w(i)+dw/2)));
% end

cdf_aux_theta=cumsum(pmf,2);
[PHI,THETA]=meshgrid(phi,theta);
%[X,Y]=meshgrid(min(w./yy_phi,phi_max),w./yy_theta);
f_theta=max(0,1/yy_theta*interp2(PHI,THETA,cdf_aux_theta/bin_width_theta,min(w./yy_phi,phi_max),w./yy_theta))*dw;

cdf_aux_phi=cumsum(pmf,1);
%[X,Y]=meshgrid(w./yy_phi,min(w./yy_theta,theta_max));
f_phi=max(0,1/yy_phi*interp2(PHI,THETA,cdf_aux_phi/bin_width_phi,w./yy_phi,min(w./yy_theta,theta_max)))*dw;

elseif wd_comp==2;

% USE FORMULAS FOR INDEPENDENT CASE    
    
f_theta=max(0,1/yy_theta*interp1(theta,pmf_theta/bin_width_theta,w./yy_theta).*interp1(phi,cdf_phi,min(w./yy_phi,phi_max)))*dw;
f_phi=max(0,1/yy_phi*interp1(phi,pmf_phi/bin_width_phi,w./yy_phi).*interp1(theta,cdf_theta,min(w./yy_theta,theta_max)))*dw;

end

f=f_theta+f_phi;
f_theta=f_theta/sum(f);
f_phi=f_phi/sum(f);
f=f/sum(f);
ff=min(1,cumsum(f));
ff(end)=1;

% PARETO WEIGHTS
%r=1.3; See near xgrid -- set it there for easy testing
gg=1-(1-ff).^r;
g=gg-[0;gg(1:num_w-1)];

%% FIND xi
xi=fzero(@findxi,0);

% xi=(-10:.1:10);
% for i=1:length(xi)
%    check(i)=findxi(xi(i)); 
% end
% plot(xi,check,xi,zeros(size(xi)));

% xi=0;
% aux=zeros(num_w,1);
% for i=1:num_w
%        aux(i)=w(i).^(gam./(gam-1)).*(max(0,(1+xi.*(1+ggamma)*(yy_phi/(yy_phi+x*yy_theta)-f_phi(i)./f(i)))./(1+gam*(gg(i)-ff(i))./(w(i)*f(i)/dw)))).^(1/(gam-1)).*(ggamma*f_theta(i)-f_phi(i));
% end
% s = sum(aux);

% COMPUTE EFFORT AND MTR's
effort=w.^(1/(gam-1)).*(max(0,(1+xi*(1+ggamma)*(yy_phi/(yy_phi+x*yy_theta)-f_phi./f))./(1+gam*(gg-ff)./(w.*f/dw)))).^(1/(gam-1));
income =w.*effort;
mks=effort.^(gam-1)./w;
mtr=1-mks;

%% compute tax schedule
% Basic idea here: sort by income. Then compute tax schedule as a function of
% income. Then unsort to have taxes as a function of w. Then sum.

[ysort,ysortind]=sort(income);
[temp,unsortind]=sort(ysortind,'ascend');
mtrsort=mtr(ysortind);

ftemp = (f(1:num_w-1)+f(2:num_w))/2;
ftemp = [0;ftemp];

%display(sum(fftemp)*dw);

tax=zeros(num_w,1);
for i=2:num_w
   tax(i,1)=tax(i-1)+mtrsort(i-1).*(ysort(i)-ysort(i-1));
end
tax=tax(unsortind); %idea here is to re-attach taxes to wages

tax_sum=sum(tax.*ftemp); % since dw is constant, this sums tax(i)*((ff(i)+ff(i-1))/2)*dw from i=2 to gridsize
% sum(fftemp)dw-1 is very very close to zero
tax=tax-tax_sum;


%% compute consumption and welfare
cons=income-tax;
welfare_vec=cons-effort.^gam./gam;
welfare=sum(welfare_vec.*g);
cons_sum=sum(cons.*f);
inc_sum=sum(income.*f);

cons_m_inc=cons_sum-inc_sum;

welfaregrid(j)=welfare;

end

%% PLOT THINGS
plot(xgrid,welfaregrid)
[a b]=max(welfaregrid);
x_max=xgrid(b);

%% The remainder of the code only kicks in when we've already found the optimal x

if length(xgrid)==1

%% save the PO data
w_PO=w;
dw_PO=dw;
mtr_PO=mtr;
tax_PO=tax;
effort_PO=effort;
income_PO=income;
ff_PO=ff;
x_PO=x;
yy_theta_PO=yy_theta;
yy_phi_PO=yy_phi;

f_theta_PO=f_theta;
f_phi_PO=f_phi;
f_PO=f;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                                                      %%
%% SCPE                                                                 %%
%%                                                                      %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('SCPE');
x=xgrid;

%% iterate on x

disp('iterate on x');
crit=1;
while crit>1e-8
    
if rho==1;
    yy_phi=1-alpha;
    yy_theta=alpha;
elseif rho==0;
    yy_phi=(1-alpha)*x.^alpha;
    yy_theta=alpha*x.^(alpha-1);
else
    yy_phi=(1-alpha)*(alpha*x^rho+1-alpha)^((1-rho)/rho);
    yy_theta=alpha/(1-alpha)*x^(rho-1)*yy_phi;
end

%% COMPUTE WAGE DISTRIBUTION
w_min=max(yy_theta*theta_min,yy_phi*phi_min);
w_max=max(yy_theta*theta_max,yy_phi*phi_max);
num_w=num_theta;
dw=(w_max-w_min)/(num_w-1);
w=(w_min:dw:w_max)';

if wd_comp==1;

% WAGE AND OCCUPATIONAL CHOICE MATRIX
% WW=max(yy_theta*theta*ones(1,num_phi),yy_phi*ones(num_theta,1)*phi');
% OO_theta=(yy_theta*theta*ones(1,num_phi)>=yy_phi*ones(num_theta,1)*phi');
% OO_phi=ones(num_theta,num_phi)-OO_theta;
% 
% f_theta=zeros(num_w,1);
% f_phi=zeros(num_w,1);
% for i=1:num_w
%    f_theta(i)=sum(sum(OO_theta.*pmf.*(WW>=w(i)-dw/2).*(WW<w(i)+dw/2))); 
%    f_phi(i)=sum(sum(OO_phi.*pmf.*(WW>=w(i)-dw/2).*(WW<w(i)+dw/2)));
% end

cdf_aux_theta=cumsum(pmf,2);
[PHI,THETA]=meshgrid(phi,theta);
%[X,Y]=meshgrid(min(w./yy_phi,phi_max),w./yy_theta);
f_theta=max(0,1/yy_theta*interp2(PHI,THETA,cdf_aux_theta/bin_width_theta,min(w./yy_phi,phi_max),w./yy_theta))*dw;

cdf_aux_phi=cumsum(pmf,1);
%[X,Y]=meshgrid(w./yy_phi,min(w./yy_theta,theta_max));
f_phi=max(0,1/yy_phi*interp2(PHI,THETA,cdf_aux_phi/bin_width_phi,w./yy_phi,min(w./yy_theta,theta_max)))*dw;

elseif wd_comp==2;

% USE FORMULAS FOR INDEPENDENT CASE    
    
f_theta=max(0,1/yy_theta*interp1(theta,pmf_theta/bin_width_theta,w./yy_theta).*interp1(phi,cdf_phi,min(w./yy_phi,phi_max)))*dw;
f_phi=max(0,1/yy_phi*interp1(phi,pmf_phi/bin_width_phi,w./yy_phi).*interp1(theta,cdf_theta,min(w./yy_theta,theta_max)))*dw;

end

f=f_theta+f_phi;
f_theta=f_theta/sum(f);
f_phi=f_phi/sum(f);
f=f/sum(f);
ff=min(1,cumsum(f));
ff(end)=1;

% aux=find(f==0);
% f(aux)=[];
% f_theta(aux)=[];
% f_phi(aux)=[];
% ff(aux)=[];
% w(aux)=[];
% num_w=length(w);

% PARETO WEIGHTS
gg=1-(1-ff).^r;
g=gg-[0;gg(1:num_w-1)];

%% compute effort

effort=max(0,((w.*f./dw)./((gg-ff).*gam./w+f./dw)).^(1/(gam-1)));

%% compute implied E

lambda=.9;
x_impl=sum(w.*effort.*f_theta./yy_theta)./sum(w.*effort.*f_phi./yy_phi);
x_new=lambda*x+(1-lambda)*x_impl;
disp(x_new);
crit=abs(x-x_new);
x=x_new;

end

%% compute income and marginal tax rates

income =w.*effort;
mks=effort.^(gam-1)./w;
mtr=1-mks;

%% compute tax schedule

tax=zeros(1,num_w);
tax_sum=zeros(1,num_w);
for i=2:num_w
   tax(1,i)=tax(i-1)+mtr(i-1).*(income(i)-income(i-1));
   tax_sum(1,i)=tax(1,i).*f(i);
end
tax=tax-sum(tax_sum);
tax=tax';
    
% figure(1);
%    subplot(2,2,1); plot(w_PO,effort_PO,w,effort); title('effort'); grid on
%    subplot(2,2,2); plot(w_PO, mtr_PO,w,mtr); title('mtr'); grid on
%    subplot(2,2,3); plot(w_PO, f_PO,w,f_phi_PO); title('f and f phi'); grid on
%    subplot(2,2,4); plot(w_PO, f_phi_PO./f_PO,w,f_phi./f); title('phi-share'); grid on
% figure(2);
%    subplot(1,2,1); plot(theta,pmf_theta); title('pmf theta'); grid on
%    subplot(1,2,2); plot(theta,pmf_theta,phi,pmf_phi); title('pmf phi'); grid on
 
figure(1);
   subplot(2,2,1); plot(w_PO, mtr_PO,w,mtr); title('Marginal Tax Rate'); grid on
   subplot(2,2,2); plot(w_PO, tax_PO,w,tax); title('Tax Schedule'); grid on
   subplot(2,2,3); plot(w_PO, tax_PO./income_PO,w,tax./income); title('Average Tax Rate'); grid on
   subplot(2,2,4); plot(w_PO, f_phi_PO./f_PO,w,f_phi./f); title('Phi-share'); grid on

% % for asymptotics
% ind=max(find(w_PO<5));
% figure(1);
%    subplot(2,2,1); plot(w_PO(1:ind), mtr_PO(1:ind),w(1:ind),mtr(1:ind)); title('Marginal Tax Rate'); grid on
%    subplot(2,2,2); plot(w_PO(1:ind), tax_PO(1:ind),w(1:ind),tax(1:ind)); title('Tax Schedule'); grid on
%    subplot(2,2,3); plot(w_PO(1:ind), tax_PO(1:ind)./income_PO(1:ind),w(1:ind),tax(1:ind)./income(1:ind)); title('Average Tax Rate'); grid on
%    subplot(2,2,4); plot(w_PO(1:ind), f_phi_PO(1:ind)./f_PO(1:ind),w(1:ind),f_phi(1:ind)./f(1:ind)); title('Phi-share'); grid on

figure(2);
   subplot(1,2,1); plot(w_PO,effort_PO,w,effort); title('Effort'); grid on
   subplot(1,2,2); plot(w_PO,f_PO,w,f); title('Wage distribution'); grid on

figure(3);
   subplot(2,2,1); plot(income_PO, mtr_PO,income,mtr); title('Marginal Tax Rate'); grid on
   subplot(2,2,2); plot(income_PO, tax_PO,income,tax); title('Tax Schedule'); grid on
   subplot(2,2,3); plot(income_PO, tax_PO./income_PO,income,tax./income); title('Average Tax Rate'); grid on
   subplot(2,2,4); plot(income_PO, f_phi_PO./f_PO,income,f_phi./f); title('Phi-share'); grid on
figure(2);
   subplot(1,2,1); plot(income_PO,effort_PO,income,effort); title('Effort'); grid on
   subplot(1,2,2); plot(income_PO,f_PO,income,f); title('Wage distribution'); grid on
end

%% save stuff
%save input2.mat w_PO f_PO f_theta_PO f_phi_PO;

%w4=w_PO;
%mtr4=mtr_PO;

%save graphs.mat w4 mtr4 -append;

% load graphs.mat;
% 
% nn=length(w1);
% qq=(1/nn:1/nn:1);
% 
% figure(4);
% plot(qq,mtr1,qq,mtr2,qq,mtr3,qq,mtr4); title('Marginal Tax Rate'); grid on