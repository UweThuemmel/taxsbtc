function likelihood=likefun_flo(param)
%param is a five-vector with parameters mu1, mu2, sigma1, sigma2, rho12
%not sure if I need to allow these parameters to be vector valued or not

global DATA
% data should be an n x 1 vector of log wages

mu1=param(:,1);
mu2=param(:,2);
sigma1=param(:,3);
sigma2=param(:,4);
rho12 =param(:,5);

alpha1=1-rho12.*sigma1./sigma2;  % Basu-Ghosh 2.4
alpha2=1-rho12.*sigma2./sigma1;  % Basu-Ghosh 2.4

if alpha1==0
    mu1p=mu1-mu2; % Basu-Ghosh 2.5
    sigma1p=sigma1.*(1-rho12.^2).^.5; % Basu-Ghosh 2.5
else
    mu1p=(mu1-rho12.*sigma1.*mu2./sigma2)/alpha1; % Basu-Ghosh 2.5
    sigma1p=(sigma1.*(1-rho12.^2).^.5)./abs(alpha1); % Basu-Ghosh 2.5
end

if alpha2==0
    mu2p=mu2-mu1; % Basu-Ghosh 2.5
    sigma2p=sigma2.*(1-rho12.^2).^.5;% Basu-Ghosh 2.5
else
    mu2p=(mu2-rho12.*sigma2.*mu1./sigma1)/alpha2; % Basu-Ghosh 2.5
    sigma2p=(sigma2.*(1-rho12.^2).^.5)./abs(alpha2); % Basu-Ghosh 2.5
end

phi11 = pdf('norm',DATA,mu1,sigma1);
phi22 = pdf('norm',DATA,mu2,sigma2);
Phi11p= cdf('norm',DATA,mu1p,sigma1p);
Phi22p= cdf('norm',DATA,mu2p,sigma2p);

%compute the (log) likelihood given these parameters
likevec = phi11.*(1-Phi22p)+phi22.*(1-Phi11p); % Basu-Ghosh 6.2
likelihood=-sum(log(likevec)); % take negative so I can minimize




 

