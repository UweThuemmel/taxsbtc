* create wage sample from march 2011 cps;

#delimit ;
clear all;
set mem 500m;
set more off;

* load downloaded MORG data;
*use "/Users/scheuer/Downloads/morg07.dta";
use "/Users/scheuer/Downloads/morg11.dta";

* generate wages from weekly earnings and weekly hours;
gen wage=earnwke/uhourse;

* drop other variables and observations;
keep wage;
drop if wage==.;
drop if wage==0;
drop if wage>200;

* save wage sample;
save "/Users/scheuer/Downloads/wage.dta", replace;

* compute percentiles of wage sample;
pctile pct = wage, nq(20);
list pct in 1/20;
drop pct;

* compute kernel density of wage sample;
kdensity wage, w(1) n(500);

* compute lognormal approximation;
lognfit wage;
