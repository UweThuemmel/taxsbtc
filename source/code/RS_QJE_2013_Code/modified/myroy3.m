%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Roy Model Optimal Tax Simulation
%
% EXECUTION FILE
% roy3.m
%
% Casey Rothschild & Florian Scheuer, June 2012
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clear all
close all
clc

delete roy3;
diary roy3;

global x num_w ggamma yy_theta yy_phi gam w dw ff gg f f_phi f_theta

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SETUP

% SET FORMATS
format long;
format compact;
warning off;

% HOW TO COMPUTE WAGE DISTRIBUTION
wd_comp=1; % 1=completely numerical approach, 2=half-analytical approach using independence of theta and phi

% LOAD DATA AND UNDERLYING PARAMETERS
load('logndist.mat'); % load skill distribution and other parameters
options=optimset('TolCon',1e-13,'TolFun',1e-13,'TolX',1e-13,'Display','off','MaxFunEvals',10000000,'MaxIter',200);

theta = grid_t';
phi = grid_p';

pmf_theta=max(0,pdf_t'-pdf_t(end)); %so that there is no jump at upper
%bound (ONLY DO ONE)
%pmf_theta=pdf_p';
cdf_theta=cumsum(pmf_theta);

aux=find(pmf_theta==0);
theta(aux)=[];
pmf_theta(aux)=[];
cdf_theta(aux)=[];
num_theta=length(theta);

%pmf_phi=max(0,pdf_p'-pdf_p(end)); %so that there is no jump at upper bound (ONLY DO ONE)
pmf_phi=pdf_p';
cdf_phi=cumsum(pmf_phi);

aux=find(pmf_phi==0);
phi(aux)=[];
pmf_phi(aux)=[];
cdf_phi(aux)=[];
num_phi=length(phi);

bin_width_theta = theta(2) - theta(1);
bin_width_phi = phi(2) - phi(1);
theta_min = min(theta);
theta_max = max(theta);
phi_min = min(phi);
phi_max = max(phi);

% using independence here
pmf=pmf_theta*pmf_phi';

%% FIX x
%xgrid=(.9:.01:1.1);
%xgrid=1; %W<7.25 truncation case, c-d, bottom at 1

%xgrid=.875; %W<1 truncation case, cobb-douglass, bottom truncated at 1

r=1.3;
xgrid = (.7:.001:.88);
% xgrid = .875; %W<1 truncation case, cobb-douglass, bottom
% truncated at 1, 1.3 in swf 
%xgrid=.870; %W<1 truncation case, cobb-douglass, bottom truncated at 1, 1.5 in swf, top rate about -1.2%
%xgrid = .863; %W<1 truncation case, cobb-douglass, bottom truncated at 1, 1.8 in swf  -- top rate: about -1%
%xgrid = .858; %W<1 truncation case, cobb-douglass, bottom truncated at 1, 2 in swf  -- top rate: about -1%
%xgrid = .837; %W<1 truncation case, cobb-douglass, bottom truncated at 1, 3 in swf
%xgrid = .826; %W<1 truncation case, cobb-douglass, bottom truncated at 1, 4 in swf
%xgrid = .818; %W<1 truncation case, cobb-douglass, bottom truncated at 1, 5 in swf
%xgrid = .806; %W<1 truncation case, cobb-douglass, bottom truncated at 1, 10 in swf
%xgrid = .803; %W<1 truncation case, cobb-douglass, bottom truncated at 1, 15 in swf
%xgrid = .799; %W<1 truncation case, cobb-douglass, bottom truncated at 1, 1000 in swf (near Rawlsian) -- get bunching at bottom
%xgrid=(.54:.001:.58); 
%xgrid=.556;
%xgrid=1.034; %W<0.5 truncation, bottom at .1, cobb-douglass



%xgrid=(94.6:.1:94.8); %W<5 truncation case, rho = -0.5, alpha =.2
%xgrid=94.7; %W<5 truncation case, with bottom truncated at .1


% COMPUTE WELFARE FOR EACH x
welfaregrid=zeros(size(xgrid));
for j=1:size(xgrid,2)
x=xgrid(j);

welfaregrid(j)=results(x,pmf_theta,pmf_phi,rho,elast,alpha,theta_min,theta_max,phi_min,phi_max,num_theta,phi,theta,r,bin_width_theta,bin_width_phi);

end

%% PLOT THINGS
plot(xgrid,welfaregrid)
[a b]=max(welfaregrid);
x_max=xgrid(b);
