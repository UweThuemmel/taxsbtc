% This file experiments with the skill distribution to figure out
% why the authors use certain normalizations

% Load file with grid and pdf
load('logndist.mat'); % load skill distribution and other parameters
                      % Note that pdf_t is already normalized

% print parameters
mu_t

% calculate mean based on grid and pdf_t
dot(grid_t,pdf_t)

% now, normalize by grid step size
bin_width_theta = grid_t(2)-grid_t(1);
1/bin_width_theta*dot(grid_t,pdf_t)

% division by bin_width_theta improves result for mean, but still
% quite off

% Try to figure out how the mean of a lognormal distribution is
% defined in MATLAB
mygrid = linspace(0,1000,100000);
mypdf = lognpdf(mygrid,mu_t,sigma_t);
mypdfnorm = mypdf/sum(mypdf);
dot(mygrid,mypdfnorm)

% Expectation
exp(mu_t+sigma_t^2/2)