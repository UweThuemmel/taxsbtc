function welfare = results(x,pmf_theta,pmf_phi,rho,elast,alpha,theta_min,theta_max,phi_min,phi_max,num_theta,phi,theta,r,bin_width_theta,bin_width_phi)
% RESULTS Computes welfare for a given value of x
% Based on roy3.m by Rothschild and Scheuer
% Modified by Uwe Thuemmel

pmf=pmf_theta*pmf_phi';

% TECHNOLOGY & PREFERENCES
%alpha=.2;
%alpha=0.026593390690445; % now contained in the lognormal.mat loaded at
%the top of the file
%rho=0; % now contained in the lognormal.mat loaded at the top
if rho==1;
    yy_phi=1-alpha;
    yy_theta=alpha;
elseif rho==0;
    yy_phi=(1-alpha)*x.^alpha;
    yy_theta=alpha*x.^(alpha-1);
else
    yy_phi=(1-alpha)*(alpha*x^rho+1-alpha)^((1-rho)/rho);
    yy_theta=alpha/(1-alpha)*x^(rho-1)*yy_phi;
end

ggamma=yy_phi/(x*yy_theta);
%elast=.5; %now constained in the lognormal.mat loaded at the top
gam=1+1./elast;

%% COMPUTE WAGE DISTRIBUTION
w_min=max(yy_theta*theta_min,yy_phi*phi_min);
w_max=max(yy_theta*theta_max,yy_phi*phi_max);
num_w=num_theta;
dw=(w_max-w_min)/(num_w-1);
w=(w_min:dw:w_max)';


kcdf_aux_theta=cumsum(pmf,2);
[PHI,THETA]=meshgrid(phi,theta);
%[X,Y]=meshgrid(min(w./yy_phi,phi_max),w./yy_theta);
f_theta=max(0,1/yy_theta*interp2(PHI,THETA,cdf_aux_theta/bin_width_theta,min(w./yy_phi,phi_max),w./yy_theta))*dw;

cdf_aux_phi=cumsum(pmf,1);
%[X,Y]=meshgrid(w./yy_phi,min(w./yy_theta,theta_max));
f_phi=max(0,1/yy_phi*interp2(PHI,THETA,cdf_aux_phi/bin_width_phi,w./yy_phi,min(w./yy_theta,theta_max)))*dw;


f=f_theta+f_phi;
f_theta=f_theta/sum(f);
f_phi=f_phi/sum(f);
f=f/sum(f);
ff=min(1,cumsum(f));
ff(end)=1;

% PARETO WEIGHTS
%r=1.3; See near xgrid -- set it there for easy testing
gg=1-(1-ff).^r;
g=gg-[0;gg(1:num_w-1)];

%% FIND xi
xi=fzero(findxi,0);

% xi=(-10:.1:10);
% for i=1:length(xi)
%    check(i)=findxi(xi(i)); 
% end
% plot(xi,check,xi,zeros(size(xi)));

% xi=0;
% aux=zeros(num_w,1);
% for i=1:num_w
%        aux(i)=w(i).^(gam./(gam-1)).*(max(0,(1+xi.*(1+ggamma)*(yy_phi/(yy_phi+x*yy_theta)-f_phi(i)./f(i)))./(1+gam*(gg(i)-ff(i))./(w(i)*f(i)/dw)))).^(1/(gam-1)).*(ggamma*f_theta(i)-f_phi(i));
% end
% s = sum(aux);

% COMPUTE EFFORT AND MTR's
effort=w.^(1/(gam-1)).*(max(0,(1+xi*(1+ggamma)*(yy_phi/(yy_phi+x*yy_theta)-f_phi./f))./(1+gam*(gg-ff)./(w.*f/dw)))).^(1/(gam-1));
income =w.*effort;
mks=effort.^(gam-1)./w;
mtr=1-mks;

%% compute tax schedule
% Basic idea here: sort by income. Then compute tax schedule as a function of
% income. Then unsort to have taxes as a function of w. Then sum.

[ysort,ysortind]=sort(income);
[temp,unsortind]=sort(ysortind,'ascend');
mtrsort=mtr(ysortind);

ftemp = (f(1:num_w-1)+f(2:num_w))/2;
ftemp = [0;ftemp];

%display(sum(fftemp)*dw);

tax=zeros(num_w,1);
for i=2:num_w
   tax(i,1)=tax(i-1)+mtrsort(i-1).*(ysort(i)-ysort(i-1));
end
tax=tax(unsortind); %idea here is to re-attach taxes to wages

tax_sum=sum(tax.*ftemp); % since dw is constant, this sums tax(i)*((ff(i)+ff(i-1))/2)*dw from i=2 to gridsize
% sum(fftemp)dw-1 is very very close to zero
tax=tax-tax_sum;


%% compute consumption and welfare
cons=income-tax;
welfare_vec=cons-effort.^gam./gam;
welfare=sum(welfare_vec.*g);
cons_sum=sum(cons.*f);
inc_sum=sum(income.*f);

cons_m_inc=cons_sum-inc_sum;