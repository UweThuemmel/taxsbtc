%function mlebiv_cas 
%Casey Rothschild and Florian Scheuer 6-12-12
%Use maximium likelihood to estimate underlying bivariate normal from a
%given wage distribution and simple parameterization of taxes/technology
%Input files: wage.mat
%Required non-native functions: likefun_flo.m 

close all;
clear all;
clc

global DATA

%% production/preferences parameters
elast=.5;
tau=.25;
alpha=.2; % will be updated in case of rho==0
rho=0; %rho =0 -> Cobb-Douglass

%% load cps wage data
load wage.mat;
%wage(wage<1)=[];
minw = 1; % truncate bottom wages. 
wage(wage<minw)=[]; 
%wage(wage>150)=[];
DATA=-log(wage);
numpoints=length(DATA);

% index=randsample(numpoints,20000);
% DATA=DATA(index);
% numpoints=length(DATA);
 
% save data.mat DATA numpoints;

% load data.mat;

% mu = [2 3];
% sigma1 = 1;
% sigma2 = 1;
% rho12 = 0;
% param=[mu sigma1 sigma2 rho12];
% 
% numpoints=10000;
% 
% %generate numpoints observations from the bivariate normal with this set of
% %parameters
% sigma12=rho12*sigma1*sigma2;
% Sigma = [sigma1^2 sigma12; sigma12 sigma2^2]; R = chol(Sigma);
% DATA = repmat(mu,numpoints,1) + randn(numpoints,2)*R;
% %convert into a single observation
% DATA = min(DATA,[],2);


%% mle estimation
FUN=@likefun_flo;

% Initial parameter guess

altparam=[-.5 -2 .5 .1 0]; %W<1 truncation
%altparam=[-1 -1 1 1 0];
%altparam=[-1 -2 .5 .1 0];
%altparam=[-2 -.5 .1 .5 0]; %W<5 truncation
% altparam=[-3 -2 .5 .1 .1];
%feval(FUN,altparam)
%feval(FUN,param)
[xx,fval,exitflag,output]=fminsearch(FUN,altparam)

mu1 = -xx(1);
mu2 = -xx(2);
sigma1 = xx(3);
sigma2 = xx(4);
rho12 = xx(5); % correlation coefficient
param=[mu1 mu2 sigma1 sigma2 rho12];

%% draw sample and check shape of resulting wage distribution
mu=[mu1 mu2];
sigma12=rho12*sigma1*sigma2; % convert to a covariance
Sigma = [sigma1^2 sigma12; sigma12 sigma2^2]; R = chol(Sigma);
DATA2 = repmat([mu1 mu2],numpoints,1) + randn(numpoints,2)*R; % 2-dimensional wage distribution

DATA2 = max(DATA2,[],2); %convert into a single observation by selecting maximum of the two wages

[f1,x1]=ksdensity(-DATA);
[f2,x2]=ksdensity(DATA2);

%[f1,x1]=ksdensity(exp(-DATA));
%[f2,x2]=ksdensity(exp(DATA2));


%figure(1);
figure1 = figure(1);
% Create axes
axes1 = axes('Parent',figure1);
box(axes1,'on');
hold(axes1,'all');
% Create plot
plot(x1,f1,'Parent',axes1,'DisplayName','Empirical Distribution');
% Create plot
plot(x2,f2,'Parent',axes1,'DisplayName','Fitted Distribution');
% Create xlabel
xlabel('$\ln$(wage)','Interpreter','latex','FontSize',14);
% Create ylabel
ylabel('Density','Interpreter','latex','FontSize',14);
% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.155967570441255 0.774712643678161 0.288676236044657 0.0931034482758621]);
%plot(x1,f1,x2,f2);

% plot resulting 'skill' distribution
sigma12=rho12*sigma1*sigma2;
[X1,X2]=meshgrid(linspace(0,4,100)',linspace(0,4,100)');
X=[X1(:) X2(:)];
y=mvnpdf(X, [mu1 mu2], [sigma1 sigma12; sigma12 sigma2]);

figure(2);
surf(X1',X2',reshape(y,100,100));

%% draw shorter sample 
numpoints=10000;
R = chol(Sigma);
MC = repmat(mu,numpoints,1) + randn(numpoints,2)*R;
MC=exp(MC);
[W,S]=max(MC,[],2);

% back out efforts for a fixed tax rate from individual optimization condition.  
e=(W.*(1-tau)).^elast;

% use to compute sectoral incomes and back out x
Y1=sum(W.*e.*(S==1));
Y2=sum(W.*e.*(S==2));
Y=Y1+Y2;
incomefrac = Y1/Y
typefrac=sum(S==1)/numpoints

if rho==0 % Cobb-Douglass Case
    x = 1; % can normalize to x=1 WLOG in Cobb-Douglass case
    alpha = Y1/Y;
else
    x=((1-alpha)/alpha*Y1/Y2)^(1/rho);
end

% compute marginal products
if rho==1;
    yy_phi=1-alpha;
    yy_theta=alpha;
elseif rho==0;
    yy_phi=(1-alpha)*x.^alpha;
    yy_theta=alpha*x.^(alpha-1);
else
    yy_phi=(1-alpha)*(alpha*x^rho+1-alpha)^((1-rho)/rho);
    yy_theta=alpha/(1-alpha)*x^(rho-1)*yy_phi;
end

% back out skill
skill_theta=MC(:,1)/yy_theta;
skill_phi=MC(:,2)/yy_phi;
skill=[skill_theta skill_phi];
lskill=log(skill);

% the skill distribution will be lognormal with parameters:
mu_t=mu1-log(yy_theta)
mu_p=mu2-log(yy_phi)
sigma_t=sigma1
sigma_p=sigma2
rho_tp=rho12

% plot (log) skill distribution
[X1s,X2s]=meshgrid(linspace(0-log(yy_theta),4-log(yy_theta),100)',linspace(0-log(yy_phi),4-log(yy_phi),100)');
Xs=[X1s(:) X2s(:)];
ys=mvnpdf(Xs, [mu1-log(yy_theta) mu2-log(yy_phi)], [sigma1 sigma12; sigma12 sigma2]);

figure(3);
surf(X1s',X2s',reshape(ys,100,100));

% sectoral shares
sec=S-1;
%figure(5);
%ksr(W,sec,5);

% determine where to crop the top of the distribution
cutpercent = .00001; % where to crop the top of the distribution
wmaxer_t = icdf('logn',1-cutpercent,-xx(1),xx(3));
wmaxer_p = icdf('logn',1-cutpercent,-xx(2),xx(4));
%wmaxer = max([wmaxer_t wmaxer_p]);
maxp = wmaxer_p./yy_phi;
maxt = wmaxer_t./yy_theta;

% plot skill density using rho_tp==0
grid_t=linspace(1,maxt,500); %alternate W<1 truncation
grid_p=linspace(1,maxp,500); %alternate W<1 truncation
%grid_t=linspace(1,600,500); %for W<1 truncation
%grid_p=linspace(1,120,500); %for W<1 truncation
%grid_p=linspace(1,100,500); %for W<5 truncation
%grid_t=linspace(1,500,500); %for W<5 truncation
%grid_t=linspace(1,80,500); %for W<3 truncation
%grid_p=linspace(1,600,500); %for W<3 truncation
%grid_p=linspace(1,400,500); %for W<7.25 truncation
%grid_t=linspace(1,800,500); %for W<7.25 truncation
%grid_p=linspace(1,200,500); %for W<2 truncation
%grid_t=linspace(1,500,500); %for W<2 truncation


pdf_t=lognpdf(grid_t,mu_t,sigma_t);
pdf_t=max(0,pdf_t/sum(pdf_t));
pdf_p=lognpdf(grid_p,mu_p,sigma_p);
pdf_p=max(0,pdf_p/sum(pdf_p));
pdf=pdf_t'*pdf_p;

[X1sL,X2sL]=meshgrid(grid_t',grid_p');
%XsL=[X1sL(:),X2sL(:)];
figure(4);
%surf(grid_t,grid_p,pdf); %older version
surf(X1sL',X2sL',pdf);

% % plot skill density
% stt=max(skill_theta);
% stt=2500;
% stb=min(skill_theta);
% spt=max(skill_phi);
% spt=5;
% spb=min(skill_phi);
% 
% nn=100;
% grid_t=(stb:(stt-stb)/(nn-1):stt);
% grid_p=(spb:(spt-spb)/(nn-1):spt);
% bw=[50 .1];
% f=ksdensity2d(skill,grid_t,grid_p,bw);
% 
% figure(5);
% surf(grid_t,grid_p,f);

save logndist.mat grid_t grid_p pdf_t pdf_p pdf mu_t mu_p sigma_t sigma_p alpha rho elast x1 x2 f1 f2;

