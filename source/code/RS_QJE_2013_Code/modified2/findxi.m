function s=findxi(xi,x,w,num_w,dw,gam,ggamma,yy_theta,yy_phi,gg,ff,f,f_phi,f_theta,version)
% Evaluates the consistency condition at xi

aux=zeros(num_w,1);
for i=1:num_w
    switch version
        case 1
            aux(i)=w(i).^(gam./(gam-1)).*(max(0,(1+xi.*(1+ggamma)*(yy_phi/(yy_phi+x*yy_theta)-f_phi(i)./f(i)))./(1+gam*(gg(i)-ff(i))./(w(i)*f(i)/dw)))).^(1/(gam-1)).*(ggamma*f_theta(i)-f_phi(i));
        case 2
            aux(i)=w(i).^(gam./(gam-1)).*(max(0,(1+xi.*(yy_phi/(yy_phi+x*yy_theta)-f_phi(i)./f(i)))./(1+gam*(gg(i)-ff(i))./(w(i)*f(i))))).^(1/(gam-1)).*(ggamma*f_theta(i)-f_phi(i)); 

case 3
  % I think this is the correct way of numerically integrating this
  % expression - also got rid of multiplication by (1+ggamma)

aux(i)=w(i).^(gam./(gam-1)).*(max(0,(1+xi.*(yy_phi/(yy_phi+x*yy_theta)-f_phi(i)./f(i)))./(1+gam*(gg(i)-ff(i))./(w(i)*f(i))))).^(1/(gam-1)).*(ggamma*f_theta(i)-f_phi(i))*dw;


    end
end

s = sum(aux);
