function outstruct = dbread(db, table, rowid)
%Read into struct from rowid in table in database
%
%% Syntax:  function outstruct = dbread(db, table, rowid)
%
%% Description
%    db - Database identifier
%    table - Name of table to read from (string)
%    rowid - id of row from which to read
%
%% Outputs:
%    outstruct - struct with names corresponding to the columns in table
%

% Author: Uwe Thuemmel
% PhD candidate Erasmus University Rotterdam, VU University
% Amsterdam, Tinbergen Institute
% email: u.thuemmel@tinbergen.nl
% January 2014

%------------- BEGIN CODE --------------

% Read from database
command = strcat({'SELECT * FROM '},table,{' WHERE rowid = (?)'});
out = sqlite3.execute(db,command{1},rowid);

if isempty(out)
    error('No results returned. Check whether rowid exists.');
end

% Find serialized objects
names = fieldnames(out);
serialized = {};
nonserialized = {};
for i=1:length(names)
    name = names{i};
    position = strfind(name,'_data');
    if position >0
        serialized{end+1} = name(1:position-1);
    else
        % detect nonserialized elements
        if isempty(strfind(name,'_class')) & isempty(strfind(name,'_size'))
            nonserialized{end+1} = name;
        end
    end
end

% deserialize
for i=1:length(serialized)
    name = serialized{i};
    
    % Assign field names
    sz_name = strcat(name,'_size');
    cls_name = strcat(name,'_class');
    data_name = strcat(name,'_data');

    % Access fields
    sz = getfield(out,sz_name);
    cls = getfield(out,cls_name);
    data = getfield(out,data_name);

    if isempty(data)
    else
    % De-serialize
    matrix = deserialize(sz,cls,data);
    outstruct.(name) = matrix;
    end
end

% append non-serialized elements to outstruct
for i=1:length(nonserialized)
    name = nonserialized{i};
    outstruct.(name) = out.(name);
end

% include rowid as a field
outstruct.rowid = rowid;
%------------- END OF CODE --------------
