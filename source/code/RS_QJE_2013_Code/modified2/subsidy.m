% Experiment with adjusting the wage distribution in response to an
% increase in tau

%% Parameters
r=1.3;

%% specify version: 1 is original, 2 is modified, 3 uses corrected
%% numerical integration, version 4 uses adjustement of wage
%% distribution if tau !=0
normalize_skill = 0; %correct setting is 0, tried to normalize skill to keep extreme wages unchanged

rs_skilldist = 3;
sbtc=1;
a_phi = 1;
a_theta = 1.5;
rho = 1/3;
% grid_p = grid_t;
% pdf_p = pdf_t;
alpha = 0.5;
elast = 0.5;

tau = 10;

%% Specify skill distribution

mu_t = 4;
sigma_t = 1;

% mu_p = 2.8655;
mu_p=mu_t;
sigma_p = 1;


grid_t = linspace(0.1,1000,1000);
grid_p = linspace(0.1,1000,1000);

pdf_t = lognpdf(grid_t,mu_t,sigma_t);
pdf_p = lognpdf(grid_p,mu_p,sigma_p);


[theta, phi, pmf, pmf_theta, pmf_phi, cdf_theta, cdf_phi, bin_width_theta, ...
 bin_width_phi, theta_min, theta_max, phi_min, phi_max, ...
 num_theta, num_phi] = genskilldist(grid_t, grid_p, pdf_t, pdf_p,rs_skilldist);

x=1.09;


%% Set threshold
version = 3;
threshold_phi =[];
threshold_theta = [];

res = results(x,pmf_theta,pmf_phi,rho,elast,alpha,theta_min,theta_max,phi_min,phi_max,num_theta,phi,theta,r,bin_width_theta,bin_width_phi,pmf,a_phi,a_theta,version,sbtc,normalize_skill,grid_t,grid_p,pdf_t,pdf_p,rs_skilldist,tau,threshold_theta,threshold_phi);

% calculate initial thresholds
threshold_phi = (theta * res.yy_theta)/res.yy_phi;
threshold_theta = (phi * res.yy_phi)/res.yy_theta;


[threshold_theta,threshold_phi,new_threshold_theta,new_threshold_phi,utility_t,utility_p,utility_t_tau,phi_adjusted,theta_adjusted,del_theta,del_phi,utility_diff_theta,utility_diff_phi] = compute_thresholds(res,theta,phi,elast,tau,threshold_theta,threshold_phi);

% delete elements for which NaN was obtained in utility
% interpolations from pmf

pmf(del_theta,:)=[];
pmf(:,del_theta)=[];

pmf(del_phi,:)=[];
pmf(:,del_phi)=[];


figure(1)
plot(theta_adjusted,utility_diff_theta)

figure(2)
plot(phi_adjusted,utility_diff_phi)

delta = 0.2;

threshold_theta = threshold_theta + delta * (new_threshold_theta - threshold_theta);
threshold_phi = threshold_phi + delta * (new_threshold_phi - threshold_phi);

version = 4;
res = results(x,pmf_theta,pmf_phi,rho,elast,alpha,theta_min,theta_max,phi_min,phi_max,num_theta,phi_adjusted,theta_adjusted,r,bin_width_theta,bin_width_phi,pmf,a_phi,a_theta,version,sbtc,normalize_skill,grid_t,grid_p,pdf_t,pdf_p,rs_skilldist,tau,threshold_theta,threshold_phi);

[threshold_theta,threshold_phi,new_threshold_theta,new_threshold_phi,utility_t,utility_p,utility_t_tau,phi_adjusted,theta_adjusted,del_theta,del_phi,utility_diff_theta,utility_diff_phi] = compute_thresholds(res,theta_adjusted,phi_adjusted,elast,tau,threshold_theta,threshold_phi);

figure(3)
plot(theta_adjusted,utility_diff_theta)

figure(4)
plot(phi_adjusted,utility_diff_phi)



% %% Plots

% % % plot utility agains type
% % path = ['/media/data/Dropbox/PhD/projects/taxsbtc/source/lyx/' ...
% %         'figures/'];
% % % Plot of utilities against theta and phi
% % figure(1)
% % subplot(1,2,1);
% % plot(theta,utility_t)
% % title('Utility against theta')
% % xlabel('theta')
% % axis([0,max(theta),0,max(utility_t)]);
% % subplot(1,2,2);
% % plot(phi,utility_p)
% % title('Utility against phi')
% % xlabel('phi')
% % axis([0,max(theta),0,max(utility_t)]);
% % print(figure(1),horzcat(path,'utility_type.eps'),'-depsc')


% % a plot shows that indeed utilities are equal at the threshold
% % plot(theta,utility_t,theta,utility_threshold_phi)


% % Difference between old and new threshold for phi by theta
% figure(2)
% plot(theta_adjusted,new_threshold_phi-threshold_phi)
% title('Difference between new and old threshold');
% xlabel('theta');
% print(figure(2),horzcat(path,'threshold_difference.eps'),'-depsc')

% % Compare utility at new threshold for phi
% figure(3)
% utility_new_threshold_phi = interp1(phi_adjusted,utility_p, ...
%                                     new_threshold_phi);
% plot(theta_adjusted,utility_t_tau-utility_new_threshold_phi)

% % Compare utility at new threshold for theta
% figure(4)
% utility_new_threshold_theta = interp1(theta_adjusted,utility_t_tau, ...
%                                     new_threshold_theta);
% plot(phi_adjusted,utility_p-utility_new_threshold_theta)
