function output = serialize(matrix, name)
%serialize matrix to then be able to save it to a database
%
% Syntax:  output = serialize(matrix, name)
%
% Inputs:
%    matrix - Matrix to be serialized
%    name - Name of matrix, will be used to refer to database
%    columns (string)
%
% Outputs:
%    output - Structure with fields size, class, data, and name
%
%------------- BEGIN CODE --------------

temp = size(matrix);
sz = typecast(temp(:), 'uint8');
cls = class(matrix);
data = typecast(matrix(:), 'uint8');

output = struct('size',sz,'class',cls,'data',data,'name',name);


%------------- END OF CODE --------------
