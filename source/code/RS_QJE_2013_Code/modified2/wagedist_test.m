% function [w_min, w_max, num_w, dw, w, f_theta, f_phi, f, ff]=wagedist_test()
%WAGEDIST_TEST
% Test for function wagedist.m
% Syntax:  [output1,output2] = function_name(input1,input2,input3)
%
% Inputs:
%    input1 - Description
%    input2 - Description
%    input3 - Description
%
% Outputs:
%    output1 - Description
%    output2 - Description
%
% Example: 
%    Line 1 of example
%    Line 2 of example
%    Line 3 of example
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: OTHER_FUNCTION_NAME1,  OTHER_FUNCTION_NAME2

% Author: Uwe Thuemmel
% PhD candidate Erasmus University Rotterdam, VU University
% Amsterdam, Tinbergen Institute
% email: u.thuemmel@tinbergen.nl
% January 2014

%------------- BEGIN CODE --------------

load('logndist.mat'); % load skill distribution and other
                      % parameters





% generate skill distribution
[theta, phi, pmf, pmf_theta, pmf_phi, cdf_theta, cdf_phi, bin_width_theta, ...
         bin_width_phi, theta_min, theta_max, phi_min, phi_max, ...
          num_theta, num_phi] = genskilldist(grid_t, grid_p, pdf_t, pdf_p);

x = 1;

yy_phi=(1-alpha)*x.^alpha;
yy_theta=alpha*x.^(alpha-1);

version = 1;

pmffun = @(theta,phi) lognpdf(theta,mu_t,sigma_t)*lognpdf(phi,mu_p, ...
                                                  sigma_p)

pmf_theta = lognpdf(theta,mu_t,sigma_t);
pmf_phi = lognpdf(phi,mu_p,sigma_p);

% pmf = zeros(length(theta),length(phi));
% for i=1:length(theta)
%     for j=1:length(phi)
%         pmf(i,j)=pmffun(theta(i),phi(j));
    
%     end
% end

pmf=pmf_theta*pmf_phi';

% choose yy_theta so high that no one will want to work in the phi sector
% yy_theta = 100;
% yy_phi = 1;
% version = 4

% display('phi_max*yy_phi')
% phi_max*yy_phi


% display('theta_min*yy_theta')
% theta_min*yy_theta

% % Check whether condition is true that no one will want to work in
% % the phi sector
% phi_max*yy_phi < theta_min*yy_theta

% if version == 4
% pmf = @(theta,phi) lognpdf(theta,mu_t,sigma_t)*lognpdf(phi,mu_t,sigma_t);
% end

% [w_min, w_max, num_w, dw, w, f_theta, f_phi, f, ff] = wagedist(yy_theta, yy_phi,phi, theta,pmf,version);

% plot(log(theta),pmf_theta - f_theta)

% dw
% plot(log(phi),f_phi)

% result = f_theta_cont(3,pmf,yy_theta,yy_phi,theta_min,phi_min)

bin_width_theta = theta(2) - theta(1);
bin_width_phi = phi(2) - phi(1);
theta_min = min(theta);
theta_max = max(theta);
phi_min = min(phi);
phi_max = max(phi);

num_theta=length(theta);

w_min=max(yy_theta*theta_min,yy_phi*phi_min)
w_max=max(yy_theta*theta_max,yy_phi*phi_max)
num_w=num_theta;
dw=(w_max-w_min)/(num_w-1);
% w=(w_min:dw:w_max)';
 
cdf_aux_theta=cumsum(pmf,2);
cdf_aux_phi=cumsum(pmf,1);
[PHI,THETA]=meshgrid(phi,theta);

w = theta(300)*yy_theta

% Testing different ways of 

f_theta=max(0,1/yy_theta*interp2(PHI,THETA,cdf_aux_theta/bin_width_theta,min(w./yy_phi,phi_max),w./yy_theta))*dw

f_theta_alt=max(0,1/yy_theta*interp2(PHI,THETA,cdf_aux_theta, ...
                                     min(w./yy_phi,phi_max),w./yy_theta))

f_theta_alt2=max(0,1/yy_theta*interp2(PHI,THETA,cdf_aux_theta, ...
                                      min(w./yy_phi,phi_max),w./ ...
                                      yy_theta))*bin_width_theta 

f_theta_alt3 = max(0,1/yy_theta*interp2(PHI,THETA,cdf_aux_theta, ...
                                      min(w./yy_phi,phi_max),w./ ...
                                      yy_theta))*bin_width_phi 

% for transparancy get rid of multiplication by 1/yy_theta
integral1 = max(0,interp2(PHI,THETA,cdf_aux_theta/bin_width_theta, ...
                          min(w./yy_phi,phi_max),w./yy_theta))*dw

% now calculate without normalization
integral2 = max(0,interp2(PHI,THETA,cdf_aux_theta, ...
                          min(w./yy_phi,phi_max),w./yy_theta))

% now multiply by bin_width_phi
integral3 = max(0,interp2(PHI,THETA,cdf_aux_theta, ...
                          min(w./yy_phi,phi_max),w./yy_theta))*bin_width_phi


%------------- END OF CODE --------------
