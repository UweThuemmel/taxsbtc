function genplots(arrayofstructs,legendname,legendtitle,path,prefix)
%Generate plots based on structures with results in arrayofstructs
%
% Syntax:  genplots({arrayofstructs})
%
% Inputs:
%    arrayofstructs - array of structs with results
%    legendname - field in results struct to use as legend
%    legendtitle - Title for legends. If left to '' uses legendname.     
%
% Example:
%    genplots({out1,out2},'version')
%    will plot results from structures out1 and out2, using
%    out1.version, and out2.version as labels for the legend
% 
% Author: Uwe Thuemmel
% PhD candidate Erasmus University Rotterdam, VU University
% Amsterdam, Tinbergen Institute
% email: u.thuemmel@tinbergen.nl
% January 2014

%------------- BEGIN CODE --------------

%% Define functions for plotting
clf;

function v=mylegend(legendnames,legendtitle)
   h = legend(legendnames);
   v = get(h,'title');
   set(v,'string',legendtitle);
end

function fig1(out,colors,index)
figure(1);
% line specifications
mylinespec = struct('LineStyle',linest{index},'Color',colors{index},'LineWidth',linewdth);

% plots
plot(out.xgrid,out.welfaregrid,mylinespec);title('Social Welfare'); ...
       xlabel('X'); grid on; hold all;
end

function fig2(out,colors,index)
figure(2);
% line specifications
% mylinespec = struct('Color',colors{index});
mylinespec = struct('LineStyle',linest{index},'Color',colors{index},'LineWidth',linewdth);
% plots
   subplot(2,2,1); plot(out.w,out.mtr,mylinespec); title('Marginal Tax Rate'); ...
       xlabel('Wage rate');grid on;    hold all;
   subplot(2,2,2); plot(out.w,out.tax,mylinespec); title('Tax Schedule'); ...
       xlabel('Wage rate'); grid on;    hold all;
   subplot(2,2,3); plot(out.w(cutoffavg:end),out.tax(cutoffavg:end)./out.income(cutoffavg:end),mylinespec); title(['Average ' ...
                       'Tax Rate']); xlabel('Wage rate');  grid on;    hold all;
   subplot(2,2,4); plot(out.w,out.f_theta./out.f,mylinespec); title('Theta-share'); ...
       xlabel('Wage rate'); grid on;  hold all;
end

function fig3(out,colors,index)
figure(3);
% line specifications
mylinespec = struct('LineStyle',linest{index},'Color',colors{index},'LineWidth',linewdth);

% plots
   subplot(2,1,1); plot(out.w,out.effort,mylinespec); title('Effort'); ...
       xlabel('Wage rate'); grid on; hold all;
   subplot(2,1,2); plot(log(out.w),out.f,mylinespec); title(['Log-wage ' ...
                       'distribution']);  xlabel('Log-wage rate'); ...
       grid on; hold all;
end


function fig4(out,colors,index)
figure(4);
% line specifications
mylinespec = struct('LineStyle',linest{index},'Color',colors{index},'LineWidth',linewdth);

% plots
   subplot(2,2,1); plot(out.income,out.mtr,mylinespec); title(['Marginal Tax ' ...
                       'Rate']);  xlabel('Income'); grid on; hold all;
   subplot(2,2,2); plot(out.income,out.tax,mylinespec); title('Tax Schedule'); ...
       xlabel('Income'); grid on; hold all;
   subplot(2,2,3); plot(out.income(cutoffavg:end),out.tax(cutoffavg:end)./out.income(cutoffavg:end),mylinespec); ...
       title('Average Tax Rate'); xlabel('Income'); grid on; hold all;
   subplot(2,2,4); plot(out.income,out.f_theta./out.f,mylinespec); ...
       title('Theta-share'); xlabel('Income'); grid on; hold all;

end

%% Define colors and legendnames

colors = {'blue','red','black','green','cyan','magenta','blue','blue','blue'};
if length(colors) < length(arrayofstructs)
    error('Less colors specified than plots! Adjust function!')
end

% Define plot linestyle
linest = {'-','--','-.',':'};
linewdth = 2.5;
cutoffavg = 50; %lower cutoff for avg tax rate to avoid plotting
                %extreme values


legendnames = {};
% generate array of legendnames
for i=1:length(arrayofstructs)
    result = arrayofstructs{i};
    if strcmp(class(result.(legendname)),'char')
    legendnames{i}=result.(legendname);
    else
    legendnames{i}=num2str(result.(legendname));
    end
end

% Define legendtitle in case not specified
if strcmp(legendtitle,'')
   legendtitle = legendname
end
  

%% Generate plots

% figure 1
for i=1:length(arrayofstructs)
    result = arrayofstructs{i};
    fig1(result,colors,i);
end

mylegend(legendnames,legendtitle);
hold off;

% figure 2
for i=1:length(arrayofstructs)
    result = arrayofstructs{i};
    fig2(result,colors,i);
end

mylegend(legendnames,legendtitle);
hold off;

% figure 3
for i=1:length(arrayofstructs)
    result = arrayofstructs{i};
    fig3(result,colors,i);
end

mylegend(legendnames,legendtitle);
hold off;

% figure 4
for i=1:length(arrayofstructs)
    result = arrayofstructs{i};
    fig4(result,colors,i);
end

mylegend(legendnames,legendtitle);
hold off;

%% Saving plots
if strcmp('',path)
else
    for i=1:4         
        print(figure(i),horzcat(path,prefix,'_','fig',int2str(i)),'-depsc')
    end
end

end
%------------- END OF CODE --------------
