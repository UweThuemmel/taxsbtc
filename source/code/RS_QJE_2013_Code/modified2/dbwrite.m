function dbwrite(db,table,parameters,matrices,comment)
%write serialized data to database db
%
% Syntax:
% dbwrite(db,table,parameters,matrices,comment,commentfield='comment',gitfield='git')
% 
% Description:
%     database needs to be opened first, using
%     sqlite3.open('dbname.sqlite')
%     for more info concerning the package used, see 
%     https://github.com/kyamagu/matlab-sqlite3-driver 
%
% Inputs:
%    db - database identifier
%    table - database table to write to (string)
%    parameters - struct of parameters (where each entry needs to
%    be a single number)
%    matrices - struct of matrices, where the name of the field
%    needs to correspond to columns name_size, name_class,
%    name_data in the database
%    comment - comment (string)
%    commentfield - field name for saving comments
%    gitfield - field in which to save name of git commit
%
% Explanation:
%    Each element in 'serialized' contains the fields 'size',
%    'class', 'data', and 'name'. These fields will be written to
%    the corresponding database columns 'name_size', 'name_class',
%    'name_data'.
%    Please make sure that these columns exist.
%    For each element in structure parameters the values are
%    inserted into the column with the same name as the structure
%    field. Also here, please make sure that these columns exist.
% 
%    Note: Inserting into empty table may cause error. Just try
%    again, and it should work.
%
% Author: Uwe Thuemmel
% PhD candidate Erasmus University Rotterdam, VU University
% Amsterdam, Tinbergen Institute
% email: u.thuemmel@tinbergen.nl
% January 2014

%------------- BEGIN CODE --------------
% Default fields for comment and git commit
commentfield='comment';
gitfield='git';
timestampfield='timestamp';

% serialize matrices
matrixnames = fieldnames(matrices);
serialized = {};
for i=1:length(matrixnames)
    serialized = [serialized serialize(getfield(matrices,matrixnames{i}),matrixnames{i})];
end


% get current git commit
[status commit] =  system('git rev-parse HEAD');

% insert git commit into table
command = strcat({'INSERT INTO '},table,{' (git) VALUES(?)'});
sqlite3.execute(db,command{1},commit);

% get rowid
command = strcat({'SELECT max(rowid) FROM '},table);
temp = sqlite3.execute(db,command{1});
rowid = temp.max_rowid;

% insert time stamp
command = strcat({'UPDATE '},table,{' SET '},timestampfield,' = (CURRENT_TIMESTAMP)',{' WHERE rowid ='},int2str(rowid));
sqlite3.execute(db,command{1});

% insert comment  into database
command = strcat({'UPDATE '},table,{' SET '},commentfield,' = (?)',{' WHERE rowid ='},int2str(rowid));    
sqlite3.execute(db,command{1},comment);


% throw error if there are uncommited changes - changed to warning
[status cmdout] =  system('git diff-files');
if length(cmdout) ~= 0
    % error('Uncommited changes. First commit changes, then run
    % again.')
    display('Warning: There are uncommited changes.');
end

% update cells in row corresponding to rowid - with parameters
parameternames = fieldnames(parameters);
for i=1:length(parameternames)
    command = strcat({'UPDATE '},table,{' SET '},parameternames{i},' = (?)',{' WHERE rowid ='},int2str(rowid));    
    sqlite3.execute(db,command{1},getfield(parameters,parameternames{i}));
end

% with serialized data
for i=1:length(serialized)
    name = serialized{i}.name;
    % size
    command = strcat({'UPDATE '},table,{' SET '},name,['_size = ' ...
                        '(?)'],{' WHERE rowid ='},int2str(rowid));
    sqlite3.execute(db,command{1},serialized{i}.size);
    % class
    command = strcat({'UPDATE '},table,{' SET '},name,['_class = ' ...
                        '(?)'],{' WHERE rowid ='},int2str(rowid));
    sqlite3.execute(db,command{1},serialized{i}.class);
    % data
    command = strcat({'UPDATE '},table,{' SET '},name,['_data = ' ...
                        '(?)'],{' WHERE rowid ='},int2str(rowid));
    sqlite3.execute(db,command{1},serialized{i}.data);
end


%------------- END OF CODE --------------
