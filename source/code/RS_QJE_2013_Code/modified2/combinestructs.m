function combined = combinestructs(arrayofstructs)
%Combines several structs into one (note, this is not about merging
%corresponding fields)
% Syntax: combined = combinestructs(arrayofstructs)
%
% Inputs:
%    arrayofstructs - array of structs to be combined
%
% Outputs:
%    combined - struct of combined structs

% Author: Uwe Thuemmel
% PhD candidate Erasmus University Rotterdam, VU University
% Amsterdam, Tinbergen Institute
% email: u.thuemmel@tinbergen.nl
% January 2014

%------------- BEGIN CODE --------------

% get field names
names = [];
for i=1:length(arrayofstructs)
    names = [names; fieldnames(arrayofstructs{i})];
end

% combine structs in cell
structsincell = [];
for i=1:length(arrayofstructs)
    structsincell = [structsincell; struct2cell(arrayofstructs{i})];
end

% combine back into struct
combined = cell2struct(structsincell, names, 1);

%------------- END OF CODE --------------
