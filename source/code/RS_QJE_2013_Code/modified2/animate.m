function animate(selection)
% Produces the animation corresponding to selection

% animation of wage distribution plots for different values of x
% load data to plot

%% Animation settings
numtimes=1;
fps=10;
 

%% Connect to database
db = sqlite3.open('test.sqlite');
out = dbread(db,'results',63);


figure(1)
plot(out.xgrid,out.welfaregrid)

switch selection


case 2

figure(2)
for i=1:length(out.xgrid)
plot(log(out.w),out.f_theta_all(:,i),log(out.w),out.f_phi_all(:,i),log(out.w),out.f_all(:,i));
% axis equal;
% daspect([10 1 1])
axis([0,8,0,max(out.f_all(:))]);
legend('f_theta','f_phi','f');
xlabel('log wage');
str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
text(6,0.03,str1);
title('Wage distributions');
M(i)=getframe;
end

case 3

% animation for welfare_vec
figure(3)
f = getframe;
[im,map] = rgb2ind(f.cdata,256,'nodither');
im(1,1,1,length(out.xgrid)) = 0;
for i=1:length(out.xgrid)
plot(log(out.w),out.welfare_vec(:,i));
% axis equal;
% daspect([10 1 1])
axis([0,8,min(out.welfare_vec(:)),mean(out.welfare_vec(:))]);
xlabel('log wage');
str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
text(6,0,str1);
title('Welfare vector');
f=getframe;
im(:,:,1,i) = rgb2ind(f.cdata,map,'nodither');
M2(i)=getframe;
end

case 4

% animation for effort_all
figure(4)
f = getframe;
[im,map] = rgb2ind(f.cdata,256,'nodither');
im(1,1,1,length(out.xgrid)) = 0;
for i=1:length(out.xgrid)
plot(log(out.w),out.effort_all(:,i));
% axis equal;
% daspect([10 1 1])
axis([0,8,min(out.effort_all(:))-10,max(out.effort_all(:))]);
xlabel('log wage');
str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
text(6,45,str1);
title('Effort schedule');
f=getframe;
im(:,:,1,i) = rgb2ind(f.cdata,map,'nodither');
M3(i)=getframe;
end

case 5

% animation for mtr_all
figure(5)
f = getframe;
[im,map] = rgb2ind(f.cdata,256,'nodither');
im(1,1,1,length(out.xgrid)) = 0;
for i=1:length(out.xgrid)
plot(out.w,out.mymtr_all(:,i),out.w,ones(1,length(out.w)),out.w,zeros(1,length(out.w)));
% axis equal;
% daspect([10 1 1])
axis([0,max(out.w),-2,2]);
xlabel('wage');
str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
text(1000,-1,str1);
title('Untruncated MTR schedule');
f=getframe;
im(:,:,1,i) = rgb2ind(f.cdata,map,'nodither');
M4(i)=getframe;
end

case 6

% animation for fthetax / fx
figure(6)
f = getframe;
[im,map] = rgb2ind(f.cdata,256,'nodither');
im(1,1,1,length(out.xgrid)) = 0;
for i=1:length(out.xgrid)
plot(log(out.w),out.f_theta_all(:,i) ./ out.f_all(:,i));
% axis equal;
% daspect([10 1 1])
axis([0,max(log(out.w)),0,1]);
xlabel('log wage');
str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
text(5,0.5,str1);
title('fthetax / fx');
f=getframe;
im(:,:,1,i) = rgb2ind(f.cdata,map,'nodither');
M6(i)=getframe;
end

case 7
% animation of fthetax/fphix
figure(7)
f = getframe;
[im,map] = rgb2ind(f.cdata,256,'nodither');
im(1,1,1,length(out.xgrid)) = 0;
for i=1:length(out.xgrid)
plot(log(out.w),out.f_theta_all(:,i) ./ out.f_phi_all(:,i));
% axis equal;
% daspect([10 1 1])
axis([0,max(log(out.w)),0,1]);
xlabel('log wage');
str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
text(5,0.5,str1);
title('fthetax / fphix');
f=getframe;
im(:,:,1,i) = rgb2ind(f.cdata,map,'nodither');
M7(i)=getframe;
end
movie2avi(M7, 'fthetax_over_fphix.avi');

case 8

figure(8)
out = dbread(db,'results',69);
% animation for mtr_all - based on row 69
f = getframe;
[im,map] = rgb2ind(f.cdata,256,'nodither');
im(1,1,1,length(out.xgrid)) = 0;
for i=1:length(out.xgrid)
plot(out.w,out.mymtr_all(:,i),out.w,ones(1,length(out.w)),out.w,zeros(1,length(out.w)));
% axis equal;
% daspect([10 1 1])
axis([0,max(out.w),-2,2]);
xlabel('wage');
str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
text(1000,-1,str1);
title('Untruncated MTR schedule');
f=getframe;
im(:,:,1,i) = rgb2ind(f.cdata,map,'nodither');
M(i)=getframe;
end

movie2avi(M, 'mtr69.avi');


end
 

% play as smooth movie 3 times at 10 frames per second

% note that it goes through the frames 2x initially to get ready for

% full speed play.  So it will actually play 2x slower and 3x at full speed.


% movie(M2,numtimes,fps)

% save as avi
% movie2avi(M3, 'effort_all.avi');
% movie2avi(M2, 'welfare_vec.avi');
% imwrite(im,map, 'welfare_vec.gif');
% movie2avi(M4, 'mymtr_all.avi');
% movie2avi(M6, 'fthetax_over_fx.avi');
