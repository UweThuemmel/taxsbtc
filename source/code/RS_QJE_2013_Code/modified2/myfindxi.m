function s=myfindxi(xi)
% modified version, based on findxi, excluding multiplication by
% (1+ggamma), and division by dw

global x num_w gam ggamma yy_theta yy_phi w dw ff gg f f_phi f_theta;


aux=zeros(num_w,1);
for i=1:num_w
   %if 1+xi.*(1+ggamma)*(yy_phi/(yy_phi+x*yy_theta)-f_phi(i)./f(i))<0;
   %    aux(i)=1e10;
   %else
       aux(i)=w(i).^(gam./(gam-1)).*(max(0,(1+xi.*(yy_phi/(yy_phi+x*yy_theta)-f_phi(i)./f(i)))./(1+gam*(gg(i)-ff(i))./(w(i)*f(i))))).^(1/(gam-1)).*(ggamma*f_theta(i)-f_phi(i));
   %end
end

s = sum(aux);
