function [welfare, welfare_vec,effort, income, mtr, mymtr, tax, w_mean_theta, w_mean_phi, ...
         cwp, effort_nonmon, income_nonmon,xi] = compute_welfare(x,gam,ggamma,r,yy_theta,yy_phi,w_min, w_max, num_w, dw, w, f_theta, f_phi, f, ff,theta_min,theta_max,phi_min,phi_max,version,tau)

effort_complex = 0; %indicator for whether effort is
                    %complex-valued. In case it is, set welfare to
                    %zero. It results from a negative expression
                    %inside the parenthesis of the effort formula.


% PARETO WEIGHTS
gg=1-(1-ff).^r;
g=gg-[0;gg(1:num_w-1)];

%% Calculate bounds for xi
delta = f_theta./f-x*yy_theta/(x*yy_theta+yy_phi);
delta_min = min(delta);
delta_max = max(delta);

if (delta_min <= 0) & (delta_max <= 0)
xi_max = -1/delta_min;
xi_min = -Inf;
elseif (delta_min >= 0) & (delta_max >= 0)
xi_min = -1/delta_max;
xi_max = Inf;
else
xi_min = -1/delta_max;
xi_max = -1/delta_min;
end

xi_bounds = [xi_min xi_max];
%% FIND xi
errorfindxi = 0;
fhandle = @(xi) findxi(xi,x,w,num_w,dw,gam,ggamma,yy_theta,yy_phi,gg,ff,f,f_phi,f_theta,version);

try
% [xi fval exitflag fzero_out]=fzero(fhandle,xi_bounds);
[xi fval exitflag fzero_out]=fzero(fhandle,0);
if exitflag ~= 1
xi=0
display('no convergence of findxi')
    % error('no convergence of findxi')
end
catch err
if (strcmp(err.identifier,'MATLAB:fzero:ValuesAtEndPtsSameSign'))
% display('Sign does not differ at bounds');
errorfindxi = 1; % identifier for error - will set effort zero everywhere
else
    rethrow(err);
end
end


%% COMPUTE EFFORT AND MTR's
if errorfindxi == 0


if version == 1
  % original version as by R&S
    effort=w.^(1/(gam-1)).*(max(0,(1+xi*(1+ggamma)*(yy_phi/(yy_phi+x*yy_theta)-f_phi./f))./(1+gam*(gg-ff)./(w.*f/dw)))).^(1/(gam-1));
elseif version == 2
  % modified version
    effort=w.^(1/(gam-1)).*(max(0,(1+xi*(yy_phi/(yy_phi+x*yy_theta)- ...
                                     f_phi./f))./(1+gam*(gg-ff)./ ...
                                                  (w.*f)))).^(1/(gam-1));
else
  % modified version - same as case 2, but version 3 differs at
  % other places, also used for version 4

    effort=w.^(1/(gam-1)).*((1+xi*(yy_phi/(yy_phi+x*yy_theta)- ...
                                     f_phi./f))./(1+gam*(gg-ff)./ ...
                                                  (w.*f))).^(1/(gam-1));

    if min(effort) < 0
        msg = horzcat('Negative effort encountered at x=',num2str(x),' with xi=',num2str(xi));
        error(msg)
    end
    if  isreal(effort)==0
        msg = horzcat('Complex effort encountered at x=',num2str(x),' with xi=',num2str(xi));
        display(msg)
        effort_complex = 1;
    end
end

else
xi = 0;
effort = zeros(length(w),1);
end

% non-truncated mtr
mymtr = 1- (1+xi*(yy_phi/(yy_phi+x*yy_theta)-f_phi./f))./(1+gam*(gg-ff)./(w.*f));

effort_nonmon = 0; %indicator for whether effort schedule is
                   %monotonic
income_nonmon = 0; %indicator for whether income schedule is monotonic


% Check whether effort is strictly monotonic

if ismonotonic(effort,1) == 0

    effort_nonmon = 1;

end

income =w.*effort;

% Check whether income is weakly monotonic - also set welfare to
% zero if effort is complex
% if 1==0
if ismonotonic(income) == 0 | effort_complex == 1
    msg = horzcat('income not monotonic or effort complex-valued at x=',num2str(x));
    display(msg);
    % plot(w,income);
    % ismonotonic(income)
    % income
    income_nonmon = 1;
    xi = 0;
    effort = zeros(length(w),1);
    income = zeros(length(w),1);

    mks=effort.^(gam-1)./w;
    mtr=1-mks; 
    
    cons=zeros(length(w),1);
    welfare_vec=zeros(length(w),1);
    welfare=sum(welfare_vec.*g);
    cons_sum=sum(cons.*f);
    inc_sum=sum(income.*f);
    cons_m_inc=cons_sum-inc_sum;

    [ysort,ysortind]=sort(income);
    [temp,unsortind]=sort(ysortind,'ascend');
    mtrsort=mtr(ysortind);

    ftemp = (f(1:num_w-1)+f(2:num_w))/2;
    ftemp = [0;ftemp];

    tax = zeros(length(w),1);
else

mks=effort.^(gam-1)./w;
mtr=1-mks; 

%% compute tax schedule
% Basic idea here: sort by income. Then compute tax schedule as a function of
% income. Then unsort to have taxes as a function of w. Then sum.

[ysort,ysortind]=sort(income);
[temp,unsortind]=sort(ysortind,'ascend');
mtrsort=mtr(ysortind);

ftemp = (f(1:num_w-1)+f(2:num_w))/2;
ftemp = [0;ftemp];

%display(sum(fftemp)*dw);

tax=zeros(num_w,1);
for i=2:num_w
   tax(i,1)=tax(i-1)+mtrsort(i-1).*(ysort(i)-ysort(i-1));
end
tax=tax(unsortind); %idea here is to re-attach taxes to wages

tax_sum=sum(tax.*ftemp); % since dw is constant, this sums tax(i)*((ff(i)+ff(i-1))/2)*dw from i=2 to gridsize
% sum(fftemp)dw-1 is very very close to zero

if version ~=4
    tax=tax-tax_sum; %this line ensures that the resource constraint is satisfied!
end

if version == 4
    % calculate total expenditure on tau
    totaltau = sum(tau.*f_theta)*dw;
    % adjust taxes
    tax = tax + totaltau - tax_sum; %this line ensures that the resource constraint is satisfied!
end

%% compute consumption and welfare
cons=income-tax;
welfare_vec=cons-effort.^gam./gam;
welfare=sum(welfare_vec.*g);
cons_sum=sum(cons.*f);
inc_sum=sum(income.*f);
cons_m_inc=cons_sum-inc_sum;

end

%% For analytical reasons also output potential min and max wages by sector

w_min_theta = yy_theta*theta_min;
w_max_theta = yy_theta*theta_max;

w_min_phi = yy_phi*phi_min;
w_max_phi = yy_phi*phi_max;

%% Compute average wage by sector - and sectoral wage premium

w_mean_theta = sum(w.*f_theta)*dw;
w_mean_phi = sum(w.*f_phi)*dw;

% college wage premium
cwp = w_mean_theta / w_mean_phi;

end