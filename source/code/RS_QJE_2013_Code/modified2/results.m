function output_struct = results(x,pmf_theta,pmf_phi,rho,elast,alpha,theta_min,theta_max,phi_min,phi_max,num_theta,phi,theta,r,bin_width_theta,bin_width_phi,pmf,a_phi,a_theta,version,sbtc,normalize_skill,grid_t,grid_p,pdf_t,pdf_p,rs_skilldist,tau,threshold_theta,threshold_phi)
% RESULTS Computes welfare for a given value of x
% Based on roy3.m by Rothschild and Scheuer
% Modified by Uwe Thuemmel



%% TECHNOLOGY & PREFERENCES
if sbtc == 1
    % version with SBTC

yy_phi = (alpha/(1-alpha)*(a_theta/a_phi*x)^rho+1)^((1-rho)/rho)*(1-alpha)^(1/rho)*a_phi;

yy_theta = (1+(1-alpha)/alpha*(a_theta/a_phi*x)^(-rho))^((1-rho)/rho)*alpha^(1/rho)*a_theta;

else
if rho==1;
    yy_phi=1-alpha;
    yy_theta=alpha;
elseif rho==0;
    yy_phi=(1-alpha)*x.^alpha;
    yy_theta=alpha*x.^(alpha-1);
else
    yy_phi=(1-alpha)*(alpha*x^rho+1-alpha)^((1-rho)/rho);
    yy_theta=alpha/(1-alpha)*x^(rho-1)*yy_phi;
end
end



ggamma=yy_phi/(x*yy_theta);
gam=1+1./elast;

%% Extreme wages 
w_min_theta = yy_theta*theta_min;
w_max_theta = yy_theta*theta_max;

w_min_phi = yy_phi*phi_min;
w_max_phi = yy_phi*phi_max;

w_min = max(w_min_theta,w_min_phi);
w_max = max(w_max_theta,w_max_phi);

%% Normalization of skill distribution
% this is actually wrong, don't use this option
if normalize_skill == 1
    steps = length(grid_t);
    min_theta = min(grid_t);
    min_phi = min(grid_p);
    max_theta = max(grid_t);
    max_phi = max(grid_p);

    min_theta = min_theta / w_min;
    min_phi = min_phi / w_min;
    max_theta = max_theta / w_max;
    max_phi = max_phi / w_max;
    
    grid_t = linspace(min_theta,max_theta,steps);
    grid_p = linspace(min_phi,max_phi,steps);

    % Generate lists and variables related to skill distribution
    [theta, phi, pmf, pmf_theta, pmf_phi, cdf_theta, cdf_phi, bin_width_theta, ...
     bin_width_phi, theta_min, theta_max, phi_min, phi_max, ...
     num_theta, num_phi] = genskilldist(grid_t, grid_p, pdf_t, pdf_p,rs_skilldist);

end



%% COMPUTE WAGE DISTRIBUTION
if version ~= 4
    % in versions other than 4 there is no need to compute the thresholds
    threshold_theta = [];
    threshold_phi = [];
end

    
[w_min, w_max, num_w, dw, w, f_theta, f_phi, f, ff] = wagedist(yy_theta, yy_phi,phi, theta,pmf,version,threshold_theta,threshold_phi);

%% Compute welfare and other measures 
[welfare, welfare_vec,effort, income, mtr, mymtr, tax, w_mean_theta, w_mean_phi, ...
         cwp, effort_nonmon, income_nonmon,xi] = compute_welfare(x,gam,ggamma,r,yy_theta,yy_phi,w_min, w_max, num_w, dw, w, f_theta, f_phi, f, ff,theta_min,theta_max,phi_min,phi_max,version,tau);




%% Output
output_struct = struct('x', x,'welfare', welfare,'w', w,'effort', ...
                effort,'income', income,'mtr', mtr,'tax', tax,'f_theta', ...
                f_theta,'f_phi', f_phi,'f',f,'xi',xi,'welfare_vec', ...
                       welfare_vec,'mymtr',mymtr,'yy_theta',yy_theta,'yy_phi',yy_phi,'w_min_theta',w_min_theta,'w_max_theta',w_max_theta,'w_min_phi',w_min_phi,'w_max_phi',w_max_phi,'w_mean_theta',w_mean_theta,'w_mean_phi',w_mean_phi,'cwp',cwp,'effort_nonmon',effort_nonmon,'income_nonmon',income_nonmon,'tau',tau);


