function matrix = deserialize(sz,cls,data)
%de-serialize data, e.g. when reading from database
%
% Syntax:  matrix = deserialize(size,class,data)
%
% Inputs:
%    sz - Dimensions of the original matrix (before serialization)
%    cls - Class (type) of the original matrix
%    data - serialized data
%
% Outputs:
%    matrix - Original matrix (as before serialization)
%
%------------- BEGIN CODE --------------

sz_double = reshape(typecast(sz,'double'),[1,2]);
matrix = reshape(typecast(data,cls),sz_double);

%------------- END OF CODE --------------
