function [threshold_theta,threshold_phi,new_threshold_theta,new_threshold_phi,utility_t,utility_p,utility_t_tau,phi_adjusted,theta_adjusted,del_theta,del_phi,utility_diff_theta,utility_diff_phi] = compute_thresholds(res,theta,phi,elast,tau,threshold_theta,threshold_phi)

% Compute new thresholds for tau ~= 0 case

wage_t = (theta * res.yy_theta);
wage_p = (phi * res.yy_phi);

% assign mtr to type using interpolation
mtr_t = interp1(res.w,res.mtr,wage_t);
mtr_p = interp1(res.w,res.mtr,wage_p);

% assign effort to type using interpolation
effort_t = interp1(res.w,res.effort,wage_t);
effort_p = interp1(res.w,res.effort,wage_p);

% assign tax rate to type using interpolation
tax_t = interp1(res.w,res.tax,wage_t);
tax_p = interp1(res.w,res.tax,wage_p);

% calculate utility
gam=1+1./elast;

income_t = wage_t.*effort_t;
cons_t = income_t-tax_t;
utility_t = (cons_t-effort_t.^gam./gam);

income_p = wage_p.*effort_p;
cons_p = income_p-tax_p;
utility_p = (cons_p-effort_p.^gam./gam);


% % calculate utility corresponding to threshold
% utility_threshold_phi = interp1(phi,utility_p,threshold_phi);
% utility_threshold_theta = interp1(theta,utility_t,threshold_theta);


% bring phi and utility_p in line - and also the corresponding
% elements in theta and utility_t (this needs a quadratic skill
% grid to begin with)

% generate matrix which indicates which elements of phi and theta to delete
% to keep symmetry delete these elements from both grids
del_phi = find(isnan(utility_p));
del_theta = find(isnan(utility_t));

% delete these elements, using aux as index
aux = isnan(utility_p);
utility_p(aux)=[];
phi(aux)=[];

utility_t(aux)=[];
theta(aux)=[];

threshold_phi(aux)=[];
threshold_theta(aux)=[];

% bring theta and utility_t in line
aux = isnan(utility_t);
utility_t(aux)=[];
theta(aux)=[];

utility_p(aux)=[];
phi(aux)=[];

threshold_phi(aux)=[];
threshold_theta(aux)=[];


% calculate threshold utility as a function of tau
utility_t_tau = utility_t + tau;


% compute new threshold for phi
new_threshold_phi = interp1(utility_p,phi,utility_t_tau);

% compute new threshold for theta
new_threshold_theta = interp1(utility_t_tau,theta,utility_p);

theta_adjusted = theta;
phi_adjusted = phi;

% calculate utility levels at old threshold
utility_threshold_phi = interp1(phi_adjusted,utility_p, ...
                                    threshold_phi);

utility_diff_theta = utility_t_tau - utility_threshold_phi;

utility_threshold_theta = interp1(theta_adjusted,utility_t, ...
                                    threshold_theta);
utility_diff_phi = utility_p - utility_threshold_theta + tau;

end