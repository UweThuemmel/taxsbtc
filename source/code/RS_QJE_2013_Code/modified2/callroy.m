% File to call function roy3.m with parameters
% On Linux start MATLAB from the terminal using
% LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libstdc++.so.6:/lib/x86_64-linux-gnu/libgcc_s.so.1
% matlab
% then from the MATLAB shell issue
% !ldd matlab-sqlite3-driver-master/+sqlite3/private/libsqlite3_.mexa64 

clear all
close all
clc

delete roy3;
diary roy3;

% global x num_w ggamma yy_theta yy_phi gam w dw ff gg f f_phi f_theta;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SETUP

% SET FORMATS
format long;
format compact;
warning off;

% OTHER SETTINGS
options=optimset('TolCon',1e-13,'TolFun',1e-13,'TolX',1e-13,'Display','off','MaxFunEvals',10000000,'MaxIter',200);



%% Connect to database
db = sqlite3.open('test.sqlite');


%% SPECIFY GRID FOR x
xmin = 1.15;
xmax = 1.25;
dx = .005;

xgrid = (xmin:dx:xmax);

% Settings for social welfare function
r=1.3;

%% specify version: 1 is original, 2 is modified, 3 uses corrected
%% numerical integration
normalize_skill = 0; %correct setting is 0, tried to normalize skill to keep extreme wages unchanged
version = 3;
rs_skilldist = 3;
sbtc=1;
a_phi = 1;
a_theta = 2.5;
rho = 1/3;
% grid_p = grid_t;
% pdf_p = pdf_t;
alpha = 0.5;
elast = 0.5;

if rs_skilldist == 1
% LOAD DATA AND UNDERLYING PARAMETERS
load('logndist.mat'); % load skill distribution and other parameters
else
    % specify skill-distribution
mu_t = 4;
sigma_t = 1;

% mu_p = 2.8655;
mu_p=mu_t;
sigma_p = 1;


grid_t = linspace(0.1,1000,1000);
grid_p = linspace(0.1,1000,1000);

pdf_t = lognpdf(grid_t,mu_t,sigma_t);
pdf_p = lognpdf(grid_p,mu_p,sigma_p);

end


comment = 'zoom in for atheta = 2.5';

if version ~= 4
    tau = 0
end

[opt parameters] = roy3(grid_t, grid_p, pdf_t, pdf_p,mu_t,sigma_t,mu_p,sigma_p, rho, version, elast, alpha,r,xmin,xmax,dx,a_phi,a_theta,sbtc,rs_skilldist,comment,normalize_skill,tau);

% write to database
struct2db(db,'results',combinestructs({parameters,opt}),1,1);

% %% specify version: 1 is original, 2 is modified
% version = 2;

% [x_max_ver2, welfaregrid_ver2] = roy3(grid_t, grid_p, pdf_t, pdf_p, xgrid, rho, version, elast, alpha, ...
%      r);

% parameters = struct('rho',rho,'alpha',alpha,'version',version, ...
%                     'elast', elast, 'r',r,'xmin',xmin,'xmax',xmax,'dx',dx);
% matrices = struct('welfare',welfaregrid_ver2,'xgrid',xgrid);

% % write to database
% dbwrite(db,'test',parameters,matrices,'version 2 testing');
