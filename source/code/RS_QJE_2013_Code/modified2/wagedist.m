function [w_min, w_max, num_w, dw, w, f_theta, f_phi, f, ff] = wagedist(yy_theta, yy_phi,phi, theta,pmf,version,threshold_theta,threshold_phi)
% WAGEDIST
% Generate wage distribution bases on the production function and
% the skill distribution
% Syntax:  
%  [w_min, w_max, num_w, dw, w, f_theta, f_phi, f, ff] = wagedist(yy_theta, yy_phi,phi, theta,pmf,version)
% 
% Inputs:
%    yy_theta - Marginal product wrt E_theta
%    yy_phi - Marginal product wrt E_phi
%    phi - Phi grid
%    theta - Theta grid
%    pmf - Probability mass function of types (theta,phi)
%    version - version concerning calculation of the wage
%    distribution
%    threshold_theta - vector of thresholds of theta skill
%    corresponding to being indifferent between both sectors at
%    skill phi and wage w. The standard specification is
%    w./yy_theta. Used when gradually adjusting the wage
%    distribution in response to a change in tau
%    threshold_phi - symmetric to threshold_phi
%
% Outputs:
%    w_min - Lowest observed wag
%    w_max - Highest observed wage
%    num_w - Number of points on wage grid
%    dw - distance between two grid points of wage grid
%    w - wage grid
%    f_theta - density of wages earned in the theta sector
%    f_phi - density of wages earned in the phi sector
%    f - joint density of wages
%    ff - CDF of wages
%
%
% See also: results.m
% 
% Author: Uwe Thuemmel
% PhD candidate Erasmus University Rotterdam, VU University
% Amsterdam, Tinbergen Institute
% email: u.thuemmel@tinbergen.nl
% January 2014

%------------- BEGIN CODE --------------


bin_width_theta = theta(2) - theta(1);
bin_width_phi = phi(2) - phi(1);
theta_min = min(theta);
theta_max = max(theta);
phi_min = min(phi);
phi_max = max(phi);

num_theta=length(theta);

w_min=max(yy_theta*theta_min,yy_phi*phi_min);
w_max=max(yy_theta*theta_max,yy_phi*phi_max);
num_w=num_theta;
dw=(w_max-w_min)/(num_w-1);
w=(w_min:dw:w_max)';


cdf_aux_theta=cumsum(pmf,2);
cdf_aux_phi=cumsum(pmf,1);
[PHI,THETA]=meshgrid(phi,theta);


switch version

case 1
  % original version as by R&S
      f_theta=max(0,1/yy_theta*interp2(PHI,THETA,cdf_aux_theta/bin_width_theta,min(w./yy_phi,phi_max),w./yy_theta))*dw;

    f_phi=max(0,1/yy_phi*interp2(PHI,THETA,cdf_aux_phi/bin_width_phi,w./yy_phi,min(w./yy_theta,theta_max)))*dw;

case 2
  % modified version
    f_theta=max(0,1/yy_theta*interp2(PHI,THETA,cdf_aux_theta,min(w./yy_phi,phi_max),w./yy_theta));

    f_phi=max(0,1/yy_phi*interp2(PHI,THETA,cdf_aux_phi,w./yy_phi,min(w./yy_theta,theta_max)));

case 3
  % version with what I think is the correct numerical integration

     f_theta=max(0,1/yy_theta*interp2(PHI,THETA,cdf_aux_theta,min(w./yy_phi,phi_max),w./yy_theta))*bin_width_phi;

     f_phi=max(0,1/yy_phi*interp2(PHI,THETA,cdf_aux_phi,w./yy_phi,min(w./yy_theta,theta_max)))*bin_width_theta;

case 4

  % like 3, but now with the threshold-skill passed as argument

     f_theta=max(0,1/yy_theta*interp2(PHI,THETA,cdf_aux_theta,min(threshold_phi,phi_max),w./yy_theta))*bin_width_phi;

     f_phi=max(0,1/yy_phi*interp2(PHI,THETA,cdf_aux_phi,w./yy_phi,min(threshold_theta,theta_max)))*bin_width_theta;


end

f=f_theta+f_phi;
f_theta=f_theta/sum(f);
f_phi=f_phi/sum(f);
f=f/sum(f);
ff=min(1,cumsum(f));
ff(end)=1;


end


% pmf = @(theta,phi) lognpdf(theta,mu_t,sigma_t)*lognpdf(phi,mu_t,sigma_t);
% function result = f_theta_cont(w,pmf,yy_theta,yy_phi,theta_min,phi_min)
% pmf_theta_fixed = @(phi) pmf(w/yy_theta,phi);
% result = 1/yy_theta*integral(pmf_theta_fixed,phi_min,w/yy_phi)
% end


%------------- END OF CODE --------------
