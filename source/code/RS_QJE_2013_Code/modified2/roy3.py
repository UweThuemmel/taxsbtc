# Python version of roy3.m
# Based on Rothschild and Scheuer, 2013
# Modified by Uwe Thuemmel

from scipy.stats import lognorm
from scipy.interpolate import RectBivariateSpline
import scipy.io


def initialize(version):
    """Initializes the skill distribution
    
    Arguments:
    - `version`: 1 refers to the original distribution as used by  Rothschild and Scheuer
    """

    if version == 1:
        print('Version as in Rothschild and Scheuer, 2013')
        # load data and underlying parameters
        mat = scipy.io.loadmat('logndist.mat')
        # read from matlab mat
        theta = mat['grid_t'].T
        phi = mat['grid_p'].T
        pdf_t = mat['pdf_t']
        pdf_p = mat['pdf_p']

        pmf_theta = maximum(0,pdf_t.T - pdf_t[0,-1])
        cdf_theta = cumsum(pmf_theta,axis=0)
        # get rid of zero elements
        theta = theta[1:499,:]
        pmf_theta = pmf_theta[1:499,:]
        cdf_theta = cdf_theta[1:499,:]
        num_theta = len(theta)

        pmf_phi = pdf_p.T
        cdf_phi = cumsum(pmf_phi,axis=0)

        bind_width_theta = theta[1,0] - theta[0,0]
        bind_width_phi = phi[1,0] - phi[0,0]
        theta_min = min(theta)[0]
        theta_max = max(theta)[0]
        phi_min = min(phi)[0]
        phi_max = max(phi)[0]

        # using independence
        pmf = dot(pmf_theta,pmf_phi.T)
    elif version == 2:
        theta = arange(0,500,3)
        mu_theta = 1
        sigma_theta = 2
        pdf_theta = lognorm.pdf(theta,sigma_theta,scale=exp(mu_theta))

        phi = arange(0,500,3)
        mu_phi = 1
        sigma_phi = 2
        pdf_phi = lognorm.pdf(phi,sigma_phi,scale=exp(mu_phi))
        
def welfare(x,version,alpha,elast,rho):
    """
    Compute welfare based on the value for x
    
    Arguments:
    
    - `x`: ratio of effort across sectors
    - `version`: which version to use
        - `1`: corresponds to R&S
    - `alpha`: parameter for production function
    - `rho`:  parameter for production function
    """
    if rho==1:
        yy_phi = 1-alpha
        yy_theta = alpha
    elif rho==0:
        yy_phi = (1-alpha)*x**alpha
        yy_theta = alpha*x**(alpha-1)
    else:
        yy_phi = (1-alpha)*(alpha*x**rho+1-alpha)**((1-rho)/rho)
        yy_theta = alpha/(1-alpha)*x**(rho-1)*yy_phi


    ggamma=yy_phi/(x*yy_theta)
    gam=1+1/elast

    w_min = max(yy_theta*theta_min,yy_phi*phi_min)
    w_max = max(yy_theta*theta_max,yy_phi*phi_max)
    num_w = num_theta
    dw = (w_max-w_min)/(num_w-1)

    # generate wage grid
    w = arange(w_min, w_max, dw)

    cdf_aux_theta = cumsum(pmf,axis=1)
    cdf_aux_phi = cumsum(pmf,axis=0)
    PHI, THETA = meshgrid(phi, theta)

    f_theta = 1/yy_theta
    return({'ggamma':ggamma,'yy_phi':yy_phi,'yy_theta':yy_theta,'gam':gam,'w_min':w_min,'num_w':num_w})
    
welfare(1,1,0.5,0.3,1)



service = np.array(range(10, 31, 10))
years = np.array(range(1950, 1991, 10))

wage = np.array([[150.6970,199.5920,187.6250],
    [179.3230, 195.0720, 250.2870],
    [203.2120, 179.0920, 322.7670],
    [226.5050, 153.7060, 426.7300],
    [249.6330, 120.2810, 598.2430]])

ip = RectBivariateSpline(years, service, wage, kx=2, ky=2)