function dbcheckcolumn(db,table,name,type)
%Check whether a certain column exists in a table in the database. If not,
%create it
%
% Syntax:  [output1,output2] = function_name(input1,input2,input3)
%
% Inputs:
%    input1 - Description
%    input2 - Description
%    input3 - Description
%
% Outputs:
%    output1 - Description
%    output2 - Description
%
% Example: 
%    Line 1 of example
%    Line 2 of example
%    Line 3 of example
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: OTHER_FUNCTION_NAME1,  OTHER_FUNCTION_NAME2

% Author: Uwe Thuemmel
% PhD candidate Erasmus University Rotterdam, VU University
% Amsterdam, Tinbergen Institute
% email: u.thuemmel@tinbergen.nl
% January 2014

%------------- BEGIN CODE --------------
% Obtain information for table
command = strcat('PRAGMA table_info(',table,')');
info = sqlite3.execute(db,command);

% array of column names
colnames = {info.name};

% check whether column name exists
exists=max(ismember(colnames,name));

% create column of type 'type' if does not exist
if exists == 0
    command = strcat({'ALTER TABLE '},table,{' ADD COLUMN '},name,{' '},type);
    sqlite3.execute(db,command{1});
    message = horzcat('Column ',name,' inserted into table ', ...
                      table);
    display(message);
end


%------------- END OF CODE --------------
