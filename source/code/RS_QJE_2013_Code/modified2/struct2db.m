function [output1,output2] = struct2db(db,table,data,git,timestamp)
%Write struct to table in db, where struct names correspond to
%column names
%
% Syntax:  [output1,output2] = function_name(input1,input2,input3)
%
% Inputs:
%    input1 - Description
%    input2 - Description
%    input3 - Description
%    git - 1: insert last git commit
%    timestamp - 1: insert timestamp
%
% Outputs:
%    output1 - Description
%    output2 - Description
%
% Example: 
%    Line 1 of example
%    Line 2 of example
%    Line 3 of example
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: OTHER_FUNCTION_NAME1,  OTHER_FUNCTION_NAME2

% Author: Uwe Thuemmel
% PhD candidate Erasmus University Rotterdam, VU University
% Amsterdam, Tinbergen Institute
% email: u.thuemmel@tinbergen.nl
% January 2014

%------------- BEGIN CODE --------------
% get id of new row
command = horzcat('SELECT max(rowid) FROM ',table);
temp = sqlite3.execute(db,command);
if temp.max_rowid == []
    % case of new table
    rowid = 1
else
    rowid = temp.max_rowid + 1;
end

% insert into rowid
command = horzcat('INSERT INTO ',table,' (rowid) VALUES(?)');
sqlite3.execute(db,command,rowid);

% git commit
if git
[status commit] =  system('git rev-parse HEAD');
        dbcheckcolumn(db,table,'git','TEXT');
% insert git commit into table
        command = horzcat('UPDATE ',table,' SET ','git','=(?) WHERE rowid =',int2str(rowid));
sqlite3.execute(db,command,commit);
end

% timestamp
if timestamp
     dbcheckcolumn(db,table,'timestamp','DATETIME');
command = strcat({'UPDATE '},table,{' SET '},'timestamp',' = (CURRENT_TIMESTAMP)',{' WHERE rowid ='},int2str(rowid));
sqlite3.execute(db,command{1});
end

% iterate over data struct
fields = fieldnames(data);
for i=1:length(fields)
    item2db(db,table,getfield(data,fields{i}),fields{i},rowid);
end

end


function item2db(db,table,item,itemname,rowid)
% Write item from struct to db

% suffixes for serializing
suf = ['size','class','data'];

% Check class of item
cls = class(item);

% Different cases, depending on class and dimension
if strcmp(cls,'double')
    % check dimension
    if size(item) == [1 1]
        % singleton
        dbcheckcolumn(db,table,itemname,'REAL');
        command = horzcat('UPDATE ',table,' SET ',itemname,'=(?) WHERE rowid =',int2str(rowid));
        sqlite3.execute(db,command,item);
    else
        % vector or matrix
        % serialize
        
        try
            serialized = serialize(item,itemname);
        
        catch err
            if (strcmp(err.identifier,['MATLAB:typecastc:' ...
                                    'firstArgMustBeFullNumArray']))
                msg = horzcat('serializing of item ',itemname,[' ' ...
                                    'failed']);
                display(msg);
                return
             end
        end
                

        % check whether columns exist
        dbcheckcolumn(db,table,strcat(itemname,'_size'),'BLOB');
        dbcheckcolumn(db,table,strcat(itemname,'_class'), ...
                          'TEXT');
        dbcheckcolumn(db,table,strcat(itemname,'_data'),'BLOB');

        % write to db
        % size
        command = horzcat('UPDATE ',table,' SET ',itemname,['_size = ' ...
                        '(?)'],' WHERE rowid =',int2str(rowid));
        sqlite3.execute(db,command,serialized.size);
        % class
        command = horzcat('UPDATE ',table,' SET ',itemname,['_class = ' ...
                        '(?)'],' WHERE rowid =',int2str(rowid));
        sqlite3.execute(db,command,serialized.class);
        % data
        command = horzcat('UPDATE ',table,' SET ',itemname,['_data = ' ...
                        '(?)'],' WHERE rowid =',int2str(rowid));
        sqlite3.execute(db,command,serialized.data);
    end

elseif strcmp(cls,'char')
    % string
    dbcheckcolumn(db,table,itemname,'TEXT');
    command = horzcat('UPDATE ',table,' SET ',itemname,['=(?) WHERE ' ...
                        'rowid ='],int2str(rowid));
    sqlite3.execute(db,command,item);
end

end

%------------- END OF CODE --------------
