function [theta, phi, pmf, pmf_theta, pmf_phi, cdf_theta, cdf_phi, bin_width_theta, ...
         bin_width_phi, theta_min, theta_max, phi_min, phi_max, ...
          num_theta, num_phi] = genskilldist(grid_t, grid_p, pdf_t, pdf_p,rs_skilldist)
%GENSKILLDIST Generate lists and variables related to skill distribution
%
% Syntax:  [pmf, pmf_theta, pmf_phi, cdf_theta, cdf_phi, bin_width_
%          bin_width_phi, theta_min, theta_max, phi_min, phi_max] =
%          genskilldist(grid_t,
%          grid_p, pdf_t, pdf_p)
%
% Inputs:
%    grid_t - Grid for theta
%    grid_p - Grid for phi
%    pdf_t - Probability density function for theta
%    pdf_p - Probability density function for phi
%
% Outputs:
%    output1 - Description
%    output2 - Description
%
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: roy3.m

% Author: Based on Rotschild & Scheuer, 2013, QJE

%------------- BEGIN CODE --------------


theta = grid_t';
phi = grid_p';

if rs_skilldist ==1

    pmf_theta=max(0,pdf_t'-pdf_t(end)); %so that there is no jump at upper
                                        %bound (ONLY DO ONE)
                                        %pmf_theta=pdf_p';
    cdf_theta=cumsum(pmf_theta);

    aux=find(pmf_theta==0);
    theta(aux)=[];
    pmf_theta(aux)=[];
    cdf_theta(aux)=[];
    num_theta=length(theta);

    %pmf_phi=max(0,pdf_p'-pdf_p(end)); %so that there is no jump at upper bound (ONLY DO ONE)
    pmf_phi=pdf_p';
    cdf_phi=cumsum(pmf_phi);

    aux=find(pmf_phi==0);
    phi(aux)=[];
    pmf_phi(aux)=[];
    cdf_phi(aux)=[];
    num_phi=length(phi);

elseif rs_skilldist==2
    % approach as in R&S
    pmf_theta=max(0,pdf_t'-pdf_t(end)); %so that there is no jump at upper
                                        %bound (ONLY DO ONE)
                                        %pmf_theta=pdf_p';
    cdf_theta=cumsum(pmf_theta);

    aux=find(pmf_theta==0);
    theta(aux)=[];
    pmf_theta(aux)=[];
    cdf_theta(aux)=[];
    num_theta=length(theta);

    %pmf_phi=max(0,pdf_p'-pdf_p(end)); %so that there is no jump at upper bound (ONLY DO ONE)
    pmf_phi=pdf_p';
    cdf_phi=cumsum(pmf_phi);

    aux=find(pmf_phi==0);
    phi(aux)=[];
    pmf_phi(aux)=[];
    cdf_phi(aux)=[];
    num_phi=length(phi);

elseif rs_skilldist==3
    % approach as in R&S - but now for both
    pmf_theta=max(0,pdf_t'-pdf_t(end)); %so that there is no jump at upper
                                        %bound 
                                        %pmf_theta=pdf_p';
    cdf_theta=cumsum(pmf_theta);

    aux=find(pmf_theta==0);
    theta(aux)=[];
    pmf_theta(aux)=[];
    cdf_theta(aux)=[];
    num_theta=length(theta);

    pmf_phi=max(0,pdf_p'-pdf_p(end)); %so that there is no jump at upper bound (ONLY DO ONE)
    % pmf_phi=pdf_p';
    cdf_phi=cumsum(pmf_phi);

    aux=find(pmf_phi==0);
    phi(aux)=[];
    pmf_phi(aux)=[];
    cdf_phi(aux)=[];
    num_phi=length(phi);

elseif rs_skilldist==4
    % approach as in R&S - but now for phi instead of theta
    % pmf_theta=max(0,pdf_t'-pdf_t(end)); %so that there is no jump at upper
                                        %bound 
                                        %pmf_theta=pdf_p';
    pmf_theta=pdf_t';
    cdf_theta=cumsum(pmf_theta);

    aux=find(pmf_theta==0);
    theta(aux)=[];
    pmf_theta(aux)=[];
    cdf_theta(aux)=[];
    num_theta=length(theta);

    pmf_phi=max(0,pdf_p'-pdf_p(end)); %so that there is no jump at upper bound (ONLY DO ONE)
    % pmf_phi=pdf_p';
    cdf_phi=cumsum(pmf_phi);

    aux=find(pmf_phi==0);
    phi(aux)=[];
    pmf_phi(aux)=[];
    cdf_phi(aux)=[];
    num_phi=length(phi);


else

    pmf_theta=pdf_t';
    cdf_theta=cumsum(pmf_theta);
    num_theta=length(theta);

    pmf_phi=pdf_p';
    cdf_phi=cumsum(pmf_phi);
    num_phi=length(phi);

end



bin_width_theta = theta(2) - theta(1);
bin_width_phi = phi(2) - phi(1);
theta_min = min(theta);
theta_max = max(theta);
phi_min = min(phi);
phi_max = max(phi);

% using independence here
pmf=pmf_theta*pmf_phi';
