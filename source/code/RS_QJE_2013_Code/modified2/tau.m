% Experiment with adjusting the wage distribution in response to an
% increase in tau

%% Parameters
r=1.3;

%% specify version: 1 is original, 2 is modified, 3 uses corrected
%% numerical integration, version 4 uses adjustement of wage
%% distribution if tau !=0
normalize_skill = 0; %correct setting is 0, tried to normalize skill to keep extreme wages unchanged
version = 3;
rs_skilldist = 3;
sbtc=1;
a_phi = 1;
a_theta = 1.5;
rho = 1/3;
% grid_p = grid_t;
% pdf_p = pdf_t;
alpha = 0.5;
elast = 0.5;

%% Specify skill distribution

mu_t = 4;
sigma_t = 1;

% mu_p = 2.8655;
mu_p=mu_t;
sigma_p = 1;


grid_t = linspace(0.1,1000,1000);
grid_p = linspace(0.1,1000,1000);

pdf_t = lognpdf(grid_t,mu_t,sigma_t);
pdf_p = lognpdf(grid_p,mu_p,sigma_p);


[theta, phi, pmf, pmf_theta, pmf_phi, cdf_theta, cdf_phi, bin_width_theta, ...
 bin_width_phi, theta_min, theta_max, phi_min, phi_max, ...
 num_theta, num_phi] = genskilldist(grid_t, grid_p, pdf_t, pdf_p,rs_skilldist);

x=1.085;

r = results(x,pmf_theta,pmf_phi,rho,elast,alpha,theta_min,theta_max,phi_min,phi_max,num_theta,phi,theta,r,bin_width_theta,bin_width_phi,pmf,a_phi,a_theta,version,sbtc,normalize_skill,grid_t,grid_p,pdf_t,pdf_p,rs_skilldist);

% tranform  skills into corresponding wages
wage_t = (grid_t * r.yy_theta)';
wage_p = (grid_p * r.yy_phi)';

% assign mtr to type using interpolation
mtr_t = interp1(r.w,r.mtr,wage_t);
mtr_p = interp1(r.w,r.mtr,wage_p);

% assign effort to type using interpolation
effort_t = interp1(r.w,r.effort,wage_t);
effort_p = interp1(r.w,r.effort,wage_p);

% assign tax rate to type using interpolation
tax_t = interp1(r.w,r.tax,wage_t);
tax_p = interp1(r.w,r.tax,wage_p);

% calculate utility
gam=1+1./elast;

income_t =wage_t.*effort_t;
cons_t=income_t-tax_t;
utility_t=cons_t-effort_t.^gam./gam;

income_p =wage_p.*effort_p;
cons_p=income_p-tax_p;
utility_p=cons_p-effort_p.^gam./gam;

% plot utility agains type
path = ['/media/data/Dropbox/PhD/projects/taxsbtc/source/lyx/' ...
        'figures/'];
% Plot of utilities against theta and phi
figure(1)
subplot(1,2,1);
plot(grid_t,utility_t)
title('Utility against theta')
xlabel('theta')
axis([0,max(grid_t),0,max(utility_t)]);
subplot(1,2,2);
plot(grid_p,utility_p)
title('Utility against phi')
xlabel('phi')
axis([0,max(grid_t),0,max(utility_t)]);
print(figure(1),horzcat(path,'utility_type.eps'),'-depsc')

% calculate thresholds
threshold_phi = (grid_t * r.yy_theta)/r.yy_phi;

% calculate utility corresponding to threshold
utility_threshold_phi = interp1(grid_p,utility_p,threshold_phi);

% a plot shows that indeed utilities are equal at the threshold
% plot(grid_t,utility_t,grid_t,utility_threshold_phi)

% calculate threshold utility as a function of tau
tau = 10;
utility_t_tau = utility_t + tau;

% calculate threshold type corresponding to threshold utility
new_threshold_phi = interp1(utility_p,grid_p,utility_t_tau);