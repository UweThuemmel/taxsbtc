function myanimate(selection)
% Produces the animation corresponding to selection

% animation of wage distribution plots for different values of x
% load data to plot

%% Animation settings
numtimes=1;
fps=10;
% figure('units','normalized','position',[.1 .1 1 1]) 

%% Connect to database
db = sqlite3.open('test.sqlite');
out = dbread(db,'results',63);


% figure(1)
% plot(out.xgrid,out.welfaregrid)

switch selection


case 2

figure(2)
for i=1:length(out.xgrid)
plot(log(out.w),out.f_theta_all(:,i),log(out.w),out.f_phi_all(:,i),log(out.w),out.f_all(:,i));
% axis equal;
% daspect([10 1 1])
axis([0,8,0,max(out.f_all(:))]);
legend('f_theta','f_phi','f');
xlabel('log wage');
str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
text(6,0.03,str1);
title('Wage distributions');
M(i)=getframe;
end

case 3

% animation for welfare_vec
figure(3)
f = getframe;
[im,map] = rgb2ind(f.cdata,256,'nodither');
im(1,1,1,length(out.xgrid)) = 0;
for i=1:length(out.xgrid)
plot(log(out.w),out.welfare_vec(:,i));
% axis equal;
% daspect([10 1 1])
axis([0,8,min(out.welfare_vec(:)),mean(out.welfare_vec(:))]);
xlabel('log wage');
str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
text(6,0,str1);
title('Welfare vector');
f=getframe;
im(:,:,1,i) = rgb2ind(f.cdata,map,'nodither');
M2(i)=getframe;
end

case 4

% animation for effort_all
figure(4)
f = getframe;
[im,map] = rgb2ind(f.cdata,256,'nodither');
im(1,1,1,length(out.xgrid)) = 0;
for i=1:length(out.xgrid)
plot(log(out.w),out.effort_all(:,i));
% axis equal;
% daspect([10 1 1])
axis([0,8,min(out.effort_all(:))-10,max(out.effort_all(:))]);
xlabel('log wage');
str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
text(6,45,str1);
title('Effort schedule');
f=getframe;
im(:,:,1,i) = rgb2ind(f.cdata,map,'nodither');
M3(i)=getframe;
end

case 5

% animation for mtr_all
figure(5)
f = getframe;
[im,map] = rgb2ind(f.cdata,256,'nodither');
im(1,1,1,length(out.xgrid)) = 0;
for i=1:length(out.xgrid)
plot(out.w,out.mymtr_all(:,i),out.w,ones(1,length(out.w)),out.w,zeros(1,length(out.w)));
% axis equal;
% daspect([10 1 1])
axis([0,max(out.w),-2,2]);
xlabel('wage');
str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
text(1000,-1,str1);
title('Untruncated MTR schedule');
f=getframe;
im(:,:,1,i) = rgb2ind(f.cdata,map,'nodither');
M4(i)=getframe;
end

case 6

% animation for fthetax / fx
figure(6)
f = getframe;
[im,map] = rgb2ind(f.cdata,256,'nodither');
im(1,1,1,length(out.xgrid)) = 0;
for i=1:length(out.xgrid)
plot(log(out.w),out.f_theta_all(:,i) ./ out.f_all(:,i));
% axis equal;
% daspect([10 1 1])
axis([0,max(log(out.w)),0,1]);
xlabel('log wage');
str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
text(5,0.5,str1);
title('fthetax / fx');
f=getframe;
im(:,:,1,i) = rgb2ind(f.cdata,map,'nodither');
M6(i)=getframe;
end

case 7
% animation of fthetax/fphix
figure(7)
f = getframe;
[im,map] = rgb2ind(f.cdata,256,'nodither');
im(1,1,1,length(out.xgrid)) = 0;
for i=1:length(out.xgrid)
plot(log(out.w),out.f_theta_all(:,i) ./ out.f_phi_all(:,i));
% axis equal;
% daspect([10 1 1])
axis([0,max(log(out.w)),0,1]);
xlabel('log wage');
str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
text(5,0.5,str1);
title('fthetax / fphix');
f=getframe;
im(:,:,1,i) = rgb2ind(f.cdata,map,'nodither');
M7(i)=getframe;
end
movie2avi(M7, 'fthetax_over_fphix.avi');

case 8

figure(8)
out = dbread(db,'results',69);
% animation for mtr_all - based on row 69
f = getframe;
[im,map] = rgb2ind(f.cdata,256,'nodither');
im(1,1,1,length(out.xgrid)) = 0;
for i=1:length(out.xgrid)
plot(out.w,out.mtr_all(:,i),out.w,ones(1,length(out.w)),out.w,zeros(1,length(out.w)));
% axis equal;
% daspect([10 1 1])
axis([0,max(out.w),-2,2]);
xlabel('wage');
str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
text(1000,-1,str1);
title('MTR schedule');
f=getframe;
im(:,:,1,i) = rgb2ind(f.cdata,map,'nodither');
M(i)=getframe;
end

movie2avi(M, 'mtr69.avi');

case 9

% animation for effort_all - row 69
out = dbread(db,'results',69);
f = getframe;
[im,map] = rgb2ind(f.cdata,256,'nodither');
im(1,1,1,length(out.xgrid)) = 0;
for i=1:length(out.xgrid)
plot(out.w,out.effort_all(:,i));
% axis equal;
% daspect([10 1 1])
axis([0,max(out.w),min(out.effort_all(:))-10,max(out.effort_all(:))]);
xlabel('wage');
str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
text(100,10,str1);
title('Effort schedule');
f=getframe;
im(:,:,1,i) = rgb2ind(f.cdata,map,'nodither');
M(i)=getframe;
end

movie2avi(M, 'effort_69.avi');

case 10
writerObj = VideoWriter('effort_70','Uncompressed AVI');
open(writerObj);
% animation for effort_all - row 70
out = dbread(db,'results',70);

f = getframe;
[im,map] = rgb2ind(f.cdata,256,'nodither');
im(1,1,1,length(out.xgrid)) = 0;
for i=1:length(out.xgrid)
plot(out.w,out.effort_all(:,i));
% axis equal;
% daspect([10 1 1])
axis([0,max(out.w),min(out.effort_all(:))-10,max(out.effort_all(:))]);
xlabel('wage');
str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
text(100,5,str1);
title('Effort schedule');
f=getframe;
im(:,:,1,i) = rgb2ind(f.cdata,map,'nodither');
M(i)=getframe;
writeVideo(writerObj,M(i));
end

close(writerObj);
% movie2avi(M, 'effort_70.avi','quality',100);

case 11

% version with full canvas included
% h=figure('Position',[1 1 1000 1000]) ;
h=figure
out = dbread(db,'results',70);
vidObj = VideoWriter('effort_70','Uncompressed AVI');
open(vidObj);
movegui(h, 'onscreen');
rect = get(h,'Position'); 
rect(1:2) = [0 0]; 
for i=1:length(out.xgrid)
  h = plot(out.w,out.effort_all(:,i));
  axis([0,max(out.w),min(out.effort_all(:))-10,max(out.effort_all(:))]);
  xlabel('wage');
  str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
  text(100,5,str1);
  title('Effort schedule');
  movegui(h, 'onscreen');
  % hold all;
  % datetick;
  % drawnow;
  writeVideo(vidObj,getframe(gcf,rect));
end
close(vidObj);  



case 12
% income against wage (to check monotonicity condition)
h=figure
out = dbread(db,'results',71);
vidObj = VideoWriter('income_71','Uncompressed AVI');
open(vidObj);
movegui(h, 'onscreen');
rect = get(h,'Position'); 
rect(1:2) = [0 0]; 
for i=1:length(out.xgrid)
  h = plot(out.w,out.income_all(:,i));
  % axis([0,max(out.w),min(out.income_all(:))-10,max(out.income_all(:))]);
  axis([0,200,min(out.income_all(:))-10,0.5*10^3]);
  xlabel('wage');
  str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
  text(100,5,str1);
  title('Income schedule');
  movegui(h, 'onscreen');
  % hold all;
  % datetick;
  % drawnow;
  writeVideo(vidObj,getframe(gcf,rect));
end
close(vidObj);  

case 13
% wage distributions
h=figure
out = dbread(db,'results',73);
vidObj = VideoWriter('wagedist_73','Uncompressed AVI');
open(vidObj);
movegui(h, 'onscreen');
rect = get(h,'Position'); 
rect(1:2) = [0 0]; 
for i=1:length(out.xgrid)
  h = plot(log(out.w),out.f_theta_all(:,i),log(out.w),out.f_phi_all(:,i),log(out.w),out.f_all(:,i));
  % axis([0,max(out.w),min(out.income_all(:))-10,max(out.income_all(:))]);
  axis([0,max(log(out.w)),min(out.f_all(:)),max(out.f_all(:))]);
  xlabel('Log wage');
  str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
  text(6,0.03,str1);
  title('Density');
  legend('f_{theta}','f_{phi}','f')
  % movegui(h, 'onscreen');
  % hold all;
  % datetick;
  % drawnow;
  writeVideo(vidObj,getframe(gcf,rect));
end
close(vidObj);  



case 14
% wage distributions - same as before but zoomed in, non-log
h=figure
out = dbread(db,'results',73);
vidObj = VideoWriter('wagedist_zoomed_73','Uncompressed AVI');
open(vidObj);
movegui(h, 'onscreen');
rect = get(h,'Position'); 
rect(1:2) = [0 0]; 
for i=1:length(out.xgrid)
  h = plot(out.w,out.f_theta_all(:,i),out.w,out.f_phi_all(:,i),out.w,out.f_all(:,i));
  axis([40,80,min(out.f_all(:)),0.000002]);
  xlabel('Wage');
  str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
  text(70,0.000001,str1);
  title('Density');
  legend('f_{theta}','f_{phi}','f')
  % movegui(h, 'onscreen');
  % hold all;
  % datetick;
  % drawnow;
  writeVideo(vidObj,getframe(gcf,rect));
end
close(vidObj);  


case 15
% wage distributions - same as before but zoomed in, non-log,
% zoomed in on phi
h=figure
out = dbread(db,'results',73);
vidObj = VideoWriter('wagedist_zoomed_phi_73','Uncompressed AVI');
open(vidObj);
movegui(h, 'onscreen');
rect = get(h,'Position'); 
rect(1:2) = [0 0]; 
for i=1:length(out.xgrid)
  h = plot(out.w,out.f_phi_all(:,i));
  axis([40,80,min(out.f_all(:)),0.008]);
  xlabel('Wage');
  str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
  text(70,0.004,str1);
  title('Density');
  legend('f_{phi}')
  % movegui(h, 'onscreen');
  % hold all;
  % datetick;
  % drawnow;
  writeVideo(vidObj,getframe(gcf,rect));
end
close(vidObj);  

case 16

% effort in case of fixed xi=17
% h=figure('Position',[1 1 1000 1000]) ;
h=figure
out = dbread(db,'results',74);
vidObj = VideoWriter('effort_74','Uncompressed AVI');
open(vidObj);
movegui(h, 'onscreen');
rect = get(h,'Position'); 
rect(1:2) = [0 0]; 
for i=1:length(out.xgrid)
  h = plot(out.w,out.effort_all(:,i));
  axis([40,80,0,10]);
  xlabel('wage');
  str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
  text(50,8,str1);
  title('Effort schedule with xi fixed to xi=17');
  movegui(h, 'onscreen');
  % hold all;
  % datetick;
  % drawnow;
  writeVideo(vidObj,getframe(gcf,rect));
end
close(vidObj);  

case 17

% effort in case of fixed xi=6
% h=figure('Position',[1 1 1000 1000]) ;
h=figure
out = dbread(db,'results',75);
vidObj = VideoWriter('effort_75','Uncompressed AVI');
open(vidObj);
movegui(h, 'onscreen');
rect = get(h,'Position'); 
rect(1:2) = [0 0]; 
for i=1:length(out.xgrid)
  h = plot(out.w,out.effort_all(:,i));
  axis([40,80,0,10]);
  xlabel('wage');
  str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
  text(50,8,str1);
  title('Effort schedule with xi fixed to xi=6');
  movegui(h, 'onscreen');
  % hold all;
  % datetick;
  % drawnow;
  writeVideo(vidObj,getframe(gcf,rect));
end
close(vidObj);  


case 18

% effort in case of fixed xi=17 - combined with ftheta
% h=figure('Position',[1 1 1000 1000]) ;
h=figure
out = dbread(db,'results',74);
vidObj = VideoWriter('effort_ftheta_74','Uncompressed AVI');
open(vidObj);
movegui(h, 'onscreen');
rect = get(h,'Position'); 
rect(1:2) = [0 0]; 
for i=1:length(out.xgrid)
  subplot(2,1,1)  
  plot(out.w,out.effort_all(:,i));
  axis([40,80,1,4]);
  xlabel('wage');
  str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
  text(70,3,str1);
  title('Effort schedule with xi fixed to xi=17')
  hold off
  subplot(2,1,2)
  plot(out.w,out.f_theta_all(:,i));
  axis([40,80,0,0.000002]);

  title('f_{theta}');
  xlabel('wage');
  hold off;
  % movegui(h, 'onscreen');
  % hold all;
  % datetick;
  % drawnow;
  writeVideo(vidObj,getframe(gcf,rect));
end
close(vidObj);  

case 19

% effort in case of fixed xi=17 - combined with ftheta, skilldist
% normalized as in R&S
% h=figure('Position',[1 1 1000 1000]) ;
h=figure
out = dbread(db,'results',78);
vidObj = VideoWriter('effort_ftheta_78','Uncompressed AVI');
open(vidObj);
movegui(h, 'onscreen');
rect = get(h,'Position'); 
rect(1:2) = [0 0]; 
for i=1:length(out.xgrid)
  subplot(2,1,1)  
  plot(out.w,out.effort_all(:,i));
  axis([40,80,1,4]);
  xlabel('wage');
  str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
  text(70,3,str1);
  title('Effort schedule with xi fixed to xi=17')
  hold off
  subplot(2,1,2)
  plot(out.w,out.f_theta_all(:,i));
  axis([40,80,0,0.000002]);

  title('f_{theta}');
  xlabel('wage');
  hold off;
  % movegui(h, 'onscreen');
  % hold all;
  % datetick;
  % drawnow;
  writeVideo(vidObj,getframe(gcf,rect));
end
close(vidObj);  

case 20
% income against wage (to check monotonicity condition)
h=figure
out = dbread(db,'results',79);
vidObj = VideoWriter('income_79','Uncompressed AVI');
open(vidObj);
movegui(h, 'onscreen');
rect = get(h,'Position'); 
rect(1:2) = [0 0]; 
for i=1:length(out.xgrid)
  h = plot(out.w,out.income_all(:,i));
  % axis([0,max(out.w),min(out.income_all(:))-10,max(out.income_all(:))]);
  axis([0,200,min(out.income_all(:))-10,0.5*10^3]);
  xlabel('wage');
  str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
  text(100,5,str1);
  title('Income schedule');
  movegui(h, 'onscreen');
  % hold all;
  % datetick;
  % drawnow;
  writeVideo(vidObj,getframe(gcf,rect));
end
close(vidObj);  

case 21

% effort in case of free xi - combined with ftheta, skilldist
% normalized as in R&S
% h=figure('Position',[1 1 1000 1000]) ;
h=figure
out = dbread(db,'results',79);
vidObj = VideoWriter('effort_ftheta_79','Uncompressed AVI');
open(vidObj);
movegui(h, 'onscreen');
rect = get(h,'Position'); 
rect(1:2) = [0 0]; 
for i=1:length(out.xgrid)
  subplot(2,1,1)  
  plot(out.w,out.effort_all(:,i));
  axis([40,80,1,4]);
  xlabel('wage');
  str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
  text(70,3,str1);
  title('Effort schedule with xi free')
  hold off
  subplot(2,1,2)
  plot(out.w,out.f_theta_all(:,i));
  axis([40,80,0,0.000002]);

  title('f_{theta}');
  xlabel('wage');
  hold off;
  % movegui(h, 'onscreen');
  % hold all;
  % datetick;
  % drawnow;
  writeVideo(vidObj,getframe(gcf,rect));
end
close(vidObj);  


case 22

% wage distributions - experimenting with SBTC
% h=figure('Position',[1 1 1000 1000]) ;
h=figure
out = dbread(db,'results',97);
vidObj = VideoWriter('wagedist_97','Uncompressed AVI');
open(vidObj);
movegui(h, 'onscreen');
rect = get(h,'Position'); 
rect(1:2) = [0 0]; 
for i=1:length(out.xgrid)
  plot(out.w,out.f_theta_all(:,i),out.w,out.f_phi_all(:,i),out.w,out.f_all(:,i));
  legend('f_{theta}','f_{phi}','f')
  axis([0,max(out.w),0,0.012]);
  xlabel('wage');
  str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
  text(200,0.01,str1);
  title('Wage densities')
  % movegui(h, 'onscreen');
  % hold all;
  % datetick;
  % drawnow;
  writeVideo(vidObj,getframe(gcf,rect));
end
close(vidObj);  

case 23

% wage distributions - experimenting with SBTC - increased spread
% between atheta and aphi
% h=figure('Position',[1 1 1000 1000]) ;
h=figure
out = dbread(db,'results',99);
vidObj = VideoWriter('wagedist_99','Uncompressed AVI');
open(vidObj);
movegui(h, 'onscreen');
rect = get(h,'Position'); 
rect(1:2) = [0 0]; 
for i=1:length(out.xgrid)
  plot(out.w,out.f_theta_all(:,i),out.w,out.f_phi_all(:,i),out.w,out.f_all(:,i));
  legend('f_{theta}','f_{phi}','f')
  axis([0,max(out.w),0,0.012]);
  xlabel('wage');
  str1(1) = {horzcat('x = ',num2str(out.xgrid(i)))};
  text(200,0.01,str1);
  title('Wage densities')
  % movegui(h, 'onscreen');
  % hold all;
  % datetick;
  % drawnow;
  writeVideo(vidObj,getframe(gcf,rect));
end
close(vidObj);  


end %of switch block
 

% play as smooth movie 3 times at 10 frames per second

% note that it goes through the frames 2x initially to get ready for

% full speed play.  So it will actually play 2x slower and 3x at full speed.


% movie(M2,numtimes,fps)

% save as avi
% movie2avi(M3, 'effort_all.avi');
% movie2avi(M2, 'welfare_vec.avi');
% imwrite(im,map, 'welfare_vec.gif');
% movie2avi(M4, 'mymtr_all.avi');
% movie2avi(M6, 'fthetax_over_fx.avi');
