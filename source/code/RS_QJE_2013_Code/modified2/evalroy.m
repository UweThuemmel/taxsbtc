% Read from database and plot results


%% Connect to database
db = sqlite3.open('test.sqlite');

%% read from database
out9 = dbread(db,'results',9);
out10 = dbread(db,'results',10);
out14 = dbread(db,'results',14);
out15 = dbread(db,'results',15); 
out16 = dbread(db,'results',16);
out20 = dbread(db,'results',20);
out27 = dbread(db,'results',27);
out28 = dbread(db,'results',28);
out30 = dbread(db,'results',30);
out37 = dbread(db,'results',37);
out38 = dbread(db,'results',38);
out39 = dbread(db,'results',39);
out40 = dbread(db,'results',40);
out41 = dbread(db,'results',41);
out44 = dbread(db,'results',44);
out45 = dbread(db,'results',45);
out46 = dbread(db,'results',46);
out47 = dbread(db,'results',47);
out48 = dbread(db,'results',48);
out49 = dbread(db,'results',49);
out50 = dbread(db,'results',50);
out51 = dbread(db,'results',51);
out53 = dbread(db,'results',53);
out55 = dbread(db,'results',55);
out57 = dbread(db,'results',57);
out68 = dbread(db,'results',68);
out69 = dbread(db,'results',69);
out70 = dbread(db,'results',70);
out73 = dbread(db,'results',73);
out79 = dbread(db,'results',79);
out80 = dbread(db,'results',80);
out81 = dbread(db,'results',81);
out83 = dbread(db,'results',83);
out90 = dbread(db,'results',90);
out107 = dbread(db,'results',107);
out108 = dbread(db,'results',108);
out109 = dbread(db,'results',109);
out112 = dbread(db,'results',112);
out113 = dbread(db,'results',113);
out114 = dbread(db,'results',114);
out117 = dbread(db,'results',117);
out120 = dbread(db,'results',120);
out122 = dbread(db,'results',122);
out123 = dbread(db,'results',123);
out124 = dbread(db,'results',124);
out125 = dbread(db,'results',125);
out127 = dbread(db,'results',127);
out130 = dbread(db,'results',130);
out133 = dbread(db,'results',133);
out135 = dbread(db,'results',135);
%% Plots

path = '/media/data/Dropbox/PhD/projects/taxsbtc/source/lyx/figures/';
prefix = 'row_130_133_135_new';

genplots({out130,out133,out135},'a_theta','A_{\theta}',path,prefix);