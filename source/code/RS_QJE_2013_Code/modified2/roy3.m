function [optimal_struct parameters] = roy3(grid_t, grid_p, pdf_t, pdf_p, mu_t,sigma_t,mu_p,sigma_p,  rho, version, ...
              elast, alpha, r,xmin,xmax,dx,a_phi,a_theta,sbtc,rs_skilldist,comment,normalize_skill,tau)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Roy Model Optimal Tax Simulation
%
% EXECUTION FILE
% roy3.m
%
% Casey Rothschild & Florian Scheuer, June 2012
% Modified by Uwe Thuemmel, December 2013
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% global x  num_w w dw ff gg ;
% yy_theta yy_phi  ggamma  gam f f_phi f_theta x


% Generate lists and variables related to skill distribution
[theta, phi, pmf, pmf_theta, pmf_phi, cdf_theta, cdf_phi, bin_width_theta, ...
 bin_width_phi, theta_min, theta_max, phi_min, phi_max, ...
 num_theta, num_phi] = genskilldist(grid_t, grid_p, pdf_t, pdf_p,rs_skilldist);


%% SPECIFY GRID FOR x
xgrid = (xmin:dx:xmax);

%% COMPUTE WELFARE FOR EACH x

% Welfare loop

for j=1:size(xgrid,2)
    x=xgrid(j);
    % welfaregrid(j)=results(x,pmf_theta,pmf_phi,rho,elast,alpha, ...
    %                        theta_min,theta_max,phi_min,phi_max,num_theta,phi,theta,r,bin_width_theta,bin_width_phi,pmf,version);

results_array{j}=results(x,pmf_theta,pmf_phi,rho,elast,alpha,theta_min,theta_max,phi_min,phi_max,num_theta,phi,theta,r,bin_width_theta,bin_width_phi,pmf,a_phi,a_theta,version,sbtc,normalize_skill,grid_t,grid_p,pdf_t,pdf_p,rs_skilldist,tau);
end

% make a list of all structs
results_all_struct = [results_array{:}];
% make a list of all welfare points
welfaregrid = [results_all_struct.welfare];
% look up the index of the maximum welfare
[temp,index] = max(welfaregrid);
% assign the results corresponding to the welfare maximum
optimal_struct = results_array{index};

% assign xi grid
xigrid = [results_all_struct.xi];

% assign matrices with wage distributions
f_all = [results_all_struct.f];
f_theta_all = [results_all_struct.f_theta];
f_phi_all = [results_all_struct.f_phi];

% assign matrices with welfare vectors
welfare_vec = [results_all_struct.welfare_vec];
optimal_struct.welfare_vec = welfare_vec;

% effort vector
effort_all = [results_all_struct.effort];
optimal_struct.effort_all = effort_all;

% mtr vector
mtr_all = [results_all_struct.mtr];
optimal_struct.mtr_all = mtr_all;


% untruncated mymtr vector
mymtr_all = [results_all_struct.mymtr];
optimal_struct.mymtr_all = mymtr_all;

% income vector
income_all = [results_all_struct.income];
optimal_struct.income_all = income_all;


% extreme wages
w_min_theta_all = [results_all_struct.w_min_theta];
optimal_struct.w_min_theta_all = w_min_theta_all;

w_max_theta_all = [results_all_struct.w_max_theta];
optimal_struct.w_max_theta_all = w_max_theta_all;

w_min_phi_all = [results_all_struct.w_min_phi];
optimal_struct.w_min_phi_all = w_min_phi_all;

w_max_phi_all = [results_all_struct.w_max_phi];
optimal_struct.w_max_phi_all = w_max_phi_all;


% marginal products
yy_phi_all = [results_all_struct.yy_phi];
optimal_struct.yy_phi_all = yy_phi_all;

yy_theta_all = [results_all_struct.yy_theta];
optimal_struct.yy_theta_all = yy_theta_all;

% monotonicity indicators
effort_nonmon_all = [results_all_struct.effort_nonmon];
optimal_struct.effort_nonmon = effort_nonmon_all;

income_nonmon_all = [results_all_struct.income_nonmon];
optimal_struct.income_nonmon = income_nonmon_all;



% append welfaregrid and xgrid to optimal_struct, such that all
% results are contained in this struct
optimal_struct.welfaregrid = welfaregrid;
optimal_struct.xgrid = xgrid;
optimal_struct.xigrid = xigrid;

% assign wage distributions
optimal_struct.f_all = f_all;
optimal_struct.f_theta_all = f_theta_all;
optimal_struct.f_phi_all = f_phi_all;



% struct with parameters
parameters = struct('comment',comment,'rho',rho,'alpha',alpha,'version',version, ...
                    'elast', elast, 'r',r,'xmin',xmin,'xmax',xmax, ...
                    'dx',dx,'a_phi',a_phi,'a_theta',a_theta,'sbtc',sbtc, 'grid_t',grid_t,'grid_p', grid_p, 'pdf_t',pdf_t, 'pdf_p',pdf_p,'rs_skilldist',rs_skilldist,'mu_t',mu_t,'sigma_t',sigma_t,'mu_p',mu_p,'sigma_p',sigma_p,'normalize_skill',normalize_skill);


%% PLOT THINGS
plot(xgrid,welfaregrid)

end