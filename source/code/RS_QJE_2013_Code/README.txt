#+TITLE: README for code from 
#+LaTeX_HEADER: \usepackage{natbib}
#+FILETAGS: :thesis:
#+TAGS: question(
* Descriptions
The code corresponds to the paper:

Redistributive Taxation in the Roy Model, \cite{Rothschild2013}

I obtained it from Casey Rothschild, crothsch@wellesley.edu, on 05.12.2013. The folder /original/ contains the original files. The folder /modified/ contains potentially modified files.
* References
\bibliographystyle{plainnat}
\bibliography{MyLibrary}
