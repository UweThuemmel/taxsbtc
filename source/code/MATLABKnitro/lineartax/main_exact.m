% main_exact
% Solves model with linear tax rate and education subsidy - using
% exact formulas
% Uwe Thuemmel, Summer 2014, u.thuemmel@tinbergen.nl

%% General setup 

% include path for functions
addpath(['/media/data/Dropbox/PhD/projects/taxsbtc/source/code/' ...
         'MATLABKnitro/lineartax/include']);

% include path for plotly
addpath(['~/Downloads/MATLAB-api-master/']);

%% Set index for saving in results
i = 1; 


%% Set parameters

p.mu_theta = 0;
p.sigma_theta = 0.4711;
p.epsilon = 0.3;
p.sigma = 1.41;
p.gamma = 0.4774;
p.p = 1/0.684 * 0.26;
p.lb = 0.0001;%.00000001;
p.ub = 10;
p.AL = 1;
p.eta = 1;
p.plots = ['/media/data/Dropbox/PhD/projects/taxsbtc/source/code/' ...
           'MATLABKnitro/lineartax/plots/'];
p.csv = '/media/data/Dropbox/PhD/projects/taxsbtc/source/code/MATLABKnitro/lineartax/csv/';
p.r = 0.1;
AH = 3;
p.weight = 'Exponential';

%% Parameter for welfare weights
% p.r = 0 Utilitarian, p.r -> Inf Rawlsian



% also define a grid for r and sigma

% rgrid = [0.1,0.5,1,1.5];
rgrid = [0.0001];

% sigmagrid = [1.1,1.41,2,5,10000];
sigmagrid = [1.41];

% Grid of skill-bias values
AHgrid = 3:0.1:6;

% empty struct for saving results
results = {};

%% Optimization

    
% b = sol(1);
% s = sol(2);
% t = sol(3);
% L = sol(4);
% H = sol(5);

lb.b = 0;
lb.L = 0;
lb.H = 0;

ub.b = Inf;
ub.L = Inf;
ub.H = Inf;

in.b = 0.2;
in.L = 0.2;
in.H = 0.2;


lb.s = -Inf;
lb.t = 0;

ub.s = Inf;
ub.t = Inf;

in.s = 0.2;
in.t = 0.5;

    
%% Constraints and bounds for optimization
% Specify vectors for linear constraints as well as upper and lower bounds
A = []; b = [];
Aeq = [];
beq = [];
lb = [lb.b;lb.s;lb.t;lb.L;lb.H];
ub = [ub.b;ub.s;ub.t;ub.L;ub.H];

% initial values
init = [in.b;in.s;in.t;in.L;in.H];

%% Optimization problem

% calculate normalization for weighting function
fn = @(theta) theta.^(-p.r);
p.mass = quad(fn,p.lb,p.ub);

% Function handle for objective (pseudo objective))
fun = @(vars) -1*(foc_tax(vars(2),vars(3),AH,vars(4),vars(5),p)^2 + foc_subsidy(vars(2),vars(3),AH,vars(4),vars(5),p)^2);

% Function handle for constraint
nonlcon = @(vars) NonLinearConstrExact(AH,vars(1),vars(2),vars(3),vars(4),vars(5),p);

sol=knitromatlab(fun,init,A,b,Aeq,beq,lb,ub,nonlcon);

% Assign solutions
b = sol(1);
s = sol(2);
t = sol(3);
L = sol(4);
H = sol(5);


% Evaluate foc's at the optimal point
foc_tax(s,t,AH,L,H,p)
foc_subsidy(s,t,AH,L,H,p)

