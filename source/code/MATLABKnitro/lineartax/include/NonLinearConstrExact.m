function [IneqValues,EqValues] = NonLinearConstrExact(AH,b,s,t,L,H,p)
% Evaluates non-linear inequality and equality constraints at var
% and returns their values - here using exact formulas for tax and subsidy
% 
% [IneqValues,EqValues] = NonLinearConstrExact(AH,b,s,t,L,H,p)
% 
% INPUTS
% 
% OUTPUTS
% IneqValues: vector of values of inequality constraints 
% EqValues: vector of values of equality constraints 
% 

% force wH > wL
wageconstr =  wL(AH,L,H,p) - wH(AH,L,H,p);


% IneqValues = [constr_thetastar_lb(s,t,AH,L,H,p); ...
%               constr_thetastar_ub(s,t,AH,L,H,p);wageconstr];
IneqValues = [constr_thetastar_lb(s,t,AH,L,H,p);constr_thetastar_ub(s,t,AH,L,H,p);wageconstr];
EqValues = [excessL(AH,L,H,s,t,p);excessH(AH,L,H,s,t,p)];
% EqValues = [excessL(AH,L,H,s,t,p);excessH(AH,L,H,s,t,p);gbc(AH,L,H,b,s,t,p);foc_tax(s,t,AH,L,H,p);foc_subsidy(s,t,AH,L,H,p)];
