function out = alpha(AH,L,H,p)
% Income share of college graduates
% 
% out = alpha(AH,L,H,p)
% 
% INPUTS
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% p: parameters
% 
out = zbarH(AH,L,H,p) / zbar(AH,L,H,p);