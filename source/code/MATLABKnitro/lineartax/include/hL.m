function out = hL(theta,AH,L,H,t,p)
% labor supply of the low-skilled
% 
% OUT = hL(theta,AH,L,H,t,p)
% 
% theta: skill value
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% t: linear tax rate
% p: parameters
% 
% SEE ALSO WL

out = (theta.*wL(AH,L,H,p).*(1 - t)).^p.epsilon;
end
