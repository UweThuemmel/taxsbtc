function out=comp_earnings(theta,s,t,AH,L,H,p)
% compute earnings for a vector of theta's
% 
% out=comp_earnings(theta,s,t,AH,L,H,p)
% 
% theta: vector of thetas
% s: subsidy
% t: tax rate
% AH: productivity of the high-skilled - via production function
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% p: struct with parameters
% 

% pre-compute thetastar
thst = thetastar(s,t,AH,L,H,p);

% make out vector that same as theta vector
out = theta;


% earnings of low-skilled
out(out<thst)=hL(out(out<thst),AH,L,H,t,p).*wL(AH,L,H,p).*out(out< ...
                                                  thst);
% earnings of high-skilled
out(out>=thst)=hH(out(out>=thst),AH,L,H,t,p).*wH(AH,L,H,p).*out(out>=thst);