function out = tax_terms(s,t,AH,L,H,p)
% Calculates parts of the optimal tax formula
% 
% out = foc_tax(s,t,AH,L,H,p)
% 
% INPUTS
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% s: subsidy
% t: linear tax rate
% p: paramters


% precompute variables, for speed and readability
v.alpha = alpha(AH,L,H,p);
v.thetastar = thetastar(s,t,AH,L,H,p);
v.delta = delta(AH,L,H,s,t,p);
v.beta = beta(AH,L,H,p);
v.zbar = zbar(AH,L,H,p);
v.ddelta = ddelta(AH,L,H,s,t,p);
v.gL = gL(AH,L,H,s,t,p);
v.gH = gH(AH,L,H,s,t,p);

out.a_term = t / (1-t)  * p.epsilon;


out.b_term =  v.ddelta/(1-t) *  f(v.thetastar,p) * v.thetastar / v.zbar * (p.sigma +   p.epsilon)/(p.sigma ...
                                                  + p.epsilon + ...
                                                  v.delta*(v.beta-v.alpha));

out.c_term = (1-v.alpha)*v.alpha*v.delta / (p.sigma ...
                                                  + p.epsilon + ...
                                                  v.delta*(v.beta- ...
                                                  v.alpha))*(v.gL-v.gH);

