function [IneqValues,EqValues] = NonLinearConstrCalibrate(AH,mu_theta,sigma_theta,gamma,price,s,L,H,b,t,p,targets)
% Non-linear constraints for calibration
% Evaluates non-linear inequality and equality constraints at var
% and returns their values
% 
% [IneqValues,EqValues] = NONLINEARCONSTR(x,xi,w,w_tilde,w_hat,p)
% 
% INPUTS
% 
% OUTPUTS
% IneqValues: vector of values of inequality constraints 
% EqValues: vector of values of equality constraints 
% 

p.mu_theta = mu_theta;
p.sigma_theta = sigma_theta;
p.gamma = gamma;
p.p = price;

v = othervars(AH,b,s,t,L,H,p);


tar={};
tar.enrollment_elasticity =v.target_eps_s_thetastar- targets.enrollment_elasticity;
tar.subsidy_share = s/p.p - targets.subsidy_share;
tar.empH = v.empH - targets.empH;
tar.eduprice_wL = p.p/v.zbarL - targets.eduprice_wL;
tar.earnings_premium = v.zbarH/v.zbarL - ...
                      targets.earnings_premium;

% % compute normalization
% p.norm = normalization(w,w,w,x,p);

% force wH > wL
wageconstr = @(AH,L,H,p) wL(AH,L,H,p) - wH(AH,L,H,p);


IneqValues = [constr_thetastar_lb(s,t,AH,L,H,p);constr_thetastar_ub(s,t,AH,L,H,p);wageconstr(AH,L,H,p)];
EqValues = [excessL(AH,L,H,s,t,p);excessH(AH,L,H,s,t,p);tar.enrollment_elasticity];
% [excessL(AH,L,H,s,t,p);excessH(AH,L,H,s,t,p);tar.subsidy_share;tar.empH;tar.enrollment_elasticity;tar.earnings_premium];
