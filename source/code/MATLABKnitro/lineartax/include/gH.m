function out = gH(AH,L,H,s,t,p)
% Average social welfare weight for the high-skilled
% 
% out = gH(AH,L,H,s,t,p)
% 
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% s: subsidy
% t: tax rate
% p: struct with fields
% 
% 

funH = @(theta) weight(theta,p).*zH(theta,AH,L,H,t,p).*f(theta,p);
out = 1./p.eta .* 1./zbarH(AH,L,H,p) .* quad(funH,thetastar(s,t,AH,L,H,p),p.ub);