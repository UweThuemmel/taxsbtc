function eval_elast(perc,s,t,AH,L,H,p)
% Numerically compute elasticities and compare to analytical
% expressions
% 
% eval_elast(s,t,AH,L,H,p)
% 
% perc: relative change of t in percent
% s: subsidy
% t: tax rate to start from
% AH: skill bias
% p: parameters


%% values before

% Clear labor market
[L,H] = clearlabor(AH,s,t,[L,H],p);
fprintf('Initial L: %f \n',L)
fprintf('Initial H: %f \n',H)
fprintf('Initial wL: %f \n',wL(AH,L,H,p))
fprintf('Initial wH: %f \n',wH(AH,L,H,p))


v.alpha = alpha(AH,L,H,p);
v.thetastar1 = thetastar(s,t,AH,L,H,p);
v.delta = delta(AH,L,H,s,t,p);
v.beta = beta(AH,L,H,p);

% eps_thetastar
v.eps_thetastar_t =  (p.sigma +   p.epsilon)./(p.sigma + p.epsilon ...
                                               + v.delta.*(v.beta- ...
                                                  v.alpha));
% eps_wL_t
v.eps_wL_t = (v.alpha * v.delta)/(p.sigma + p.epsilon ...
                                               + v.delta.*(v.beta- ...
                                                  v.alpha));

% eps_wH_t
v.eps_wH_t = -((1-v.alpha) * v.delta)/(p.sigma + p.epsilon ...
                                               + v.delta.*(v.beta- ...
                                                  v.alpha));

% eps_l_t
v.eps_l_t =p.epsilon* (p.sigma +p.epsilon + v.delta*v.beta)/(p.sigma + p.epsilon ...
                                               + v.delta.*(v.beta- ...
                                                  v.alpha));

% eps_h_t
v.eps_h_t = p.epsilon*(p.sigma +p.epsilon + v.delta*(v.beta-1))/(p.sigma + p.epsilon ...
                                               + v.delta.*(v.beta- ...
                                                  v.alpha));

v.wL1 = wL(AH,L,H,p);
v.wH1 = wH(AH,L,H,p);

% need to compute a theta value for numerical evaluation of l and h
v.thetaL = p.lb + (v.thetastar1 - p.lb)/2;
v.thetaH = v.thetastar1 + (p.ub - v.thetastar1)/2;
v.l1 = hL(v.thetaL,AH,L,H,t,p);
v.h1 = hH(v.thetaH,AH,L,H,t,p);

v

%% values after
% one percent increase in tax rate
x = (perc/100);
t2 = t*(1+x)-x;
v.t=t;
v.t2 = t2;

% Clear labor market
[L,H] = clearlabor(AH,s,t2,[L,H],p);

fprintf('2nd L: %f \n',L)
fprintf('2nd H: %f \n',H)

% Compute new thetastar
v.thetastar2 = thetastar(s,t2,AH,L,H,p);

% Compute new wages
v.wL2 = wL(AH,L,H,p);
v.wH2 = wH(AH,L,H,p);

% Compute new labor supplies
v.l2 = hL(v.thetaL,AH,L,H,t2,p);
v.h2 = hH(v.thetaH,AH,L,H,t2,p);

% numerical elasticity
v.eps_thetastar_t_num = - ((v.thetastar2 - v.thetastar1)/v.thetastar1) / x;
v.eps_wL_t_num = ((v.wL2 - v.wL1) / v.wL1) / x;
v.eps_wH_t_num = ((v.wH2 - v.wH1) / v.wH1) / x;
v.eps_l_t_num = ((v.l2 - v.l1) / v.l1) / x;
v.eps_h_t_num = ((v.h2 - v.h1) / v.h1) / x;


v

t2/t
%% Report

fprintf('Analytical elasticity: eps_thetastar_t: %f \n',v.eps_thetastar_t)
fprintf('Numerical elasticity: eps_thetastar_t: %f \n',v.eps_thetastar_t_num)

fprintf('Analytical elasticity: eps_wL_t: %f \n',v.eps_wL_t)
fprintf('Numerical elasticity: eps_wL_t: %f \n',v.eps_wL_t_num)

fprintf('Analytical elasticity: eps_wH_t: %f \n',v.eps_wH_t)
fprintf('Numerical elasticity: eps_wH_t: %f \n',v.eps_wH_t_num)

fprintf('Analytical elasticity: eps_l_t: %f \n',v.eps_l_t)
fprintf('Numerical elasticity: eps_l_t: %f \n',v.eps_l_t_num)

fprintf('Analytical elasticity: eps_h_t: %f \n',v.eps_h_t)
fprintf('Numerical elasticity: eps_h_t: %f \n',v.eps_h_t_num)



end
