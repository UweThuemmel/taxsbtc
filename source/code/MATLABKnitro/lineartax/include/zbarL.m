function out = zbarL(AH,L,H,p)
% Average income in the high-school sector
% 
% out = zbarL(AH,L,H,p)
% 
% INPUTS
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% p: parameters

out = wL(AH,L,H,p) * L;