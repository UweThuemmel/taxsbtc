function [s,t,L,H]=solveoptimum(AH,init,p)
% Solve system of labor market clearing and foc's for tax and
% subsidy

% define objective function (should be a vector of 0's at the
% optimum)

fun = @(x) [excessL(AH,x(3),x(4),x(1),x(2),p);excessH(AH,x(3),x(4),x(1),x(2), ...
                                                p);foc_tax(x(1), ...
                                                  x(2),AH,x(3), ...
                                                  x(4),p);foc_subsidy(x(1),x(2),AH,x(3),x(4),p)];



x0 = init;


out=fsolve(fun,x0);

s = out(1);
t = out(2);
L = out(3);
H = out(4);