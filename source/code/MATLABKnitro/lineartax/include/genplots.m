function genplots(results,p)
% Function to generate plots
% 
% genplots(results)
% 
% INPUT
% results: array with results
% 
% OUTPUT
% prints plots to screen


% shorthand function for conversion of struct to mat
function out=r(fieldname)
out = cell2mat(getfield(results,fieldname));
end


figure(1)
subplot(2,2,1)
plot(cell2mat(results.AH),cell2mat(results.t));
title('Optimal linear tax rate and SBTC');
ylabel('tax rate')
xlabel('A'); 

subplot(2,2,2)
plot(r('AH'),r('s'));
title('Optimal subsidy and SBTC');
ylabel('subsidy')
xlabel('A'); 

subplot(2,2,3)
plot(r('AH'),r('b'));
title('Optimal transfer and SBTC');
ylabel('transfer')
xlabel('A'); 


subplot(2,2,4)
plot(cell2mat(results.AH),cell2mat(results.thetastar));
title('Ability threshold and SBTC');
ylabel('Ability threshold')
xlabel('A'); 

figure(2)

subplot(1,2,1)
plot(cell2mat(results.AH),cell2mat(results.wL),cell2mat(results.AH),cell2mat(results.wH));
title('Wage rates and SBTC');
ylabel('Wage rate')
xlabel('A'); 
legend('wL','wH');

subplot(1,2,2)
plot(cell2mat(results.AH),cell2mat(results.L),cell2mat(results.AH),cell2mat(results.H));
title('Sectoral labor supplies and SBTC');
ylabel('Sectoral labor supply')
xlabel('A'); 
legend('L','H');

% Plot skill distribution
figure(3)

range_theta = p.lb:0.01:p.ub;
plot(range_theta,f(range_theta,p));
title('Skill density');
xlabel('Skill level \\theta');
ylabel('Density f(\\theta)');

% Plot other variables

figure(4)
subplot(2,2,1)
plot(r('AH'),r('zbarL'),r('AH'),r('zbarH'),r('AH'),r('zbar'));
title('Incomes and SBTC');
ylabel('Income')
xlabel('A'); 
legend('zbarL','zbarH','zbar');

subplot(2,2,1)
plot(r('AH'),r('zbarL'),r('AH'),r('zbarH'),r('AH'),r('zbar'));
title('Incomes and SBTC');
ylabel('Income')
xlabel('A'); 
legend('zbarL','zbarH','zbar');

subplot(2,2,2)
plot(r('AH'),r('empL'),r('AH'),r('empH'));
title('Sectoral employment and SBTC');
ylabel('Employment')
xlabel('A'); 
legend('empl. L','empl. H');

subplot(2,2,3)
plot(r('AH'),r('cwp'),r('AH'),r('cwp_emp'));
title('College-wage-premium and SBTC');
ylabel('CWP')
xlabel('A'); 
legend('as in Acemoglu & Autor (2012)','as it would be measured empirically');

subplot(2,2,4)
plot(r('AH'),r('cwp'),r('AH'),r('cwp_emp'));
title('College-wage-premium and SBTC');
ylabel('CWP')
xlabel('A'); 
legend('as in Acemoglu & Autor (2012)','as it would be measured empirically');

figure(5)
subplot(2,2,1)
plot(r('AH'),r('xi'));
title('Distributional characteristic and SBTC');
ylabel('Distributional characteristic \xi')
xlabel('A'); 

subplot(2,2,2)
plot(r('AH'),r('alpha'));
title('Income share of the high-skilled and SBTC');
ylabel('Income share \alpha')
xlabel('A'); 

subplot(2,2,3)
plot(r('AH'),r('wedge'));
title('Wedge \Delta and SBTC');
ylabel('Wedge \Delta')
xlabel('A'); 

subplot(2,2,4)
plot(r('AH'),r('delta'));
title('Measure \delta and SBTC');
ylabel('\delta')
xlabel('A'); 


% save figures

for i = 1:5
    print(figure(i),horzcat(p.plots,'fig',int2str(i)),'-dpdf')
end

end