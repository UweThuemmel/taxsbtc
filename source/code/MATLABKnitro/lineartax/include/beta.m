function out = beta(AH,L,H,p)
% beta
% 
% out = beta(AH,L,H,p)
% 
% % AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% p: parameters


out = (wH(AH,L,H,p)).^(1+p.epsilon)./((wH(AH,L,H,p)).^(1+p.epsilon) - (wL(AH,L,H,p)).^(1+p.epsilon));