function out = f(theta,p)
% skill density
% 
% OUT = f(theta,p)
% 
% theta: skill value
% p: struct with fields
% - mu_theta: mu of skill distribution
% - sigma_theta: sigma of skill distribution

% log-normal skill distribution
if p.uniform ==1
out = 1; %uniform distribution for ability
else
out = lognpdf(theta,p.mu_theta,p.sigma_theta);
end;
end
