function out = swf(AH,b,s,t,L,H,p)
% Evaluates objective function (social welfare function)
% 
% OUT = swf(AH,b,s,t,L,H,p)
% 
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% b: lump-sum transfer (benefit)
% t: linear tax rate
% s: subsidy
% p: struct with parameters
% 
% SEE ALSO WEIGHT, VL, VH, F


% objective function for a low-skilled of type theta
objL = @(theta) weight(theta,p).*vL(theta,AH,L,H,b,t,p).*f(theta, ...
                                                  p);

% objective function for a high-skilled of type theta
objH = @(theta) weight(theta,p).*vH(theta,AH,L,H,b,t,s,p).*f(theta,p);

out = quad(objL,p.lb,thetastar(s,t,AH,L,H,p)) + quad(objH,thetastar(s,t,AH,L,H,p),p.ub); 
end

