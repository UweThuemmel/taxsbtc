function out = zbarH(AH,L,H,p)
% Average income in the college sector
% 
% out = zbarH(AH,L,H,p)
% 
% INPUTS
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% p: parameters

out = wH(AH,L,H,p) * H;