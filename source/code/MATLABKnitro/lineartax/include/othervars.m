function out = othervars(AH,b,s,t,L,H,p)
% Compute other variables based on the solution
% 
% OUT = othervars(AH,b,s,t,L,H,p)
% 
% INPUTS
% AH: productivity of the high-skilled
% b: lump-sum subsidy (benefit)
% s: subsidy
% t: linear tax rate
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% p: struct with parameters
% 
% OUTPUT
% out: struct with other variables
% - wL: wage of low-skilled
% - wH: wage of high-skilled
% - thetastar: ability threshold


% Critical theta
out.thetastar = thetastar(s,t,AH,L,H,p);

% Wage rates
out.wL = wL(AH,L,H,p);
out.wH = wH(AH,L,H,p);

% Sectoral income
out.zbarL = out.wL * L;
out.zbarH = out.wH * H;

% Output
out.zbar = out.zbarL + out.zbarH;
 
% Sectoral employment
out.empL = ff(out.thetastar,p) - ff(p.lb,p);
out.empH = ff(p.ub,p) - ff(out.thetastar,p);


%% Distributional characteristic xi

% define individual income zL and zH as a function of theta
zL = @(theta) hL(theta,AH,L,H,t,p) .* theta .* out.wL;
zH = @(theta) hH(theta,AH,L,H,t,p) .* theta .* out.wH;

% define integrand for low-skilled
integL = @(theta) weight(theta,p) ./ p.eta .* zL(theta) .* f(theta,p);
integH = @(theta) weight(theta,p) ./ p.eta .* zH(theta) .* f(theta,p);

% calculate xi

out.xi_L = quad(integL,p.lb,out.thetastar);
out.xi_H = quad(integH,out.thetastar,p.ub);
out.xi_L_zbar = out.xi_L/out.zbar;
out.xi_H_zbar = out.xi_H/out.zbar;

out.xi = 1 - (out.xi_L_zbar + out.xi_H_zbar);

%% Income shares

% Note: alpha here is the income share of the college sector
out.alpha = out.zbarH / out.zbar;

%% Wedge

out.wedge = f(out.thetastar,p) * (zH(out.thetastar) - zL(out.thetastar))* t - s;

%% delta

% note that we specify delta in terms of incomes rather than labor
% supplies (by multiplying by wL / wL , wH / wH)

out.delta = (zH(out.thetastar)* f(out.thetastar,p)/out.zbarH + zL(out.thetastar)* f(out.thetastar,p)/out.zbarL);

%% Weights qL, qH 

% note: again we specify this in terms of incomes

funL = @(theta) weight(theta,p).*zL(theta).*f(theta, ...
                                                  p);

funH = @(theta) weight(theta,p).*zH(theta).*f(theta, ...
                                                  p);

out.qL = 1./p.eta .* 1./out.zbarL .* quad(funL,p.lb,out.thetastar);

out.qH = 1./p.eta .* 1./out.zbarH .* quad(funH,out.thetastar,p.ub);

%% alpha * (1-alpha) / sigma

out.alphaterm = out.alpha .* (1-out.alpha) ./ p.sigma;

%% qL - qH

out.qLminqH = out.qL - out.qH;

% beta
out.beta = (out.wH).^(1+p.epsilon)./((out.wH).^(1+p.epsilon) - (out.wL).^(1+p.epsilon));

% rho
out.rho = 1/((p.p/s-1)*(1+p.epsilon));

% zL at thetastar
out.zLthetastar = zL(out.thetastar);
out.zHthetastar = zH(out.thetastar);

%% check L and H

ls = @(theta) hL(theta,AH,L,H,t,p).*theta.*f(theta,p);

out.Lcheck = quad(ls,p.lb,thetastar(s,t,AH,L,H,p));

hs = @(theta) hH(theta,AH,L,H,t,p).*theta.*f(theta,p);

out.Hcheck = quad(hs,thetastar(s,t,AH,L,H,p),p.ub);

%% hours
% not skill weighted

hoursL = @(theta) hL(theta,AH,L,H,t,p).*f(theta,p);
out.hoursL =  quad(hoursL,p.lb,out.thetastar);

hoursH = @(theta) hH(theta,AH,L,H,t,p).*f(theta,p);
out.hoursH =  quad(hoursH,out.thetastar,p.ub);


%% Total output (should be equal to zbar)
out.Y = Y(AH,L,H,p); 

%% f(thetastar)
out.fthetastar = f(out.thetastar,p);

out.zL_f_thetastar = out.fthetastar * out.zLthetastar;
out.zH_f_thetastar = out.fthetastar * out.zHthetastar;

%% subsidy-elasticity of thetastar

out.eps_s_thetastar = out.rho * (p.sigma + p.epsilon) / (p.sigma + ...
                                                  p.epsilon + out.thetastar *out.delta * ( out.beta- out.alpha));

% Average sectoral wage
out.avgwL = out.zbarL / out.hoursL;
out.avgwH = out.zbarH / out.hoursH;

% College wage premium as in Acemoglu and Autor (2012)
out.cwp = log(out.wH) - log(out.wL);

% College wage premium as empirically observed
out.cwp_emp = log(out.avgwH) - log(out.avgwL);

% Wage ratio
out.wrat = out.wH / out.wL;

% Net price of education
out.eduprice = p.p - s;

out.target_eps_s_thetastar = f(out.thetastar,p) * out.thetastar * ...
    out.eps_s_thetastar * out.eduprice/s;

out.earnings_premium = out.zbarH / out.zbarL;

out.eduprice_wL = out.eduprice / out.wL;

out.subsidy_share = s/p.p;

% variance of log-earnings
rng('default');
theta_vector = lognrnd(p.mu_theta,p.sigma_theta,1,p.nrn);
earnings = comp_earnings(theta_vector,s,t,AH,L,H,p);
var_earnings = var(log(earnings));
out.var_earnings = var_earnings;
% f(out.thetastar,p)

% out.thetastar

% out.eps_s_thetastar

end