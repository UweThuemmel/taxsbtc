function out = ddelta(AH,L,H,s,t,p)
% Wedge
% 
% out = ddelta(AH,L,H,t,p)
% 
% INPUTS
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% t: tax rate
% p: parameters


out = (zH(thetastar(s,t,AH,L,H,p),AH,L,H,t,p) - zL(thetastar(s,t,AH,L,H,p),AH,L,H,t,p))* t - s;