function [L,H]=clearlabor(AH,s,t,init,p)
% Calculate labor market clearing levels of L and H, taking all
% policy variables as given.
% OUT = clearlabor(AH,s,t,init,p)
% 
% INPUTS
% s: subsidy
% t: tax rate
% init: vector of initial points
% p: struct of paramters
% 

% define multivariate function for labor market clearing
fun = @(x) [excessL(AH,x(1),x(2),s,t,p);excessH(AH,x(1),x(2),s,t, ...
                                                p)];
x0 = init;
% options = optimoptions('fsolve','Display','iter');
options = optimset('Display','off');
out=fsolve(fun,x0,options);

L = out(1);
H = out(2);