function [t,L,H,b]=solve_foc_t(s,t,AH,L,H,p)
% solves foc for t

fun = @(x) [excessL(AH,x(2),x(3),s,x(1),p);excessH(AH,x(2),x(3),s,x(1), ...
                                                p);foc_tax(s, ...
                                                  x(1),AH,x(2), ...
                                                  x(3),p)];



x0 = [t;L;H];


out=fsolve(fun,x0);


t = out(1);
L = out(2);
H = out(3);



end