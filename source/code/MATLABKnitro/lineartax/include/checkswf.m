function out=checkswf(AH,b,s,t,init,p)
% Evaluates objective function (social welfare function)
% automatically computing labor market clearing
% 
% out=checkswf(AH,b,s,t,init,p)
% 
% AH: productivity of the high-skilled
% b: lump-sum transfer (benefit)
% t: linear tax rate
% s: subsidy
% init: [L,H] for solver
% p: struct with parameters


[L,H]=clearlabor(AH,s,t,init,p);

% compute b which clears the gbc
fun = @(x) gbc(AH,L,H,x,s,t,p);

x0 = b;

options = optimset('Display','off');
b = fsolve(fun,x0,options);

out.b = b;
out.s = s;
out.t = t;
out.obj = swf(AH,b,s,t,L,H,p);
out.L = L;
out.H = H;

