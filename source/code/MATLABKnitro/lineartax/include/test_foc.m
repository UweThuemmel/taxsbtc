% This script tests the MATLAB implementation of foc's in order to
% compare to the implementation in Mathematica

% Set parameters as in Mathematica

p.mu_theta = 0;
p.sigma_theta = 0.4711;
p.epsilon = 0.3;
p.sigma = 1.41;
p.gamma = 0.4774;
p.p = 0.3801;
p.lb = 0.0001;
p.ub = 10;
p.AL = 1;
p.eta = 1;
p.r = 1;

% p.lb = 0.01;
% p.ub = 2.5;
% p.AL = 1;
% p.mu_theta = -1;
% p.sigma_theta = 0.39;
% p.epsilon = 0.3;
% p.p = 1;
% p.sigma = 1.4;
% p.eta = 1;
% p.gamma = 0.3;
% p.r = 1;
p.weight = 'Exponential';

s = 0.3;
t = 0.45;
AH = 2;
L = 0.2;
H = 0.25;
theta = 0.5;

p

% Density
fprintf('density f: %f\n',f(theta,p))
% works

% Wage rates
fprintf('wage wL: %f\n',wL(AH,L,H,p))
fprintf('wage wH: %f\n',wH(AH,L,H,p))

% works

% Labor supplies
fprintf('labor supply hL: %f\n',hL(theta,AH,L,H,t,p))
fprintf('labor supply hH: %f\n',hH(theta,AH,L,H,t,p))

% works

% thetastar
fprintf('thetastar: %f\n',thetastar(s,t,AH,L,H,p))

% works

% excess labor supply
fprintf('excess L: %f\n',excessL(AH,L,H,s,t,p))
fprintf('excess H: %f\n',excessH(AH,L,H,s,t,p))
% works

% xi
fprintf('xi: %f\n',xi(s,t,AH,L,H,p))
% works

% beta
fprintf('beta: %f\n',beta(AH,L,H,p))
% works

% delta
fprintf('delta: %f\n',delta(AH,L,H,s,t,p))
% works

% gL, gH
fprintf('gL: %f\n',gL(AH,L,H,s,t,p))
fprintf('gH: %f\n',gH(AH,L,H,s,t,p))
% works

% alpha
fprintf('alpha: %f\n',alpha(AH,L,H,p))
% works

% zH, zL
fprintf('zL: %f\n',zL(theta,AH,L,H,t,p))
fprintf('zH: %f\n',zH(theta,AH,L,H,t,p))
% works

% ddelta
fprintf('ddelta: %f\n',ddelta(AH,L,H,s,t,p))
% works

% zeta
fprintf('zeta: %f\n',zeta(s,t,AH,L,H,p))
% works

% zbar
fprintf('zbar: %f\n',zbar(AH,L,H,p))
% works

% foc_tax
fprintf('foc tax: %f\n',foc_tax(s,t,AH,L,H,p))
% works

% foc_subsidy
fprintf('foc subsidy: %f\n',foc_subsidy(s,t,AH,L,H,p))
