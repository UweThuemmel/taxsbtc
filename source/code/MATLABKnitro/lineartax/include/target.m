function out = target(AH,mu_theta,sigma_theta,gamma,price,s,L,H,b,t,p,targets)
% target function for calibration

% assign values to parameter struct
p.mu_theta = mu_theta;
p.sigma_theta = sigma_theta;
p.gamma = gamma;
p.p = price;



% calculate other variables
v = othervars(AH,b,s,t,L,H,p);

% targets
tar={};

tar.enrollment_elasticity =v.target_eps_s_thetastar- targets.enrollment_elasticity;
tar.subsidy_share = s/p.p - targets.subsidy_share;
tar.empH = v.empH - targets.empH;
tar.eduprice_wL = p.p/v.zbarL - targets.eduprice_wL;
tar.earnings_premium = v.zbarH/v.zbarL - ...
                      targets.earnings_premium;

% variance of log-earnings
tar.var_earnings = v.var_earnings - targets.var_earnings;


% objective value

out =  tar.earnings_premium^2 + tar.var_earnings^2 + tar.subsidy_share ...
       ^2 + tar.empH^2 + tar.eduprice_wL^2;

end
