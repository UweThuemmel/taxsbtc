function out = constr_thetastar_ub(s,t,AH,L,H,p)
% Inequality constraint, ensuring that thetastar < p.ub
% The value of this condition has to be <= 0.
% 
% OUT = constr_thetastar_ub(s,t,AH,L,H,p)
% 
% INPUTS
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% p: struct with fields
% - epsilon: compensated wage-elasticity of labor supply
% - AL: productivity of the low skilled
% - sigma: elasticity of substitution between high and low skilled
% - gamma: share parameter
% - p: price of college
% 
% OUTPUTS
% out: thetastar - p.ub
% 
% See also THETASTAR

out = thetastar(s,t,AH,L,H,p) - p.ub;