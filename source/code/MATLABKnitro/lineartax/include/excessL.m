function out = excessL(AH,L,H,s,t,p)
% excess supply of low-skill labor
% 
% OUT = excessL(AH,L,H,s,t,p)
% 
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% s: subsidy
% t: linear tax rate
% p: struct with parameters
% 
% SEE ALSO HL, F, THETASTAR

ls = @(theta) hL(theta,AH,L,H,t,p).*theta.*f(theta,p);

out = quad(ls,p.lb,thetastar(s,t,AH,L,H,p)) - L;


end
