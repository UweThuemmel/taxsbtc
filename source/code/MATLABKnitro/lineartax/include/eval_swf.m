function eval_swf(AH,b,s,t,L,H,p)
% Evaluate swf at a grid of t values
% Find welfare-maximizing t in tgrid
% Report summary and plot objective

tgrid=t;
results = {};
for i=1:length(tgrid)
    temp=checkswf(AH,b,s,tgrid(i),[L,H],p);
    results.s{i}=temp.s;
    results.t{i}=temp.t;
    results.L{i}=temp.L;
    results.H{i}=temp.H;
    results.b{i}=temp.b;
    results.obj{i}=temp.obj;
end

% get index of maximum
obj = cell2mat(results.obj);
ind = find(obj == max(obj));

wageconstr = wL(AH,results.L{ind},results.H{ind},p) - wH(AH,results.L{ind},results.H{ind},p);

% report maximum values found by calculating "manually"
fprintf('t at max: %f\n',results.t{ind})
fprintf('L at max: %f\n',results.L{ind})
fprintf('H at max: %f\n',results.H{ind})
fprintf('b at max: %f\n',results.b{ind})
fprintf('obj at max: %f\n',results.obj{ind})
fprintf('excess L at max: %f\n',excessL(AH,results.L{ind}, ...
                                        results.H{ind},s,results.t{ind},p))
fprintf('excess H at max: %f\n',excessH(AH,results.L{ind}, ...
                                        results.H{ind},s, ...
                                        results.t{ind},p))
fprintf('constr thetastar lb at max: %f\n',constr_thetastar_lb(s, ...
                                                  results.t{ind}, ...
                                                  AH,results.L{ind},results.H{ind},p))
fprintf('constr thetastar ub at max: %f\n',constr_thetastar_ub(s, ...
                                                  results.t{ind},AH,results.L{ind},results.H{ind},p))
fprintf('wL - wH thetastar ub at max: %f\n',wageconstr)



% plot objective for tgrid
fid = gcf;
figure(fid+1)
plot(tgrid,obj)
title('Objective')

end
