function out = wH(AH,L,H,p)
% wage of the high-skilled, derived from FOC of CES production function
% OUT = wH(AH,L,H,p)
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% p: struct with fields
% - AL: productivity of the low skilled
% - sigma: elasticity of substitution between high and low skilled
% - gamma: share parameter
   
out = AH .*(AH .*H).^(-1 + (-1 + p.sigma)./p.sigma).* (1 - p.gamma) .*((AH ...
                                                  .*H).^((-1 + p.sigma)./p.sigma).* (1 - p.gamma) + (p.AL .*L).^((-1 + p.sigma)./p.sigma).* p.gamma).^(-1 + p.sigma./(-1 + p.sigma));


end
                                                
