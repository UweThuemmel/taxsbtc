% Script to cross check the foc's with the swf

% main
% Solves model with linear tax rate and education subsidy
% Uwe Thuemmel, Summer 2014, u.thuemmel@tinbergen.nl

%% General setup 

% include path for functions
addpath(['/media/data/Dropbox/PhD/projects/taxsbtc/source/code/' ...
         'MATLABKnitro/lineartax/include']);

%% Set parameters

p.mu_theta = 0;
p.sigma_theta = 0.4711;
p.epsilon = 0.3;
p.sigma = 1.41;
p.gamma = 0.4774;
p.p = 0.3811;
p.lb = 0.0001;
p.ub = 10;
p.AL = 1;
p.eta = 1;
p.plots = ['/media/data/Dropbox/PhD/projects/taxsbtc/source/code/' ...
           'MATLABKnitro/lineartax/plots/'];
p.csv = '/media/data/Dropbox/PhD/projects/taxsbtc/source/code/MATLABKnitro/lineartax/csv/';
p.weight = 'Exponential';
% p.weight = 'Rawlsian';
p.options = '';
p.nrn = 10;

rgrid = [5];
sigmagrid = [1.41];
AHgrid = 3;

p.r = 5;
p.sigma = 1.41;
AH = 3;
%% Optimization

    
% b = sol(1);
% s = sol(2);
% t = sol(3);
% L = sol(4);
% H = sol(5);


lb.b = 0;
lb.L = 0;
lb.H = 0;

ub.b = Inf;
ub.L = Inf;
ub.H = Inf;

in.b = 0.2;
in.L = 0.2;
in.H = 0.2;

lb.s = -Inf;
lb.t = 0;

ub.s = Inf;
ub.t = Inf;

in.s = 0.2;
in.t = 0.2;


% compute equilibrium
[res,dist]=compute_equilibrium(lb,ub,in,sigmagrid,rgrid,AHgrid,p);

% get values from solution
b = cell2mat(res.b)
s = cell2mat(res.s)
t = cell2mat(res.t)
L = cell2mat(res.L)
H = cell2mat(res.H)

% evaluate swf at grid of t values
start = t - 0.05;
stop = t + 0.05;
tgrid = start:0.01:stop;

results = {};
for i=1:length(tgrid)
    temp=checkswf(AH,b,s,tgrid(i),[L,H],p);
    results.s{i}=temp.s;
    results.t{i}=temp.t;
    results.L{i}=temp.L;
    results.H{i}=temp.H;
    results.b{i}=temp.b;
    results.obj{i}=temp.obj;
end

% get index of maximum
obj = cell2mat(results.obj);
ind = find(obj == max(obj));

% report maximum values
fprintf('t\n')
results.t{ind}
fprintf('L\n')
results.L{ind}
fprintf('H\n')
results.H{ind}
fprintf('b\n')
results.b{ind}
fprintf('obj\n')
results.obj{ind}

% plot objective
plot(tgrid,obj)

%% Check foc
foc_t = zeros(1,length(tgrid));
for i = 1:length(tgrid)
    foc_t(i) = foc_tax(s,tgrid(i),AH,L,H,p)
end

plot(tgrid,foc_t)

% solve for optimal t
fun = @(x) foc_tax(s,x,AH,L,H,p);
x0 = t;
options = optimset('Display','off');
t_foc = fsolve(fun,x0,options);

% plot around solution
start = t_foc - 0.2;
stop = t_foc + 0.2;
tgrid_new = start:0.01:stop;

foc_t_new = zeros(1,length(tgrid_new));
for i = 1:length(tgrid_new)
    foc_t_new(i) = foc_tax(s,tgrid_new(i),AH,L,H,p);
end

plot(tgrid_new,foc_t_new)

% plot objective around optimal t found using foc
results_new= {}

for i=1:length(tgrid_new)
    temp=checkswf(AH,b,s,tgrid_new(i),[L,H],p);
    results_new.s{i}=temp.s;
    results_new.t{i}=temp.t;
    results_new.L{i}=temp.L;
    results_new.H{i}=temp.H;
    results_new.b{i}=temp.b;
    results_new.obj{i}=temp.obj;
end

% get index of maximum
obj_new = cell2mat(results_new.obj);
ind = find(obj_new == max(obj_new));

% report maximum values
fprintf('t\n')
results_new.t{ind}
fprintf('L\n')
results_new.L{ind}
fprintf('H\n')
results_new.H{ind}
fprintf('b\n')
results_new.b{ind}
fprintf('obj\n')
results_new.obj{ind}

% plot objective
plot(tgrid_new,obj_new)

%% now check without GE
% p.sigma = 1.41;
p.sigma = 1000000;
% p.sigma = 100000000;
sigmagrid=p.sigma;

in.s = s;
lb.s = s;
ub.s = s;
% compute equilibrium using Knitro
p.options = optimset('TolCon', 1e-20);
[res,dist]=compute_equilibrium(lb,ub,in,sigmagrid,rgrid,AHgrid,p);

% get values from solution
b = cell2mat(res.b);
s = cell2mat(res.s);
t = cell2mat(res.t);
L = cell2mat(res.L);
H = cell2mat(res.H);
Kobj = cell2mat(res.obj);

% report maximum values found using Knitro
fprintf('t using Knitro: %f\n',t)
fprintf('L using Knitro: %f\n',L)
fprintf('H using Knitro: %f\n',H)
fprintf('b using Knitro: %f\n',b)
fprintf('obj using Knitro: %f\n',Kobj)


% evaluate swf at grid of t values (using labor market clearing and
% clearing of the gbc
start = t - 0.05;
stop = t + 0.05;
tgrid = start:0.01:stop;

eval_swf(AH,b,s,tgrid,L,H,p);

%% Check foc
fprintf('foc at optimum found using Knitro: %f\n',foc_tax(s,t,AH,L,H,p))

foc_t = zeros(1,length(tgrid));
for i = 1:length(tgrid)
    foc_t(i) = foc_tax(s,tgrid(i),AH,L,H,p);
end

figure(2)
plot(tgrid,foc_t)
title('FOC around t found using Knitro')

% solve for optimal t
t_foc = real(solve_foc_t(s,t,AH,L,H,p))
fprintf('t found by solving foc: %f\n',t_foc)

fprintf('swf at t found by solving foc:\n')
eval_swf(AH,b,s,t_foc,L,H,p)


% plot around solution
start = t_foc - 0.2;
stop = t_foc + 0.2;
tgrid_new = start:0.01:stop;

foc_t_new = zeros(1,length(tgrid_new));
for i = 1:length(tgrid_new)
    foc_t_new(i) = foc_tax(s,tgrid_new(i),AH,L,H,p);
end

figure(3)
plot(tgrid_new,foc_t_new)
title('Plot foc around t found by solving foc')


% plot objective around optimal t found using foc
fprintf('Evaluate around t found using foc\n')
eval_swf(AH,b,s,tgrid_new,L,H,p)