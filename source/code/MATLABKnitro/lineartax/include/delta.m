function out = delta(AH,L,H,s,t,p)
% delta
% 
% out = delta(AH,L,H,s,t,p)
% 
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% s: subsidy
% t: linear tax rate

ths = thetastar(s,t,AH,L,H,p);
out = ths.^2.*f(ths,p).*(hL(ths,AH,L,H,t,p)./L + hH(ths,AH,L,H,t,p)./H);
