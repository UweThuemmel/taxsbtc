% script for testing functions
%% Tolerance
% tolerance for equality tests
tol=0.00000001;


%% Wages
if wL(1,1,1,p) == p.gamma
    fprintf('wL test passed\n')
else
    error('wL test failed')
end

if wH(1,1,1,p) == 1-p.gamma
    fprintf('wH test passed\n')
else
    error('wH test failed')
end

%% thetastar
ptest = p;
ptest.p = s;
if thetastar(s,t,AH,L,H,ptest) == 0
    fprintf('thetastar test 1 passed\n')
else
    error('thetastar test 1 failed')
end

ptest.p = s+1;
% setting variables such that numerator 1 + eps and denominator to (1-gamma)^(1+eps)
% - (gamma)^(1+eps)
if thetastar(s,0,1,1,1,ptest) == ((1+p.epsilon)/((1-p.gamma)^(1+p.epsilon)-p.gamma^(1+p.epsilon)))^(1/(1+p.epsilon));
    fprintf('thetastar test 2 passed\n')
else
    error('thetastar test 2 failed')
end


% as before but now also setting p.epsilon = 0
ptest.epsilon = 0;
if abs(thetastar(s,0,1,1,1,ptest) - 1/( 1-2*p.gamma)) < tol;
    fprintf('thetastar test 3 passed\n')
else
    error('thetastar test 3 failed')
end


% as before but now setting t = 0.5
ptest.epsilon = 0;
ptest.p = s+1;
if abs(thetastar(s,0.5,1,1,1,ptest) - 1/(0.5*( 1-2*p.gamma))) < tol;
    fprintf('thetastar test 4 passed\n')
else
    error('thetastar test 4 failed')
end

% as before but now with eps = 1
ptest.epsilon = 1;
ptest.p = s+1;
if abs(thetastar(s,0.5,1,1,1,ptest) - (2/(0.25*( (1-p.gamma)^2 - p.gamma^2)))^(1/2)) < tol;
    fprintf('thetastar test 5 passed\n')
else
    error('thetastar test 5 failed')
end

%% hL, hH
if hL(1,1,1,1,0,p) == (p.gamma)^p.epsilon;
    fprintf('hL test passed\n')
else
    error('hL test failed')
end

if hH(1,1,1,1,0,p) == (1-p.gamma)^p.epsilon;
    fprintf('hH test passed\n')
else
    error('hH test failed')
end

%% zL, zH
if zL(1,1,1,1,0,p) == (p.gamma)^p.epsilon * p.gamma;
    fprintf('zL test passed\n')
else
    error('zL test failed')
end

if zH(1,1,1,1,0,p) == (1-p.gamma)^p.epsilon * (1-p.gamma);
    fprintf('zH test passed\n')
else
    error('zH test failed')
end

%% ddelta
ptest.epsilon = 1;
if ddelta(1,1,1,s,0,p) == -s;
    fprintf('ddelta test passed\n')
else
    error('ddelta test failed')
end

%% zbarL, zbarH, zbar
if zbarL(1,1,1,p) == p.gamma;
    fprintf('zbarL test passed\n')
else
    error('zbarL test failed')
end

if zbarH(1,1,1,p) ==1- p.gamma;
    fprintf('zbarH test passed\n')
else
    error('zbarH test failed')
end

if zbar(1,1,1,p) == 1;
    fprintf('zbar test passed\n')
else
    error('zbar test failed')
end

%% delta
ptest.p = s+1;
ptest.epsilon = 0;
ths =  1/( 1-2*p.gamma);
if abs(delta(1,1,1,s,0,ptest) - ths^2 * f(ths,ptest) * 2) < tol;
    fprintf('delta test passed\n')
else
    error('delta test failed')
end

%% beta
ptest.epsilon = 0;
if abs(beta(1,1,1,ptest) - (1-p.gamma)/(1-2*p.gamma))<tol
   fprintf('beta test passed\n')
else
    error('beta test failed')
end 

%% alpha
if alpha(1,1,1,p) == 1-p.gamma
    fprintf('alpha test passed\n')
else
    error('alpha test failed')
end

%% vL, vH
ptest.epsilon = 1;
if vL(5,1,1,1,2,0.5,ptest) == 2 + ((5*0.5 * p.gamma)^2)/2
    fprintf('vL test passed\n')
else
    error('vL test failed')
end

if vH(5,1,1,1,2,0.5,s,ptest) == 2 + ((5*0.5 *(1- p.gamma))^2)/2 - ptest.p + s
    fprintf('vH test passed\n')
else
    error('vH test failed')
end
