function out = hH(theta,AH,L,H,t,p)
% labor supply of the high-skilled
% 
% OUT = hH(theta,AH,L,H,t,p)
% 
% theta: skill value
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% t: linear tax rate
% 
% SEE ALSO WH

out = (theta.*wH(AH,L,H,p).*(1 - t)).^p.epsilon;
end
