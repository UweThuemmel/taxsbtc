function out = vL(theta,AH,L,H,b,t,p)
% indirect utility of the low-skilled
% 
% OUT = VL(theta,AH,L,H,b,t,p) 
% 
% theta: skill value
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% b: lump-sum transfer (benefit)
% t: linear tax rate

out = b + ((theta.*wL(AH, L, H,p).*(1 - t)).^(1 + p.epsilon))./(1 + p.epsilon);

end
