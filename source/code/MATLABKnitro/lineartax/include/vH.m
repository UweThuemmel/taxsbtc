function out = vH(theta,AH,L,H,b,t,s,p)
% indirect utility of the high-skilled
% 
% OUT = VH(theta,AH,L,H,b,t,s,p) 
% 
% theta: skill value
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% b: lump-sum transfer (benefit)
% t: linear tax rate

out = b + ((theta.*wH(AH, L, H,p).*(1 - t)).^(1 + p.epsilon))./( 1 + p.epsilon) - p.p + s;

end