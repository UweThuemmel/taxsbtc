function out = thetastar(s,t,AH,L,H,p)
% critical skill above which people choose college
% 
% OUT = thetastar(s,t,AH,L,H,p)
% 
% s: subsidy
% t: tax rate
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% p: struct with fields
% - epsilon: compensated wage-elasticity of labor supply
% - AL: productivity of the low skilled
% - sigma: elasticity of substitution between high and low skilled
% - gamma: share parameter
% - p: price of college
% 
% See also WH, WL


numerator = (p.p - s).*(1 + p.epsilon);
denominator = ((1 - t).^(1 + p.epsilon)).*(wH(AH,L,H,p).^(1 + p.epsilon) - wL(AH,L,H,p).^(1 + p.epsilon));

out = (numerator./denominator).^(1./(1 + p.epsilon));

end
                                                
