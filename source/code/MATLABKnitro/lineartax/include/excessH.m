function out = excessH(AH,L,H,s,t,p)
% excess supply of high-skill labor
% 
% OUT = excessH(AH,L,H,s,t,p)
% 
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% s: subsidy
% t: linear tax rate
% p: struct with parameters
% 
% SEE ALSO HL, F, THETASTAR

ls = @(theta) hH(theta,AH,L,H,t,p).*theta.*f(theta,p);

out = quad(ls,thetastar(s,t,AH,L,H,p),p.ub) - H;


end
