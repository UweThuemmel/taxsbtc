function out=simulate(AH,b,s,t,L,H,init,p,results,i)
% simulate, given the paramters
    [L,H] = clearlabor(AH,s,t,init,p);
    out = comp_results(AH,b,s,t,L,H,p,results,i)
end