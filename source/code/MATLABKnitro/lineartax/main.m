% main
% Solves model with linear tax rate and education subsidy
% Uwe Thuemmel, Summer 2014, u.thuemmel@tinbergen.nl

%% General setup 

% include path for functions
addpath(['/media/data/Dropbox/PhD/projects/taxsbtc/source/code/' ...
         'MATLABKnitro/lineartax/include']);

% include path for plotly
addpath(['~/Downloads/MATLAB-api-master/']);

%% Set index for saving in results
i = 1; 


%% Set parameters

p.mu_theta = 0;
p.sigma_theta = 0.4711;
p.epsilon = 0.3;
p.sigma = 1.41;
p.gamma = 0.4774;
p.p = 1/0.684 * 0.26;
p.lb = 0.0001;
p.ub = 10;
p.AL = 1;
p.eta = 1;
p.plots = ['/media/data/Dropbox/PhD/projects/taxsbtc/source/code/' ...
           'MATLABKnitro/lineartax/plots/'];
p.csv = '/media/data/Dropbox/PhD/projects/taxsbtc/source/code/MATLABKnitro/lineartax/csv/';
p.weight = 'Exponential';

%% Parameter for welfare weights
% p.r = 0 Utilitarian, p.r -> Inf Rawlsian



% also define a grid for r and sigma

% rgrid = [0.1,0.5,1,1.5];
rgrid = [1];
% sigmagrid = [1.1,1.41,2,5,10000];
sigmagrid = [1.41];
%% Set parameter optimize
optimize = 1;
% if == 1, then optimization problem is solved
% otherwise, AH,s,t are taken as given as specified below (b is
% adjusted such that the government budget constraint is satisfied)

s = 0.26;
t = 0.424;

% Grid of skill-bias values
AHgrid = 3;

% empty struct for saving results
results = {};

%% Optimization

    
% b = sol(1);
% s = sol(2);
% t = sol(3);
% L = sol(4);
% H = sol(5);

lb.b = 0;
lb.L = 0;
lb.H = 0;

ub.b = Inf;
ub.L = Inf;
ub.H = Inf;

in.b = 0.2;
in.L = 0.2;
in.H = 0.2;

if optimize ==1 
lb.s = -Inf;
lb.t = 0;

ub.s = Inf;
ub.t = Inf;

in.s = 0.2;
in.t = 0.2;

else
    % assign bounds and initial values to values specified above
lb.s = s;
lb.t = t;

ub.s = s;
ub.t = t;

in.s = s;
in.t = t;

end

% compute equilibrium
[res,dist]=compute_equilibrium(lb,ub,in,sigmagrid,rgrid,AHgrid,p);

% convertresults(res,horzcat(p.csv, ...
%                                'optimized_results_ver06.csv'));
% convertresults(dist,horzcat(p.csv,'optimized_distributions_ver06.csv'));

% % Evaluate foc's at the optimal point
% foc_tax(s,t,AH,L,H,p)
% foc_subsidy(s,t,AH,L,H,p)

% %% comparative statics
% 
% % first optimize the model at the initial point
% AHgrid = 3;
% [res1,dist1]=compute_equilibrium(lb,ub,in,sigmagrid,rgrid,AHgrid,p);
% 
% % now take the computed policy variables
% s = cell2mat(res.s)
% t = cell2mat(res.t)
% 
% % and compute a non-optimized model at a higher AH
% lb.s = s;
% lb.t = t;
% 
% ub.s = s;
% ub.t = t;
% 
% in.s = s;
% in.t = t;
% 
% AHgrid = 3:0.1:6;
% 
% [res2,dist2]=compute_equilibrium(lb,ub,in,sigmagrid,rgrid,AHgrid,p);
% 
% convertresults(res2,horzcat(p.csv, ...
%                                'cs_results_ver01.csv'));
% convertresults(dist2,horzcat(p.csv,'cs_distributions_ver01.csv'));
% 
% 
% % Try a larger sigma
% 
% % first optimize the model at the initial point
% AHgrid = 3;
% sigmagrid = 10;
% [res3,dist3]=compute_equilibrium(lb,ub,in,sigmagrid,rgrid,AHgrid,p);
% 
% % now take the computed policy variables
% s = cell2mat(res3.s)
% t = cell2mat(res3.t)
% 
% % and compute a non-optimized model at a higher AH
% lb.s = s;
% lb.t = t;
% 
% ub.s = s;
% ub.t = t;
% 
% in.s = s;
% in.t = t;
% 
% AHgrid = 6;
% 
% [res4,dist4]=compute_equilibrium(lb,ub,in,sigmagrid,rgrid,AHgrid,p);