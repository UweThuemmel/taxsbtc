% Version of R&S, now implemented via MATLAB and Knitro

% include path for functions
addpath(['/media/data/Dropbox/PhD/projects/taxsbtc/source/code/' ...
         'MATLABKnitro/include']);
addpath('~/Programs/ampl/knitro/examples/Matlab');


% Parameters
mu_theta = 3;
mu_phi = 3;

sigma_theta = 0.5;
sigma_phi = 0.5;

dw = 1;

ETI = 0.25;
k = 1/ETI;
r = 1.3;	
eps = 1e-5;

alpha = 0.5;
a_theta = 1.5;
a_phi = 1;
rho = 1/3;

% Marginal products

yy_phi = @(x) (alpha/(1-alpha)*(a_theta/a_phi*x)^rho+1)^((1-rho)/rho)*(1-alpha)^(1/rho)*a_phi;
yy_theta = @(x) (1+(1-alpha)/alpha*(a_theta/a_phi*x)^(-rho))^((1-rho)/rho)*alpha^(1/rho)*a_theta;

% alpha(x)

alphax = @(x) x*yy_theta(x)/(x*yy_theta(x) + yy_phi(x));

% Wage densities
f_theta = @(x,w) 1./(sqrt(2.*pi).*sigma_theta.*w./yy_theta(x)) .* (exp(-(log(w./yy_theta(x))-mu_theta).^2/(2.*sigma_theta.^2)));
f_phi = @(x,w) 1./(sqrt(2.*pi).*sigma_phi.*w./yy_phi(x)) .*( exp(-(log(w./ ...
                                                  yy_phi(x))-mu_phi).^2/(2.*sigma_phi.^2)));


ff_theta = @(x,w) logncdf(w./yy_theta(x),mu_theta,sigma_theta);
ff_phi = @(x,w) logncdf(w./yy_phi(x),mu_phi,sigma_phi);

% For now ignore normalization issues
f_thetax = @(x,w) 1./yy_theta(x) .* f_theta(x,w) .* ff_phi(x,w)
f_phix = @(x,w) 1./yy_phi(x) .* f_phi(x,w) .* ff_theta(x,w);

f_x = @(x,w) f_thetax(x,w) + f_phix(x,w);


util = @(c,y,w) c - (y./w).^(1+k)./(1+k); 

g_x = @(x,w) f_x(x,w).*r.*(1-ff_x(x,w,f_x)).^(r-1);

x_in = 1;
c_in = ones(1,50);
y_in = ones(1,50);
w = 1:50;

nonlcon = @(argument) NonLinearConstr(argument',x_in,c_in,y_in,w,util, ...
                                 f_thetax,f_phix,f_x,g_x,alphax)

fun = @(argument) Objective(argument,x_in,c_in,y_in,w,util,g_x)

mtr =  @(y,w)1-y.^k./w.^(k+1)

% Specify vectors for linear constraints as well as upper and lower bounds

A = []; b = [];
Aeq = [];
beq = [];
lb = zeros(101,1);
ub = [];

% Call Knitro
sol=knitromatlab(fun,[x_in;c_in';y_in'],A,b,Aeq,beq,lb,ub,nonlcon)