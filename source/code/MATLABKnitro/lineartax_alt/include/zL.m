function out = zL(theta,AH,L,H,t,p)
% Income for high-school type theta
% 
% out = zL(theta,AH,L,H,t,p)
% 
% INPUTS
% theta: skill
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% t: tax rate
% p: parameters

out = hL(theta,AH,L,H,t,p) .* theta .* wL(AH,L,H,p);