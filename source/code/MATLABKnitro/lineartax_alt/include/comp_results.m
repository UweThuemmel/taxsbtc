function out = comp_results(A,AH,b,s,t,L,H,p,results,i)
% computes results and returns struct
% results is the structure to which the results should be added
% i is a counter which refers to the row of the struct in which to
% save the results

    results.b{i} = b;
    results.s{i} = s;
    results.t{i} = t;
    results.L{i} = L;
    results.H{i} = H;
    results.AH{i} = AH;
    results.A{i} = A;


    % calculate other variables (derived from solution)
    v = othervars(A,AH,b,s,t,L,H,p);

    % assign to results array
    results.thetastar{i} = v.thetastar;
    results.thetast_paren{i} = v.thetast_paren;
    results.wL{i} = v.wL;
    results.wH{i} = v.wH;
    results.eduprice{i} = v.eduprice;
    results.eduprice_wL{i} = v.eduprice_wL;
    results.subsidy_share{i} = v.subsidy_share;
    results.zbarL{i} = v.zbarL;
    results.zbarH{i} = v.zbarH;
    results.zbar{i} = v.zbar;
    results.empL{i} = v.empL;
    results.empH{i} = v.empH;
    results.avgwL{i} = v.avgwL;
    results.avgwH{i} = v.avgwH;
    results.cwp{i} = v.cwp;
    results.cwp_emp{i} = v.cwp_emp;
    results.earnings_premium{i} = v.earnings_premium;
    results.wrat{i} = v.wrat;
    results.xi{i} = v.xi;
    results.alpha{i} = v.alpha;
    results.wedge{i} = v.wedge;
    results.delta{i} = v.delta;
    results.qL{i} = v.qL;
    results.qH{i} = v.qH;
    results.alphaterm{i} = v.alphaterm;
    results.qLminqH{i} = v.qLminqH;
    results.beta{i} = v.beta;
    results.rho{i} = v.rho;
    results.zLthetastar{i} = v.zLthetastar;
    results.zHthetastar{i} = v.zHthetastar;
    results.Lcheck{i} = v.Lcheck;
    results.Hcheck{i} = v.Hcheck;
    results.Y{i} = v.Y;
    results.fthetastar{i} = v.fthetastar;
    results.zL_f_thetastar{i} = v.zL_f_thetastar;
    results.zH_f_thetastar{i} = v.zH_f_thetastar;
    results.eps_s_thetastar{i} = v.eps_s_thetastar;
    results.target_eps_s_thetastar{i} = v.target_eps_s_thetastar;
    results.hoursL{i} = v.hoursL;
    results.hoursH{i} = v.hoursH;
    results.xi_L{i} = v.xi_L;
    results.xi_H{i} = v.xi_H;
    results.xi_L_zbar{i} = v.xi_L_zbar;
    results.xi_H_zbar{i} = v.xi_H_zbar;

    % also include parameters

    results.gamma{i} = p.gamma;
    results.epsilon{i} = p.epsilon;
    results.sigma{i} = p.sigma;
    results.sigma_theta{i} = p.sigma_theta;
    results.mu_theta{i} = p.mu_theta;
    results.p{i} = p.p;
    results.lb{i} = p.lb;
    results.ub{i} = p.ub;
    
out = results;

end