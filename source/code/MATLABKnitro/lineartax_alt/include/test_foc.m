% This script tests the MATLAB implementation of foc's in order to
% compare to the implementation in Mathematica

% Set parameters as in Mathematica

p.lb = 0.01;
p.ub = 2.5;
p.AL = 1;
p.mu_theta = -1;
p.sigma_theta = 0.39;
p.epsilon = 0.3;
p.p = 1;
p.sigma = 1.4;
p.eta = 1;
p.gamma = 0.3;
p.weight = 'Exponential';

s = 0.3;
t = 0.45;
AH = 2;
L = 0.2;
H = 0.25;
theta = 0.5;

% Density
fprintf('density f')
f(theta,p)
% works

% Wage rates
fprintf('wage wL')
wL(AH,L,H,p)
fprintf('wage wH')
wH(AH,L,H,p)
% works

% Labor supplies
fprintf('labor supply hL')
hL(theta,AH,L,H,t,p)
fprintf('labor supply hH')
hH(theta,AH,L,H,t,p)
% works

% thetastar
fprintf('thetastar')
thetastar(s,t,AH,L,H,p)
% works

% excess labor supply
fprintf('excess L')
excessL(AH,L,H,s,t,p)
fprintf('excess H')
excessH(AH,L,H,s,t,p)
% works

% xi
fprintf('xi')
xi(s,t,AH,L,H,p)
% works

% beta
fprintf('beta')
beta(AH,L,H,p)
% works

% delta
fprintf('delta')
delta(AH,L,H,s,t,p)
% works

% gL, gH
fprintf('gL')
gL(AH,L,H,s,t,p)
fprintf('gH')
gH(AH,L,H,s,t,p)
% works

% alpha
fprintf('alpha')
alpha(AH,L,H,p)
% works

% zH, zL
fprintf('zL')
zL(theta,AH,L,H,t,p)
fprintf('zH')
zH(theta,AH,L,H,t,p)
% works

% ddelta
fprintf('ddelta')
ddelta(AH,L,H,s,t,p)
% works

% zeta
fprintf('zeta')
zeta(s,t,AH,L,H,p)
% works

% zbar
fprintf('zbar')
zbar(AH,L,H,p)
% works

% foc_tax
fprintf('foc tax')
foc_tax(s,t,AH,L,H,p)
% works

% foc_subsidy
fprintf('foc subsidy')
foc_subsidy(s,t,AH,L,H,p)