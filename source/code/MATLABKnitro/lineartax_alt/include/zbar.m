function out = zbar(AH,L,H,p)
% Average income
% 
% out = zbar(AH,L,H,p)
% 
% INPUTS
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% p: parameters

out = zbarL(AH,L,H,p) + zbarH(AH,L,H,p);