function out = excessH(A,AH,L,H,s,t,p)
% excess supply of high-skill labor
% 
% OUT = excessH(A,AH,L,H,s,t,p)
% 
% A: productivity of the high-skilled
% AH: productivity of the high-skilled - via production function
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% s: subsidy
% t: linear tax rate
% p: struct with parameters
% 
% SEE ALSO HL, F, THETASTAR

ls = @(theta) hH(theta.^A,AH,L,H,t,p).*theta.^A.*f(theta,p);

out = quad(ls,thetastar(s,t,A,AH,L,H,p),p.ub) - H;


end
