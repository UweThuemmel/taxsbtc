function convertresults(results,filename)
% converts structure of results and writes as csv to filename

% get fieldnames
headers = fieldnames(results);

data = [];

% loop over fields and add to data matrix
for i =1:length(headers)
%     if strcmp(headers{i},'note')
% %         convert to int as indicator
%         m = results.note';
%         idx = find(strcmp(m(:,1),'worked'));
%         m2 = zeros(length(results.note),1);
%         m2(idx,1) = 1;
%         data = [data, m2];
%             else
        data = [data, cell2mat(results.(headers{i}))'];

end

try
    csvwrite_with_headers(filename,data,headers);
catch
    fprintf('Something went wrong');
end
