function [IneqValues,EqValues] = NonLinearConstr(A,AH,b,s,t,L,H,p)
% Evaluates non-linear inequality and equality constraints at var
% and returns their values
% 
% [IneqValues,EqValues] = NONLINEARCONSTR(x,xi,w,w_tilde,w_hat,p)
% 
% INPUTS
% 
% OUTPUTS
% IneqValues: vector of values of inequality constraints 
% EqValues: vector of values of equality constraints 
% 


% % compute normalization
% p.norm = normalization(w,w,w,x,p);
try
IneqValues = [constr_thetastar_lb(s,t,A,AH,L,H,p);constr_thetastar_ub(s,t,A,AH,L,H,p)];
EqValues = [excessL(A,AH,L,H,s,t,p);excessH(A,AH,L,H,s,t,p);gbc(A,AH,L,H,b,s,t,p)];
catch
IneqValues = [NaN;NaN];
EqValues = [NaN;NaN;NaN];
end