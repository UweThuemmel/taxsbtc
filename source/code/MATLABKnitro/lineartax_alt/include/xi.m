function out = xi(s,t,AH,L,H,p)
% Compute distributional characteristic xi
% 
% out = xi(AH,L,H,s,t,p) 
% 
% INPUTS
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% s: subsidy
% t: linear tax rate
% p: paramters

% define individual income zL and zH as a function of theta
zL = @(theta) hL(theta,AH,L,H,t,p) .* theta .* wL(AH,L,H,p);
zH = @(theta) hH(theta,AH,L,H,t,p) .* theta .* wH(AH,L,H,p);

% define integrand for low-skilled
integL = @(theta) weight(theta,p) ./ p.eta .* zL(theta) .* f(theta,p);
integH = @(theta) weight(theta,p) ./ p.eta .* zH(theta) .* f(theta,p);

% calculate xi

out = 1 - 1./zbar(AH,L,H,p) * (quad(integL,p.lb,thetastar(s,t,AH,L,H,p)) + ...
                           quad(integH,thetastar(s,t,AH,L,H,p),p.ub));
