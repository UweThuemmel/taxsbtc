function out = ff(theta,p)
% skill cdf
% 
% OUT = ff(theta,p)
% 
% theta: skill value
% p: struct with fields
% - mu_theta: mu of skill distribution
% - sigma_theta: sigma of skill distribution

% log-normal skill distribution
out = logncdf(theta,p.mu_theta,p.sigma_theta);

end
