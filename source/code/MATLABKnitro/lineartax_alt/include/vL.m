function out = vL(theta,A,AH,L,H,b,t,p)
% indirect utility of the low-skilled
% 
% OUT = VL(theta,A,AH,L,H,b,t,p) 
% 
% theta: skill value
% A: productivity of the high-skilled
% AH: productivity of the high-skilled - via production function
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% b: lump-sum transfer (benefit)
% t: linear tax rate

out = b + (theta.^A.*wL(AH, L, H,p).*(1 - t)).^(1 + p.epsilon)./(1 + p.epsilon);

end
