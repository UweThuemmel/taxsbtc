function out = thetastar(s,t,A,AH,L,H,p)
% critical skill above which people choose college
% 
% OUT = thetastar(s,t,A,AH,L,H,p)
% 
% s: subsidy
% t: tax rate
% A: productivity of the high-skilled
% AH: productivity of the high-skilled - via production function
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% p: struct with fields
% - epsilon: compensated wage-elasticity of labor supply
% - AL: productivity of the low skilled
% - sigma: elasticity of substitution between high and low skilled
% - gamma: share parameter
% - p: price of college
% 
% See also WH, WL


out = (((p.p - s)*(1 + p.epsilon))/((1 - t)^(1 + p.epsilon)*(wH(AH, ...
                                                  L, H, p)^(1 + p.epsilon) - wL(AH, L, H,p)^(1 + p.epsilon))))^(1/(A*(1 + p.epsilon)));

end
                                                
