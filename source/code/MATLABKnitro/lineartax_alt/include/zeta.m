function out = zeta(s,t,AH,L,H,p)
% Compute distributional characteristic of the subsidy, zeta
% 
% out = zeta(AH,L,H,s,t,p) 
% 
% INPUTS
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% s: subsidy
% t: linear tax rate
% p: paramters


% define integrand
integ = @(theta) (1 - weight(theta,p) ./ p.eta ).*  f(theta,p);


out = quad(integ,thetastar(s,t,AH,L,H,p),p.ub);
