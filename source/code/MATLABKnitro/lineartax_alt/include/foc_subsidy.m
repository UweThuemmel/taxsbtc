function out = foc_subsidy(s,t,AH,L,H,p)
% First-order-condition for optimal subsidy
% 
% out = foc_tax(s,t,AH,L,H,p)
% 
% INPUTS
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% s: subsidy
% t: linear tax rate
% p: paramters


% precompute variables, for speed and readability
v.alpha = alpha(AH,L,H,p);
v.thetastar = thetastar(s,t,AH,L,H,p);
v.delta = delta(AH,L,H,s,t,p);
v.beta = beta(AH,L,H,p);
v.zbar = zbar(AH,L,H,p);
v.ddelta = ddelta(AH,L,H,s,t,p);
v.gL = gL(AH,L,H,s,t,p);
v.gH = gH(AH,L,H,s,t,p);



a_term = v.ddelta / (1-t);

b_term =  f(v.thetastar,p) * ...
         v.thetastar / v.zbar * (p.sigma + ...
                                                  p.epsilon)/(p.sigma ...
                                                  + p.epsilon + ...
                                                  v.delta*(v.beta-v.alpha));


c_term = (p.p - s) * (1 + p.epsilon)*zeta(s,t,AH,L,H,p);

d_term = (1-v.alpha)*v.alpha*v.delta / (p.sigma ...
                                                  + p.epsilon + ...
                                                  v.delta*(v.beta- ...
                                                  v.alpha))*(v.gL-v.gH);

out =a_term * b_term - c_term +  d_term;
