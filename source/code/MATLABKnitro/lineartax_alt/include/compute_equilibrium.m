function [results,distributions]=compute_equilibrium(lb,ub,in, ...
                                                  sigmagrid,rgrid,Agrid,AHgrid,p)
% Compute equilibrium, using solving the social planner problem
% 
% [results,distributions]=compute_equilibrium(lb,ub,in, sigmagrid,Agrid,AHgrid,p)
% 
% INPUTS
% 
% lb: struct of lower bounds for optimization variables
% ub: struct of upper bounds for optimization variables
% in: struct of initial values for optimization variables
% sigmagrid: grid of sigma values
% rgrid: grid of r values
% AHgrid: grid of Avalues (all grids can be singletons)
% AHgrid: grid of AHvalues (all grids can be singletons)
% 
% The optimization variables are
% b = sol(1);
% s = sol(2);
% t = sol(3);
% L = sol(4);
% H = sol(5);

    
%% Constraints and bounds for optimization
% Specify vectors for linear constraints as well as upper and lower bounds
Am = []; b = [];
Aeq = [];
beq = [];
lb = [lb.b;lb.s;lb.t;lb.L;lb.H];
ub = [ub.b;ub.s;ub.t;ub.L;ub.H];

% initial values
init = [in.b;in.s;in.t;in.L;in.H];
b = init(1);
s = init(2);
t = init(3);
L = init(4);
H = init(5);

%% Optimization problem
counter = 1;
for k=1:length(sigmagrid)
    init = [in.b;in.s;in.t;in.L;in.H];
    p.sigma = sigmagrid(k);
    fprintf('sigma is ')
    p.sigma
    for j=1:length(rgrid)
        init = [in.b;in.s;in.t;in.L;in.H];
        p.r = rgrid(j);
        fprintf('r is ')
        p.r
        p.rinit = p.r;
        % calculate normalization for weighting function
        fn = @(theta) theta.^(-p.r);
        p.mass = quad(fn,p.lb,p.ub);
     
        for i=1:length(AHgrid)
            
            AH = AHgrid(i);
            fprintf('AH is ')
            AH
            for l=1:length(Agrid)
                A=Agrid(l);
                fprintf('A is ')
                A
               
                p.r = p.rinit / A;
            % Function handle for objective
            fun = @(vars) -1*swf(A,AH,vars(1),vars(2),vars(3),vars(4),vars(5),p);

            % Function handle for constraint
            nonlcon = @(vars) NonLinearConstr(A,AH,vars(1),vars(2),vars(3),vars(4),vars(5),p);

            fprintf('thetastar')
            thetastar(s,t,A,AH,L,H,p)
            [ineq,eq]=nonlcon(init)
            % Call Knitro
%             try
            options = optimset('AlwaysHonorConstraints','bounds');
            sol=knitromatlab(fun,init,Am,b,Aeq,beq,lb,ub,nonlcon,options);
            results.note{counter} = 1;
            % Assign solutions
            b = sol(1);
            s = sol(2);
            t = sol(3);
            L = sol(4);
            H = sol(5);
            
       

            % results.b{counter} = b;
            % results.s{counter} = s;
            % results.t{counter} = t;
            % results.L{counter} = L;
            % results.H{counter} = H;
            % results.AH{counter} = AH;
            % results.A{counter} = A;

            % % compute results based on solutions
            results = comp_results(A,AH,b,s,t,L,H,p,results,counter);
            results.obj{counter} = - fun(sol);
            results.r{counter} = p.r;
            % % compute terms in tax and subsidy formula
            % tt=tax_terms(s,t,AH,L,H,p);  
            % results.tax_a_term{counter} = tt.a_term;
            % results.tax_b_term{counter} = tt.b_term;
            % results.tax_c_term{counter} = tt.c_term;

            % st=subsidy_terms(s,t,AH,L,H,p);
            % results.subsidy_a_term{counter} = st.a_term;
            % results.subsidy_b_term{counter} = st.b_term;
            % results.subsidy_c_term{counter} = st.c_term;
            % results.subsidy_d_term{counter} = st.d_term;
            % results.zeta{counter} = zeta(s,t,AH,L,H,p);
         

            % % compute income distribution
            zL = @(theta) hL(theta.^A,AH,L,H,t,p) .* theta.^A .* wL(AH,L,H,p);
            zH = @(theta) hH(theta.^A,AH,L,H,t,p) .* theta.^A .* wH(AH,L,H,p);

            thetagrid = linspace(p.lb,p.ub,100);
            distributions.zL{counter} = zL(thetagrid);
            distributions.zH{counter} = zH(thetagrid);
            distributions.weights{counter} = weight(thetagrid,p);
            distributions.ftheta{counter} = f(thetagrid,p);
            distributions.fftheta{counter} = ff(thetagrid,p);
            distributions.AH{counter} = ones(1,length(thetagrid)) ...
                .* AH;
            distributions.A{counter} = ones(1,length(thetagrid)) ...
                .* A;
            distributions.thetastar{counter} = ones(1,length(thetagrid)) ...
                .* thetastar(s,t,A,AH,L,H,p);
            distributions.theta{counter} = thetagrid;
            
            % use solution as new initial value
            init = [sol(1);sol(2);sol(3);sol(4);sol(5)];
            % increment counter
%             catch
%             results.note{counter} = 0;
%             end
            counter = counter + 1;
            end
        end
    end
end

end