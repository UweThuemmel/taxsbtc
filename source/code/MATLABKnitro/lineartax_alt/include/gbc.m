function out = gbc(A,AH,L,H,b,s,t,p)
% Evaluates government budget constraint
% 
% OUT = swf(A,AH,L,H,b,s,t,p)
% 
% A: productivity of the high-skilled
% AH: productivity of the high-skilled - via production function
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% b: lump-sum transfer (benefit)
% t: linear tax rate
% s: subsidy
% p: struct with parameters
% 
% SEE ALSO WEIGHT, VL, VH, F

% mass in the high-skill sector
empH = ff(p.ub,p) - ff(thetastar(s,t,A,AH,L,H,p),p);

% total income
inc_total = wL(AH,L,H,p) .* L + wH(AH,L,H,p) .* H;

out = t.*inc_total - b - s.*empH; 

end

