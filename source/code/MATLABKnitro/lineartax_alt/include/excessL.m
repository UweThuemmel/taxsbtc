function out = excessL(A,AH,L,H,s,t,p)
% excess supply of low-skill labor
% 
% OUT = excessL(A,AH,L,H,s,t,p)
% 
% A: productivity of the high-skilled
% AH: productivity of the high-skilled - via production function
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% s: subsidy
% t: linear tax rate
% p: struct with parameters
% 
% SEE ALSO HL, F, THETASTAR

ls = @(theta) hL(theta.^A,AH,L,H,t,p).*theta.^A.*f(theta,p);

out = quad(ls,p.lb,thetastar(s,t,A,AH,L,H,p)) - L;


end
