function out = target(AH,mu_theta,sigma_theta,gamma,price,s,L,H,b,t,p,targets)
% target function for calibration

% assign values to parameter struct
p.mu_theta = mu_theta;
p.sigma_theta = sigma_theta;
p.gamma = gamma;
p.p = price;



% calculate other variables
v = othervars(AH,b,s,t,L,H,p);

% targets
tar={};
tar.enrollment_elasticity = (f(v.thetastar,p) * v.thetastar * ...
                           v.eps_s_thetastar * v.eduprice/s - targets.enrollment_elasticity)^2;
tar.eduprice_wL = (v.eduprice/v.wL - targets.eduprice_wL)^2;
tar.earnings_premium = (v.zbarH/v.zbarL - ...
                      targets.earnings_premium)^2;
tar.subsidy_share = (s/p.p - targets.subsidy_share)^2;
tar.empH = (v.empH - targets.empH)^2;

% objective value

out = tar.eduprice_wL;

end
