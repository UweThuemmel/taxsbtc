function out = gL(AH,L,H,s,t,p)
% Average social welfare weight for the low-skilled
% 
% out = gL(AH,L,H,s,t,p)
% 
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% s: subsidy
% t: tax rate
% p: struct with fields
% 
% 

funL = @(theta) weight(theta,p).*zL(theta,AH,L,H,t,p).*f(theta,p);
out = 1./p.eta .* 1./zbarL(AH,L,H,p) .* quad(funL,p.lb,thetastar(s,t,AH,L,H,p));