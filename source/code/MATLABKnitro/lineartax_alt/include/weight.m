function out = weight(theta,p)
% welfare weights
% 
% OUT = weight(theta,p)
% 
% theta: skill value
% p: struct with fields
% - mu_theta: mu of skill distribution
% - sigma_theta: sigma of skill distribution


if strcmp(p.weight,'Exponential')
    mass = expcdf(p.ub,p.r) - expcdf(p.lb,p.r);
    out = exppdf(theta,p.r) / mass;

else
    if p.r == 1
        mass = log(p.ub) - log(p.lb);
        if (theta <= p.ub) 
            out = 1./(mass.*theta);
        else
            out = 0;
        end
    else
        % alternative weights
        if (theta <= p.ub) 
            out = theta.^(-p.r) ./ p.mass;
        else
            out = 0;
        end
    end
end