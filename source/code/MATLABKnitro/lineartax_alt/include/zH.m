function out = zH(theta,AH,L,H,t,p)
% Income for college type theta
% 
% out = zH(theta,AH,L,H,t,p)
% 
% INPUTS
% theta: skill
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% t: tax rate
% p: parameters

out = hH(theta,AH,L,H,t,p) .* theta .* wH(AH,L,H,p);