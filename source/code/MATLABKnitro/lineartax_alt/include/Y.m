function out = Y(AH,L,H,p)
% production function
% OUT = Y(AH,L,H,p)
% AH: productivity of the high-skilled
% L: labor supply / demand of the low-skilled
% H: labor supply / demand of the high-skilled
% p: struct with fields
% - AL: productivity of the low skilled
% - sigma: elasticity of substitution between high and low skilled
% - gamma: share parameter
   
out = (p.gamma*(p.AL*L)^((p.sigma -  1)/p.sigma) + (1 - p.gamma)*(AH*H)^((p.sigma -  1)/p.sigma))^(p.sigma/(p.sigma - 1));


end
                                                
