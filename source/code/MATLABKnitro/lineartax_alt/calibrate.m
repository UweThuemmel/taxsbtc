% Script used in order to calibrate the model

% include path for functions
addpath(['/media/data/Dropbox/PhD/projects/taxsbtc/source/code/' ...
         'MATLABKnitro/lineartax/include']);





b = 0;

% Parameters
p.lb = 1;
p.ub = 3000;
p.epsilon = 0.3;
p.sigma = 1.41;
p.AL = 10;
p.eta = 1;
p.r=1;
p.gamma = 0.3;

%% Targets
% Target value for tax rate (actually, fix it here)
t = 0.424;


targets.enrollment_elasticity = 0.07;
targets.eduprice_wL = 1/3;
targets.earnings_premium = 1.64;
targets.subsidy_share = 0.684;
targets.empH = 0.32;

%% Constraints and bounds for optimization
% Specify vectors for linear constraints as well as upper and lower bounds
A = []; b = [];
Aeq = [];
beq = [];

lb.AH = 10;
lb.mu_theta = 3;
lb.sigma_theta = 0.001;
lb.gamma = 0.001;
lb.price = 0.001;
lb.s = 0.001;
lb.L = 0.001;
lb.H = 0.001;

ub.AH = 50;
ub.mu_theta = 3;
ub.sigma_theta = 0.5;
ub.gamma = 1;
ub.price = 10;
ub.s = 100;
ub.L = 100;
ub.H = 100;

in.AH = 20;
in.mu_theta = 3;
in.sigma_theta = 0.4711;
in.gamma = 0.3;
in.price = 5;
in.s = in.price * 0.685;
in.L = 20;
in.H = 70;


lb = [lb.AH;lb.mu_theta;lb.sigma_theta;lb.gamma;lb.price;lb.s;lb.L;lb.H];
ub = [ub.AH;ub.mu_theta;ub.sigma_theta;ub.gamma;ub.price;ub.s;ub.L; ...
      ub.H];
init = [in.AH;in.mu_theta;in.sigma_theta;in.gamma;in.price;in.s;in.L;in.H];

% vars(1) = AH
% vars(2) = mu_theta
% vars(3) = sigma_theta
% vars(4) = gamma
% vars(5) = price
% vars(6) = s
% vars(7) = L
% vars(8) = H

% Initial values

%init = [3.1453;-0.8979;0.9999;0.6504;1.4001;0.4010;0.2101]
%init = [1.2345;-0.7371;0.7284;0.7048;1.0457;0.3591;0.0786]
%init = [12.0519;1.3915;0.9951;0.4503;5.0398;3.7262;12.6453];


%solution of workig calibration
%init=[12.2080;1.8151;0.6829;0.6769;2.5690;7.7240;7.1188];

% Function handle for objective
fun = @(vars) target(vars(1),vars(2),vars(3),vars(4),vars(5),vars(6),vars(7),vars(8),b,t,p,targets);

fun(init)

% Function handle for constraint
nonlcon = @(vars) NonLinearConstrCalibrate(vars(1),vars(2),vars(3),vars(4),vars(5),vars(6),vars(7),vars(8),b,t,p,targets);

[ineq,eq]=nonlcon(init)

wL(in.AH,in.L,in.H,p)
wH(in.AH,in.L,in.H,p)

% Call Knitro

sol=knitromatlab(fun,init,A,b,Aeq,beq,lb,ub,nonlcon);


results = {};

% vars(1) = AH
% vars(2) = mu_theta
% vars(3) = sigma_theta
% vars(4) = gamma
% vars(5) = price
% vars(6) = s
% vars(7) = L
% vars(8) = H

AH = sol(1);
p.mu_theta = sol(2);
p.sigma_theta = sol(3);
p.gamma = sol(4);
p.p = sol(5);
s = sol(6);
L = sol(7);
H = sol(8);

% AH = 2;
% p.mu_theta = 0;
% p.sigma_theta = 0.4711;
% p.gamma = 0.4481;
% p.p = 0.3310;
% s = 0.2264;
% L = 0.4157;
% H = 0.5567;

results = comp_results(AH,b,s,t,L,H,p,results,1)