function [values] = ICconstr(var,x_in,c_in,y_in,w,util)
% Computes values of incentive compatibility constraint
% which is a non-linear inequality constraint. Note that inequality
% constraints are defined as constr(var)<=0
%
% var = [x,c,y]
% x_in: ratio of aggregate effective labor supply across sectors
% c_in: vector of consumption
% y_in: vector of income
% x_in, y_in, c_in are only used to determine the dimensions of these
% vectors
% actual values are then read from var
% w: wage grid
% util: function handle of utility function

% obtain dimensions
c_size = size(c_in,2);
y_size = size(y_in,2);

% obtain values from var
x = var(1);
c = var(2:c_size+1);
y = var(c_size+2:end);

% test
% x == x_in
% c == c_in
% y == y_in

values = -(util(c(2:end),y(2:end),w(2:end)) - util(c(1:end-1),y(1:end-1),w(2:end)));

end

