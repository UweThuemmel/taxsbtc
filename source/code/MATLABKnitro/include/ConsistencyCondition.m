function [values] = ConsistencyCondition(var,x_in,c_in,y_in,w,f_thetax,f_phix,alphax)
% Calculates value of Consistency Condition, which is a non-linear
% equality constraint (once x is endogenous)
% var = [x,c,y]
% x_in: ratio of aggregate effective labor supply across sectors
% c_in: vector of consumption
% y_in: vector of income
% x_in, y_in, c_in are only used to determine the dimensions of these
% vectors
% actual values are then read from var
% w: wage grid
% f_thetax: function handle for wage density in theta sector
% f_phix: function handle for wage density in phi sector
% alphax: function handle for alphax

% obtain dimensions
c_size = size(c_in,2);
y_size = size(y_in,2);

% obtain values from var
x = var(1);
c = var(2:c_size+1);
y = var(c_size+2:end);

% get dw from w for trapezoid integration
dw = w(2) - w(1);

% for now ignoring multiplication by dw (does not matter for == 0)
values = (1-alphax(x)) * dw * f_thetax(x,w) * y' - alphax(x) * dw * f_phix(x,w) * y';