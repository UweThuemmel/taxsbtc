function [values] = yIncr(var,x_in,c_in,y_in)
% Monotonicity condition, requiring that y is increasing in w

% obtain dimensions
c_size = size(c_in,2);
y_size = size(y_in,2);

% obtain values from var
x = var(1);
c = var(2:c_size+1);
y = var(c_size+2:end);

values = -( y(2:end) - y(1:end-1));