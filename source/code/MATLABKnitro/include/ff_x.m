function [values] = ff_x(x,w,f_x)
% Calculates values of CDF
% x: ratio of effector aggregate labor supply across sectors
% w: wage grid
% f_x: function handle for wage density
% values: row vector of CDF evaluated at grid w

% get dw from w for trapezoid integration
dw = w(2) - w(1);

% for now ignoring multiplication by dw (does not matter for == 0)
values = cumsum(f_x(x,w) * dw);