clear all

% include path for functions
addpath(['/media/data/Dropbox/PhD/projects/taxsbtc/source/code/' ...
         'MATLABKnitro/rs/include_norm']);

addpath('/home/uwe/Programs/ampl/knitro/examples/Matlab');

% Set parameters
p.mu_theta = 3;
p.mu_phi = 3;

p.sigma_theta = 0.5;
p.sigma_phi = 0.5;

% Bounds for theta and phi
p.theta_bnds = [];
p.phi_bnds = [];

% Parameters of production function
p.alpha = 0.5;
p.a_theta = 1.5;
p.a_phi = 1;
p.rho = 1/3;

p.epsilon = 0.5;
p.r = 1.3;

% Education subsidy
p.tau = 0;
% 
% % Calculate min and max wage
% minwage = @(x) max(p.theta_bnds(1)*yy_theta(x,p), p.phi_bnds(1) * yy_phi(x,p))
% maxwage = @(x) max(p.theta_bnds(2) * yy_theta(x,p), p.phi_bnds(2) * ...
%                    yy_phi(x,p))
% 
% Specify grid for w
w = 1:100;
lw = length(w);
dw = w(2) - w(1);
% 
% % For now set w_tilde = w_hat = []
% w_tilde = [];
% w_hat = [];
% 
% % Specify vectors for linear constraints as well as upper and lower bounds
% A = []; b = [];
% Aeq = [];
% beq = [];
% lb = [0;-Inf];
% ub = [];
% 
% % p.norm = normalization(w,w,w,1,p);
% 
% mtrtest = @(x,xi,p) mtr(w,w,w,x,xi,ff_x(w,w,w,x,p),gg_x(ff_x(w,w,w,x,p),p),p)
% 
% % Plot densities
% x=1.085;
% 
% clf
% figure(1)
% plot(log(w),f_theta_x(w,w,x,p))
% hold all
% plot(log(w),f_phi_x(w,w,x,p))
% legend('fthetax','fphix')
% xlabel('log(w)')
% ylabel('density')
% title('Sectoral wage densities')
% hold off
% 
% % Integrate over densities
% mass_theta = sum(f_theta_x(w,w,x,p)) * dw
% mass_phi = sum(f_phi_x(w,w,x,p)) * dw
% 
% mass_total = mass_theta + mass_phi
% 
% % alpha(x)
% alpha_x(x,p)
% 
% % parts of consistency condition
xigrid = -10:0.1:10;
% 
% v_ff_x = ff_x(w,w_tilde,w_hat,x,p);
% v_gg_x = gg_x(v_ff_x,p);
% 
% cc1 = @(xi) (1-alpha_x(x,p)) *dw * f_theta_x(w,w,x,p) * income(w,w,w,x,xi,v_ff_x,v_gg_x,p)';
% cc2 = @(xi) alpha_x(x,p) * dw * f_phi_x(w,w,x,p) * income(w,w,w,x,xi,v_ff_x,v_gg_x,p)';
% 
% cc1_val = zeros(1,length(xigrid));
% cc2_val = zeros(1,length(xigrid));
% 
% for i =1:length(xigrid)
%     cc1_val(i)=cc1(xigrid(i));
%     cc2_val(i)=cc2(xigrid(i));
% end;
% 
% cc1_val(cc1_val~=real(cc1_val))=NaN;
% cc2_val(cc2_val~=real(cc2_val))=NaN;
% 
% figure(2)
% plot(xigrid,cc1_val)
% hold all
% plot(xigrid,cc2_val)
% legend('cc1','cc2')
% title('Analyzing the Consistency Condition')
% xlabel('xi')
% ylabel('value')
% hold off
% 
% % How alpha(x) changes with x
% xgrid = 0.1:0.1:2;
% figure(3)
% plot(xgrid,alpha_x(xgrid,p));
% title('alpha(x) wrt. x')
% xlabel('x')
% ylabel('alpha(x)')

% Investigate parts of CC for different values of x
  
cc1 = @(x,xi) (1-alpha_x(x,p)) *dw * f_theta_x(w,w,x,p) * income(w,w,w,x,xi,ff_x(w,w,w,x,p),gg_x(ff_x(w,w,w,x,p),p),p)';
cc2 = @(x,xi) alpha_x(x,p) * dw * f_phi_x(w,w,x,p) * income(w,w,w,x,xi,ff_x(w,w,w,x,p),gg_x(ff_x(w,w,w,x,p),p),p)';


xgrid = 0.5:0.5:2;
for i = 1:length(xgrid)
    
    cc1_val = zeros(1,length(xigrid));
    cc2_val = zeros(1,length(xigrid));
    
    for j =1:length(xigrid)
        cc1_val(j)=cc1(xgrid(i),xigrid(j));
        cc2_val(j)=cc2(xgrid(i),xigrid(j));
       
    end;
    
    % replace imaginary with NaN
    cc1_val(cc1_val~=real(cc1_val)) = NaN;
    cc1_val(cc2_val~=real(cc2_val)) = NaN;
    
    % save to array
    results.x{i} = xgrid(i);
    results.cc1{i} = cc1_val;
    results.cc2{i} = cc2_val;
    results.f_theta_x{i} = f_theta_x(w,w,xgrid(i),p);
    results.f_phi_x{i} = f_phi_x(w,w,xgrid(i),p);
    results.f_x{i} = f_x(w,w,w,xgrid(i),p);
       
end;

% replace imaginary with NaN

% plot results

figure(4)
plot(xigrid,results.cc1{1},xigrid,results.cc2{1},xigrid,results.cc1{2},xigrid,results.cc2{2},xigrid,results.cc1{3},xigrid,results.cc2{3},xigrid,results.cc1{4},xigrid,results.cc2{4})
legend('cc1,x=0.5','cc2,x=0.5','cc1,x=1','cc2,x=1','cc1,x=1.5','cc2,x=1.5','cc1,x=2','cc2,x=2')

figure(5)
plot(log(w),results.f_theta_x{1},log(w),results.f_phi_x{1},log(w),results.f_theta_x{2},log(w),results.f_phi_x{2})
legend('fthetax,x=0.5','fphix,x=0.5','fthetax,x=1','fphix,x=1')
