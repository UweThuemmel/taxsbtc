function out = f_phi(phi, parameters)
% Skill density of phi-skill

% assign parameters
p = parameters;

mu_phi = p.mu_phi;
sigma_phi = p.sigma_phi;

if isempty(p.phi_bnds)
    out =  lognpdf(phi,mu_phi,sigma_phi);
else 
    % assign lower and upper bounds
    lb = p.phi_bnds(1);
    ub = p.phi_bnds(2);
    % % compute normalization (based on total mass between bounds)
    % norm = logncdf(ub,mu_phi,sigma_phi) - logncdf(lb,mu_phi, ...
    %                                                   sigma_phi);

    out = lognpdf(phi,mu_phi,sigma_phi);
    out(phi < lb) = 0;
    out(phi > ub) = 0;

end