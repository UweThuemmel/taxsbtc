function out = indiff_hat(x,xi,w,w_tilde,w_hat,p)
% Calculates value of indifference constraint wrt. w_hat, which is a non-linear
% equality constraint (once x is endogenous)

% x: ratio of aggregate effective labor supplies across sectors
% xi: related to multiplier on consistency condition
% w: wage grid
% w_tilde: threshold values used by f_theta_x
% w_hat: threshold values used by f_phi_x
% p: struct with parameters


v_ff_x = ff_x(w,w_tilde,w_hat,x,p);
v_gg_x = gg_x(v_ff_x,p);

% vector of marginal tax rates
v_mtr = mtr(w,w_tilde,w_hat,x,xi,v_ff_x,v_gg_x,p);

% compute consumption vector
cs = income(w,w_tilde,w_hat,x,xi,v_ff_x,v_gg_x,p) - tr(w,w_tilde,w_hat,x,v_mtr,p);

% compute effort vector
ef = effort(w,w_tilde,w_hat,x,xi,v_ff_x,v_gg_x,p);

% compute utility vector
ut = utility(cs,ef,p);

% Construct splines
% cs_sp = spline(w,cs);
% ef_sp = spline(w,ef);

% Construct spline based on utility
ut_sp = spline(w,ut);

% Indifference condition
out = fnval(ut_sp,w_hat) + p.tau - ut;
