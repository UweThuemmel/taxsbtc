function out = alpha_x(x,p)
% Calculates alpha_
% x: ratio of aggregate effective labor supplies across sectors
% p: struct of parameters

out =  x*yy_theta(x,p)/(x*yy_theta(x,p) + yy_phi(x,p));