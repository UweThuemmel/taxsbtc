function [IneqValues,EqValues] = NonLinearConstr(x,xi,w,w_tilde,w_hat,p)
% Evaluates non-linear inequality and equality constraints at var
% and returns their values
% x: ratio of aggregate effective labor supplies across sectors
% xi: related to multiplier on consistency condition
% w_tilde: threshold values used by f_theta_x
% w_hat: threshold values used by f_phi_x
% p: struct with parameters
% IneqValues: vector of values of inequality constraints 
% EqValues: vector of values of equality constraints 

IneqValues = [];
EqValues = [cc(x,xi,w,w_tilde,w_hat,p)';indiff_tilde(x,xi,w,w_tilde,w_hat,p)';indiff_hat(x,xi,w,w_tilde,w_hat,p)'];
