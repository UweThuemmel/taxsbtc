function out = indiff_tilde(x,xi,w,w_tilde,w_hat,p)
% Calculates value of indifference constraint wrt. w_tilde, which is a non-linear
% equality constraint (once x is endogenous)

% x: ratio of aggregate effective labor supplies across sectors
% xi: related to multiplier on consistency condition
% w: wage grid
% w_tilde: threshold values used by f_theta_x
% w_hat: threshold values used by f_phi_x
% p: struct with parameters


v_ff_x = ff_x(w,w_tilde,w_hat,x,p);
v_gg_x = gg_x(v_ff_x,p);

% vector of marginal tax rates
v_mtr = mtr(w,w_tilde,w_hat,x,xi,v_ff_x,v_gg_x,p);

% compute consumption vector
cs = income(w,w_tilde,w_hat,x,xi,v_ff_x,v_gg_x,p) - tr(w,w_tilde,w_hat,x,v_mtr,p);

% compute effort vector
ef = effort(w,w_tilde,w_hat,x,xi,v_ff_x,v_gg_x,p);

% Construct splines
cs_sp = spline(w,cs);
ef_sp = spline(w,ef);

% Indifference condition
out = utility(cs + p.tau,ef,p) - utility(fnval(cs_sp,w_tilde),fnval(ef_sp,w_tilde),p);
