function out = yy_theta(x,parameters)
% Calculates marginal product yy_theta(x)

% assign parameters
p = parameters;
a_theta = p.a_theta;
a_phi = p.a_phi;
rho = p.rho;
alpha = p.alpha;

% calculate value of yy_theta

out = (1+(1-alpha)./alpha.*(a_theta./a_phi.*x).^(-rho)).^((1-rho)./rho).*alpha.^(1./rho).*a_theta;