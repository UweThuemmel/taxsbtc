function out = f_x(w,w_tilde,w_hat,x,p)
% Overall wage density
% w: wage at which to evalutate density
% w_tilde: threshold values used by f_theta_x
% w_hat: threshold values used by f_phi_x
% x: ratio of aggregate effective labor supplies across sectors
% p: struct with parameters


% calculate value 
out = f_theta_x(w,w_tilde,x,p) + f_phi_x(w,w_hat,x,p);