function out = f_theta_x(w,w_tilde,x,p)
% Wage density in theta sector
% w: wage at which to evalutate density
% w_tilde: threshold values 
% x: ratio of aggregate effective labor supplies across sectors
% p: struct with parameters

% In case w_tilde is empty, set it equal to w
if isempty(w_tilde)
    w_tilde = w;
end

% calculate value 
out = 1./yy_theta(x,p) .* f_theta(w./yy_theta(x,p),p) .* ff_phi(w_tilde./yy_phi(x,p),p);