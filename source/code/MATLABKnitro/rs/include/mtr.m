function out = mtr(w,w_tilde,w_hat,x,xi,ff_x,gg_x,p)
% Calculates marginal tax rate at w
% w: wage at which to evalutate mtr
% w_tilde: threshold values used by f_theta_x
% w_hat: threshold values used by f_phi_x
% x: ratio of aggregate effective labor supplies across sectors
% xi: related to multiplier on consistency condition
% ff_x: vector of CDF of f_x, evaluated at points w (needs to have
% same dimension as w)
% gg_x: vector of CDF of g_x, evaluated at points w (needs to have
% same dimension as w)
% p: struct with parameters

nominator = 1 + xi .* (  f_theta_x(w,w_tilde,x,p)./f_x(w,w_tilde,w_hat,x,p) ...
                         - alpha_x(x,p)  );

denominator = 1 + (gg_x - ff_x) ./ (w .* f_x(w,w_tilde,w_hat,x,p)) ...
    .* (1 + 1./p.epsilon);

% clf
% figure(1)
% plot(w,nominator)
% title('Numerator')

% figure(2)
% plot(w,denominator)
% title('Denominator')

% figure(3)
% plot(w,f_theta_x(w,w_tilde,x,p)./f_x(w,w_tilde,w_hat,x,p))
% title('fthetax/fx')


% figure(4)
% plot(w,f_theta_x(w,w_tilde,x,p),w,f_x(w,w_tilde,w_hat,x,p))
% title('fthetax,fx')

% figure(5)
% plot(w,alpha_x(x,p))
% title('alphax')

% figure(6)
% plot(w,(gg_x - ff_x))
% title('ggx - ffx')

out = 1 - nominator ./ denominator;

% figure(7)
% plot(w,out)
% title('mtr')
           
                                                  

% % (gg_x - ff_x)

% % w .* f_x(w,w_tilde,w_hat,x,p)

% figure(8)
% plot(w,(gg_x - ff_x)./(w .* f_x(w,w_tilde,w_hat,x,p)))
% title('(ggx - ffx)/w*fx')