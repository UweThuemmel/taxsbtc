function out = swf(x,xi,w,w_tilde,w_hat,p)
% Evaluates objective function (social welfare function)
% x: ratio of aggregate effective labor supplies across sectors
% xi: related to multiplier on consistency condition
% w: wage grid
% w_tilde: threshold values used by f_theta_x
% w_hat: threshold values used by f_phi_x
% p: struct with parameters

% get dw from w for trapezoid integration
dw = w(2) - w(1);

v_ff_x = ff_x(w,w_tilde,w_hat,x,p);
v_gg_x = gg_x(v_ff_x,p);

% vector of marginal tax rates
v_mtr = mtr(w,w_tilde,w_hat,x,xi,v_ff_x,v_gg_x,p);

% compute consumption vector
consumption = income(w,w_tilde,w_hat,x,xi,v_ff_x,v_gg_x,p) - tr(w,w_tilde,w_hat,x,v_mtr,p);

% compute effort vector
v_effort = effort(w,w_tilde,w_hat,x,xi,v_ff_x,v_gg_x,p);

% The education subsidy is paid to all individuals in the theta
% sector. Their mass is
mass_theta = sum(f_theta_x(w,w_tilde,x,p) * dw);
gg_theta_x_max = 1-(1-mass_theta).^p.r;

% compute value of objective. Since
out = utility(consumption,v_effort,p) * g_x(w,w_tilde,w_hat,x,v_ff_x,p)' ...
      + gg_theta_x_max * p.tau;