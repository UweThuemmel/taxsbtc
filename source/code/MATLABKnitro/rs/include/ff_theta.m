function out = ff_theta(theta, parameters)
% Skill distribution of theta-skill

% assign parameters
p = parameters;

mu_theta = p.mu_theta;
sigma_theta = p.sigma_theta;

if isempty(p.theta_bnds)
    out =  logncdf(theta,mu_theta,sigma_theta);
else 
    % assign lower and upper bounds
    lb = p.theta_bnds(1);
    ub = p.theta_bnds(2);
    % % compute normalization (based on total mass between bounds)
    % norm = logncdf(ub,mu_theta,sigma_theta) - logncdf(lb,mu_theta, ...
    %                                                   sigma_theta);

    out = logncdf(theta,mu_theta,sigma_theta);
    out(theta < lb) = 0;
    out(theta > ub) = 1;

end