function out = f_phi(phi, p)
% Skill density of phi-skill
% 
% OUT = F_PHI(phi, p)
% 
% INPUTS
% phi: skill at which to evaluate density
% p: struct with parameters
% 


if isempty(p.phi_bnds)
    out =  lognpdf(phi,p.mu_phi,p.sigma_phi);
else 
    out = lognpdf(phi,p.mu_phi,p.sigma_phi);

    % assign lower and upper bounds
    lb = p.phi_bnds(1);
    ub = p.phi_bnds(2);

    % set density outside bounds to zero
    out(phi < lb) = 0;
    out(phi > ub) = 0;
end