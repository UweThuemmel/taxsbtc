function out = ff_theta(theta, p)
% Skill distribution of phi-skill
% 
% OUT = FF_THETA(theta, p)
% 
% INPUTS
% theta: skill at which to evaluate density
% p: struct with parameters
% 

if isempty(p.theta_bnds)
    out =  logncdf(theta,p.mu_theta,p.sigma_theta);
else 
    out = logncdf(theta,mu_theta,sigma_theta);

    % assign lower and upper bounds
    lb = p.theta_bnds(1);
    ub = p.theta_bnds(2);

    % set distribution below lb to zero and above ub to 1
    out(theta < lb) = 0;
    out(theta > ub) = 1;

end