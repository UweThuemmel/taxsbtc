function out = normalization(w,w_tilde,w_hat,x,p)
% Calculates normalization based on total mass in the economy
% 
% OUT = NORMALIZATION(w,w_tilde,w_hat,x,p)
% 
% INPUTS
% w: wage grid
% w_tilde: threshold values used by f_theta_x
% w_hat: threshold values used by f_phi_x
% x: ratio of effector aggregate labor supply across sectors
% p: struct of parameters
% 
% OUTPUTS
% out: scalar of total mass in the economy
% 
% out = sum(f_x_raw(w,w_tilde,w_hat,x,p) * dw);


% get dw from w for trapezoid integration
dw = w(2) - w(1);

% total mass in the economy
out = sum(f_x_raw(w,w_tilde,w_hat,x,p) * dw);

