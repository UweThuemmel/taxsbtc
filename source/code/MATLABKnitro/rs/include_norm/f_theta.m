function out = f_theta(theta, p)
% Skill density of theta-skill
% 
% OUT = F_THETA(theta, p)
% 
% INPUTS
% theta: skill at which to evaluate density
% p: struct with parameters
% 

% Check whether bounds are specified
if isempty(p.theta_bnds)
    out =  lognpdf(theta,p.mu_theta,p.sigma_theta);
else 
    out = lognpdf(theta,p.mu_theta,p.sigma_theta);

    % assign lower and upper bounds
    lb = p.theta_bnds(1);
    ub = p.theta_bnds(2);

    % set density outside bounds to zero
    out(theta < lb) = 0;
    out(theta > ub) = 0;
end