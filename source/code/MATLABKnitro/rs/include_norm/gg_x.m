function out = gg_x(ff_x,p)
% Calculates values of weighting function G_x(w)
% 
% OUT = GG_X(ff_x,p)
% 
% INPUTS
% ff_x: vector of CDF values
% p: struct of parameters
% 
% OUTPUTS
% out: row vector of G_x evaluated at grid w
% 
% out = 1 - (1-ff_x).^(p.r);

out = 1 - (1-ff_x).^(p.r);