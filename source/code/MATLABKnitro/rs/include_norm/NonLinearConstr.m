function [IneqValues,EqValues] = NonLinearConstr(x,xi,w,w_tilde,w_hat,p)
% Evaluates non-linear inequality and equality constraints at var
% and returns their values
% 
% [IneqValues,EqValues] = NONLINEARCONSTR(x,xi,w,w_tilde,w_hat,p)
% 
% INPUTS
% x: ratio of aggregate effective labor supplies across sectors
% xi: related to multiplier on consistency condition
% w_tilde: threshold values used by f_theta_x
% w_hat: threshold values used by f_phi_x
% p: struct with parameters
% 
% OUTPUTS
% IneqValues: vector of values of inequality constraints 
% EqValues: vector of values of equality constraints 
% 
% See also MTR_LEQ_ONE, CC

% % compute normalization
% p.norm = normalization(w,w,w,x,p);

IneqValues = [mtr_leq_one(x,xi,w,w_tilde,w_hat,p)];
EqValues = [cc(x,xi,w,w_tilde,w_hat,p)'];
