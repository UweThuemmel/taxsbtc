function out = f_phi_x_raw(w,w_hat,x,p)
% Non-normalized Wage density in phi sector
% 
% OUT = F_PHI_X_RAW(w,w_tilde,x,p)
% 
% INPUTS
% w: wage at which to evalutate density
% w_tilde: threshold values 
% x: ratio of aggregate effective labor supplies across sectors
% p: struct with parameters
% 
% OUTPUTS
% out: vector of sectoral density evaluated at wage grid w
% 
% out = 1./yy_phi(x,p) .* f_phi(w./yy_phi(x,p),p) .* ff_theta(w_hat./yy_theta(x,p),p);
% 
% See also YY_THETA, YY_PHI, F_PHI, FF_THETA

% In case w_hat is empty, set it equal to w
if isempty(w_hat)
    w_hat = w;
end

% calculate value 
out = 1./yy_phi(x,p) .* f_phi(w./yy_phi(x,p),p) .* ff_theta(w_hat./yy_theta(x,p),p);