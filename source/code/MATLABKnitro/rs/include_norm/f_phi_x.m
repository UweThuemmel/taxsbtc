function out = f_phi_x(w,w_hat,x,p)
% Wage density in phi sector
% 
% OUT = F_PHI_X(w,w_tilde,x,p)
% 
% INPUTS
% w: wage at which to evalutate density
% w_tilde: threshold values 
% x: ratio of aggregate effective labor supplies across sectors
% p: struct with parameters, including p.norm for normalization
% 
% OUTPUTS
% out: vector of sectoral density evaluated at wage grid w
% 
% out = f_phi_x_raw(w,w_hat,x,p)./normalization(w,w,w,x,p);
% 
% See also F_PHI_X_RAW, NORMALIZATION

% In case w_hat is empty, set it equal to w
if isempty(w_hat)
    w_hat = w;
end

% calculate value 
out = f_phi_x_raw(w,w_hat,x,p)./normalization(w,w,w,x,p);