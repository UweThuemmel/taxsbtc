function out = ff_phi(phi, p)
% Skill distribution of phi-skill
% 
% OUT = FF_PHI(phi, p)
% 
% INPUTS
% phi: skill at which to evaluate density
% p: struct with parameters
% 


if isempty(p.phi_bnds)
    out =  logncdf(phi,p.mu_phi,p.sigma_phi);
else 
    out = logncdf(phi,mu_phi,sigma_phi);

    % assign lower and upper bounds
    lb = p.phi_bnds(1);
    ub = p.phi_bnds(2);

    % set distribution below lb to zero and above ub to 1
    out(phi < lb) = 0;
    out(phi > ub) = 1;
end