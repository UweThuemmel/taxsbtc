function out = f_theta_x_raw(w,w_tilde,x,p)
% Non-normalized Wage density in theta sector
% 
% OUT = F_THETA_X_RAW(w,w_tilde,x,p)
% 
% INPUTS
% w: wage at which to evalutate density
% w_tilde: threshold values 
% x: ratio of aggregate effective labor supplies across sectors
% p: struct with parameters
% 
% OUTPUTS
% out: vector of sectoral density evaluated at wage grid w
% 
% out = 1./yy_theta(x,p) .* f_theta(w./yy_theta(x,p),p) .* ff_phi(w_tilde./yy_phi(x,p),p);
% 
% See also YY_THETA, YY_PHI, F_THETA, FF_PHI

% In case w_tilde is empty, set it equal to w
if isempty(w_tilde)
    w_tilde = w;
end

% calculate value 
out = 1./yy_theta(x,p) .* f_theta(w./yy_theta(x,p),p) .* ff_phi(w_tilde./yy_phi(x,p),p);