function out = ff_x(w,w_tilde,w_hat,x,p)
% Calculates values of CDF at wagegrid w
% 
% OUT = FF_X(w,w_tilde,w_hat,x,p)
% 
% INPUTS
% w: wage grid
% w_tilde: threshold values used by f_theta_x
% w_hat: threshold values used by f_phi_x
% x: ratio of effective aggregate labor supply across sectors
% p: struct of parameters
% 
% OUTPUTS
% out: row vector of CDF evaluated at grid w
% 
% out = cumsum(f_x(w,w_tilde,w_hat,x,p) * dw);
% 
% See also F_X

% get dw from w for trapezoid integration
dw = w(2) - w(1);

% cumulate over f_x
out = cumsum(f_x(w,w_tilde,w_hat,x,p) * dw);