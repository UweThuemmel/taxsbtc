function out = income(w,w_tilde,w_hat,x,xi,ff_x,gg_x,p)
% Income corresponding to w
% 
% OUT = INCOME(w,w_tilde,w_hat,x,xi,ff_x,gg_x,p)
% 
% INPUTS
% w: wage at which to evalutate income
% w_tilde: threshold values used by f_theta_x
% w_hat: threshold values used by f_phi_x
% x: ratio of aggregate effective labor supplies across sectors
% xi: related to multiplier on consistency condition
% ff_x: vector of CDF of f_x, evaluated at points w (needs to have
% same dimension as w)
% gg_x: vector of CDF of g_x, evaluated at points w (needs to have
% same dimension as w)
% p: struct with parameters
% 
% OTUPUT
% out: vector with incomes, evaluated at w
% 
% out = w .* effort(w,w_tilde,w_hat,x,xi,ff_x,gg_x,p);           
% 
% See also EFFORT


out = w .* effort(w,w_tilde,w_hat,x,xi,ff_x,gg_x,p);           
                                                  

