function out = utility(consumption,effort,p)
% Utility function
% 
% OUT = UTILITY(consumption,effort,p)
% 
% INPUTS
% consumption: consumption vector
% effort: effort vector
% p: struct of parameters
% 
% OUTPUTS
% out: utility function evaluated at consumption and effort
% 
% out = consumption - effort.^(1+1./p.epsilon)/(1+1./p.epsilon);           

out = consumption - effort.^(1+1./p.epsilon)/(1+1./p.epsilon);           
                                                  

