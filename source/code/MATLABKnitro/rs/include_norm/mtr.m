function out = mtr(w,w_tilde,w_hat,x,xi,ff_x,gg_x,p)
% Calculates marginal tax rate at w using the formula from R&S
% (2013), QJE
% 
% OUT = MTR(w,w_tilde,w_hat,x,xi,ff_x,gg_x,p)
% 
% INPUTS
% w: wage at which to evalutate mtr
% w_tilde: threshold values used by f_theta_x
% w_hat: threshold values used by f_phi_x
% x: ratio of aggregate effective labor supplies across sectors
% xi: related to multiplier on consistency condition
% ff_x: vector of CDF of f_x, evaluated at points w (needs to have
% same dimension as w)
% gg_x: vector of CDF of g_x, evaluated at points w (needs to have
% same dimension as w)
% p: struct with parameters
% 
% OUTPUT
% out: vector of marginal tax rates evaluated at w
% 
% See also: F_THETA_X, F_X, ALPHA_X

nominator = 1 + xi .* (  f_theta_x(w,w_tilde,x,p)./f_x(w,w_tilde,w_hat,x,p) ...
                         - alpha_x(x,p)  );

denominator = 1 + (gg_x - ff_x) ./ (w .* f_x(w,w_tilde,w_hat,x,p)) ...
    .* (1 + 1./p.epsilon);

% clf
% figure(1)
% plot(w,nominator)
% title('Numerator')

% figure(2)
% plot(w,denominator)
% title('Denominator')

% figure(3)
% plot(w,f_theta_x(w,w_tilde,x,p)./f_x(w,w_tilde,w_hat,x,p))
% title('fthetax/fx')


% figure(4)
% plot(w,f_theta_x(w,w_tilde,x,p),w,f_x(w,w_tilde,w_hat,x,p))
% title('fthetax,fx')

% figure(5)
% plot(w,alpha_x(x,p))
% title('alphax')

% figure(6)
% plot(w,(gg_x - ff_x))
% title('ggx - ffx')

out = real(1 - nominator ./ denominator);

% set nan resulting from division by zero due to zero density to a
% zero mtr. This won't matter in the SWF later anyway, as all
% related values will be weighted with zero.
out(isnan(out))=0;

% figure(7)
% plot(w,out)
% title('mtr')
           
                                                  

% % (gg_x - ff_x)

% % w .* f_x(w,w_tilde,w_hat,x,p)

% figure(8)
% plot(w,(gg_x - ff_x)./(w .* f_x(w,w_tilde,w_hat,x,p)))
% title('(ggx - ffx)/w*fx')