function out = cc(x,xi,w,w_tilde,w_hat,p)
% Calculates value of Consistency Condition, which is a non-linear
% equality constraint (once x is endogenous)
% 
% OUT = CC(x,xi,w,w_tilde,w_hat,p)
% 
% INPUTS
% x: ratio of aggregate effective labor supplies across sectors
% xi: related to multiplier on consistency condition
% w_tilde: threshold values used by f_theta_x
% w_hat: threshold values used by f_phi_x
% get dw from w for trapezoid integration
% p: struct with parameters
% 
% OUTPUT
% out: value of consistency condition
% 
% See also FF_X, GG_X, ALPHA_X, F_THETA_X, INCOME, F_PHI_X

dw = w(2) - w(1);

% vectors of cdfs
v_ff_x = ff_x(w,w_tilde,w_hat,x,p);
v_gg_x = gg_x(v_ff_x,p);


out = (1-alpha_x(x,p)) * dw * f_theta_x(w,w_tilde,x,p) * income(w,w_tilde,w_hat,x,xi,v_ff_x,v_gg_x,p)' - alpha_x(x,p) * dw * f_phi_x(w,w_hat,x,p) * income(w,w_tilde,w_hat,x,xi,v_ff_x,v_gg_x,p)';