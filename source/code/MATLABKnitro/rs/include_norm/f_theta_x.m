function out = f_theta_x(w,w_tilde,x,p)
% Wage density in theta sector
% 
% OUT = F_THETA_X(w,w_tilde,x,p)
% 
% INPUTS
% w: wage at which to evalutate density
% w_tilde: threshold values 
% x: ratio of aggregate effective labor supplies across sectors
% p: struct with parameters, including p.norm for normalization
% 
% OUTPUTS
% out: vector of sectoral density evaluated at wage grid w
% 
% out = f_theta_x_raw(w,w_tilde,x,p) ./ normalization(w,w,w,x,p);
% 
% See also F_THETA_X_RAW, NORMALIZATION

% In case w_tilde is empty, set it equal to w
if isempty(w_tilde)
    w_tilde = w;
end

% calculate value 
out = f_theta_x_raw(w,w_tilde,x,p) ./ normalization(w,w,w,x,p);