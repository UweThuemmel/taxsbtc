function out = f_x(w,w_tilde,w_hat,x,p)
% Overall wage density
% 
% OUT = F_X(w,w_tilde,w_hat,x,p) 
% 
% INPUTS
% w: wage at which to evalutate density
% w_tilde: threshold values used by f_theta_x
% w_hat: threshold values used by f_phi_x
% x: ratio of aggregate effective labor supplies across sectors
% p: struct with parameters
% 
% OUTPUTS
% out: vector of overall density evaluated at wage grid w
% 
% out = f_theta_x(w,w_tilde,x,p) + f_phi_x(w,w_hat,x,p);
% 
% See also F_THETA_X, F_PHI_X

% calculate value 
out = f_theta_x(w,w_tilde,x,p) + f_phi_x(w,w_hat,x,p);