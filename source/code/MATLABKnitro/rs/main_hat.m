% include path for functions
addpath(['/media/data/Dropbox/PhD/projects/taxsbtc/source/code/' ...
         'MATLABKnitro/rs/include_hat']);

addpath('/home/uwe/Programs/ampl/knitro/examples/Matlab');

% Set parameters
p.mu_theta = 3;
p.mu_phi = 3;

p.sigma_theta = 0.5;
p.sigma_phi = 0.5;

% Bounds for theta and phi
p.theta_bnds = [];
p.phi_bnds = [];

% Parameters of production function
p.alpha = 0.5;
p.a_theta = 1.1;
p.a_phi = 1;
p.rho = 1/3;

p.epsilon = 0.25;
p.r = 1.3;

% Education subsidy
p.tau = 0;

% Calculate min and max wage
minwage = @(x) max(p.theta_bnds(1)*yy_theta(x,p), p.phi_bnds(1) * yy_phi(x,p))
maxwage = @(x) max(p.theta_bnds(2) * yy_theta(x,p), p.phi_bnds(2) * ...
                   yy_phi(x,p))

% Specify grid for w
w = 1:2:100;
lw = length(w);

% For now set w_tilde = w_hat = []
w_tilde = [];
w_hat = [];

% Function to generate matrix for taking first differences
diffmatrix = @(n) [eye(n-1),zeros(n-1,1)] + [zeros(n-1,1),eye(n-1)] * (-1)

% Full matrix for linear inequality constraints, where n is the
% number of grid points in w
Amatrix = @(n) [zeros((n-1),2),diffmatrix(n)];


% Specify vectors for linear constraints as well as upper and lower bounds
A = Amatrix(lw); b = zeros(lw - 1,1);
Aeq = [];
beq = [];
lb = [0;-Inf;zeros(lw,1)];
ub = [Inf;Inf;w'];

% Function handle for objective


fun = @(vars) -1*swf(vars(1),vars(2),p.tau,w,vars(3:end)',p);

% Function handle for constraint
nonlcon = @(vars) NonLinearConstr(vars(1),vars(2),p.tau,w,vars(3:end)',p);

% Initial values
init = [1;1;w'];

% Call Knitro
sol=knitromatlab(fun,init,A,b,Aeq,beq,lb,ub,nonlcon)

% v_ff_x = ff_x(w,w_hat,sol(1),p);
% v_gg_x = gg_x(v_ff_x,p);

% % vector of marginal tax rates
% v_mtr = mtr(w,w_hat,sol(1),sol(2),v_ff_x,v_gg_x,p);

% % compute consumption vector
% cs = income(w,w_hat,sol(1),sol(2),v_ff_x,v_gg_x,p) - tr(w,w_hat,sol(1),v_mtr,p);


% % Use solution to compute effort as function of w
% ef = effort(w,w_hat,sol(1),sol(2),v_ff_x,v_gg_x,p);

% % Construct splines
% cs_sp = spline(w,cs);
% ef_sp = spline(w,ef);

% % Evaluate splines
% fnval(cs_sp,5)
