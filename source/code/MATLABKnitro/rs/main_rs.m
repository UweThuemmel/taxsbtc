clear all

% include path for functions
addpath(['/media/data/Dropbox/PhD/projects/taxsbtc/source/code/' ...
         'MATLABKnitro/rs/include']);

addpath('/home/uwe/Programs/ampl/knitro/examples/Matlab');

% Set parameters
p.mu_theta = 3;
p.mu_phi = 3;

p.sigma_theta = 0.5;
p.sigma_phi = 0.5;

% Bounds for theta and phi
p.theta_bnds = [];
p.phi_bnds = [];

% Parameters of production function
p.alpha = 0.5;
p.a_theta = 1.5;
p.a_phi = 1;
p.rho = 1/3;

p.epsilon = 0.25;
p.r = 1.3;

% Education subsidy
p.tau = 0;

% Calculate min and max wage
minwage = @(x) max(p.theta_bnds(1)*yy_theta(x,p), p.phi_bnds(1) * yy_phi(x,p))
maxwage = @(x) max(p.theta_bnds(2) * yy_theta(x,p), p.phi_bnds(2) * ...
                   yy_phi(x,p))

% Specify grid for w
w = 1:500;
lw = length(w);

% For now set w_tilde = w_hat = []
w_tilde = [];
w_hat = [];

% Specify vectors for linear constraints as well as upper and lower bounds
A = []; b = [];
Aeq = [];
beq = [];
lb = [0;0];
ub = [];

mtrtest = @(x,xi,p) mtr(w,w,w,x,xi,ff_x(w,w,w,x,p),gg_x(ff_x(w,w,w,x,p),p),p)


% Initial values
init = [1;0];
j =1;
for i=1:0.1:1.2
    p.a_theta = i;

    % Function handle for objective
    fun = @(vars) -1*swf(vars(1),vars(2),w,w,w,p);

    % Function handle for constraint
    nonlcon = @(vars) NonLinearConstr_rs(vars(1),vars(2),w,p);

    % Use previous solution as new initial values
    % init = sol;

    % Call Knitro
    sol=knitromatlab(fun,init,A,b,Aeq,beq,lb,ub,nonlcon);

    % Call Knitro
    sol=knitromatlab(fun,init,A,b,Aeq,beq,lb,ub,nonlcon)

    w_tilde = w;
    w_hat = w;

    v_ff_x = ff_x(w,w_tilde,w_hat,sol(1),p);
    v_gg_x = gg_x(v_ff_x,p);

    % vector of marginal tax rates
    v_mtr = mtr(w,w_tilde,w_hat,sol(1),sol(2),v_ff_x,v_gg_x,p);

    % compute consumption vector
    cs = income(w,w_tilde,w_hat,sol(1),sol(2),v_ff_x,v_gg_x,p) - tr(w,w_tilde,w_hat,sol(1),v_mtr,p);


    % Use solution to compute effort as function of w
    ef = effort(w,w_tilde,w_hat,sol(1),sol(2),v_ff_x,v_gg_x,p);

    % Compute utility vector
    ut = utility(cs,ef,p);

    results.ut{j} = ut;
    results.x{j} = sol(1);
    results.xi{j} = sol(2);
    results.ef{j} = ef;
    results.cs{j} = cs;
    results.mtr{j} = v_mtr;
    results.atheta{j} = i;
    
    j=j+1;
end

clf
figure(1)
plot(cell2mat(results.atheta),cell2mat(results.xi),'.')


% % Construct splines
% cs_sp = spline(w,cs);
% ef_sp = spline(w,ef);
% ut_sp = spline(w,ut);

% % Evaluate splines
% fnval(cs_sp,5)

% % Analytics
% clf
% figure(1)
% hold all
% plot(w,ut)
% plot(0:2:100,fnval(ut_sp,0:2:100),'+')
% legend('original','fitted')
% title('Approximation of utility using a cubic spline')
% xlabel('w')
% ylabel('utility')
% hold off

% figure(2)
% hold all
% plot(w,ut)
% plot(0:5:300,fnval(ut_sp,0:5:300),'+')
% legend('original','fitted')
% title('Approximation of utility using a cubic spline - out of sample')
% xlabel('w')
% ylabel('utility')
% hold off
% for x = 1:0.1:2
%     mycc = @(xi) cc(x,xi,w,w,w,p)
%     sol = fzero(mycc,0)
% end

% plot(mtrtest(1.05,0)) 
% why does this go up to 1? Check parts in isolation
% gg_x_min_ff_x = @(x,p) gg_x(ff_x(w,w,w,x,p),p) - ff_x(w,w,w,x,p)
