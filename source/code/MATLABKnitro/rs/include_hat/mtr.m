function out = mtr(w,w_hat,x,xi,ff_x,gg_x,p)
% Calculates marginal tax rate at w
% w: wage at which to evalutate mtr
% w_hat: threshold values
% x: ratio of aggregate effective labor supplies across sectors
% xi: related to multiplier on consistency condition
% ff_x: vector of CDF of f_x, evaluated at points w (needs to have
% same dimension as w)
% gg_x: vector of CDF of g_x, evaluated at points w (needs to have
% same dimension as w)
% p: struct with parameters

out = 1 - (1 + xi .* (f_theta_x(w,w_hat,x,p)./f_x(w,w_hat,x,p) - alpha_x(x,p))) ./ (1 + (gg_x - ff_x) ./ (w .* f_x(w,w_hat,x,p)) .* (1 + 1./p.epsilon));
           
                                                  

