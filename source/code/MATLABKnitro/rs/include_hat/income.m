function out = income(w,w_hat,x,xi,ff_x,gg_x,p)
% Income corresponding to w
% w: wage at which to evalutate effort
% w_hat: threshold values
% x: ratio of aggregate effective labor supplies across sectors
% xi: related to multiplier on consistency condition
% ff_x: vector of CDF of f_x, evaluated at points w (needs to have
% same dimension as w)
% gg_x: vector of CDF of g_x, evaluated at points w (needs to have
% same dimension as w)
% p: struct with parameters

out = w .* effort(w,w_hat,x,xi,ff_x,gg_x,p);           
                                                  

