function out = tr(w,w_hat,x,mtr,p)
% Calculates tax rate at w
% w: wage grid
% w_hat: threshold values
% mtr: vector of marginal tax rates

dw = w(2) - w(1);

% Integrate over marginal tax rates, using trapezoid method
tr_temp = cumsum(mtr .* f_x(w,w_hat,x,p) .* dw);

% Integrate over temporary tax rates to obtain total tax revenue
tr_sum = sum(tr_temp .* f_x(w,w_hat,x,p) .* dw);

% The actual tax rates are now obtained by subtracting the total
% tax revenue from the individual tax rates, such that the resource
% constraint will be satisfied. Also, we adjust for the expenditure
% for the education subsidy.

% The education subsidy is paid to all individuals in the theta
% sector. Their mass is
mass_theta = sum(f_theta_x(w,w_hat,x,p) * dw);

% tax rates
out = tr_temp - tr_sum + p.tau * mass_theta;
           
                                                  

