function out = f_theta(theta, parameters)
% Skill density of theta-skill

% assign parameters
p = parameters;

mu_theta = p.mu_theta;
sigma_theta = p.sigma_theta;

if isempty(p.theta_bnds)
    out =  lognpdf(theta,mu_theta,sigma_theta);
else 
    % assign lower and upper bounds
    lb = p.theta_bnds(1);
    ub = p.theta_bnds(2);
    % compute normalization (based on total mass between bounds)
    norm = logncdf(ub,mu_theta,sigma_theta) - logncdf(lb,mu_theta, ...
                                                      sigma_theta);

    out = lognpdf(theta,mu_theta,sigma_theta) / norm;
    out(theta < lb) = 0;
    out(theta > ub) = 0;

end