function out = utility(consumption,effort,p)
% Utility function

out = consumption - effort.^(1+1./p.epsilon)/(1+1./p.epsilon);           
                                                  

