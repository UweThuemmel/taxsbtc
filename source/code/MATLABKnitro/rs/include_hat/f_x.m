function out = f_x(w,w_hat,x,p)
% Overall wage density
% w: wage at which to evalutate density
% w_hat: threshold values
% x: ratio of aggregate effective labor supplies across sectors
% p: struct with parameters


% calculate value 
out = f_theta_x(w,w_hat,x,p) + f_phi_x(w,w_hat,x,p);