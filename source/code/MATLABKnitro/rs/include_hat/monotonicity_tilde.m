function out = monotonicity_tilde(w_tilde)
% Set of linear inequality constraints, ensuring ttilde w_tilde is
% monotonically increasing. (Has to be < 0)

% w_tilde: threshold values used by f_phi_x

out = w_tilde(1:end-1) - w_tilde(2:end);