function [IneqValues,EqValues] = NonLinearConstr(x,xi,tau,w,w_hat,p)
% Evaluates non-linear inequality and equality constraints at var
% and returns their values
% x: ratio of aggregate effective labor supplies across sectors
% xi: related to multiplier on consistency condition
% tau: value of education subsidy going lumpsum to the theta sector
% w_hat: threshold values
% p: struct with parameters
% IneqValues: vector of values of inequality constraints 
% EqValues: vector of values of equality constraints 

% Assign tau to p.tau
p.tau = tau;

IneqValues = [];
EqValues = [cc(x,xi,w,w_hat,p)';indiff_hat(x,xi,w,w_hat,p)'];
