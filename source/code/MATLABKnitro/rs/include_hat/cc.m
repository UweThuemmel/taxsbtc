function out = cc(x,xi,w,w_hat,p)
% Calculates value of Consistency Condition, which is a non-linear
% equality constraint (once x is endogenous)


% w_hat: threshold values
% get dw from w for trapezoid integration
dw = w(2) - w(1);

% vectors of cdfs
v_ff_x = ff_x(w,w_hat,x,p);
v_gg_x = gg_x(v_ff_x,p);


out = (1-alpha_x(x,p)) * dw * f_theta_x(w,w_hat,x,p) * income(w,w_hat,x,xi,v_ff_x,v_gg_x,p)' - alpha_x(x,p) * dw * f_phi_x(w,w_hat,x,p) * income(w,w_hat,x,xi,v_ff_x,v_gg_x,p)';