function out = g_x(w,w_hat,x,ff_x,p)
% Calculates g_x(w)
% w: wage at which to evalutate g_x(w)
% w_hat: threshold values
% x: ratio of aggregate effective labor supplies across sectors
% ff_x: vector of CDF of f_x, evaluated at points w (needs to have
% same dimension as w)
% p: struct with parameters

out = f_x(w,w_hat,x,p).*p.r.*(1-ff_x).^(p.r-1);