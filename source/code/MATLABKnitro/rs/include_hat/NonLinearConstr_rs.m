function [IneqValues,EqValues] = NonLinearConstr_rs(x,xi,w,p)
% Evaluates non-linear inequality and equality constraints at var
% and returns their values
% x: ratio of aggregate effective labor supplies across sectors
% xi: related to multiplier on consistency condition
% p: struct with parameters
% IneqValues: vector of values of inequality constraints 
% EqValues: vector of values of equality constraints 

IneqValues = [];
EqValues = [cc(x,xi,w,w,w,p)];
