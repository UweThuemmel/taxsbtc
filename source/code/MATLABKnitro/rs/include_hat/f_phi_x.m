function out = f_phi_x(w,w_hat,x,p)
% Wage density in phi sector
% w: wage at which to evalutate density
% w_hat: threshold values
% x: ratio of aggregate effective labor supplies across sectors
% p: struct with parameters

% In case w_hat is empty, set it equal to w
if isempty(w_hat)
    w_hat = w;
end

% calculate value 
out = 1./yy_phi(x,p) .* f_phi(w./yy_phi(x,p),p) .* ff_theta(w_hat./yy_theta(x,p),p);