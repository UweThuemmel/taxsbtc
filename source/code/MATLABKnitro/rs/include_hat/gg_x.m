function out = gg_x(ff_x,p)
% Calculates values of weighting function G_x(w)
% x: ratio of effector aggregate labor supply across sectors
% w: wage grid
% ff_x: vector of CDF values
% p: struct of parameters
% out: row vector of G_x evaluated at grid w


out = 1 - (1-ff_x).^(p.r);