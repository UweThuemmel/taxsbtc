function out = f_theta_x(w,w_hat,x,p)
% Wage density in theta sector
% w: wage at which to evalutate density
% w_hat: threshold values 
% x: ratio of aggregate effective labor supplies across sectors
% p: struct with parameters

% In case w_hat is empty, set it equal to w
if isempty(w_hat)
    w_hat = w;
end

% calculate value 
% idea: add mass of those individuals who switch from Phi to Theta sector
out = 1./yy_theta(x,p) .* f_theta(w./yy_theta(x,p),p) .* ...
      (ff_phi(w./yy_phi(x,p),p) + ff_theta(w./yy_theta(x,p),p) - ff_theta(w_hat./yy_theta(x,p),p));