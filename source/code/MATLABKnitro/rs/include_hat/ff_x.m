function out = ff_x(w,w_hat,x,p)
% Calculates values of CDF
% x: ratio of effector aggregate labor supply across sectors
% w: wage grid
% w_hat: threshold values
% p: struct of parameters
% out: row vector of CDF evaluated at grid w

% get dw from w for trapezoid integration
dw = w(2) - w(1);

% for now ignoring multiplication by dw (does not matter for == 0)
out_temp = cumsum(f_x(w,w_hat,x,p) * dw);

% make sure that there are no values exceeding 1
out_temp(out_temp > 1) = 1;
out = out_temp;