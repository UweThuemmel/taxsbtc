function out = monotonicity_hat(w_hat)
% Set of linear inequality constraints, ensuring that w_hat is
% monotonically increasing. (Has to be < 0)

% w_hat: threshold values used by f_phi_x

out = w_hat(1:end-1) - w_hat(2:end);