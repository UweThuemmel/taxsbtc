% File to analyze programs

clear all

% include path for functions
addpath(['/media/data/Dropbox/PhD/projects/taxsbtc/source/code/' ...
         'MATLABKnitro/rs/include']);

addpath('/home/uwe/Programs/ampl/knitro/examples/Matlab');

% Set parameters
p.mu_theta = 3;
p.mu_phi = 3;

p.sigma_theta = 0.5;
p.sigma_phi = 0.5;

% Bounds for theta and phi
p.theta_bnds = [];
p.phi_bnds = [];

% Parameters of production function
p.alpha = 0.5;
p.a_theta = 1.5;
p.a_phi = 1;
p.rho = 1/3;

p.epsilon = 0.25;
p.r = 1.3;

% Education subsidy
p.tau = 0;

% Specify grid for w
w = 1:500;

% Investigate cc
mycc = @(x,xi) cc(x,xi,w,w,w,p)

% Plot x, xi and cc
xrange = 0.1:0.5:5;
xirange = -20:1:20;
data = zeros(length(xrange),length(xirange));

for i=1:length(xrange)
    for j=1:length(xirange)
        data(i,j) = mycc(i,j);
    end
end

mat = data;
mat(data ~= real(data)) = NaN;
% image(xrange,xirange,mat,'CDataMapping')
% 


HeatMap(mat,'RowLabels',xrange,'ColumnLabels',xirange)
