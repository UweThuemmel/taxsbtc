function [wlb,wub] = wbar(x,p)
% Calculates minimum and maximum wage
% which can then be used for integration
% 
% [wlb,wub] = wbar(x,p)
% 
% INPUTS
% x: ratio of effective aggregate labor supply across sectors
% p: struct of parameters
% 
% OUTPUTS
% [wlb,wub]: vector with lower and upper bound on wages
% 

wlb = max(p.theta_bnds(1)*yy_theta(x,p), p.phi_bnds(1) * yy_phi(x,p));
wub = max(p.theta_bnds(2) * yy_theta(x,p), p.phi_bnds(2) * ...
                   yy_phi(x,p));



