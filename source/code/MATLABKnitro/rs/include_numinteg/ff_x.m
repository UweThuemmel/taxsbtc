function out = ff_x(w,x,p)
% Calculates value of CDF at wage w
% 
% OUT = FF_X(w,w_tilde,w_hat,x,p)
% 
% INPUTS
% w: wage at which to evaluate CDF
% x: ratio of effective aggregate labor supply across sectors
% p: struct of parameters
% 
% OUTPUTS
% out: scalar of CDF evaluated at w
% 
% 
% 
% See also F_X

% Define anonymous function for overall wage density

f = @(w) f_x(w,x,p);

% Assign lower and upper bound wage
[wlb,wub] = wbar(x,p);

% Handle a wage grid correctly
out = zeros(1,length(w));

for i=1:length(w)
    out(i) = quadgk(f,wlb,w(i));
end

% % Alternative approach - was not faster
% for i=1:length(w)
%     if i == 1
%         out(1) = quad(f,wlb,w(1));
%     else
%         out(i) = quad(f,out(i-1),w(i));
%     end 
% end


% Assign 1 to those elements which exceed 1 - p.tolerance
out(out>=1-p.tolerance)=1;



