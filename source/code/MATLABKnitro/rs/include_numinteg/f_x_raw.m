function out = f_x_raw(w,x,p)
% Non-normalized overall wage density (technically not yet a density)
% w: wage at which to evalutate density
% w_tilde: threshold values used by f_theta_x
% w_hat: threshold values used by f_phi_x
% x: ratio of aggregate effective labor supplies across sectors
% p: struct with parameters


% calculate value 
out = f_theta_x_raw(w,x,p) + f_phi_x_raw(w,x,p);