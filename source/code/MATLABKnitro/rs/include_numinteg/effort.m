function out = effort(w,x,xi,p)
% Effort exerted at w
% w: wage at which to evalutate effort
% x: ratio of aggregate effective labor supplies across sectors
% xi: related to multiplier on consistency condition
% p: struct with parameters

out = w.^p.epsilon .* (1-mtr(w,x,xi,p)).^p.epsilon;           
                                                  

