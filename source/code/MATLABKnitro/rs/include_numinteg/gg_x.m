function out = gg_x(v_ff_x,p)
% Calculates values of weighting function G_x(w)
% 
% OUT = GG_X(w,x,p)
% 
% INPUTS
% v_ff_x: vector of ff_x values
% x: ratio of effective aggregate labor supply across sectors
% p: struct of parameters
% 
% OUTPUTS
% out: row vector of G_x evaluated at grid w
% 
% out = 1 - (1-ff_x).^(p.r);

out = 1 - (1-v_ff_x).^(p.r);