function out = normalize(p)
% Calculates normalization based on total mass in the economy by
% integrating over the joint skill density
% 
% OUT = NORMALIZE(p)
% 
% INPUTS
% p: struct of parameters
% 
% OUTPUTS
% out: scalar of total mass in the economy
 
% Define anonymous function for joint skill density
f = @(theta,phi) f_theta(theta,p)*f_phi(phi,p)

% total mass in the economy
out = dblquad(f,p.theta_bnds(1),p.theta_bnds(2),p.phi_bnds(1),p.phi_bnds(2));

