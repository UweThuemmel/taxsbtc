function out = normalization(x,p)
% Calculates normalization based on total mass in the economy
% 
% OUT = NORMALIZATION(x,p)
% 
% INPUTS
% x: ratio of effector aggregate labor supply across sectors
% p: struct of parameters
% 
% OUTPUTS
% out: scalar of total mass in the economy
% 

% total mass in the economy
[wlb,wub] = wbar(x,p);

f = @(w) f_x_raw(w,x,p);

out = quadgk(f,wlb,wub);

