function out = f_theta_x_raw(w,x,p)
% Non-normalized Wage density in theta sector
% 
% OUT = F_THETA_X_RAW(w,x,p)
% 
% INPUTS
% w: wage at which to evalutate density
% x: ratio of aggregate effective labor supplies across sectors
% p: struct with parameters
% 
% OUTPUTS
% out: vector of sectoral density evaluated at wage grid w
% 
% out = 1./yy_theta(x,p) .* f_theta(w./yy_theta(x,p),p) .* ff_phi(w./yy_phi(x,p),p);
% 
% See also YY_THETA, YY_PHI, F_THETA, FF_PHI

% anonymous function for density
f = @(w) 1./yy_theta(x,p) .* f_theta(w./yy_theta(x,p),p) .* ff_phi(w./ ...
                                                  yy_phi(x,p),p);

% make sure that there is no jump at the top - here the sector
% specific upper bound is important

wub = p.theta_bnds(2) * yy_theta(x,p);

f_top = f(wub);

% make sure there is no jump at the bottom - note: the lower bound
% is the maximum lower bound across both sectors
wlb = max(p.theta_bnds(1)*yy_theta(x,p), p.phi_bnds(1) * yy_phi(x,p));
f_bottom = f(wlb);

% subtract max of top and bottom density value
out = f(w) - max(f_top,f_bottom);

% set potentially negative density values to zero
out(out<0) = 0;
