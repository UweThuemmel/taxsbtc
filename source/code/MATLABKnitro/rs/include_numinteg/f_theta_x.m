function out = f_theta_x(w,x,p)
% Wage density in theta sector
% 
% OUT = F_THETA_X(w,x,p)
% 
% INPUTS
% w: wage at which to evalutate density
% x: ratio of aggregate effective labor supplies across sectors
% p: struct with parameters, including p.norm for normalization
% 
% OUTPUTS
% out: vector of sectoral density evaluated at wage grid w
% 
% out = f_theta_x_raw(w,x,p) ./ p.norm;
% 
% See also F_THETA_X_RAW, NORMALIZATION


% calculate value 
out = f_theta_x_raw(w,x,p) ./ normalization(x,p);