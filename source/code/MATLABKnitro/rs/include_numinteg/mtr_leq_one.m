function out = mtr_leq_one(x,xi,w,w_tilde,w_hat,p)
% Inequality constraint, ensuring that the maximum mtr is leq
% The value of this condition has to be <= 0.
% 
% INPUTS
% x: ratio of aggregate effective labor supplies across sectors
% xi: related to multiplier on consistency condition
% w: wage at which to evalutate mtr
% w_tilde: threshold values used by f_theta_x
% w_hat: threshold values used by f_phi_x
% p: struct with parameters
% 
% OUTPUTS
% out: max of mtr - 1
% 
% See also FF_X, GG_X, MTR

v_ff_x = ff_x(w,w_tilde,w_hat,x,p);
v_gg_x = gg_x(v_ff_x,p);

% vector of marginal tax rates
v_mtr = mtr(w,w_tilde,w_hat,x,xi,v_ff_x,v_gg_x,p);

out = max(v_mtr) - 1;