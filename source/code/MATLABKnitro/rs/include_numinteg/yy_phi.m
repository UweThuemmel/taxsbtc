function out = yy_phi(x,parameters)
% Calculates marginal product yy_phi(x)
% 
% OUT = YY_PHI(x,parameters)
% 
% INPUTS
% x: ratio of aggregate effective labor supplies across sectors
% parameters: struct with parameters
%
% OUTPUTS
% out: scalar value of yy_phi
% 
% out = (alpha./(1-alpha).*(a_theta./a_phi.*x).^rho+1).^((1-rho)./rho).*(1-alpha).^(1./rho).*a_phi;
% assign parameters
p = parameters;
a_theta = p.a_theta;
a_phi = p.a_phi;
rho = p.rho;
alpha = p.alpha;

% calculate value of yy_phi
out = (alpha./(1-alpha).*(a_theta./a_phi.*x).^rho+1).^((1-rho)./rho).*(1-alpha).^(1./rho).*a_phi;