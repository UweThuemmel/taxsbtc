function out = income(w,x,xi,p)
% Income corresponding to w
% 
% OUT = INCOME(w,w_tilde,w_hat,x,xi,ff_x,gg_x,p)
% 
% INPUTS
% w: wage at which to evalutate income
% x: ratio of aggregate effective labor supplies across sectors
% xi: related to multiplier on consistency condition
% p: struct with parameters
% 
% OTUPUT
% out: vector with incomes, evaluated at w
% 
% out = w .* effort(w,x,xi,p);           
% 
% See also EFFORT


out = w .* effort(w,x,xi,p);           
                                                  

