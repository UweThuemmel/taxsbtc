function out = tr(w,x,xi,rev,p)
% Calculates tax at w
% 
% OUT = TR(w,w_tilde,w_hat,x,mtr,p)
% 
% INPUTS
% w: wage grid
% mtr: vector of marginal tax rates
% rev: total tax revenue
% p: struct of parameters
% 
% OUTPUTS
% out: vector of tax rates, evaluated at w
% 
% See also F_X, F_THETA_X


% Integrate over marginal tax rates
f_mtr = @(w) mtr(w,x,xi,p);
[wlb,wub] = wbar(x,p);

tr_temp = zeros(1,length(w));
for i = 1:length(w)
    tr_temp(i) = quad(f_mtr,wlb,w(i));
end


% The actual tax rates are now obtained by subtracting the total
% tax revenue from the individual tax rates, such that the resource
% constraint will be satisfied. Also, we adjust for the expenditure
% for the education subsidy.

% tax rates
out = tr_temp - rev;
           
                                                  

