function out = f_x(w,x,p)
% Overall wage density
% 
% OUT = F_X(w,x,p) 
% 
% INPUTS
% w: wage at which to evalutate density
% x: ratio of aggregate effective labor supplies across sectors
% p: struct with parameters
% 
% OUTPUTS
% out: vector of overall density evaluated at wage grid w
% 
% out = f_theta_x(w,x,p) + f_phi_x(w,x,p);
% 
% See also F_THETA_X, F_PHI_X

% calculate value 
out = f_theta_x(w,x,p) + f_phi_x(w,x,p);