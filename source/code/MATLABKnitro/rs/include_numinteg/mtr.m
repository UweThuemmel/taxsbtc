function out = mtr(w,x,xi,p)
% Calculates marginal tax rate at w using the formula from R&S
% (2013), QJE
% 
% OUT = MTR(w,w_tilde,w_hat,x,xi,ff_x,gg_x,p)
% 
% INPUTS
% w: wage at which to evalutate mtr
% w_tilde: threshold values used by f_theta_x
% w_hat: threshold values used by f_phi_x
% x: ratio of aggregate effective labor supplies across sectors
% xi: related to multiplier on consistency condition
% ff_x: vector of CDF of f_x, evaluated at points w (needs to have
% same dimension as w)
% gg_x: vector of CDF of g_x, evaluated at points w (needs to have
% same dimension as w)
% p: struct with parameters
% 
% OUTPUT
% out: vector of marginal tax rates evaluated at w
% 
% See also: F_THETA_X, F_X, ALPHA_X

nominator = 1 + xi .* (  f_theta_x(w,x,p)./f_x(w,x,p) ...
                         - alpha_x(x,p)  );

v_ff_x = ff_x(w,x,p);

denominator = 1 + (gg_x(v_ff_x,p) - v_ff_x) ./ (w .* f_x(w,x,p)) ...
    .* (1 + 1./p.epsilon);

% clf
% figure(1)
% plot(w,nominator)
% title('Numerator')

% figure(2)
% plot(w,denominator)
% title('Denominator')

% figure(3)
% plot(w,f_theta_x(w,x,p)./f_x(w,x,p))
% title('fthetax/fx')


% figure(4)
% plot(w,f_theta_x(w,x,p),w,f_x(w,x,p))
% title('fthetax,fx')

% figure(5)
% plot(w,alpha_x(x,p))
% title('alphax')

% figure(6)
% plot(w,(gg_x(v_ff_x,p) - v_ff_x))
% title('ggx - ffx')

out = real(1 - nominator ./ denominator);

% set nan resulting from division by zero due to zero density to a
% zero mtr. This won't matter in the SWF later anyway, as all
% related values will be weighted with zero.
out(isnan(out))=0;

% figure(7)
% plot(w,out)
% title('mtr')
           
                                                  

% % % (gg_x - ff_x)

% % % w .* f_x(w,w_tilde,w_hat,x,p)

% figure(8)
% plot(w,(gg_x(v_ff_x,p) - v_ff_x)./(w .* f_x(w,x,p)))
% title('(ggx - ffx)/w*fx')

% figure(9)
% plot(w,(w .* f_x(w,x,p)))
% title('w*fx')

% w1 = 160:180;

% figure(10)
% plot(w1,(w1 .* f_x(w1,x,p)))
% title('w1*fx')

% v_ff_x1 = ff_x(w1,x,p);
% figure(11)
% plot(w1,(gg_x(v_ff_x1,p) - v_ff_x1))
% title('ggx - ffx at w1')
