function out = f_phi_x(w,x,p)
% Wage density in phi sector
% 
% OUT = F_PHI_X(w,x,p)
% 
% INPUTS
% w: wage at which to evalutate density
% x: ratio of aggregate effective labor supplies across sectors
% p: struct with parameters, including p.norm for normalization
% 
% OUTPUTS
% out: vector of sectoral density evaluated at wage grid w
% 
% out = f_phi_x_raw(w,x,p)./ p.norm
% 
% See also F_PHI_X_RAW, NORMALIZATION

% calculate value 
out = f_phi_x_raw(w,x,p)./ normalization(x,p);