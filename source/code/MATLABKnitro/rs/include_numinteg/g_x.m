function out = g_x(w,x,v_ff_x,p)
% Calculates g_x(w)
% w: wage at which to evalutate g_x(w)
% x: ratio of aggregate effective labor supplies across sectors
% v_ff_x: vector of CDF of f_x, evaluated at points w (needs to have
% same dimension as w)
% p: struct with parameters

out = f_x(w,x,p).*p.r.*(1-v_ff_x).^(p.r-1);