function out = swf(w,x,xi,p)
% Evaluates objective function (social welfare function) at w,
% where w needs to be a scalar!
%
% OUT = SWF(x,xi,p)
%
% w: wage at which to evaluate swf
% x: ratio of aggregate effective labor supplies across sectors
% xi: related to multiplier on consistency condition
% p: struct with parameters
%
% 
% See also FF_X, GG_X, MTR, INCOME, TR, EFFORT, UTILITY


%% Calculate  distributions, mtr, consumption, etc.

% compute vector of CDF values evaluated at wage grid
v_ff_x = ff_x(w,x,p);

% calculate total tax revenue
% anonymous function for mtr
f_mtr = @(w) mtr(w,x,xi,p);

[wlb,wub] = wbar(x,p);
rev = quad(f_mtr,wlb,wub);

v_taxes = tr(w,x,xi,rev,p);


% compute consumption vector (income(w) - tax(w))
consumption = income(w,x,xi,p) - v_taxes;

% compute effort vector
v_effort = effort(w,x,xi,p);

% compute value of objective.
out = utility(consumption,v_effort,p) .* g_x(w,x,v_ff_x,p)
