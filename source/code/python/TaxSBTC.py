from scipy import integrate
from scipy.stats import lognorm, expon
from scipy.optimize import fsolve
from collections import defaultdict
import numpy as np
import pylab
import copy

pars = {'mu_theta':0,'sigma_theta':0.4711,'epsilon':0.3,'sigma':1.41,\
        'gamma':0.4774,'p':1/0.684 * 0.26,'lb':0.0001,'ub':10,'AL':1,\
        'eta':1,'r':1}

class taxsbtc:
    """Class representing the model of optimal taxation and education
    subsidies with skill-biased technological change."""

    def __init__(self,mu_theta,sigma_theta,epsilon,sigma,gamma,p,lb,ub,AL,eta,r):
        self.mu_theta = mu_theta 
        self.sigma_theta = sigma_theta 
        self.epsilon = epsilon 
        self.sigma = sigma 
        self.gamma = gamma 
        self.p = p 
        self.lb = lb 
        self.ub = ub 
        self.AL = AL 
        self.eta = eta 
        self.r = r 
       
    def f(self,theta):
        """skill density"""
        return lognorm([self.sigma_theta],loc=self.mu_theta).pdf(theta)
        
    def ff(self,theta):
        """skill distribution"""
        return lognorm([self.sigma_theta],loc=self.mu_theta).cdf(theta)

    # Wages

    def wL(self,AH,L,H):
        '''wage of the low-skilled, derived from FOC of CES production function'''
        return self.AL *(self.AL* L)**(-1 + (-1 + self.sigma)/self.sigma)* self.gamma*((AH* H)**((-1 + self.sigma)/self.sigma) *(1 - self.gamma) + (self.AL*L)**((-1 + self.sigma)/self.sigma)* self.gamma)**(-1 + self.sigma/(-1 + self.sigma))

    def wH(self,AH,L,H):
        '''wage of the high-skilled, derived from FOC of CES production function'''
        return AH *(AH *H)**(-1 + (-1 + self.sigma)/self.sigma)* (1 - self.gamma) *((AH *H)**((-1 + self.sigma)/self.sigma)* (1 - self.gamma) + (self.AL *L)**((-1 + self.sigma)/self.sigma)* self.gamma)**(-1 + self.sigma/(-1 + self.sigma))

    # Critical theta

    def thetastar(self,s,t,AH,L,H):
        '''critical skill above which people choose college'''
        numerator = (self.p - s)*(1 + self.epsilon)
        denominator = ((1 - t)**(1 + self.epsilon))*(self.wH(AH,L,H)**(1 + self.epsilon) - self.wL(AH,L,H)**(1 + self.epsilon))
        return (numerator/denominator)**(1/(1 + self.epsilon))

    # Labor supplies
    
    def hH(self,theta,AH,L,H,t):
        '''labor supply of the high-skilled'''
        return (theta*self.wH(AH,L,H)*(1 - t))**self.epsilon

    def hL(self,theta,AH,L,H,t):
        '''labor supply of the low-skilled'''
        return (theta*self.wL(AH,L,H)*(1 - t))**self.epsilon

    # Excess labor supplies

    def excessH(self,AH,L,H,s,t):
        '''excess supply of high-skill labor'''
        ls = lambda theta: self.hH(theta,AH,L,H,t)*theta*self.f(theta)
        return integrate.quad(ls,self.thetastar(s,t,AH,L,H),self.ub)[0] - H


    def excessL(self,AH,L,H,s,t):
        """Excess supply of low-skill labor.

        Args:
            AH: productivity of the high-skilled
            L: labor supply / demand of the low-skilled
            H: labor supply / demand of the high-skilled
            s: subsidy
            t: linear tax rate
        """
        ls = lambda theta: self.hL(theta,AH,L,H,t)*theta*self.f(theta)
        return integrate.quad(ls,self.lb,self.thetastar(s,t,AH,L,H))[0] - L

    # Labor market clearing

    def clearlabor(self,AH,s,t,init):
        """Calculate labor market clearing levels of L and H, taking all policy variables as given.

            Args:
                s: subsidy
                t: tax rate
                init: vector of initial points            
        """
        # define multivariate function for labor market clearing
        fun = lambda x: [self.excessL(AH,x[0],x[1],s,t),self.excessH(AH,x[0],x[1],s,t)]
        sol = fsolve(fun,init,full_output=1)   
        print sol[-1]
        return sol[0]
    
    # Incomes

    def zL(self,theta,AH,L,H,t):
        """Income for high-school type theta.

        Args
            theta: skill
            AH: productivity of the high-skilled
            L: labor supply / demand of the low-skilled
            H: labor supply / demand of the high-skilled
            t: tax rate
           """
        return self.hL(theta,AH,L,H,t) * theta * self.wL(AH,L,H)


    def zH(self,theta,AH,L,H,t):
        """Income for college-school type theta.

        Args
            theta: skill
            AH: productivity of the high-skilled
            L: labor supply / demand of the low-skilled
            H: labor supply / demand of the high-skilled
            t: tax rate
           """
        return self.hH(theta,AH,L,H,t) * theta * self.wH(AH,L,H)

    def zbarL(self,AH,L,H):
        """Average income in the high-school sector.


        Args:
            AH: productivity of the high-skilled
            L: labor supply / demand of the low-skilled
            H: labor supply / demand of the high-skilled
        """
        return self.wL(AH,L,H) * L

    def zbarH(self,AH,L,H):
        """Average income in the college sector.


        Args:
            AH: productivity of the high-skilled
            L: labor supply / demand of the low-skilled
            H: labor supply / demand of the high-skilled
        """
        return self.wH(AH,L,H) * H

    def zbar(self,AH,L,H):
        """Average income.


        Args:
            AH: productivity of the high-skilled
            L: labor supply / demand of the low-skilled
            H: labor supply / demand of the high-skilled
        """
        return self.zbarL(AH,L,H) + self.zbarH(AH,L,H)
        
    # Indirect utilities
        
    def vL(self,theta,AH,L,H,b,t):
        """Indirect utility of the low-skilled.
        
        Args:
            AH: productivity of the high-skilled
            L: labor supply / demand of the low-skilled
            H: labor supply / demand of the high-skilled
            b: lump-sum transfer (benefit)
            t: linear tax rate
        """
        
        return  b + ((theta*self.wL(AH, L, H)*(1 - t))**(1 + self.epsilon))/(1 + self.epsilon)
    
    def vH(self,theta,AH,L,H,b,s,t):
        """Indirect utility of the high-skilled.
        
        Args:
            AH: productivity of the high-skilled
            L: labor supply / demand of the low-skilled
            H: labor supply / demand of the high-skilled
            b: lump-sum transfer (benefit)
            s: subsidy
            t: linear tax rate
        """

    # Compute earnings
        return  b + ((theta*self.wH(AH, L, H)*(1 - t))**(1 + self.epsilon))/( 1 + self.epsilon) - self.p + s

    def comp_earnings(self,theta,s,t,AH,L,H):
        """Compute earnings for a vector of theta's.

            theta: array of thetas
            s: subsidy
            t: tax rate
            AH: productivity of the high-skilled - via production function
            L: labor supply / demand of the low-skilled
            H: labor supply / demand of the high-skilled
        """


        # pre-compute thetastar
        thst = self.thetastar(s,t,AH,L,H)

        # create out array that same as theta vector
        out = copy.deepcopy(theta)

        
        # earnings of low-skilled
        out[theta<thst]=self.hL(out[theta<thst],AH,L,H,t)*self.wL(AH,L,H)*out[theta<thst]

        # earnings of high-skilled
        out[theta>=thst]=self.hH(out[theta>=thst],AH,L,H,t)*self.wH(AH,L,H)*out[theta>=thst]
        
        return out

        
    def weight(self,theta,b,s,t,AH,L,H):
        """Welfare weight for type theta.
        
        Args:
            b: lump-sum transfer (benefit)
            s: subsidy
            t: linear tax rate
            theta: ability
            AH: productivity of the high-skilled
            L: labor supply / demand of the low-skilled
            H: labor supply / demand of the high-skilled

        """
        return expon.pdf(theta,scale=self.r)/self.eta


    # Distributional characteristic

    def xi(self,s,t,AH,L,H,b=0):
        """Compute distributional characteristic xi.
        
        Args:
            AH: productivity of the high-skilled
            L: labor supply / demand of the low-skilled
            H: labor supply / demand of the high-skilled
            s: subsidy
            t: linear tax rate
            b: lump-sum transfer (benefit)
        """        
        
        # anonymous functions to work as integrands
        integL = lambda theta: self.weight(theta,b,s,t,AH,L,H) * self.zL(theta,AH,L,H,t) * self.f(theta)
        integH = lambda theta: self.weight(theta,b,s,t,AH,L,H) * self.zH(theta,AH,L,H,t) * self.f(theta)
        integWeight = lambda theta: self.weight(theta,b,s,t,AH,L,H) * self.f(theta) #integrand for cumulative weight
        
        thst = self.thetastar(s,t,AH,L,H)        
        
        # average weights
        gbar = integrate.quad(integWeight,self.lb,self.ub)[0]
        gbarL = integrate.quad(integWeight,self.lb,thst)[0]
        gbarH = integrate.quad(integWeight,thst,self.ub)[0]
        
        # compute xi
        xi =  1-1./(self.zbar(AH,L,H) * gbar)\
        * (integrate.quad(integL,self.lb,thst)[0]\
        + integrate.quad(integH,thst,self.ub)[0])
        
        # compute xi_L and xi_H
        xi_L = 1-self.ff(thst) * 1./(self.zbarL(AH,L,H) * gbarL)\
        * (integrate.quad(integL,self.lb,thst)[0])
        xi_H = 1-(1-self.ff(thst)) * 1./(self.zbarH(AH,L,H) * gbarH)\
        * (integrate.quad(integH,thst,self.ub)[0])
        
        return {'xi':xi,'xi_L':xi_L,'xi_H':xi_H,'gbar':gbar,'gbarL':gbarL,'gbarH':gbarH}


        
        
        
    # Compute equilibrium
        
    def equilibrium(self,AH,b,s,t,init=[1,1]):
        """Compute labor market equilibrium, taking technology, subsidy and tax
        as given.
        
        Args:
            AH: productivity of the high-skilled
            s: subsidy
            t: linear tax rate
            init: list of initial values for L and H for labor market clearing
                    
        """
        [L,H] = self.clearlabor(AH,s,t,[1,1])
        wL_val = self.wL(AH,L,H)
        wH_val = self.wH(AH,L,H)
        zbar_val = self.zbar(AH,L,H)
        zbarL_val = self.zbarL(AH,L,H)
        zbarH_val = self.zbarH(AH,L,H)
        xis = self.xi(s,t,AH,L,H)
        xi_val = xis['xi']
        xi_L = xis['xi_L']
        xi_H = xis['xi_H']
        gbar = xis['gbar']
        gbarL = xis['gbarL']
        gbarH = xis['gbarH']
        
        # save L and H to object in order to use them as new initial values        
        self.L = L
        self.H = H
        # critical theta
        thst = self.thetastar(s,t,AH,L,H)
        # share of college graduates
        empL = self.ff(thst)
        empH = 1-empL
        # wage rate ratio
        wrat = wH_val/wL_val
        # vector of earnings
        thetagrid = np.linspace(self.lb,self.ub,100)
#        pylab.plot(thetagrid)
        earnings = self.comp_earnings(thetagrid,s,t,AH,L,H)
#        pylab.plot(thetagrid)
        
        # means of incomes
        mean_income_L = (wL_val * L)/empL
        mean_income_H = (wH_val * H)/empH
        
#        mean_income_L = integrate.quad(integL,self.lb,self.thetastar(s,t,AH,L,H))[0]
#        mean_income_H = integrate.quad(integH,self.thetastar(s,t,AH,L,H),self.ub)[0]
        mean_income = empL * mean_income_L + empH * mean_income_H
        
        # within variances of incomes
        ## anonymous functions to work as integrands
        integL = lambda theta: (self.zL(theta,AH,L,H,t) - mean_income_L)**2 * self.f(theta)
        integH = lambda theta: (self.zH(theta,AH,L,H,t) - mean_income_H)**2 * self.f(theta)
        integL_total = lambda theta: (self.zL(theta,AH,L,H,t) - mean_income)**2 * self.f(theta)
        integH_total = lambda theta: (self.zH(theta,AH,L,H,t) - mean_income)**2 * self.f(theta)
        
        var_income_L = integrate.quad(integL,self.lb,self.thetastar(s,t,AH,L,H))[0]
        var_income_H = integrate.quad(integH,self.thetastar(s,t,AH,L,H),self.ub)[0]
        var_income_between = empL * (mean_income_L - mean_income)**2 + empH * (mean_income_H - mean_income)**2
        
        var_income_total = integrate.quad(integL_total,self.lb,self.thetastar(s,t,AH,L,H))[0]\
        + integrate.quad(integH_total,self.thetastar(s,t,AH,L,H),self.ub)[0]
        
        # weight
        weightgrid = self.weight(thetagrid,b,s,t,AH,L,H)
        
        # indirect utilities
        vLgrid = self.vL(thetagrid,AH,L,H,b,t)
        vHgrid = self.vH(thetagrid,AH,L,H,b,s,t)
        
        # delta
        delta = thst**2 * self.f(thst) * (self.hL(thst,AH,L,H,t)/L + self.hH(thst,AH,L,H,t)/H)
        
        #alpha
        alpha = zbarH_val / zbar_val
        
        # Return results
        return [('AH',AH),('L',L),('H',H),('wL',wL_val),('wH',wH_val),\
        ('xi',xi_val),('xi_L',xi_L),('xi_H',xi_H),('thst',thst,),('empL',empL),('empH',empH),\
        ('wrat',wrat),('earnings',earnings),\
        ('f',self.f(thetagrid)),('mean_income_L',mean_income_L),\
        ('mean_income_H',mean_income_H),('mean_income',mean_income),\
        ('var_income_L',var_income_L),('var_income_H',var_income_H),\
        ('var_income_between',var_income_between),('var_income_total',var_income_total),\
        ('weightgrid',weightgrid),('vLgrid',vLgrid),('vHgrid',vHgrid),\
        ('gbar',gbar),('gbarL',gbarL),('gbarH',gbarH),\
        ('zbar',zbar_val),('zbarL',zbarL_val),('zbarH',zbarH_val),('thetagrid',thetagrid),\
        ('delta',delta),('alpha',alpha)]
        #        return {'AH':[AH],'L':[L],'H':[H],'wL':[wL_val],'wH':[wH_val],'xi':[xi_val]}
        
    def technicalchange(self,AH_range,b,s,t,init=[1,1]):
        """Compute labor market equilibrium for an array of AH values.
        
        Args:
            AH_range: array of AH values
            s: subsidy
            t: tax rate
            init: list of initial values L, H for labor market clearing
        """
        # set initial values (later on use values from previous step)
        self.L = init[0]
        self.H = init[1]
        temp = []
        for AH in AH_range:
            L = self.L
            H = self.H
            print L
            print H
            temp.extend(self.equilibrium(AH,b,s,t,[L,H]))
   
        # Save results to dictionary of lists
        results = defaultdict(list)
        for k,v in temp:
            results[k].append(v)
        return results
        
      
class taxsbtc_endogenous_weights(taxsbtc):
    """Class which modifies taxsbtc to allow for endogenous welfare weights."""
    


    def psi_prime(self,theta,b,s,t,AH,L,H):
        """First derivative of SWF, evaluated at indirect utility corresponding
        to theta.
        
        Args:
            theta: ability
            s: subsidy
            t: tax rate
            AH: skill-bias
            L: aggregate labor supply of low-skilled
            H: aggregate labor supply of high-skilled
        """
        # specify first derivative, for now focus on log SWF
        swf_prime = lambda v: 1/v        
        
        # compute thetastar
        thst = self.thetastar(s,t,AH,L,H)
        
        if type(theta)==float:        
        
            if theta <= thst:
                return swf_prime(self.vL(theta,AH,L,H,b,t))
            else:
                return swf_prime(self.vH(theta,AH,L,H,b,s,t))
    
        else:
            assert type(theta) == np.ndarray, 'theta has to be a float or a numpy array'
            out = copy.deepcopy(theta)
            out[theta<=thst]=swf_prime(self.vL(out[theta<=thst],AH,L,H,b,t))
            out[theta>thst]=swf_prime(self.vH(out[theta>thst],AH,L,H,b,s,t))
            return out
            
    def weight(self,theta,b,s,t,AH,L,H):
        """Endogenous social welfare weight.
        
        Args:
            theta: ability
            s: subsidy
            t: tax rate
            AH: skill-bias
            L: aggregate labor supply of low-skilled
            H: aggregate labor supply of high-skilled
        """
        return self.psi_prime(theta,b,s,t,AH,L,H)/self.eta        
            
            
a = taxsbtc(**pars)
b = taxsbtc_endogenous_weights(**pars)
#out = a.technicalchange(arange(1,2,0.1),0.2,0.3)
