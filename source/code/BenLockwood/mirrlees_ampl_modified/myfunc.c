#include "math.h"	/* for sqrt */
#include "funcadd.h"	/* includes "stdio1.h" */


real foo(arglist *al){

/* the return type is real, which is nothing but a double: see the definition in header file you downloaded */

/* Now, you'll get the arguments passed by AMPL */

real arg1 = al->ra[0];
real arg2 = al->ra[1];
real arg3 = al->ra[2];

/* return the result */
return (arg1+arg2+arg3);

}

void funcadd(AmplExports *ae){

/* 1st param: name of the function to import to AMPL.
2nd param: pointer to the function "foo"
3rd param: function type...you can get more info from the header file by checking enumarated type FUNCADD_TYPE.
4th param: Number of arguments passed to function "foo".
5th param: funcinfo
*/
addfunc("foo", (rfunc)foo, 0, 3, 0);
}
