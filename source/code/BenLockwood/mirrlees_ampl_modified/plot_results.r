# File for plotting tax schedule
library(ggplot2) 

d <- read.csv("/media/data/Dropbox/PhD/projects/taxsbtc/source/code/BenLockwood/mirrlees_ampl_modified/out_my_mirrlees_ampl_ver01.txt")
d2 <- read.csv("/media/data/Dropbox/PhD/projects/taxsbtc/source/code/BenLockwood/mirrlees_ampl_modified/ampl_output_uwe.txt")
S = 1e-4
d$yScaled = (d$y*1e5)*S

rs <-read.csv("/media/data/Dropbox/PhD/projects/taxsbtc/source/code/BenLockwood/mirrlees_ampl_modified/out_rs_ver01.txt")
rs2 <-read.csv("/media/data/Dropbox/PhD/projects/taxsbtc/source/code/BenLockwood/mirrlees_ampl_modified/out_rs_ver02.txt")
rs_fixedx <-read.csv("/media/data/Dropbox/PhD/projects/taxsbtc/source/code/BenLockwood/mirrlees_ampl_modified/out_rs_fixedx.txt")

orig <- read.csv("/media/data/Dropbox/PhD/projects/taxsbtc/source/code/BenLockwood/mirrlees_ampl_modified/ampl_output.txt")

# qplot(d$yScaled,d$mtr,geom="line", xlim=c(min(d$yScaled),500))
# qplot(d$yScaled,d$mtr,geom="line")
# qplot(d$yScaled,d$mtr,geom="line", xlim=c(min(d$yScaled),50))
# qplot(d$y,d$mtr,geom="line")
#qplot(d2$y,d2$mtr,geom="line")
#qplot(rs$y,rs$mtr,geom="line")
#qplot(rs2$y,rs2$mtr,geom="line")
#qplot(orig$y,orig$mtr,geom="line")
#qplot(rs_fixedx$y,rs_fixedx$mtr,geom="line")
#qplot(rs2$w,rs2$f_x,geom="line")
#qplot(rs2$w,rs2$g,geom="line")
#ggplot(data=rs2, aes(w)) + geom_line(aes(y=f_x,colour="f_x")) + geom_line(aes(y=g,colour="g"))

ampl <- list()
i <- 1
for (atheta in seq(1,2,.1))
{
ampl[[i]] <- tryCatch({read.csv(paste("/media/data/Dropbox/PhD/projects/taxsbtc/source/code/BenLockwood/mirrlees_ampl_modified/out_rs_ver01_atheta_",atheta,".txt",sep=""))},
error = function(e)
print("Not found")
)
i <- i+1
}

## a11 <- read.csv("/media/data/Dropbox/PhD/projects/taxsbtc/source/code/BenLockwood/mirrlees_ampl_modified/out_rs_ver01_atheta_1.1.txt")

## a12 <- read.csv("/media/data/Dropbox/PhD/projects/taxsbtc/source/code/BenLockwood/mirrlees_ampl_modified/out_rs_ver01_atheta_1.2.txt")


## plot for mtr
ggplot(data = ampl[[1]], aes(y)) + geom_line(aes(y=mtr,colour="1")) + geom_line(data = ampl[[2]],aes(y=mtr,colour="1.1"))+ geom_line(data = ampl[[3]],aes(y=mtr,colour="1.2"))+ geom_line(data = ampl[[4]],aes(y=mtr,colour="1.3"))+ geom_line(data = ampl[[5]],aes(y=mtr,colour="1.4"))+ geom_line(data = ampl[[6]],aes(y=mtr,colour="1.5"))+ geom_line(data = ampl[[7]],aes(y=mtr,colour="1.6"))+ geom_line(data = ampl[[8]],aes(y=mtr,colour="1.7"))+ geom_line(data = ampl[[9]],aes(y=mtr,colour="1.8"))+ geom_line(data = ampl[[10]],aes(y=mtr,colour="1.9"))+ geom_line(data = ampl[[11]],aes(y=mtr,colour="2"))

## plot for wage density
ggplot(data = ampl[[1]], aes(y)) + geom_line(aes(y=f_x,colour="1")) + geom_line(data = ampl[[2]],aes(y=f_x,colour="1.1"))+ geom_line(data = ampl[[3]],aes(y=f_x,colour="1.2"))+ geom_line(data = ampl[[4]],aes(y=f_x,colour="1.3"))+ geom_line(data = ampl[[5]],aes(y=f_x,colour="1.4"))+ geom_line(data = ampl[[6]],aes(y=f_x,colour="1.5"))+ geom_line(data = ampl[[7]],aes(y=f_x,colour="1.6"))+ geom_line(data = ampl[[8]],aes(y=f_x,colour="1.7"))+ geom_line(data = ampl[[9]],aes(y=f_x,colour="1.8"))+ geom_line(data = ampl[[10]],aes(y=f_x,colour="1.9"))+ geom_line(data = ampl[[11]],aes(y=f_x,colour="2"))
