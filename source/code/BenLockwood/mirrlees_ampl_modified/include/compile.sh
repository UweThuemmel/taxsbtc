#!/bin/bash 
echo "Compiling 32bit version"
gcc -I ~/Downloads/gsl-1.16 -m32 -c my_lognormal.c
echo "Building library"
gcc -m32 -shared -o my_lognormal.dll my_lognormal.o funcadd.h

echo "Compiling 64bit version"
gcc -I ~/Downloads/gsl-1.16 -m64 -c -fPIC my_lognormal.c
echo "Building library"
gcc -m64 -shared -o my_lognormal_64.dll my_lognormal.o funcadd.h
