#include "funcadd.h"	/* includes "stdio1.h" */
#include "lognormal.h"

/* Use definition of lognormal PDF */
double
gsl_ran_lognormal_pdf (const double x, const double zeta, const double sigma)
{
  if (x <= 0)
    {
      return 0 ;
    }
  else
    {
      double u = (log (x) - zeta)/sigma;
      double p = 1 / (x * fabs(sigma) * sqrt (2 * M_PI)) * exp (-(u * u) /2);
      return p;
    }
}

double
gsl_ran_lognormal_pdf_deriv (const double x, const double zeta, const double sigma)
{
  if (x <= 0)
    {
      return 0 ;
    }
  else
    {
      double p =-((exp(-( pow(zeta - log(x),2)  /(2* pow(sigma,2)  ))) * (-zeta + pow(sigma,2) + log(x)))/(pow(x,2) * pow(sigma,3) * sqrt(2 * M_PI)));
      return p;
    }
}



/* Define lognormal CDF and its derivatives */
real my_lognorm_cdf(arglist *al){
  real *d, *h, arg1, arg2, arg3;
  arg1 = al->ra[0];
  arg2 = al->ra[1];
  arg3 = al->ra[2];
  if (d = al->derivs) {
    d[0] = gsl_ran_lognormal_pdf (arg1,arg2,arg3);
    d[1] = 0;			/* incorrectly setting the other partial derivatives to zero */
    d[2] = 0;
    if (h = al->hes) {
      h[0] = gsl_ran_lognormal_pdf_deriv (arg1,arg2,arg3);
      h[1] = 0;
      h[2] = 0;
      h[3] = 0;
      h[4] = 0;
      h[5] = 0;
    }
  }

/* return the result */
return gsl_cdf_lognormal_P (arg1,arg2,arg3);

}

void funcadd(AmplExports *ae){

/* 1st param: name of the function to import to AMPL.
2nd param: pointer to the function "foo"
3rd param: function type...you can get more info from the header file by checking enumarated type FUNCADD_TYPE.
4th param: Number of arguments passed to function "foo".
5th param: funcinfo
*/
addfunc("my_lognorm_cdf", (rfunc)my_lognorm_cdf, 0, 3, 0);
}
