import pandas as pd
import sys
from git import *
import sqlite3
from pandas.io import sql
import datetime
import time


def lastcommit(gitdir):
    """Returns id of last git commit in directory 'gitdir'"""
    # assign repository
    repo = Repo(gitdir)
    # get latest commit from master
    commit = head=repo.commits()[0]
    return commit.id

def csv2df(file,comment='',gitdir='',iteration=0):
    """Reads csv-file 'file' and converts it into a pandas dataframe"""
    # read csv to dataframe
    df = pd.read_csv(file)
    # optionally add comment
    if comment != '':
        df['comment']=comment        
    # optionally add current git commit id
    if gitdir != '':
        df['git']=lastcommit(gitdir)
    if iteration != 0:
        df['iteration']=iteration
    # add timestamp
    ts = time.time()
    timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    df['timestamp']=timestamp
    return df

def df2db(df,db,table):
    """Write dataframe 'df' to database 'db'"""
    # establish connection
    con = sqlite3.connect(db)
    # write dataframe to db
    pd.io.sql.write_frame(df, table, con, flavor='sqlite', if_exists='append')

def csv2db(file,db,table,comment='',gitdir='',iteration=0):
    """Read from csv file and write to table in database"""
    df = csv2df(file,comment,gitdir,iteration)
    df2db(df,db,table)


def main():
    files = []
    for i in range(1,len(sys.argv)-4):
        files.append(sys.argv[i])
    
    # get dbname as fourth to last argument
    db = str(sys.argv[-4])

    # get table as third to last argument
    table = str(sys.argv[-3])

    # get gitdir as second to last argument
    gitdir = str(sys.argv[-2])

    # get comment as last argument
    comment = str(sys.argv[-1])

    for file in files:
        print(file)
        df = csv2df(file,comment,gitdir)
        df2db(df,db,table)


if __name__ == "__main__":
    main()
    

