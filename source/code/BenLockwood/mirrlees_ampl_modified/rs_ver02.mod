# Try to model Mirrlees problem with constraints that are closer to the formulation of the original problem

# Load gnu scientific library
load amplgsl.dll;
load include/my_lognormal.dll;
#########################################################################################
# Parameters for wagegrid
param wmin := 1;
param wmax := 100;
param nI = 100;

# Parameters for wage densities
param mu_theta := 4;
param mu_phi := 4;

param sigma_theta := 1;
param sigma_phi := 1;

# Parameters for marginal products
param alpha := 0.5;
param a_theta := 1;
param a_phi := 1;
param rho := 1/3;

# For now, treat x as a parameter
param x := 1;


#########################################################################################
# Generate wage grid

set I = 1..nI;
param dw = (wmax - wmin)/(nI-1);
param w {i in I} = wmin + (i-1)* dw;


#########################################################################################


param ETI := 0.25;		#U: Elasticity of substitution (leisure, labor)
param k := 1/ETI;
param r := 1.3;			#concavity parameter for welfare weights
param eps := 1e-5;



#########################################################################################
# Implement marginal products
param yy_phi = (alpha/(1-alpha)*(a_theta/a_phi*x)^rho+1)^((1-rho)/rho)*(1-alpha)^(1/rho)*a_phi >=eps;
param yy_theta = (1+(1-alpha)/alpha*(a_theta/a_phi*x)^(-rho))^((1-rho)/rho)*alpha^(1/rho)*a_theta >=eps;

#########################################################################################
# Calculate alphax
param alphax = x*yy_theta/(x*yy_theta + yy_phi)>=eps;

#########################################################################################
# Implement wage densities
function gsl_ran_lognormal_pdf;
function gsl_cdf_lognormal_P;
function my_lognorm_cdf;

param pi = 4 * atan(1);


param F_theta {i in I} = gsl_cdf_lognormal_P(w[i]/yy_theta,mu_theta,sigma_theta);
param F_phi {i in I} = gsl_cdf_lognormal_P(w[i]/yy_phi,mu_phi,sigma_phi);

param f_theta {i in I} = gsl_ran_lognormal_pdf(w[i]/yy_theta,mu_theta,sigma_theta);
#param f_theta {i in I} = 1/(sqrt(2*pi)*sigma_theta*w[i]/yy_theta) * (exp(-(log(w[i]/yy_theta)-mu_theta)^2/(2*sigma_theta^2)));


param f_phi {i in I} = gsl_ran_lognormal_pdf(w[i]/yy_phi,mu_phi,sigma_phi);
#param f_phi {i in I} = 1/(sqrt(2*pi)*sigma_phi*w[i]/yy_phi) *( exp(-(log(w[i]/yy_phi)-mu_phi)^2/(2*sigma_phi^2)));

#param F_phi {i in I} = my_lognorm_cdf(w[i]/yy_phi,mu_phi,sigma_phi);
param f_thetax_raw {i in I} = 1/yy_theta * f_theta[i] * F_phi[i]; 


#param F_theta {i in I} = my_lognorm_cdf(w[i]/yy_theta,mu_theta,sigma_theta);
param f_phix_raw {i in I} = 1/yy_phi * f_phi[i] * F_theta[i];

param f_x_raw {i in I} = f_thetax_raw[i] + f_phix_raw[i];

# Implement wage distribution
param F_x_raw {i in I} = sum {j in 1..i} f_x_raw[j] * dw;

param norm = F_x_raw[nI];
#param norm = 1;

# Normalize such that F_x is 1 at wmax
param f_thetax {i in I} = f_thetax_raw[i]/norm;
param f_phix {i in I} = f_phix_raw[i]/norm;
param f_x {i in I} = f_x_raw[i]/norm;
param F_x {i in I} = F_x_raw[i]/norm;

#########################################################################################
# Optimization variables
var c {i in I} >= eps;#, :=w[i];
var y {i in I} >= eps;#, :=w[i]*3;

#########################################################################################
# Other things


# Utility function
var util {i in I} = c[i] - (y[i]/w[i])^(1+k)/(1+k); # Quasi-linear

# Utility of imitating adjacent type below
var utilDown {i in 2..nI} = c[i-1] - (y[i-1]/w[i])^(1+k)/(1+k);

# Welfare weights
param G {i in I} = 1-(1-F_x[i])^r;

# Derive density from welfare weights
#param g {i in I} = if i=1 then G[1] else G[i] - G[i-1]; #U: derive density from CDF

param g {i in I} = f_x[i]*r*(1-F_x[i])^(r-1); #altenative definition

# Tax revenue
var surplus = sum {i in I} f_x[i]*(y[i] - c[i]);

# Marginal tax rate (derived from FOC for effort)
var mtr {i in I} = 1 - y[i]^k/w[i]^(k+1) <=1, := 1-1/(1+(G[i]-F[i])/w[i]*f_x[i] *(1+k));


#########################################################################################
# Objective function and constraints
maximize SWF: sum {i in I} g[i]*util[i]*dw;
subject to
    ICconstr {i in 2..nI}: util[i] = utilDown[i];
    BudgetConstraint: surplus = 0;
    yIncr {i in 2..nI}: y[i] >= y[i-1];
 #   UtilConstr {i in I}: c[i] - (y[i]/w[i])^(1+k)/(1+k) >= eps;
  
#    ConsistencyCondition: (1-alphax) * sum {i in I} (y[i] * f_thetax[i] * dw) - alphax *  sum {i in I} (y[i] * f_phix[i] * dw) = 0;
  