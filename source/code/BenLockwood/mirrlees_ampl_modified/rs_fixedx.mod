# Try to model Mirrlees problem with constraints that are closer to the formulation of the original problem

# Load gnu scientific library
load amplgsl.dll;
#########################################################################################
# Parameters for wagegrid
param wmin := 1;
param wmax := 100;
param nW := 100;

# Parameters for wage densities
param mu_theta := 4;
param mu_phi := 4;

param sigma_theta := 1;
param sigma_phi := 1;

# Parameters for marginal products
param alpha := 0.5;
param a_theta := 1.5;
param a_phi := 1;
param rho := 1/3;

# For now, treat x as a parameter
param x := 1.1;

#########################################################################################
# Implement marginal products
param yy_phi := (alpha/(1-alpha)*(a_theta/a_phi*x)^rho+1)^((1-rho)/rho)*(1-alpha)^(1/rho)*a_phi;
param yy_theta := (1+(1-alpha)/alpha*(a_theta/a_phi*x)^(-rho))^((1-rho)/rho)*alpha^(1/rho)*a_theta;

#########################################################################################
# Generate wage grid
param nI = 100;
set I = 1..nI;
param dw = (wmax - wmin)/(nW-1);
param w {i in I} = wmin + (i-1)* dw;


#########################################################################################
# Implement wage densities
function gsl_ran_lognormal_pdf;
function gsl_cdf_lognormal_P;

param f_thetax_raw {i in I} = 1/yy_theta * gsl_ran_lognormal_pdf(w[i]/yy_theta,mu_theta,sigma_theta) * gsl_cdf_lognormal_P(w[i]/yy_phi,mu_phi,sigma_phi);

param f_phix_raw {i in I} = 1/yy_phi * gsl_ran_lognormal_pdf(w[i]/yy_phi,mu_phi,sigma_phi) * gsl_cdf_lognormal_P(w[i]/yy_theta,mu_theta,sigma_theta);

param f_x_raw {i in I} = f_thetax_raw[i] + f_phix_raw[i];

# Implement wage distribution
param F_x_raw {i in I} = sum {j in 1..i} f_x_raw[j];

param norm = F_x_raw[nI];

# Normalize such that F_x is 1 at wmax
param f_thetax {i in I} = f_thetax_raw[i]/norm;
param f_phix {i in I} = f_phix_raw[i]/norm;
param f_x {i in I} = f_x_raw[i]/norm;
param F_x {i in I} = F_x_raw[i]/norm;
#########################################################################################


param ETI := 0.25;		#U: Elasticity of substitution (leisure, labor)
param k := 1/ETI;
param r := 1.3;			#concavity parameter for welfare weights


#########################################################################################
# Optimization variables
var c {I} >= 0;
var y {I} >= 0;

#########################################################################################
# Other things


# Utility function
var util {i in I} = log(c[i] - (y[i]/w[i])^(1+k)/(1+k)); # Quasi-linear

# Utility of imitating adjacent type below
var utilDown {i in 2..nI} = log(c[i-1] - (y[i-1]/w[i])^(1+k)/(1+k));

# Welfare weights
var G {i in I} = 1-(1-F_x[i])^r;

# Derive density from welfare weights
var g {i in I} = if i=1 then G[1] else G[i] - G[i-1]; #U: derive density from CDF

# Tax revenue
var surplus = sum {i in I} f_x[i]*(y[i] - c[i]);

# Marginal tax rate (derived from FOC for effort)
var mtr {i in I} = 1 - y[i]^k/w[i]^(k+1);

# Marginal product wrt c
var uc {i in I} =  1/(c[i] - (y[i]/w[i])^(1+k)/(1+k));

# Marginal product wrt e
var ue {i in I} = - 1/(c[i] - (y[i]/w[i])^(1+k)/(1+k)) * (y[i]/w[i])^(1+k);


#########################################################################################
# Objective function and constraints
maximize SWF: sum {i in I} g[i]*util[i];
subject to
    ICconstr {i in 2..nI}: util[i] = utilDown[i];
    BudgetConstraint: surplus = 0;
    yIncr {i in 2..nI}: y[i] >= y[i-1];
  