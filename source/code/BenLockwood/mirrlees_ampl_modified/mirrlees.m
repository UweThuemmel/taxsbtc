% Parameters
mu_theta = 3;
mu_phi = 3;

sigma_theta = 0.5;
sigma_phi = 0.5;

dw = 1;

ETI = 0.25;
k = 1/ETI;
r = 1.3;	
eps = 1e-5;

alpha = 0.5;
a_theta = 1.5;
a_phi = 1;
rho = 1/3;

% Marginal products

yy_phi = @(x) (alpha/(1-alpha)*(a_theta/a_phi*x)^rho+1)^((1-rho)/rho)*(1-alpha)^(1/rho)*a_phi;
yy_theta = @(x) (1+(1-alpha)/alpha*(a_theta/a_phi*x)^(-rho))^((1-rho)/rho)*alpha^(1/rho)*a_theta;

% alpha(x)

alphax = @(x) x*yy_theta(x)/(x*yy_theta(x) + yy_phi(x));

% Wage densities
f_theta = @(x,w) 1/(sqrt(2*pi)*sigma_theta*w/yy_theta(x)) * (exp(-(log(w/yy_theta(x))-mu_theta)^2/(2*sigma_theta^2)));
f_phi = @(x,w) 1/(sqrt(2*pi)*sigma_phi*w/yy_phi(x)) *( exp(-(log(w/ ...
                                                  yy_phi(x))-mu_phi)^2/(2*sigma_phi^2)));


F_theta = @(x,w) logncdf(w/yy_theta(x),mu_theta,sigma_theta);
F_phi = @(x,w) logncdf(w/yy_phi(x),mu_phi,sigma_phi);

% For now ignore normalization issues
f_thetax = @(x,w) 1/yy_theta(x) * f_theta(x,w) * F_phi(x,w)
f_phix = @(x,w) 1/yy_phi(x) * f_phi(x,w) * F_theta(x,w);

f_x = @(x,w) f_thetax(x,w) + f_phix(x,w);

% Skip for now - have to think how to implement this. How do I tell
% Knitro the dimension of the vector of variables over which to
% optimize.
% ff_x  = sum {j in 1..i} f_x[j] * dw; 

util = @(c,y,w) c - (y/w)^(1+k)/(1+k); 

% Welfare weights - ignore for the moment
% 1-(1-ff_x[i])^r

surplus = @(c,y,x,w) f_x(x,w)*(y - c)

w = [1:50];
x = 1;
c = [1:50];
y = 2 * c;
var = [x c y];


uc(var,w,util)

uc2 = @(var) uc(var,w,util)