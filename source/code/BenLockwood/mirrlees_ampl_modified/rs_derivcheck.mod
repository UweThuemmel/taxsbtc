# Version to check whether analytical derivatives work in simpler problem

# Load gnu scientific library
load amplgsl.dll;
load include/my_lognormal.dll;
#########################################################################################
# Parameters for wage densities
param mu_theta := 4;
param mu_phi := 4;

param sigma_theta := 1;
param sigma_phi := 1;

# Parameters for marginal products
param alpha := 0.5;
param a_theta := 1.5;
param a_phi := 1;
param rho := 1/3;

# For now, treat x as a parameter
param x := 1;


#########################################################################################


param ETI := 0.25;		#U: Elasticity of substitution (leisure, labor)
param k := 1/ETI;
param r := 1.3;			#concavity parameter for welfare weights
param eps := 1e-5;

#########################################################################################
# Optimization variables
var w>=eps, <=50;

#########################################################################################
# Implement marginal products
param yy_phi = (alpha/(1-alpha)*(a_theta/a_phi*x)^rho+1)^((1-rho)/rho)*(1-alpha)^(1/rho)*a_phi;# >=eps;
param yy_theta = (1+(1-alpha)/alpha*(a_theta/a_phi*x)^(-rho))^((1-rho)/rho)*alpha^(1/rho)*a_theta;# >=eps;

#########################################################################################
# Implement wage densities
function gsl_ran_lognormal_pdf;
function gsl_cdf_lognormal_P;
function my_lognorm_cdf;

param pi = 4 * atan(1);


#var f_theta  = gsl_ran_lognormal_pdf(w/yy_theta,mu_theta,sigma_theta);
var f_theta  = 1/(sqrt(2*pi)*sigma_theta*w/yy_theta) * (exp(-(log(w/yy_theta)-mu_theta)^2/(2*sigma_theta^2)));


#var f_phi  = gsl_ran_lognormal_pdf(w/yy_phi,mu_phi,sigma_phi);
var f_phi  = 1/(sqrt(2*pi)*sigma_phi*w/yy_phi) *( exp(-(log(w/yy_phi)-mu_phi)^2/(2*sigma_phi^2)));

#var F_theta  = gsl_cdf_lognormal_P(w/yy_theta,mu_theta,sigma_theta);
var F_theta  = my_lognorm_cdf(w/yy_theta,mu_theta,sigma_theta);


#var F_phi  = gsl_cdf_lognormal_P(w/yy_phi,mu_phi,sigma_phi);
var F_phi  = my_lognorm_cdf(w/yy_phi,mu_phi,sigma_phi);




var f_thetax_raw  = 1/yy_theta * f_theta * F_phi; 
var f_phix_raw = 1/yy_phi * f_phi * F_theta;

var f_x_raw = f_thetax_raw + f_phix_raw;



#########################################################################################
# Objective function and constraints
maximize PDF: f_x_raw;
#subject to
