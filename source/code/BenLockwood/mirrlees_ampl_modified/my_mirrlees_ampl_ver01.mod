# Try to model Mirrlees problem with constraints that are closer to the formulation of the original problem


#########################################################################################
# Parameters
param nI integer;
set I = 1..nI;

param wRaw {I};
param F {I};

# Read in data
read nI, {i in I} (wRaw[i], F[i]) < mirrlees_data.txt;

param w {i in I} = wRaw[i]*10; # helpful numerically
param ETI := 0.25;		#U: Elasticity of substitution (leisure, labor)
param k := 1/ETI;
param r := 1.3;			#concavity parameter for welfare weights


#########################################################################################
# Optimization variables
var c {I} >= 0;
var y {I} >= 0;

#########################################################################################
# Other things

var f {i in I} = if i=1 then F[1] else F[i] - F[i-1]; #U: derive density from CDF

# Utility function
var util {i in I} = log(c[i] - (y[i]/w[i])^(1+k)/(1+k)); # Quasi-linear

# Utility of imitating adjacent type below
var utilDown {i in 2..nI} = log(c[i-1] - (y[i-1]/w[i])^(1+k)/(1+k));

# Welfare weights
var G {i in I} = 1-(1-F[i])^r;

# Derive density from welfare weights
var g {i in I} = if i=1 then G[1] else G[i] - G[i-1]; #U: derive density from CDF

# Tax revenue
var surplus = sum {i in I} f[i]*(y[i] - c[i]);

# Marginal tax rate (derived from FOC for effort)
var mtr {i in I} = 1 - y[i]^k/w[i]^(k+1);

# Marginal product wrt c
var uc {i in I} =  1/(c[i] - (y[i]/w[i])^(1+k)/(1+k));

# Marginal product wrt e
var ue {i in I} = - 1/(c[i] - (y[i]/w[i])^(1+k)/(1+k)) * (y[i]/w[i])^(1+k);


#########################################################################################
# Objective function and constraints
maximize SWF: sum {i in I} g[i]*util[i]; # in principle we should be multiplying by dw, leave out for now
subject to

#      ICconstr {i in 2..nI}: util[i] = utilDown[i];


    BudgetConstraint: surplus = 0;
    yIncr {i in 2..nI}: y[i] >= y[i-1];
    ICconstr {i in I}: uc[i] * y[i]/w[i] * (1-mtr[i]) + ue[i]*y[i]/w[i]^2 = 0;