#_MATLink_

![](http://rsmenon.github.io/MATLink/assets/img/logo.png)

_MATLink_ is a [_Mathematica_](http://www.wolfram.com/mathematica/) application that allows the user to communicate data between _Mathematica_ and [MATLAB](http://www.mathworks.com/products/matlab/), as well as execute MATLAB code and seamslesly call MATLAB functions from within _Mathematica_.
It uses [_MathLink_](http://reference.wolfram.com/mathematica/tutorial/MathLinkAndExternalProgramCommunicationOverview.html) to communicate between _Mathematica_ and MATLAB (via the [MATLAB Engine library](http://www.mathworks.com/help/matlab/matlab_external/using-matlab-engine.html)).

 - Please see the [application homepage](http://matlink.org) for more information and [documentation](http://matlink.org/documentation/).
 - If you have a feature request or notice a bug or unexpected behaviour, please file a new issue on GitHub.
 - You might also be interested in browsing some [neat examples](http://matlink.org/examples/) of using MATLink for real world applications.
 - Feel free to contact us at matlink.m@gmail.com for support or to let us know how you're using it.

<sub>_Mathematica_ is a registered trademark of Wolfram Research, Inc. and MATLAB is a registered trademark of The MathWorks, Inc.</sub>
