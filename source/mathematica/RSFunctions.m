(* ::Package:: *)

BeginPackage["RSFunctions`"]
(*Specify integration method *)
integrationmethod={"GaussKronrodRule"};

(*Marginal products*)
yyphi[x_,atheta_,aphi_]:=(alpha/(1-alpha)*(atheta/aphi*x)^rho+1)^((1-rho)/rho)*(1-alpha)^(1/rho)*aphi;
yytheta[x_,atheta_,aphi_]:=(1+(1-alpha)/alpha*(atheta/aphi*x)^(-rho))^((1-rho)/rho)*alpha^(1/rho)*atheta;

(*Extreme wages*)
wmin[x_, atheta_, aphi_] := 
 Max[yytheta[x, atheta, aphi]*thetamin, yyphi[x, atheta, aphi]*phimin]
wmax[x_,atheta_,aphi_]:=Max[yytheta[x,atheta,aphi]*thetamax,yyphi[x,atheta,aphi]*phimax]

(*Marginal tax rate*)
mtr[w_, x_, xi_, atheta_, aphi_] := 
 1 - (1 + xi*(fthetax[w, x, atheta, aphi]/fx[w, x, atheta, aphi] - 
      alphax[x, atheta, aphi]))/(
  1 + gamma (ggx[w, x, atheta, aphi] - ffx[w, x, atheta, aphi])/(
    w*fx[w, x, atheta, aphi]))

(* Effort *)
effort[w_, x_, xi_, atheta_, aphi_] := 
 w^(1/(gamma - 1)) ((
   1 + xi*(fthetax[w, x, atheta, aphi]/fx[w, x, atheta, aphi] - 
       alphax[x, atheta, aphi]))/(
   1 + gamma (ggx[w, x, atheta, aphi] - ffx[w, x, atheta, aphi])/(
     w*fx[w, x, atheta, aphi])))^(1/(gamma - 1))

(* Income *)
y[w_, x_, xi_, atheta_, aphi_] := w*effort[w, x, xi, atheta, aphi]

(* Skill densities and distributions *)

ftheta[theta_] := 
 PDF[LogNormalDistribution[mutheta, sigmatheta], theta]

fftheta[theta_] := 
 CDF[LogNormalDistribution[mutheta, sigmatheta], theta]

fphi[phi_] := PDF[LogNormalDistribution[muphi, sigmaphi], phi]

ffphi[phi_] := CDF[LogNormalDistribution[muphi, sigmaphi], phi]

(* Unnormalized densities *)

ufthetax[w_, x_, atheta_, aphi_] := 
 1/yytheta[x, atheta, aphi]*ftheta[w/yytheta[x, atheta, aphi]]*
  ffphi[w/yyphi[x, atheta, aphi]]

ufphix[w_, x_, atheta_, aphi_] := 
 1/yyphi[x, atheta, aphi]*fphi[w/yyphi[x, atheta, aphi]]*
  fftheta[w/yytheta[x, atheta, aphi]]

ufx[w_, x_, atheta_, aphi_] := 
 ufthetax[w, x, atheta, aphi] + ufphix[w, x, atheta, aphi]

ufprimex[w_, x_, atheta_, aphi_] := D[ufx[s, x, atheta, aphi], s] /. s -> w

ufthetaprimex[w_, x_, atheta_, aphi_] := 
 D[ufthetax[s, x, atheta, aphi], s] /. s -> w

(* Define normalization *)


normalization[x_, atheta_, aphi_] := Module[{}, If[x == xold,
   normold,
   Global`normold =
    NIntegrate[
     ufx[s, x, atheta, aphi], {s, wmin[x, atheta, aphi],
      wmax[x, atheta, aphi]}, Method -> integrationmethod];
   Global`xold = x;
   normold
   ]
  ]

(* Distributions, using normalization *)
ffx[w_, x_, atheta_, aphi_] :=
 NIntegrate[ufx[s, x, atheta, aphi], {s, wmin[x, atheta, aphi], w},
   Method -> integrationmethod]/normalization[x, atheta, aphi]

ffthetax[w_, x_, atheta_, aphi_] :=
 NIntegrate[
   ufthetax[s, x, atheta, aphi], {s, wmin[x, atheta, aphi], w},
   Method -> integrationmethod]/normalization[x, atheta, aphi]

ffphix[w_, x_, atheta_, aphi_] :=
 NIntegrate[ufphix[s, x, atheta, aphi], {s, wmin[x, atheta, aphi], w},
    Method -> integrationmethod]/normalization[x, atheta, aphi]

(* Normalize densities *)
fthetax[w_, x_, atheta_, aphi_] :=
 ufthetax[w, x, atheta, aphi]/normalization[x, atheta, aphi]

fphix[w_, x_, atheta_, aphi_] :=
 ufphix[w, x, atheta, aphi]/normalization[x, atheta, aphi]

fx[w_, x_, atheta_, aphi_] :=
 ufx[w, x, atheta, aphi]/normalization[x, atheta, aphi]

fprimex[w_, x_, atheta_, aphi_] :=
 ufprimex[w, x, atheta, aphi]/normalization[x, atheta, aphi]

fthetaprimex[w_, x_, atheta_, aphi_] :=
 ufthetaprimex[w, x, atheta, aphi]/normalization[x, atheta, aphi]


(* Pareto weights *)

ggx[w_, x_, atheta_, aphi_] := 1 - (1 - ffx[w, x, atheta, aphi])^r

(* Define gx *)

gx[w_, x_, atheta_, aphi_] :=
 r*(1 - ffx[w, x, atheta, aphi])^(r - 1)*fx[w, x, atheta, aphi]

gthetax[w_, x_, atheta_, aphi_] :=
 r*(1 - ffx[w, x, atheta, aphi])^(r - 1)*fthetax[w, x, atheta, aphi]

gphix[w_, x_, atheta_, aphi_] :=
 r*(1 - ffx[w, x, atheta, aphi])^(r - 1)*fphix[w, x, atheta, aphi]

(* Aggregate income share of theta sector *)

alphax[x_, atheta_, aphi_] := yytheta[x, atheta, aphi]/(
 x*yytheta[x, atheta, aphi] + yyphi[x, atheta, aphi])

(* Consistency condition *)
(* Part of CC related to theta *)

cc1[x_, xi_, atheta_, aphi_] := (1 - alphax[x, atheta, aphi])*
  NIntegrate[
   y[w, x, xi, atheta, aphi]*fthetax[w, x, atheta, aphi], {w,
    wmin[x, atheta, aphi], wmax[x, atheta, aphi]},
   Method -> integrationmethod]

(* Part of CC related to phi *)

cc2[x_, xi_, atheta_, aphi_] :=
 alphax[x, atheta, aphi]*
  NIntegrate[
   y[w, x, xi, atheta, aphi]*fphix[w, x, atheta, aphi], {w,
    wmin[x, atheta, aphi], wmax[x, atheta, aphi]},
   Method -> integrationmethod]

(* Full CC *)

cc[x_, xi_, atheta_, aphi_] :=
 cc1[x, xi, atheta, aphi] - cc2[x, xi, atheta, aphi]


(* FOC for Welfare *)

(* S-term *)

(* Might still have to normalize the skill distribution *)

(* Define normalization for skill distribution *)

(* Will only be computed new if x-value is different from previously used x-value, otherwise, the previously computed normalization will be used. *)


normskill[x_, atheta_, aphi_] := Module[{}, If[x == xold,
   normskillold,
   Global`normskillold =
    NIntegrate[
     ftheta[w/yytheta[x, atheta, aphi]]*fphi[w/yyphi[x, atheta, aphi]], {s,
      wmin[x, atheta, aphi], wmax[x, atheta, aphi]},
     Method -> integrationmethod];
   Global`xold = x;
   normskillold
   ]
  ]

(* (\* S-term *\) *)

(* sterm[x_, xi_, atheta_, aphi_] :=  *)
(*  1/(yytheta[x, atheta, aphi]*yyphi[x, atheta, aphi]) *)
(*    NIntegrate[ *)
(*     w*y[w, x, xi, atheta, aphi]*ftheta[w/yytheta[x, atheta, aphi]]* *)
(*      fphi[w/yyphi[x, atheta, aphi]], {w, wmin[x, atheta, aphi],  *)
(*      wmax[x, atheta, aphi]}, Method -> integrationmethod]/ *)
(*    normskill[x, atheta, aphi] *)




EndPackage[]
