(* Load package for plotting from command line *)
<<JavaGraphics`;

         -- Java graphics initialized -- 

         -- Java graphics initialized -- 

         -- Java graphics initialized -- 

         -- Java graphics initialized -- 

          -- Java graphics initialized -- 

(* Try continous version *)

(* Boundary values for theta and phi *)
thetamin = 1;
thetamax = 5;

phimin = 1;
phimax = 5;

(* Curvature parameter for welfare function *)
r = 1.3;

(* Elasticity *)
epsilon = 0.3;

(* PDF for theta *)
mutheta = 1;
muphi = 1;
pdftheta[theta_]:=N[PDF[LogNormalDistribution[mutheta,muphi],theta]];

                                   
(* PDF for phi *)
muphi = 1;
muphi = 1;
pdfphi[phi_]:=N[PDF[LogNormalDistribution[muphi,muphi],phi]];

(* Joint density *)
pdfthetaphi[theta_,phi_]:= pdftheta[theta]*pdfphi[phi];

(* Production function *)
alpha = 0.5;
Yphi[x_]:=(1-alpha)*x^alpha;
Ytheta[x_]:=alpha*x^(alpha-1);
Ytheta::usage="Marginal product wrt. theta";
Yphi::usage="Marginal product wrt. phi";

(* Marginal densities for wage distribution (see page 633)*)
fthetaxaux[w_?NumericQ,x_?NumericQ]:=(1/Ytheta[x])*NIntegrate[pdfthetaphi[w/Ytheta[x],phi],{phi,phimin,w/Yphi[x]}];
fthetaxaux::usage="Marginal density for wage w wrt. theta - not yet normalized";

fphixaux[w_?NumericQ,x_?NumericQ]:=(1/Yphi[x])*NIntegrate[pdfthetaphi[theta,w/Yphi[x]],{theta,thetamin,w/Ytheta[x]}];
fphixaux::usage="Marginal density for wage w wrt. phi - not yet normalized";

fxaux[w_?NumericQ,x_?NumericQ]:=fthetaxaux[w,x] + fphixaux[w,x];
fxaux::usage="Density of wages - not yet normalized";

(* Calculate min and max wage *)
wminx[x_?NumericQ]:=Max[Ytheta[x]*thetamin,Yphi[x]*phimin];
wminx::usage="Minimum wage, depending on x";
wmaxx[x_?NumericQ]:=Max[Ytheta[x]*thetamax,Yphi[x]*phimax];
wminx::usage="Maximum wage, depending on x";

(* Apply normalization to densities *)
totalfx[x_]:=NIntegrate[fxaux[x,w],{w,wminx[x],wmaxx[x]}];
testx = 1;
normal = totalfx[testx];

fthetax[w_,x_]:=1/normal*fthetaxaux[w,x];
fthetax::usage="Marginal density for wage w wrt. theta";
fphix[w_,x_]:=1/normal*fphixaux[w,x];
fphix::usage="Marginal density for wage w wrt. phi";
fx[w_,x_]:=1/normal*fxaux[w,x];
fxaux::usage="Density of wages";

(* CDF for wages *)
ffx[w_?NumericQ,x_?NumericQ]:=NIntegrate[fx[z,x],{z,wminx[x],w}];
ffx::usage="CDF of wages";

(* Welfare weights *)
ggx[w_,x_]:=1-(1-ffx[w,x])^r;
ggx::usage="Function for social welfare weight at wage w";

(* Plot wage density *)
plotwagedensity[x_]:=Plot[fx[w,x],{w,wminx[x],wmaxx[x]}];

(* Function for finding the optimal xi *)
alphax[x_]:=x*Ytheta[x]/(x*Ytheta[x]+Yphi[x]);
alphax::usage="Gives income share of the Theta sector";
(* Note that in the CD case alphax is equal to alpha, independent of x *)

mtax[w_,xi_,x_]:=(1+xi*(fthetax[w,x]/fx[w,x]-alphax[x]))/(1+(ggx[w,x]-ffx[w,x])/(w*fx[w,x])*(1+1/epsilon))-1;
mtax::usage="Formula for marginal tax rate - see Corollary 638";

effort[w_,xi_,x_]:=(1-mtax[w,xi,x])^epsilon*w^epsilon;
income::usage="Function for optimal effort";

income[w_,xi_,x_]:=w*effort[w,xi,x];
income::usage="Function for income - wage times effort";

(* Consistency condition *)
consistency[xi_,x_]:=NIntegrate[Max[0,income[w,xi,x]]*((1/alphax[x]-1)*fthetax[w,x]-fphix[w,x]),{w,wminx[x],wmaxx[x]},Method->"MonteCarlo"];

(* Now move to discrete wage distribution *)
(* Define wage grid *)
wagegrid[x_,step_]:= Range[wminx[x],wmaxx[x],step];

(* Evaluate wage density at wage grid *)
fx[#,1]&/@ wagegrid[1,.001]
