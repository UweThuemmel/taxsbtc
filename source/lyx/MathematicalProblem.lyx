#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
Objective
\end_layout

\begin_layout Plain Layout
\begin_inset Formula 
\[
f(\mathbf{c},\mathbf{y},x)
\]

\end_inset


\end_layout

\begin_layout Plain Layout
Constraints
\end_layout

\begin_layout Plain Layout
\begin_inset Formula 
\[
U_{i}(\mathbf{c},\mathbf{y},x)-U_{i-1}(\mathbf{c},\mathbf{y},x)\geq0\ \forall\ i\ge2
\]

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Formula 
\[
Surplus(\mathbf{c},\mathbf{y},x)=0
\]

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Formula 
\[
y_{i}-y_{i-1}\ge0\ \forall\ i\geq2
\]

\end_inset


\begin_inset Formula 
\[
ConsistencyCondition(\mathbf{c},\mathbf{y},x)=0
\]

\end_inset


\end_layout

\begin_layout Section
Now in more detail
\end_layout

\begin_layout Subsection
Marginal products
\end_layout

\begin_layout Plain Layout
\begin_inset Formula 
\[
Y_{\theta}(x)=\left[1+\frac{1-\alpha}{\alpha}\left(\frac{A_{\theta}}{A_{\phi}}\cdot x\right)^{-\rho}\right]^{\frac{1-\rho}{\rho}}\alpha^{\frac{1}{\rho}}A_{\theta}.
\]

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Formula 
\[
Y_{\phi}(x)=\left[\frac{\alpha}{1-\alpha}(\frac{A_{\theta}}{A\phi}\cdot x)^{\rho}+1\right]^{\frac{1-\rho}{\rho}}(1-\alpha)^{\rho}A_{\phi}.
\]

\end_inset


\end_layout

\begin_layout Subsection
\begin_inset Formula $\alpha(x)$
\end_inset


\begin_inset Formula 
\[
\alpha(x)=\frac{x\cdot Y_{\theta}(x)}{x\cdot Y_{\theta}(x)+Y_{\phi}(x)}.
\]

\end_inset


\end_layout

\begin_layout Subsection
Wage densities
\end_layout

\begin_layout Plain Layout
\begin_inset Formula 
\[
f_{x}^{\theta}(w,x)=\frac{1}{Y_{\theta}(x)}f^{\theta}\left(\frac{w}{Y_{\theta}(x)}\right)F^{\phi}\left(\frac{w}{Y_{\phi}(x)}\right).
\]

\end_inset


\begin_inset Formula 
\[
f_{x}^{\phi}(w,x)=\frac{1}{Y_{\phi}(x)}f^{\phi}\left(\frac{w}{Y_{\phi}(x)}\right)F^{\theta}\left(\frac{w}{Y_{\theta}(x)}\right).
\]

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Formula 
\[
f_{x}(w,x)=f_{x}^{\theta}(w,x)+f_{x}^{\phi}(w,x)
\]

\end_inset


\end_layout

\begin_layout Subsection
Wage distributions
\begin_inset Formula 
\[
F_{x}(w,x)=\int_{\underline{w}}^{w}f_{x}(\omega,x)\ d\omega
\]

\end_inset


\end_layout

\begin_layout Subsection
Weights
\begin_inset Formula 
\[
G(w,x)=1-(1-F_{x}(w,x))^{r}
\]

\end_inset


\begin_inset Formula 
\[
g(w,x)=r\cdot f_{x}(w,x)\cdot(1-F_{x}(w,x))^{r-1}
\]

\end_inset


\end_layout

\begin_layout Subsection
Utility
\begin_inset Formula 
\[
U(w,c,y)=c-\frac{\left(\frac{y}{w}\right)}{1+k}^{1+k}
\]

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Formula 
\[
\underline{U}(w,c,y)=
\]

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Try again
\end_layout

\begin_layout Standard
Given a grid 
\begin_inset Formula $w$
\end_inset

 of dimension 
\begin_inset Formula $N\times1$
\end_inset

, and parameters (including 
\begin_inset Formula $\tau$
\end_inset

) maximize over
\end_layout

\begin_layout Itemize
\begin_inset Formula $x$
\end_inset

, 
\begin_inset Formula $1\times1$
\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Formula $\mathbf{c}$
\end_inset

, 
\begin_inset Formula $N\times1$
\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Formula $\mathbf{y}$
\end_inset

, 
\begin_inset Formula $N\times1$
\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Formula $\tilde{\mathbf{w}}$
\end_inset

, 
\begin_inset Formula $N\times1$
\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Formula $\hat{\mathbf{w}}$
\end_inset

, 
\begin_inset Formula $N\times1$
\end_inset


\end_layout

\begin_layout Subsection
Objective function
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\sum_{i=1}^{N}u\left(c_{i},\frac{y_{i}}{w_{i}}\right)g_{x}\left(w_{i},x\right)\omega_{i}
\]

\end_inset


\end_layout

\begin_layout Subsection
Constraints
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
y_{i}-y_{i-1}\geq0\ i=2\dots N\tag{Monotonicity}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
u\left(c_{i},\frac{y_{i}}{w_{i}}\right)-u\left(c_{i-1},\frac{y_{i-1}}{w_{i}}\right)\geq0\ i=2\dots N\tag{Incentives}
\]

\end_inset


\begin_inset Formula 
\[
\left(1-\alpha(x)\right)\sum_{i=1}^{N}y_{i}f_{x}^{\theta}(w_{i},x)\omega_{i}-\alpha(x)\sum_{i=1}^{N}y_{i}f_{x}^{\phi}(w_{i},x)\omega_{i}=0\tag{Consistency Condition}
\]

\end_inset


\begin_inset Formula 
\[
u\left(c_{i},\frac{y_{i}}{w_{i}}\right)+\tau-u\left(c\left(\tilde{w}_{i}\right),\frac{y\left(\tilde{w}_{i}\right)}{\tilde{w}_{i}}\right)=0\tag{Indifference Cond. 1}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
u\left(c\left(\hat{w}_{i}\right),\frac{y\left(\hat{w}_{i}\right)}{\hat{w}_{i}}\right)+\tau-u\left(c_{i},\frac{y_{i}}{w_{i}}\right)=0\tag{Indifference Cond. 2}
\]

\end_inset


\end_layout

\begin_layout Subsection
Densities
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
f_{x}^{\theta}\left(w_{i}\right)=\frac{1}{Y_{\theta}(x)}f^{\theta}\left(\frac{\tilde{w}_{i}}{Y_{\theta}(x)}\right)F^{\phi}\left(\frac{\tilde{w}_{i}}{Y_{\phi}(x)}\right)
\]

\end_inset


\begin_inset Formula 
\[
f_{x}^{\phi}\left(w_{i}\right)=\frac{1}{Y_{\phi}(x)}f^{\phi}\left(\frac{\hat{w}_{i}}{Y_{\phi}(x)}\right)F^{\theta}\left(\frac{\hat{w}_{i}}{Y_{\theta}(x)}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
g_{x}\left(w_{i},x\right)=f_{x}(w_{i})\cdot r\cdot(1-F_{x}(w_{i}))^{r-1}
\]

\end_inset

with
\begin_inset Formula 
\[
f_{x}(w_{i})=f_{x}^{\theta}(w_{i})+f_{x}^{\phi}(w_{i})
\]

\end_inset

and
\begin_inset Formula 
\[
F_{x}(w_{i})=\sum_{i=1}^{N}f_{x}(w_{i})\omega_{i}
\]

\end_inset


\end_layout

\begin_layout Subsection
Approximation of 
\begin_inset Formula $c(w)$
\end_inset

 and 
\begin_inset Formula $y(w)$
\end_inset


\end_layout

\begin_layout Standard
Approximate the unknown functions 
\begin_inset Formula $c(w)$
\end_inset

 and 
\begin_inset Formula $y(w)$
\end_inset

 by splines based on the vectors 
\begin_inset Formula $\mathbf{c}$
\end_inset

 and 
\begin_inset Formula $\mathbf{y}$
\end_inset

:
\begin_inset Formula 
\[
c(w)\approx s_{c}(\mathbf{c},w)
\]

\end_inset


\begin_inset Formula 
\[
y(w)\approx s_{y}\left(\mathbf{y},w\right).
\]

\end_inset

As a consequence,
\begin_inset Formula 
\[
\tilde{w}_{i}\left(\mathbf{c},\mathbf{y}\right)
\]

\end_inset

and
\begin_inset Formula 
\[
\hat{w}_{i}\left(\mathbf{c},\mathbf{y}\right).
\]

\end_inset


\end_layout

\begin_layout Itemize
Is this true?
\end_layout

\begin_layout Standard
Also, densities will now depend on the entire vectors 
\begin_inset Formula $\mathbf{c}$
\end_inset

 and 
\begin_inset Formula $\mathbf{y}$
\end_inset

.
\end_layout

\end_body
\end_document
