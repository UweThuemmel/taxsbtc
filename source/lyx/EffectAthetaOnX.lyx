#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 2cm
\rightmargin 2cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Section
Condition for increase in x as response to increase in 
\begin_inset Formula $A_{\theta}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
footnotesize
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align*}
\left(e\left[\bar{\theta}Y_{\theta}\left[A_{\theta}\right]\right]f^{\theta}\left[\bar{\theta}\right]\bar{\theta}^{2}-e\left[\underline{\theta}Y_{\theta}\left[A_{\theta}\right]\right]f^{\theta}\left[\underline{\theta}\right]F^{\phi}\left[\frac{\underline{\theta}Y_{\theta}\left[A_{\theta}\right]}{Y_{\phi}\left[A_{\theta}\right]}\right]\underline{\theta}^{2}\right)Y_{\theta}'\left[A_{\theta}\right] & >\\
Y_{\theta}\left[A_{\theta}\right]\frac{1}{Y_{\theta}\left[A_{\theta}\right]{}^{4}}\cdot Y_{\theta}'\left[A_{\theta}\right]\cdot\int_{\underline{\theta}Y_{\theta}\left[A_{\theta}\right]}^{\bar{\theta}Y_{\theta}\left[A_{\theta}\right]}we[w]\cdot F^{\phi}\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]\left(2f^{\theta}\left[\frac{w}{Y_{\theta}\left[A_{\theta}\right]}\right]Y_{\theta}\left[A_{\theta}\right]+w\left(f^{\theta}\right)'\left[\frac{w}{Y_{\theta}\left[A_{\theta}\right]}\right]\right)dw\\
-x\cdot Y_{\theta}\left[A_{\theta}\right]\cdot\frac{1}{Y_{\phi}\left[A_{\theta}\right]{}^{4}}\cdot Y_{\phi}'\left[A_{\theta}\right]\cdot\int_{\underline{\theta}Y_{\theta}\left[A_{\theta}\right]}^{\bar{\theta}Y_{\theta}\left[A_{\theta}\right]}we[w]\cdot F^{\theta}\left[\frac{w}{Y_{\theta}\left[A_{\theta}\right]}\right]\left(2f^{\phi}\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]Y_{\phi}\left[A_{\theta}\right]+w\left(f^{\phi}\right)'\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]\right)dw\\
+\frac{1}{Y_{\theta}\left[A_{\theta}\right]Y_{\phi}\left[A_{\theta}\right]{}^{2}}\cdot Y_{\phi}'\left[A_{\theta}\right]\cdot\int_{\underline{\theta}Y_{\theta}\left[A_{\theta}\right]}^{\bar{\theta}Y_{\theta}\left[A_{\theta}\right]}we[w]\cdot wf^{\theta}\left[\frac{w}{Y_{\theta}\left[A_{\theta}\right]}\right]f^{\phi}\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]\, dw\\
-x\cdot\frac{1}{Y_{\theta}\left[A_{\theta}\right]Y_{\phi}\left[A_{\theta}\right]{}^{2}}\cdot Y_{\theta}'\left[A_{\theta}\right]\cdot\int_{\underline{\theta}Y_{\theta}\left[A_{\theta}\right]}^{\bar{\theta}Y_{\theta}\left[A_{\theta}\right]}we[w]\cdot wf^{\theta}\left[\frac{w}{Y_{\theta}\left[A_{\theta}\right]}\right]f^{\phi}\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]\, dw & .
\end{align*}

\end_inset


\end_layout

\begin_layout Subsection
1.
 Term RHS
\begin_inset Note Note
status open

\begin_layout Plain Layout
\begin_inset Formula 
\[
\int_{\underline{\theta}Y_{\theta}\left[A_{\theta}\right]}^{\bar{\theta}Y_{\theta}\left[A_{\theta}\right]}we[w]\cdot F^{\phi}\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]\left(2f^{\theta}\left[\frac{w}{Y_{\theta}\left[A_{\theta}\right]}\right]Y_{\theta}\left[A_{\theta}\right]+w\left(f^{\theta}\right)'\left[\frac{w}{Y_{\theta}\left[A_{\theta}\right]}\right]\right)dw
\]

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
First part
\begin_inset Formula 
\[
\int_{\underline{\theta}Y_{\theta}\left[A_{\theta}\right]}^{\bar{\theta}Y_{\theta}\left[A_{\theta}\right]}we[w]\cdot F^{\phi}\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]2f^{\theta}\left[\frac{w}{Y_{\theta}\left[A_{\theta}\right]}\right]Y_{\theta}\left[A_{\theta}\right]dw
\]

\end_inset

which is
\begin_inset Formula 
\[
2Y_{\theta}\left[A_{\theta}\right]^{2}\int_{\underline{\theta}Y_{\theta}\left[A_{\theta}\right]}^{\bar{\theta}Y_{\theta}\left[A_{\theta}\right]}we[w]\cdot f_{x}^{\theta}(w)dw=2Y_{\theta}\left[A_{\theta}\right]^{2}\overline{y}_{\theta}
\]

\end_inset

Second part
\begin_inset Formula 
\[
\int_{\underline{\theta}Y_{\theta}\left[A_{\theta}\right]}^{\bar{\theta}Y_{\theta}\left[A_{\theta}\right]}we[w]\cdot F^{\phi}\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]w\left(f^{\theta}\right)'\left[\frac{w}{Y_{\theta}\left[A_{\theta}\right]}\right]dw
\]

\end_inset

Apply integration by parts
\begin_inset Formula 
\begin{align*}
\overline{w}_{x}^{2}e[\overline{w}_{x}]F^{\phi}\left[\frac{\overline{w}_{x}}{Y_{\phi}\left[A_{\theta}\right]}\right]f^{\theta}\left[\frac{\overline{w}_{x}}{Y_{\theta}\left[A_{\theta}\right]}\right]-\underline{w}_{x}^{2}e[\underline{w}_{x}]F^{\phi}\left[\frac{\underline{w}_{x}}{Y_{\phi}\left[A_{\theta}\right]}\right]f^{\theta}\left[\frac{\underline{w}_{x}}{Y_{\theta}\left[A_{\theta}\right]}\right]\\
-\int_{\underline{w}_{x}}^{\overline{w}_{x}}f^{\theta}\left[\frac{w}{Y_{\theta}\left[A_{\theta}\right]}\right]\left(\left(e'[w]w^{2}+2e[w]w\right)F^{\phi}\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]+e[w]w^{2}f^{\phi}\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]\frac{1}{Y_{\phi}}\right)\ dw
\end{align*}

\end_inset

Rewrite as
\begin_inset Formula 
\[
Y_{\theta}\left(\overline{w}_{x}^{2}e[\overline{w}_{x}]f_{x}^{\theta}\left(\overline{w}_{x}\right)-\underline{w}_{x}^{2}e[\underline{w}_{x}]f_{x}^{\theta}\left(\underline{w}_{x}\right)\right)-\int_{\underline{w}_{x}}^{\overline{w}_{x}}f^{\theta}\left[\frac{w}{Y_{\theta}\left[A_{\theta}\right]}\right]\left(\left(e'[w]w^{2}+2e[w]w\right)F^{\phi}\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]+e[w]w^{2}f^{\phi}\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]\frac{1}{Y_{\phi}}\right)\ dw
\]

\end_inset

Consider
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none

\begin_inset Formula 
\[
\int_{\underline{w}_{x}}^{\overline{w}_{x}}f^{\theta}\left[\frac{w}{Y_{\theta}\left[A_{\theta}\right]}\right]\left(\left(e'[w]w^{2}+2e[w]w\right)F^{\phi}\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]+e[w]w^{2}f^{\phi}\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]\frac{1}{Y_{\phi}}\right)\ dw.
\]

\end_inset

Write as
\begin_inset Formula 
\[
\int_{\underline{w}_{x}}^{\overline{w}_{x}}\left(\left(e'[w]w^{2}+2e[w]w\right)f^{\theta}\left[\frac{w}{Y_{\theta}\left[A_{\theta}\right]}\right]F^{\phi}\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]+e[w]w^{2}f\left[\left[\frac{w}{Y_{\theta}\left[A_{\theta}\right]}\right],\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]\right]\frac{1}{Y_{\phi}}\right)\ dw
\]

\end_inset


\end_layout

\begin_layout Standard

\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
\begin_inset Formula 
\[
\int_{\underline{w}_{x}}^{\overline{w}_{x}}\left(\left(e'[w]w^{2}+2e[w]w\right)Y_{\theta}f_{x}^{\theta}(w)+e[w]w^{2}f\left[\left[\frac{w}{Y_{\theta}\left[A_{\theta}\right]}\right],\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]\right]\frac{1}{Y_{\phi}}\right)\ dw
\]

\end_inset

Using
\begin_inset Formula 
\[
\beta\equiv\int_{\underline{w}_{x}}^{\overline{w}_{x}}e[w]w^{2}f\left[\left[\frac{w}{Y_{\theta}\left[A_{\theta}\right]}\right],\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]\right]\ dw
\]

\end_inset

this becomes
\begin_inset Formula 
\[
\int_{\underline{w}_{x}}^{\overline{w}_{x}}\left(e'[w]w^{2}+2e[w]w\right)Y_{\theta}f_{x}^{\theta}(w)\ dw+\beta\frac{1}{Y_{\phi}}=\int_{\underline{w}_{x}}^{\overline{w}_{x}}e'[w]w^{2}Y_{\theta}f_{x}^{\theta}(w)\ dw+2\overline{y}_{\theta}Y_{\theta}+\beta\frac{1}{Y_{\phi}}.
\]

\end_inset

Hence, the first expression on the RHS can be written as
\begin_inset Formula 
\[
2Y_{\theta}\left[A_{\theta}\right]^{2}\overline{y}_{\theta}+\int_{\underline{w}_{x}}^{\overline{w}_{x}}e'[w]w^{2}Y_{\theta}f_{x}^{\theta}(w)\ dw+2\overline{y}_{\theta}Y_{\theta}+\beta\frac{1}{Y_{\phi}}=2Y_{\theta}\left[A_{\theta}\right]\overline{y}_{\theta}\left(1+Y\left[A_{\theta}\right]\right)+\int_{\underline{w}_{x}}^{\overline{w}_{x}}e'[w]w^{2}Y_{\theta}f_{x}^{\theta}(w)\ dw+\beta\frac{1}{Y_{\phi}}
\]

\end_inset


\end_layout

\begin_layout Standard
We thus have
\begin_inset Formula 
\begin{align*}
\left(e\left[\bar{\theta}Y_{\theta}\left[A_{\theta}\right]\right]f^{\theta}\left[\bar{\theta}\right]\bar{\theta}^{2}-e\left[\underline{\theta}Y_{\theta}\left[A_{\theta}\right]\right]f^{\theta}\left[\underline{\theta}\right]F^{\phi}\left[\frac{\underline{\theta}Y_{\theta}\left[A_{\theta}\right]}{Y_{\phi}\left[A_{\theta}\right]}\right]\underline{\theta}^{2}\right)Y_{\theta}'\left[A_{\theta}\right] & >\\
Y_{\theta}\left[A_{\theta}\right]\frac{1}{Y_{\theta}\left[A_{\theta}\right]{}^{4}}\cdot Y_{\theta}'\left[A_{\theta}\right]\cdot\left(2Y_{\theta}\left[A_{\theta}\right]\overline{y}_{\theta}\left(1+Y_{\theta}\left[A_{\theta}\right]\right)+\int_{\underline{w}_{x}}^{\overline{w}_{x}}e'[w]w^{2}Y_{\theta}f_{x}^{\theta}(w)\ dw+\beta\frac{1}{Y_{\phi}}\right)\\
-x\cdot Y_{\theta}\left[A_{\theta}\right]\cdot\frac{1}{Y_{\phi}\left[A_{\theta}\right]{}^{4}}\cdot Y_{\phi}'\left[A_{\theta}\right]\cdot\int_{\underline{\theta}Y_{\theta}\left[A_{\theta}\right]}^{\bar{\theta}Y_{\theta}\left[A_{\theta}\right]}we[w]\cdot F^{\theta}\left[\frac{w}{Y_{\theta}\left[A_{\theta}\right]}\right]\left(2f^{\phi}\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]Y_{\phi}\left[A_{\theta}\right]+w\left(f^{\phi}\right)'\left[\frac{w}{Y_{\phi}\left[A_{\theta}\right]}\right]\right)dw\\
+\frac{1}{Y_{\theta}\left[A_{\theta}\right]Y_{\phi}\left[A_{\theta}\right]{}^{2}}\cdot Y_{\phi}'\left[A_{\theta}\right]\cdot\beta\\
-x\cdot\frac{1}{Y_{\theta}\left[A_{\theta}\right]Y_{\phi}\left[A_{\theta}\right]{}^{2}}\cdot Y_{\theta}'\left[A_{\theta}\right]\cdot\beta & .
\end{align*}

\end_inset

Applying the same procedure to the second line on the RHS gives
\begin_inset Formula 
\begin{align*}
\left(e\left[\bar{\theta}Y_{\theta}\left[A_{\theta}\right]\right]f^{\theta}\left[\bar{\theta}\right]\bar{\theta}^{2}-e\left[\underline{\theta}Y_{\theta}\left[A_{\theta}\right]\right]f^{\theta}\left[\underline{\theta}\right]F^{\phi}\left[\frac{\underline{\theta}Y_{\theta}\left[A_{\theta}\right]}{Y_{\phi}\left[A_{\theta}\right]}\right]\underline{\theta}^{2}\right)Y_{\theta}'\left[A_{\theta}\right] & >\\
Y_{\theta}\left[A_{\theta}\right]\frac{1}{Y_{\theta}\left[A_{\theta}\right]{}^{4}}\cdot Y_{\theta}'\left[A_{\theta}\right]\cdot\left(2Y_{\theta}\left[A_{\theta}\right]\overline{y}_{\theta}\left(1+Y_{\theta}\left[A_{\theta}\right]\right)+\int_{\underline{w}_{x}}^{\overline{w}_{x}}e'[w]w^{2}Y_{\theta}f_{x}^{\theta}(w)\ dw+\beta\frac{1}{Y_{\phi}}\right)\\
-x\cdot Y_{\theta}\left[A_{\theta}\right]\cdot\frac{1}{Y_{\phi}\left[A_{\theta}\right]{}^{4}}\cdot Y_{\phi}'\left[A_{\theta}\right]\cdot\left(2Y_{\phi}\left[A_{\theta}\right]\overline{y}_{\phi}\left(1+Y_{\phi}\left[A_{\theta}\right]\right)+\int_{\underline{w}_{x}}^{\overline{w}_{x}}e'[w]w^{2}Y_{\phi}f_{x}^{\phi}(w)\ dw+\beta\frac{1}{Y_{\theta}}\right)\\
+\frac{1}{Y_{\theta}\left[A_{\theta}\right]Y_{\phi}\left[A_{\theta}\right]{}^{2}}\cdot Y_{\phi}'\left[A_{\theta}\right]\cdot\beta\\
-x\cdot\frac{1}{Y_{\theta}\left[A_{\theta}\right]Y_{\phi}\left[A_{\theta}\right]{}^{2}}\cdot Y_{\theta}'\left[A_{\theta}\right]\cdot\beta & .
\end{align*}

\end_inset

Put components on one side
\begin_inset Formula 
\begin{align*}
\left(e\left[\overline{w}_{x}\right]f_{x}^{\theta}\left[\bar{w}_{x}\right]\bar{\theta}^{2}-e\left[\underline{w}_{x}\right]f_{x}^{\theta}\left[\underline{w}_{x}\right]\underline{\theta}^{2}\right)Y_{\theta}\left[A_{\theta}\right]Y_{\theta}'\left[A_{\theta}\right]\\
+x\cdot Y_{\theta}\left[A_{\theta}\right]\cdot\frac{1}{Y_{\phi}\left[A_{\theta}\right]{}^{4}}\cdot Y_{\phi}'\left[A_{\theta}\right]\cdot\left(2Y_{\phi}\left[A_{\theta}\right]\overline{y}_{\phi}\left(1+Y_{\phi}\left[A_{\theta}\right]\right)+\int_{\underline{w}_{x}}^{\overline{w}_{x}}e'[w]w^{2}Y_{\phi}f_{x}^{\phi}(w)\ dw+\beta\frac{1}{Y_{\theta}}\right)\\
-Y_{\theta}\left[A_{\theta}\right]\frac{1}{Y_{\theta}\left[A_{\theta}\right]{}^{4}}\cdot Y_{\theta}'\left[A_{\theta}\right]\cdot\left(2Y_{\theta}\left[A_{\theta}\right]\overline{y}_{\theta}\left(1+Y_{\theta}\left[A_{\theta}\right]\right)+\int_{\underline{w}_{x}}^{\overline{w}_{x}}e'[w]w^{2}Y_{\theta}f_{x}^{\theta}(w)\ dw+\beta\frac{1}{Y_{\phi}}\right)\\
+\frac{1}{Y_{\theta}\left[A_{\theta}\right]Y_{\phi}\left[A_{\theta}\right]{}^{2}}\cdot\beta\left(xY_{\theta}'\left[A_{\theta}\right]-Y_{\phi}'\left[A_{\theta}\right]\right)\\
>0 & .
\end{align*}

\end_inset

Divide by 
\begin_inset Formula $Y_{\theta}$
\end_inset


\begin_inset Formula 
\begin{align*}
\left(e\left[\overline{w}_{x}\right]f_{x}^{\theta}\left[\bar{w}_{x}\right]\bar{\theta}^{2}-e\left[\underline{w}_{x}\right]f_{x}^{\theta}\left[\underline{w}_{x}\right]\underline{\theta}^{2}\right)Y_{\theta}'\left[A_{\theta}\right]\\
+x\cdot\frac{1}{Y_{\phi}\left[A_{\theta}\right]{}^{4}}\cdot Y_{\phi}'\left[A_{\theta}\right]\cdot\left(2Y_{\phi}\left[A_{\theta}\right]\overline{y}_{\phi}\left(1+Y_{\phi}\left[A_{\theta}\right]\right)+\int_{\underline{w}_{x}}^{\overline{w}_{x}}e'[w]w^{2}Y_{\phi}f_{x}^{\phi}(w)\ dw+\beta\frac{1}{Y_{\theta}}\right)\\
-\frac{1}{Y_{\theta}\left[A_{\theta}\right]{}^{4}}\cdot Y_{\theta}'\left[A_{\theta}\right]\cdot\left(2Y_{\theta}\left[A_{\theta}\right]\overline{y}_{\theta}\left(1+Y_{\theta}\left[A_{\theta}\right]\right)+\int_{\underline{w}_{x}}^{\overline{w}_{x}}e'[w]w^{2}Y_{\theta}f_{x}^{\theta}(w)\ dw+\beta\frac{1}{Y_{\phi}}\right)\\
+\frac{1}{Y_{\theta}\left[A_{\theta}\right]^{2}Y_{\phi}\left[A_{\theta}\right]{}^{2}}\cdot\beta\left(xY_{\theta}'\left[A_{\theta}\right]-Y_{\phi}'\left[A_{\theta}\right]\right)\\
>0 & .
\end{align*}

\end_inset

Multiply by 
\begin_inset Formula $Y_{\theta}\left[A_{\theta}\right]^{4}Y_{\phi}\left[A_{\theta}\right]{}^{4}$
\end_inset


\begin_inset Formula 
\begin{align*}
\left(e\left[\overline{w}_{x}\right]f_{x}^{\theta}\left[\bar{w}_{x}\right]\bar{\theta}^{2}-e\left[\underline{w}_{x}\right]f_{x}^{\theta}\left[\underline{w}_{x}\right]\underline{\theta}^{2}\right)Y_{\theta}'\left[A_{\theta}\right]Y_{\theta}\left[A_{\theta}\right]^{4}Y_{\phi}\left[A_{\theta}\right]{}^{4}\\
+x\cdot Y_{\theta}\left[A_{\theta}\right]{}^{4}\cdot Y_{\phi}'\left[A_{\theta}\right]\cdot\left(2Y_{\phi}\left[A_{\theta}\right]\overline{y}_{\phi}\left(1+Y_{\phi}\left[A_{\theta}\right]\right)+\int_{\underline{w}_{x}}^{\overline{w}_{x}}e'[w]w^{2}Y_{\phi}f_{x}^{\phi}(w)\ dw+\beta\frac{1}{Y_{\theta}}\right)\\
-Y_{\phi}\left[A_{\theta}\right]{}^{4}\cdot Y_{\theta}'\left[A_{\theta}\right]\cdot\left(2Y_{\theta}\left[A_{\theta}\right]\overline{y}_{\theta}\left(1+Y_{\theta}\left[A_{\theta}\right]\right)+\int_{\underline{w}_{x}}^{\overline{w}_{x}}e'[w]w^{2}Y_{\theta}f_{x}^{\theta}(w)\ dw+\beta\frac{1}{Y_{\phi}}\right)\\
+\frac{1}{Y_{\theta}\left[A_{\theta}\right]^{2}Y_{\phi}\left[A_{\theta}\right]{}^{2}}\cdot\beta\left(xY_{\theta}'\left[A_{\theta}\right]-Y_{\phi}'\left[A_{\theta}\right]\right)\\
>0 & .
\end{align*}

\end_inset

If the third line does not get too large, the expression holds.
\end_layout

\end_body
\end_document
