#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\use_default_options true
\begin_modules
theorems-ams
\end_modules
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 3cm
\rightmargin 3cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
This note takes two approaches to show that in our model with endogenous
 sectoral choice, wage rates respond to changes in tax rates even if the
 production function exhibits constant returns to scale.
\end_layout

\begin_layout Section
Proof by contradiction
\end_layout

\begin_layout Proof
With constant returns to scale wage rates only depend on relative labor
 supplies.
 Suppose, to the contrary, that wage rates are unaffected by a change in
 the tax rate.
 It should then follow that a change in the tax rate does not affect relative
 labor supplies.
 Hence,
\begin_inset Formula 
\[
d\ln\left(\frac{H}{L}\right)/dt
\]

\end_inset

 should be constant if wages are kept constant.
 With
\begin_inset Formula 
\[
L=\int_{\underline{\theta}}^{\theta^{*}}\theta h_{\theta}^{L}dF(\theta),
\]

\end_inset


\begin_inset Formula 
\[
H=\int_{\theta^{*}}^{\overline{\theta}}\theta h_{\theta}^{H}dF(\theta),
\]

\end_inset

and
\begin_inset Formula 
\[
h_{\theta}^{j}=\left[\theta w^{j}\left(1-t\right)\right]^{\varepsilon},
\]

\end_inset

this becomes
\begin_inset Formula 
\begin{align}
\frac{d\ln\left(\frac{H}{L}\right)}{dt}= & \frac{\frac{dH}{dt}}{H}-\frac{\frac{dL}{dt}}{L}\nonumber \\
= & \frac{\int_{\theta^{*}}^{\overline{\theta}}\theta\frac{dh_{\theta}^{H}}{dt}dF(\theta)}{H}-\frac{\int_{\underline{\theta}}^{\theta^{*}}\theta\frac{dh_{\theta}^{L}}{dt}dF(\theta)}{L}\label{eq:d(H/L)dt}\\
 & +\theta^{*}f\left(\theta^{*}\right)\frac{d\theta^{*}}{dt}\left(\frac{h_{\theta^{*}}^{L}}{L}-\frac{h_{\theta^{*}}^{H}}{H}\right).\nonumber 
\end{align}

\end_inset

Now use that with constant wage rates
\begin_inset Formula 
\begin{align*}
-\frac{dh_{\theta}^{j}}{dt}\frac{\left(1-t\right)}{h_{\theta}^{j}}=\varepsilon & \Leftrightarrow\\
\frac{dh_{\theta}^{j}}{dt}=\varepsilon\left(-\frac{h_{\theta}^{j}}{1-t}\right).
\end{align*}

\end_inset

We can thus write
\begin_inset Formula 
\begin{align*}
\frac{\int_{\theta^{*}}^{\overline{\theta}}\theta\frac{dh_{\theta}^{H}}{dt}dF(\theta)}{H}-\frac{\int_{\underline{\theta}}^{\theta^{*}}\theta\frac{dh_{\theta}^{L}}{dt}dF(\theta)}{L}= & \frac{-\frac{\varepsilon}{1-t}\int_{\theta^{*}}^{\overline{\theta}}\theta h_{\theta}^{H}dF(\theta)}{H}-\frac{-\frac{\varepsilon}{1-t}\int_{\underline{\theta}}^{\theta^{*}}\theta h_{\theta}^{L}dF(\theta)}{L}\\
= & -\frac{\varepsilon}{1-t}\frac{H}{H}+\frac{\varepsilon}{1-t}\frac{L}{L}\\
= & 0.
\end{align*}

\end_inset

Hence 
\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:d(H/L)dt"

\end_inset

 becomes
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none

\begin_inset Formula 
\[
\frac{d\ln\left(\frac{H}{L}\right)}{dt}=\theta^{*}f\left(\theta^{*}\right)\frac{d\theta^{*}}{dt}\left(\frac{h_{\theta^{*}}^{L}}{L}-\frac{h_{\theta^{*}}^{H}}{H}\right).
\]

\end_inset

We have 
\begin_inset Formula 
\[
\theta^{*}=\left[\frac{\left(p-s\right)\left(1+\varepsilon\right)}{\left(1-t\right)^{1+\varepsilon}\left(\left(w^{H}\right)^{1+\varepsilon}-\left(w^{L}\right)^{1+\varepsilon}\right)}\right]^{\frac{1}{1+\varepsilon}},
\]

\end_inset

and thus with fixed wages
\begin_inset Formula 
\[
\frac{d\theta^{*}}{dt}=\frac{\theta^{*}}{1-t}\neq0.
\]

\end_inset

Moreover, since in general 
\begin_inset Formula 
\[
\left(\frac{h_{\theta^{*}}^{L}}{L}-\frac{h_{\theta^{*}}^{H}}{H}\right)\neq0,
\]

\end_inset

we have reached a contradiction.
\end_layout

\begin_layout Section
Proof using log-linearization
\end_layout

\begin_layout Standard
Here, we log-linearize the equilibrium relationships of the model and then
 investigate the effect of a relative change in the tax rate on relative
 changes in wage rates, keeping technology as well as the subsidy constant.
\end_layout

\begin_layout Standard
Equilibrium is characterized by the following system of seven equations
 in seven unknowns, where 
\begin_inset Formula $\tilde{x}\approx\frac{dx}{x}$
\end_inset

 for all variables apart from 
\begin_inset Formula $t$
\end_inset

, where 
\begin_inset Formula $\tilde{t}\approx\frac{dt}{1-t}.$
\end_inset

 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\tilde{w}^{L}+\frac{1-\alpha}{\sigma}\tilde{L}-\frac{1-\alpha}{\sigma}\tilde{H}=\frac{1-\alpha}{\sigma}\tilde{A},
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\tilde{w}^{H}-\frac{\alpha}{\sigma}\tilde{L}+\frac{\alpha}{\sigma}\tilde{H}=\left(1-\frac{\alpha}{\sigma}\right)\tilde{A},
\]

\end_inset


\begin_inset Formula 
\[
\tilde{h}^{L}-\varepsilon\tilde{w}^{L}=-\varepsilon\tilde{t},
\]

\end_inset


\begin_inset Formula 
\[
\tilde{h}^{H}-\varepsilon\tilde{w}^{H}=-\varepsilon\tilde{t},
\]

\end_inset


\begin_inset Formula 
\[
\tilde{L}-\tilde{h}^{L}-\frac{h_{\theta^{*}}^{L}}{L}\theta^{*}f\left(\theta^{*}\right)\tilde{\theta^{*}}=0,
\]

\end_inset


\begin_inset Formula 
\[
\tilde{H}-\tilde{h}^{H}+\frac{h_{\theta^{*}}^{H}}{H}\theta^{*}f\left(\theta^{*}\right)\tilde{\theta^{*}}=0,
\]

\end_inset


\begin_inset Formula 
\[
\tilde{\theta^{*}}\approx\frac{1}{1+\varepsilon}\tilde{\left(p-s\right)}+\tilde{t}-\frac{\left(w^{H}\right)^{1+\varepsilon}}{\left(w^{H}\right)^{1+\varepsilon}-\left(w^{L}\right)^{1+\varepsilon}}\tilde{w^{H}}+\frac{\left(w^{L}\right)^{1+\varepsilon}}{\left(w^{H}\right)^{1+\varepsilon}-\left(w^{L}\right)^{1+\varepsilon}}\tilde{w^{L}}.
\]

\end_inset

In order to simplify notation we define
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\rho\equiv1+\varepsilon
\]

\end_inset

and
\begin_inset Formula 
\[
\Delta_{w}\equiv\left(w^{H}\right)^{1+\varepsilon}-\left(w^{L}\right)^{1+\varepsilon}
\]

\end_inset

to write
\begin_inset Formula 
\[
\tilde{\theta^{*}}-\frac{\left(w^{L}\right)^{\rho}}{\Delta_{w}}\tilde{w^{L}}+\frac{\left(w^{H}\right)^{\rho}}{\Delta_{w}}\tilde{w^{H}}=\tilde{t}+\frac{1}{\rho}\tilde{\left(p-s\right)}.
\]

\end_inset

Moreover, we define
\begin_inset Formula 
\[
\kappa_{L}\equiv\theta^{*}\frac{h_{\theta^{*}}^{L}}{L}f\left(\theta^{*}\right),
\]

\end_inset

and
\begin_inset Formula 
\[
\kappa_{H}\equiv\theta^{*}\frac{h_{\theta^{*}}^{H}}{H}f\left(\theta^{*}\right).
\]

\end_inset

In matrix form the system reads 
\begin_inset Formula $Mx=b$
\end_inset

, with
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
M=\left[\begin{array}{ccccccc}
1 & 0 & 0 & 0 & \frac{1-\alpha}{\sigma} & -\frac{1-\alpha}{\sigma} & 0\\
0 & 1 & 0 & 0 & -\frac{\alpha}{\sigma} & \frac{\alpha}{\sigma} & 0\\
-\varepsilon & 0 & 1 & 0 & 0 & 0 & 0\\
0 & -\varepsilon & 0 & 1 & 0 & 0 & 0\\
0 & 0 & -1 & 0 & 1 & 0 & -\kappa_{L}\\
0 & 0 & 0 & -1 & 0 & 1 & \kappa_{H}\\
-\frac{\left(w^{L}\right)^{\rho}}{\Delta_{w}} & \frac{\left(w^{H}\right)^{\rho}}{\Delta_{w}} & 0 & 0 & 0 & 0 & 1
\end{array}\right],\ x=\left[\begin{array}{c}
\tilde{w^{L}}\\
\tilde{w^{H}}\\
\tilde{h^{L}}\\
\tilde{h^{H}}\\
\tilde{L}\\
\tilde{H}\\
\tilde{\theta^{*}}
\end{array}\right],\ b=\left[\begin{array}{c}
\frac{1-\alpha}{\sigma}\tilde{A}\\
\left(1-\frac{\alpha}{\sigma}\right)\tilde{A}\\
-\varepsilon\tilde{t}\\
-\varepsilon\tilde{t}\\
0\\
0\\
\frac{1}{\rho}\tilde{\left(p-s\right)}+\tilde{t}
\end{array}\right].
\]

\end_inset


\end_layout

\begin_layout Subsection
Solutions
\end_layout

\begin_layout Standard
The solutions are
\begin_inset Foot
status open

\begin_layout Plain Layout
Notation here is a bit sloppy, as we use 
\begin_inset Formula $=$
\end_inset

 instead of 
\begin_inset Formula $\approx$
\end_inset

.
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
\tilde{w}^{L}=\frac{(-1+\alpha)\left(-\left(\tilde{p-s}+\rho\tilde{t}\right)\Delta_{w}\left(\kappa_{H}+\kappa_{L}\right)+\rho\tilde{A}\left((1+\epsilon)\Delta_{w}+\left(w^{H}\right)^{\rho}\left(\kappa_{H}+\kappa_{L}\right)\right)\right)}{\rho\left(-(\epsilon+\sigma)\Delta_{w}+\left(\left(w^{L}\right)^{\rho}(-1+\alpha)-\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)\right)},\label{eq:wLtilde-solution}
\end{equation}

\end_inset


\begin_inset Formula 
\begin{equation}
\tilde{w}^{H}=\frac{\alpha\left(\tilde{p-s}+\rho\tilde{t}\right)\Delta_{w}\left(\kappa_{H}+\kappa_{L}\right)+\rho\tilde{A}\left((\epsilon-\alpha(1+\epsilon)+\sigma)\Delta_{w}-\left(w^{L}\right)^{\rho}(-1+\alpha)\left(\kappa_{H}+\kappa_{L}\right)\right)}{\rho\left((\epsilon+\sigma)\Delta_{w}+\left(-\left(w^{L}\right)^{\rho}(-1+\alpha)+\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)\right)},\label{eq:wHtilde-solution}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align*}
\tilde{h}^{L}= & -\epsilon\Bigg[-(-1+\alpha)\tilde{p-s}\Delta_{w}\left(\kappa_{H}+\kappa_{L}\right)+(-1+\alpha)\rho\tilde{A}\left((1+\epsilon)\Delta_{w}+\left(w^{H}\right)^{\rho}\left(\kappa_{H}+\kappa_{L}\right)\right)\\
+ & \rho\tilde{t}\left(\left(-\left(w^{L}\right)^{\rho}(-1+\alpha)+\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)+\Delta_{w}\left(\epsilon+\sigma+\kappa_{H}-\alpha\kappa_{H}+\kappa_{L}-\alpha\kappa_{L}\right)\right)\Bigg]\\
\cdot & \rho^{-1}\left((\epsilon+\sigma)\Delta_{w}+\left(-\left(w^{L}\right)^{\rho}(-1+\alpha)+\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)\right)^{-1},
\end{align*}

\end_inset


\begin_inset Formula 
\begin{align*}
\tilde{h}^{H}= & \epsilon\Bigg[\alpha\tilde{p-s}\Delta_{w}\left(\kappa_{H}+\kappa_{L}\right)+\rho\tilde{A}\left((\epsilon-\alpha(1+\epsilon)+\sigma)\Delta_{w}-\left(w^{L}\right)^{\rho}(-1+\alpha)\left(\kappa_{H}+\kappa_{L}\right)\right)\\
- & \rho\tilde{t}\left(\left(-\left(w^{L}\right)^{\rho}(-1+\alpha)+\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)+\Delta_{w}\left(\epsilon+\sigma-\alpha\left(\kappa_{H}+\kappa_{L}\right)\right)\right)\Bigg]\\
\cdot & \rho^{-1}\left((\epsilon+\sigma)\Delta_{w}+\left(-\left(w^{L}\right)^{\rho}(-1+\alpha)+\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)\right)^{-1},
\end{align*}

\end_inset


\begin_inset Formula 
\begin{align*}
\tilde{L}= & \Bigg[\tilde{p-s}\Delta_{w}\left((-1+\alpha)\epsilon\kappa_{H}+(\alpha\epsilon+\sigma)\kappa_{L}\right)+\\
+ & \rho\tilde{A}\left(-(-1+\alpha)(1+\epsilon)\left(\epsilon\Delta_{w}+\left(w^{L}\right)^{\rho}\kappa_{L}\right)+\left(w^{H}\right)^{\rho}\left((\epsilon-\alpha\epsilon)\kappa_{H}+(\alpha-\sigma)\kappa_{L}\right)\right)\\
+ & \rho\tilde{t}\left(\left(\left(w^{L}\right)^{\rho}(-1+\alpha)-\left(w^{H}\right)^{\rho}\alpha\right)\epsilon\left(\kappa_{H}+\kappa_{L}\right)+\Delta_{w}\left(-\epsilon(\epsilon+\sigma)+(-1+\alpha)\epsilon\kappa_{H}+(\alpha\epsilon+\sigma)\kappa_{L}\right)\right)\Bigg]\\
\cdot & \rho^{-1}\left((\epsilon+\sigma)\Delta_{w}+\left(-\left(w^{L}\right)^{\rho}(-1+\alpha)+\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)\right)^{-1},
\end{align*}

\end_inset


\begin_inset Formula 
\begin{align*}
\tilde{H}= & \Bigg[\tilde{p-s}\Delta_{w}\left(((-1+\alpha)\epsilon-\sigma)\kappa_{H}+\alpha\epsilon\kappa_{L}\right)\\
+ & \rho\tilde{A}\left(-(\alpha-\epsilon+\alpha\epsilon-\sigma)\left(\epsilon\Delta_{w}+\left(w^{H}\right)^{\rho}\kappa_{H}\right)+\left(w^{L}\right)^{\rho}(-1+\alpha)\left(\kappa_{H}-\epsilon\kappa_{L}\right)\right)\\
+ & \rho\tilde{t}\left(\left(\left(w^{L}\right)^{\rho}(-1+\alpha)-\left(w^{H}\right)^{\rho}\alpha\right)\epsilon\left(\kappa_{H}+\kappa_{L}\right)-\Delta_{w}\left((\epsilon-\alpha\epsilon+\sigma)\kappa_{H}+\epsilon\left(\epsilon+\sigma-\alpha\kappa_{L}\right)\right)\right)\Bigg]\\
\cdot & \rho^{-1}\left((\epsilon+\sigma)\Delta_{w}+\left(-\left(w^{L}\right)^{\rho}(-1+\alpha)+\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)\right)^{-1},
\end{align*}

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align}
\tilde{\theta}^{*}= & \frac{\rho\left(\left(w^{L}\right)^{\rho}(-1+\alpha)(1+\epsilon)+\left(w^{H}\right)^{\rho}(\epsilon-\alpha(1+\epsilon)+\sigma)\right)\tilde{A}-(\epsilon+\sigma)\left(\tilde{p-s}+\rho\tilde{t}\right)\Delta_{w}}{\rho\left(-(\epsilon+\sigma)\Delta_{w}+\left(\left(w^{L}\right)^{\rho}(-1+\alpha)-\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)\right)}.\label{eq:thetatilde}
\end{align}

\end_inset


\end_layout

\begin_layout Subsection
Effect of tax change on wage rates
\end_layout

\begin_layout Proof
We now investigate the effect of a change in the tax rate on wage rates,
 when keeping technology as well as the subsidy unchanged.
 Define
\begin_inset Formula 
\[
\Upsilon\equiv\frac{\Delta_{w}\left(\kappa_{H}+\kappa_{L}\right)}{(\epsilon+\sigma)\Delta_{w}+\left(\left(w^{L}\right)^{\rho}(1-\alpha)+\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)}.
\]

\end_inset

With 
\begin_inset Formula $\tilde{A}=\tilde{\left(p-s\right)=0}$
\end_inset

 
\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:wLtilde-solution"

\end_inset

 simplifies to
\begin_inset Formula 
\[
\tilde{w}^{L}=\left(1-\alpha\right)\Upsilon\tilde{t},
\]

\end_inset

and 
\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:wHtilde-solution"

\end_inset

 becomes
\begin_inset Formula 
\[
\tilde{w}^{H}=\alpha\Upsilon\tilde{t}.
\]

\end_inset

We thus conclude that a relative change in the tax rates affects both 
\begin_inset Formula $w^{L}$
\end_inset

 and 
\begin_inset Formula $w^{H}$
\end_inset

.
 Moreover, the effect is in general different.
\end_layout

\end_body
\end_document
