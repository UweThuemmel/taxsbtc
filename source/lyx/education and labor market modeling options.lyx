#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine natbib_authoryear
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Section
Modeling options
\end_layout

\begin_layout Standard
The individual decides whether or not to attend college.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
C=\begin{cases}
1 & \mbox{if the individual attends college}\\
0 & \mbox{otherwise}
\end{cases}.
\]

\end_inset


\end_layout

\begin_layout Standard
There are two sectors in the economy, 
\begin_inset Formula $H$
\end_inset

 and 
\begin_inset Formula $L$
\end_inset

.
 An individual can only work in the 
\begin_inset Formula $H$
\end_inset

-sector if she went to college.
 Wages in the respective sectors are determined as in 
\begin_inset CommandInset citation
LatexCommand citet
key "Rothschild2013"

\end_inset

, 
\begin_inset Formula 
\[
w(\theta,\phi)=\begin{cases}
Y_{\theta}\theta & \mbox{if }S(\theta,\phi)=H\\
Y_{\phi}\phi & \mbox{if }S(\theta,\phi)=L
\end{cases}.
\]

\end_inset

Income is then determined as 
\begin_inset Formula $y=e(\theta,\phi)\cdot w(\theta,\phi)$
\end_inset

.
 If like in 
\begin_inset CommandInset citation
LatexCommand citet
key "Rothschild2013"

\end_inset

, sectoral choice should be endogenous based on the potential wage 
\begin_inset Formula $w(\theta,\phi)$
\end_inset

 which can be earned in the sectors (and thus based on comparative advantage),
 school choice needs to be endogenous in the same way.
 That is, an individual chooses 
\begin_inset Formula $C=1$
\end_inset

 if and only if 
\begin_inset Formula $Y_{\theta}\theta>Y_{\phi}\phi$
\end_inset

.
 We can now make 
\begin_inset Formula $\theta$
\end_inset

 and 
\begin_inset Formula $\phi$
\end_inset

 a function of innate ability 
\begin_inset Formula $a$
\end_inset

 and schooling 
\begin_inset Formula $C$
\end_inset

.
 How can we ensure that for all individuals who choose 
\begin_inset Formula $C=1$
\end_inset

, 
\begin_inset Formula $Y_{\theta}\theta>Y_{\phi}\phi$
\end_inset

? This would be somehow artificial, as I would then assume that an individual
 could still work in both sectors.
 If we want to keep endogenous choice of sectors, college should not be
 a necessary requirement for working in sector 
\begin_inset Formula $H$
\end_inset

.
 The question is what education does.
 Is it a pre-condition for working in a specific sector? Or does it simply
 alter 
\begin_inset Formula $\theta$
\end_inset

 and 
\begin_inset Formula $\phi$
\end_inset

 without restricting sectoral choice per se? (it still restricts it implicitly,
 but only based on altering comparative advantage).
\end_layout

\begin_layout Standard
Modeling choices are
\end_layout

\begin_layout Itemize
a certain level of schooling is a pre-condition for working in a specific
 sector
\end_layout

\begin_layout Itemize
why don't we let someone who did not go to medical school do surgery? one
 could argue that this is so, since his productivity as a surgeon is comparative
ly lower than that of a sandwich architect.
 Suppose that 
\begin_inset Formula $\theta$
\end_inset

 is the productivity as a surgeon, and 
\begin_inset Formula $\phi$
\end_inset

 is the productivity as a sandwich architect.
 The distribution of productivities now needs to be such that without schooling
 no-one ever becomes a surgeon.
 That means, without schooling 
\begin_inset Formula $\theta$
\end_inset

 needs to be so low that it never pays off to do surgery instead of becoming
 a sandwich architect.
 (This imposes restrictions on production functions, etc.)
\end_layout

\begin_layout Itemize
Do we actually need an additional ability dimension?
\end_layout

\begin_layout Itemize
suppose, schooling simply improves 
\begin_inset Formula $\theta$
\end_inset

 without changing 
\begin_inset Formula $\phi$
\end_inset

 (this is a normalization which should be wlog)
\end_layout

\begin_layout Itemize
the improvement can be a function of 
\begin_inset Formula $(\theta,\phi)$
\end_inset

 - or not
\end_layout

\begin_layout Standard
Make clear what can - and what cannot be studied in terms of education in
 the model by 
\begin_inset CommandInset citation
LatexCommand citet
key "Rothschild2013"

\end_inset

 when introducing differential costs of sectoral choice.
\end_layout

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "/media/data/Dropbox/Tinbergen Institute/articles/My Library"
options "/media/data/Dropbox/PhD/bibtex/economic/econometrica"

\end_inset


\end_layout

\end_body
\end_document
