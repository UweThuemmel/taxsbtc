#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\begin_preamble
 \usepackage{color}
\usepackage{todonotes}
 \definecolor{hellgelb}{rgb}{1,1,0.85}
 \definecolor{colKeys}{rgb}{0,0,1}
 \definecolor{colIdentifier}{rgb}{0,0,0}
 \definecolor{colComments}{rgb}{1,0,0}
 \definecolor{colString}{rgb}{0,0.5,0}
 \lstset{%
      language=Matlab,%
      float=hbp,%
      basicstyle=\footnotesize\ttfamily,%
      identifierstyle=\color{colIdentifier},%
      keywordstyle=\color{colKeys},%
      stringstyle=\color{colString},%
      commentstyle=\itshape\color{colComments},%
      columns=fixed,
      tabsize=4,%
      frame=single,%
      framerule=1pt,
      extendedchars=true,%
      showspaces=false,%
      showstringspaces=false,%
      numbers=left,%
      numberstyle=\tiny\ttfamily,%
      numbersep=1em,%
      breaklines=true,%
      breakindent=10pt,%
      backgroundcolor=\color{hellgelb},%
      breakautoindent=true,%
      captionpos=t,%
      xleftmargin=1em,%
      xrightmargin=\fboxsep%
 }
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 3cm
\rightmargin 3cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation skip
\defskip smallskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

%%% Always I forget this so I created some aliases
\end_layout

\begin_layout Plain Layout


\backslash
def
\backslash
ContinueLineNumber{
\backslash
lstset{firstnumber=last}}
\end_layout

\begin_layout Plain Layout


\backslash
def
\backslash
StartLineAt#1{
\backslash
lstset{firstnumber=#1}}
\end_layout

\begin_layout Plain Layout


\backslash
let
\backslash
numberLineAt
\backslash
StartLineAt
\end_layout

\end_inset


\end_layout

\begin_layout Standard
The code contains a call to the function 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
verb+findxi.m+
\end_layout

\end_inset

 which in turn contains the formula for the optimal marginal tax rate for
 the case of quasi-linear utility.
\end_layout

\begin_layout Standard
\begin_inset Float algorithm
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

function s=findxi(xi)
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

global x num_w gam ggamma yy_theta yy_phi w dw ff gg f f_phi f_theta;
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

aux=zeros(num_w,1);
\end_layout

\begin_layout Plain Layout

for i=1:num_w
\end_layout

\begin_layout Plain Layout

   %if 1+xi.*(1+ggamma)*(yy_phi/(yy_phi+x*yy_theta)-f_phi(i)./f(i))<0;
\end_layout

\begin_layout Plain Layout

   %    aux(i)=1e10;
\end_layout

\begin_layout Plain Layout

   %else
\end_layout

\begin_layout Plain Layout

       aux(i)=w(i).^(gam./(gam-1)).*(max(0,(1+xi.*(1+ggamma)*(yy_phi/(yy_phi+x*yy_th
eta)-f_phi(i)./f(i)))./(1+gam*(gg(i)-ff(i))./(w(i)*f(i)/dw)))).^(1/(gam-1)).*(ggamma*
f_theta(i)-f_phi(i));
\end_layout

\begin_layout Plain Layout

   %end
\end_layout

\begin_layout Plain Layout

end
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

s = sum(aux);
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
findxi.m
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Consider the following expression
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
\mbox{aux}(i,\xi)=w(i)^{\frac{\gamma}{\gamma-1}}\cdot\max\left[0,\frac{1+\xi\cdot(1+\Gamma)\cdot\left(\frac{Y_{\varphi}}{Y_{\varphi}+x\cdot Y_{\theta}}-\frac{f_{\varphi}(i)}{f(i)}\right)}{1+\frac{\gamma\cdot(G(i)-F(i))}{w(i)\cdot\frac{f(i)}{dw}}}\right]^{\frac{1}{\gamma-1}}\cdot\left(\Gamma f_{\theta}(i)-f_{\varphi}(i)\right)\label{eq:aux}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
which corresponds to line 11 in 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
verb+findxi.m+
\end_layout

\end_inset

.
 I am concerned with 
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none

\begin_inset Formula 
\[
\frac{1+\xi\cdot(1+\Gamma)\cdot\left(\frac{Y_{\varphi}}{Y_{\varphi}+x\cdot Y_{\theta}}-\frac{f_{\varphi}(i)}{f(i)}\right)}{1+\frac{\gamma\cdot(G(i)-F(i))}{w(i)\cdot\frac{f(i)}{dw}}}.
\]

\end_inset


\end_layout

\begin_layout Standard
In the main code we have
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
StartLineAt{123}
\end_layout

\end_inset


\begin_inset listings
inline false
status open

\begin_layout Plain Layout

ggamma=yy_phi/(x*yy_theta);
\end_layout

\begin_layout Plain Layout

%elast=.5; %now constained in the lognormal.mat loaded at the top
\end_layout

\begin_layout Plain Layout

gam=1+1./elast;
\end_layout

\end_inset

Variable
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit

\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
verb+ggamma+
\end_layout

\end_inset

 is thus defined as
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align*}
\Gamma= & \frac{Y_{\varphi}}{x\cdot Y_{\theta}}=\frac{Y_{\varphi}}{Y_{\theta}}\frac{E_{\varphi}}{E_{\theta}}
\end{align*}

\end_inset

which implies 
\begin_inset Formula 
\begin{align*}
1+\Gamma=1+ & \frac{Y_{\varphi}}{Y_{\theta}}\frac{E_{\varphi}}{E_{\theta}}\\
= & \frac{Y_{\varphi}E_{\varphi}+Y_{\theta}E_{\theta}}{Y_{\theta}E_{\theta}}\\
= & \frac{Y}{Y_{\theta}E_{\theta}}\\
= & \frac{1}{\alpha(x)},
\end{align*}

\end_inset

where the second-to-last step uses Euler's Theorem, and the final step uses
 the definition of 
\begin_inset Formula $\alpha(x)$
\end_inset

.
 Moreover, use that
\begin_inset Formula 
\[
\gamma=1+\frac{1}{\varepsilon}
\]

\end_inset

Suppressing the argument 
\begin_inset Formula $x$
\end_inset

, we thus can write
\begin_inset Formula 
\begin{align}
\frac{1+\xi\cdot(1+\Gamma)\cdot\left(\frac{Y_{\varphi}}{Y_{\varphi}+x\cdot Y_{\theta}}-\frac{f_{\varphi}(i)}{f(i)}\right)}{1+\frac{\gamma\cdot(G(i)-F(i))}{w(i)\cdot\frac{f(i)}{dw}}} & =\nonumber \\
\frac{1+\xi\cdot(\frac{1}{\alpha})\cdot\left(\frac{Y_{\varphi}}{Y_{\varphi}+x\cdot Y_{\theta}}-\frac{f_{\varphi}(i)}{f(i)}\right)}{1+\frac{(G(i)-F(i))}{w(i)\cdot\frac{f(i)}{dw}}\left(1+\frac{1}{\varepsilon}\right)}.\label{eq:formula 1}
\end{align}

\end_inset

Next, consider the following part of the numerator 
\begin_inset Formula 
\[
\left(\frac{Y_{\varphi}}{Y_{\varphi}+x\cdot Y_{\theta}}-\frac{f_{\varphi}(i)}{f(i)}\right).
\]

\end_inset

Using the alternative definition for 
\begin_inset Formula $\alpha$
\end_inset

,
\begin_inset Formula 
\[
\alpha=\frac{xY_{\theta}}{xY_{\theta}+Y_{\varphi}},
\]

\end_inset

we obtain that
\begin_inset Formula 
\[
\frac{Y_{\varphi}}{Y_{\varphi}+x\cdot Y_{\theta}}=\frac{Y_{\varphi}+xY_{\theta}}{Y_{\varphi}+x\cdot Y_{\theta}}-\frac{xY_{\theta}}{Y_{\varphi}+x\cdot Y_{\theta}}=1-\alpha.
\]

\end_inset

Moreover, using
\begin_inset Formula 
\[
f_{\varphi}+f_{\theta}=f,
\]

\end_inset

we can write
\begin_inset Formula 
\[
\left(\frac{Y_{\varphi}}{Y_{\varphi}+x\cdot Y_{\theta}}-\frac{f_{\varphi}(i)}{f(i)}\right)=\left(1-\alpha-\frac{f(i)-f_{\theta}(i)}{f(i)}\right)=\left(1-\alpha-1+\frac{f_{\theta}(i)}{f(i)}\right)=\left(\frac{f_{\theta}(i)}{f(i)}-\alpha\right).
\]

\end_inset

Substituting this into 
\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:formula 1"

\end_inset

, we obtain
\begin_inset Formula 
\[
\frac{1+\xi\cdot(\frac{1}{\alpha})\cdot\left(\frac{f_{\theta}(i)}{f_{\varphi}(i)}-\alpha\right)}{1+\frac{(G(i)-F(i))}{w(i)\cdot\frac{f(i)}{dw}}\left(1+\frac{1}{\varepsilon}\right)}.
\]

\end_inset

Moving from a discrete version to a continuous version leaves us with
\begin_inset Formula 
\begin{equation}
\frac{1+\xi\cdot(\frac{1}{\alpha})\cdot\left(\frac{f_{\theta}(i)}{f_{\varphi}(i)}-\alpha\right)}{1+\frac{(G(i)-F(i))}{w(i)\cdot f(i)}\left(1+\frac{1}{\varepsilon}\right)}.\label{eq:from code}
\end{equation}

\end_inset

The formula for the optimal marginal tax rate in Corollary 2 is given by
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none

\begin_inset Formula 
\begin{equation}
1-T'(y(w))=\frac{1+\xi\left(\frac{f_{x}^{\theta}(w)}{f_{x}^{\varphi}(w)}-\alpha(x)\right)}{1+\frac{G_{x}(w)-F_{x}(w)}{wf_{x}(w)}\left(1+\frac{1}{\varepsilon(w)}\right)}.\label{eq:from paper}
\end{equation}

\end_inset

The only difference between these formulas is thus the extra factor 
\begin_inset Formula $1/\alpha$
\end_inset

 which multiplies 
\begin_inset Formula $\xi$
\end_inset

 in 
\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:from code"

\end_inset

 but not in 
\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:from paper"

\end_inset

.
\end_layout

\end_body
\end_document
