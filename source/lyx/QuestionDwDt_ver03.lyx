#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 3cm
\rightmargin 3cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Section
Individuals take wage rates as given
\end_layout

\begin_layout Standard
Recall that
\begin_inset Formula 
\[
h^{j}=\left(\theta w^{j}\left(1-t\right)\right)^{\varepsilon}.
\]

\end_inset

When deciding about labor supply, individuals take wage rates as given.
 As a result
\begin_inset Formula 
\[
\frac{\partial h^{j}}{\partial t}=-\varepsilon\left(\theta w^{j}\left(1-t\right)\right)^{\varepsilon-1}\theta w^{j},
\]

\end_inset

and thus
\begin_inset Formula 
\[
\varepsilon^{h^{j},t}\equiv-\frac{\partial h_{\theta}^{j}}{\partial t}\frac{1-t}{h_{\theta}^{j}}=\varepsilon.
\]

\end_inset

Concerning the effect of a tax change on the wage rate we have
\begin_inset Formula 
\begin{align*}
\frac{\partial w^{L}}{\partial t}= & \underset{\gtrless0}{\underbrace{\underset{<0}{\underbrace{\frac{\partial w^{L}}{\partial L}}}\left(\underset{>0}{\underbrace{\frac{\partial L}{\partial h^{L}}}}\underset{<0}{\underbrace{\frac{\partial h^{L}}{\partial t}}}+\underset{>0}{\underbrace{\frac{\partial L}{\partial\theta^{*}}}}\underset{>0}{\underbrace{\frac{\partial\theta^{*}}{\partial t}}}\right)}}\\
+ & \underset{<0}{\underbrace{\underset{>0}{\underbrace{\frac{\partial w^{L}}{\partial H}}}\left(\underset{>0}{\underbrace{\frac{\partial H}{\partial h^{H}}}}\underset{<0}{\underbrace{\frac{\partial h^{H}}{\partial t}}}+\underset{<0}{\underbrace{\frac{\partial H}{\partial\theta^{*}}}}\underset{>0}{\underbrace{\frac{\partial\theta^{*}}{\partial t}}}\right)}},
\end{align*}

\end_inset

and
\begin_inset Formula 
\begin{align*}
\frac{\partial w^{H}}{\partial t}= & \underset{\gtrless0}{\underbrace{\underset{>0}{\underbrace{\frac{\partial w^{H}}{\partial L}}}\left(\underset{>0}{\underbrace{\frac{\partial L}{\partial h^{L}}}}\underset{<0}{\underbrace{\frac{\partial h^{L}}{\partial t}}}+\underset{>0}{\underbrace{\frac{\partial L}{\partial\theta^{*}}}}\underset{>0}{\underbrace{\frac{\partial\theta^{*}}{\partial t}}}\right)}}\\
+ & \underset{>0}{\underbrace{\underset{<0}{\underbrace{\frac{\partial w^{H}}{\partial H}}}\left(\underset{>0}{\underbrace{\frac{\partial H}{\partial h^{H}}}}\underset{<0}{\underbrace{\frac{\partial h^{H}}{\partial t}}}+\underset{<0}{\underbrace{\frac{\partial H}{\partial\theta^{*}}}}\underset{>0}{\underbrace{\frac{\partial\theta^{*}}{\partial t}}}\right)}}.
\end{align*}

\end_inset

When it comes to the effect of the subsidy on labor supply, we have
\begin_inset Formula 
\[
\varepsilon^{h^{j},t}\equiv\frac{\partial h_{\theta}^{j}}{\partial s}\frac{s}{h_{\theta}^{j}}=0.
\]

\end_inset

Finally, the effect of the subsidy on the wage is given by
\begin_inset Formula 
\begin{align*}
\frac{\partial w^{L}}{\partial s}= & \frac{\partial w^{L}}{\partial L}\frac{\partial L}{\partial\theta^{*}}\frac{\partial\theta^{*}}{\partial s}\\
+ & \frac{\partial w^{L}}{\partial H}\frac{\partial H}{\partial\theta^{*}}\frac{\partial\theta^{*}}{\partial s},
\end{align*}

\end_inset

and
\begin_inset Formula 
\begin{align*}
\frac{\partial w^{H}}{\partial s}= & \frac{\partial w^{H}}{\partial L}\frac{\partial L}{\partial\theta^{*}}\frac{\partial\theta^{*}}{\partial s}\\
+ & \frac{\partial w^{H}}{\partial H}\frac{\partial H}{\partial\theta^{*}}\frac{\partial\theta^{*}}{\partial s}.
\end{align*}

\end_inset


\end_layout

\begin_layout Section
Individuals realize that wage rates are endogenous
\end_layout

\begin_layout Standard
In this case the tax-elasticity of labor supply is
\begin_inset Formula 
\[
\varepsilon^{h^{j},t}=\varepsilon\left(1+\varepsilon^{w^{j},t}\right),
\]

\end_inset

with
\begin_inset Formula 
\[
\varepsilon^{w^{j},t}\equiv-\frac{\partial w^{j}}{\partial t}\frac{1-t}{w^{j}},
\]

\end_inset

being the tax-elasticity of the wage rate.
\end_layout

\begin_layout Standard
The subsidy elasticity of labor supply is given by
\begin_inset Formula 
\[
\varepsilon^{h^{j},s}\equiv\frac{\partial h^{j}}{\partial s}\frac{s}{h^{j}}=\varepsilon\varepsilon^{w^{j},s},
\]

\end_inset

where 
\begin_inset Formula $\varepsilon^{w^{j},s}$
\end_inset

 is the subsidy-elasticity of the wage rate, with
\begin_inset Formula 
\[
\varepsilon^{w^{j},s}\equiv\frac{\partial w^{j}}{\partial s}\frac{s}{w^{j}}.
\]

\end_inset

The effects of a tax change on wage rates is then given by
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align*}
\frac{\partial w^{L}}{\partial t}= & \frac{\partial w^{L}}{\partial L}\Bigg[\frac{\partial L}{\partial h^{L}}\left(\frac{\partial h^{L}}{\partial w^{L}}\mathbf{\frac{\partial w^{L}}{\partial t}}+\frac{\partial h^{L}}{\partial t}\right)\\
 & +\frac{\partial L}{\partial\theta^{*}}\left(\frac{\partial\theta^{*}}{\partial w^{L}}\mathbf{\frac{\partial w^{L}}{\partial t}}+\frac{\partial\theta^{*}}{\partial w^{H}}\mathbf{\frac{\partial w^{H}}{\partial t}}+\frac{\partial\theta^{*}}{\partial t}\right)\Bigg]\\
+ & \frac{\partial w^{L}}{\partial H}\Bigg[\frac{\partial H}{\partial h^{H}}\left(\frac{\partial h^{H}}{\partial w^{H}}\mathbf{\frac{\partial w^{H}}{\partial t}}+\frac{\partial h^{H}}{\partial t}\right)\\
 & +\frac{\partial H}{\partial\theta^{*}}\left(\frac{\partial\theta^{*}}{\partial w^{L}}\mathbf{\frac{\partial w^{L}}{\partial t}}+\frac{\partial\theta^{*}}{\partial w^{H}}\mathbf{\frac{\partial w^{H}}{\partial t}}+\frac{\partial\theta^{*}}{\partial t}\right)\Bigg].
\end{align*}

\end_inset

and
\begin_inset Formula 
\begin{align*}
\frac{\partial w^{H}}{\partial t}= & \frac{\partial w^{H}}{\partial L}\Bigg[\frac{\partial L}{\partial h^{L}}\left(\frac{\partial h^{L}}{\partial w^{L}}\mathbf{\frac{\partial w^{L}}{\partial t}}+\frac{\partial h^{L}}{\partial t}\right)\\
 & +\frac{\partial L}{\partial\theta^{*}}\left(\frac{\partial\theta^{*}}{\partial w^{L}}\mathbf{\frac{\partial w^{L}}{\partial t}}+\frac{\partial\theta^{*}}{\partial w^{H}}\mathbf{\frac{\partial w^{H}}{\partial t}}+\frac{\partial\theta^{*}}{\partial t}\right)\Bigg]\\
+ & \frac{\partial w^{H}}{\partial H}\Bigg[\frac{\partial H}{\partial h^{H}}\left(\frac{\partial h^{H}}{\partial w^{H}}\mathbf{\frac{\partial w^{H}}{\partial t}}+\frac{\partial h^{H}}{\partial t}\right)\\
 & +\frac{\partial H}{\partial\theta^{*}}\left(\frac{\partial\theta^{*}}{\partial w^{L}}\mathbf{\frac{\partial w^{L}}{\partial t}}+\frac{\partial\theta^{*}}{\partial w^{H}}\mathbf{\frac{\partial w^{H}}{\partial t}}+\frac{\partial\theta^{*}}{\partial t}\right)\Bigg].
\end{align*}

\end_inset

This is a system of two equations in two unknowns.
\end_layout

\begin_layout Section
Log-linearization
\end_layout

\begin_layout Standard
In order to investigate the effect of a change in the policy variables 
\begin_inset Formula $t$
\end_inset

 and 
\begin_inset Formula $s$
\end_inset

, as well as the effect of SBTC on the endogenous variables, we log-linearize
 the equilibrium equations.
 We denote a relative change in variable 
\begin_inset Formula $x$
\end_inset

 by 
\begin_inset Formula $\tilde{x}$
\end_inset

, and approximate it as follows 
\begin_inset Formula 
\[
\tilde{x}\equiv\frac{dx}{x^{*}}=\frac{x-x^{*}}{x^{*}}\approx d\ln x,
\]

\end_inset

where the asterisk indicates the equilibrium value.
 The only exception is the tax rate, which we approximate by 
\begin_inset Formula $\tilde{t}=\frac{dt}{(1-t)}$
\end_inset

, as this is more convenient.
 
\begin_inset Note Note
status open

\begin_layout Plain Layout
Consider the log-deviation
\begin_inset Formula 
\[
d\ln x=\ln x-\ln x^{*}=\ln\left(\frac{x}{x^{*}}\right)=\ln\left(1+\frac{x-x^{*}}{x^{*}}\right).
\]

\end_inset

Now, take a first-order Taylor approximation
\begin_inset Formula 
\begin{align*}
d\ln x\approx & \ln\left(1+\frac{x-x^{*}}{x^{*}}\right)|_{x=x^{*}}+\frac{x^{*}}{x}\frac{1}{x^{*}}|_{x=x^{*}}\left(x-x^{*}\right)\\
= & \frac{x-x^{*}}{x^{*}}.
\end{align*}

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Wages
\end_layout

\begin_layout Standard
We begin by approximating the first order conditions of the firm problem,
 which define the wages.
 We slightly modify the notation and write 
\begin_inset Formula 
\[
w^{H}=A^{H}Y_{2}\left(L,A^{H}H\right),
\]

\end_inset

where the index indicates wrt.
 to which argument the derivative has been taken.
 We then consider 
\begin_inset Formula $w^{H}$
\end_inset

 as a function of three variables, 
\begin_inset Formula $L$
\end_inset

, 
\begin_inset Formula $H$
\end_inset

, and 
\begin_inset Formula $A^{H}$
\end_inset

 and take a first-order Taylor approximation of the log deviation
\begin_inset Formula 
\begin{align*}
d\ln w^{H}\approx & \frac{Y_{21}}{Y_{2}}\left(L-L^{*}\right)\\
+ & \frac{A^{*}Y_{22}}{Y_{2}}\left(H-H^{*}\right)\\
+ & \frac{A^{*}Y_{22}H^{*}+Y_{2}}{A^{*}Y_{2}}\left(A-A^{*}\right).
\end{align*}

\end_inset

We rewrite as
\begin_inset Formula 
\begin{align*}
d\ln w^{H}\approx & \frac{Y_{21}L^{*}}{Y_{2}}\tilde{L}\\
+ & \frac{A^{*}Y_{22}H^{*}}{Y_{2}}\tilde{H}\\
+ & \left(\frac{Y_{22}H^{*}}{Y_{2}}+\frac{1}{A^{*}}\right)\left(A-A^{*}\right).\\
= & \frac{Y_{21}L^{*}}{Y_{2}}\tilde{L}\\
+ & \frac{A^{*}Y_{22}H^{*}}{Y_{2}}\tilde{H}\\
+ & \left(\frac{A^{*}Y_{22}H^{*}}{Y_{2}}\tilde{A}+\tilde{A}\right),
\end{align*}

\end_inset

which we can rewrite as
\begin_inset Formula 
\begin{align*}
d\ln w^{H}\approx & \frac{Y_{21}L^{*}}{Y_{2}}\tilde{L}\\
+ & \frac{A^{*}Y_{22}H^{*}}{Y_{2}}\left(\tilde{H}+\tilde{A}\right)\\
+ & \tilde{A}.
\end{align*}

\end_inset

Now, try to use Euler's Theorem:
\begin_inset Formula 
\[
Y_{1}L+AY_{2}H=Y.
\]

\end_inset

As a consequence,
\begin_inset Formula 
\begin{align*}
Y_{21}L+AY_{22}H=0 & \Leftrightarrow\\
AY_{22}H=-Y_{21}L
\end{align*}

\end_inset

We can thus write
\begin_inset Formula 
\begin{align*}
d\ln w^{H}\approx & \frac{Y_{21}L^{*}}{Y_{2}}\left(\tilde{L}-\tilde{H}-\tilde{A}\right)+\tilde{A}.\\
= & Y\frac{Y_{21}}{Y_{2}Y_{1}}\frac{Y_{1}L^{*}}{Y}\left(\tilde{L}-\tilde{H}-\tilde{A}\right)+\tilde{A}\\
= & \frac{\alpha}{\sigma}\left(\tilde{L}-\tilde{H}-\tilde{A}\right)+\tilde{A}.
\end{align*}

\end_inset

Now use
\begin_inset Formula 
\[
\alpha\equiv\frac{Lw^{L}}{Y},\ \left(1-\alpha\right)\equiv\frac{Hw^{H}}{Y}.
\]

\end_inset

Moreover, use that 
\begin_inset Formula 
\[
\frac{1}{\sigma}\equiv\frac{\partial w^{H}}{\partial L}\frac{Y}{w^{H}w^{L}}=Y_{21}\frac{Y}{Y_{2}Y_{1}}.
\]

\end_inset

We then obtain
\begin_inset Formula 
\begin{align*}
d\ln w^{H}\approx\tilde{w^{H}}\approx & \frac{\alpha}{\sigma}\left(\tilde{L}-\tilde{H}-\tilde{A}\right)+\tilde{A}=\frac{\alpha}{\sigma}\left(\tilde{L}-\tilde{H}\right)+\tilde{A}\left(1-\frac{\alpha}{\sigma}\right).
\end{align*}

\end_inset

We now consider the effect on 
\begin_inset Formula $w^{L}$
\end_inset

.
\begin_inset Formula 
\begin{align*}
d\ln w^{L}\approx & \frac{Y_{11}L^{*}}{Y_{1}}\tilde{L}\\
+ & \frac{A^{*}Y_{12}H^{*}}{Y_{1}}\tilde{H}\\
+ & \frac{Y_{12}A^{*}H^{*}}{Y_{1}}\tilde{A}.
\end{align*}

\end_inset

Using Euler's Theorem,
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align*}
Y_{11}L+AY_{12}H=0 & \Leftrightarrow\\
AY_{12}H=-Y_{11}L
\end{align*}

\end_inset

we rewrite the expression as
\begin_inset Formula 
\begin{align*}
d\ln w^{L}\approx & -\frac{A^{*}Y_{12}H^{*}}{Y_{1}}\tilde{L}\\
+ & \frac{A^{*}Y_{12}H^{*}}{Y_{1}}\tilde{H}\\
+ & \frac{Y_{12}A^{*}H^{*}}{Y_{1}}\tilde{A}\\
= & \frac{A^{*}Y_{12}H^{*}}{Y_{1}}\left(\tilde{H}-\tilde{L}+\tilde{A}\right).
\end{align*}

\end_inset

Now, use
\begin_inset Formula 
\[
Y_{12}=Y_{21}
\]

\end_inset

and write
\begin_inset Formula 
\[
Y\frac{Y_{21}Y_{2}A^{*}H^{*}}{Y_{1}Y_{2}Y}=\frac{1-\alpha}{\sigma},
\]

\end_inset

and thus
\begin_inset Formula 
\begin{align*}
d\ln w^{L}\approx\tilde{w^{L}}\approx & \frac{1-\alpha}{\sigma}\left(\tilde{H}-\tilde{L}+\tilde{A}\right).
\end{align*}

\end_inset

As expected, we observe that 
\begin_inset Formula $w^{L}$
\end_inset

 is increasing in 
\begin_inset Formula $H$
\end_inset

, and 
\begin_inset Formula $A$
\end_inset

 and decreasing in 
\begin_inset Formula $L$
\end_inset

.
 Moreover, a percentage increase in 
\begin_inset Formula $A$
\end_inset

 increases 
\begin_inset Formula $w^{H}$
\end_inset

 by 
\begin_inset Formula $1-\frac{\alpha}{\sigma}=\frac{\sigma-\alpha}{\sigma}$
\end_inset

, while it increases 
\begin_inset Formula $w^{L}$
\end_inset

 by 
\begin_inset Formula $\frac{1-\alpha}{\sigma}$
\end_inset

.
 Suppose for a moment that technology has no effect on total sectoral labor
 supplies.
 We would then have that an increase in 
\begin_inset Formula $A$
\end_inset

 raises 
\begin_inset Formula $w^{H}$
\end_inset

 more than 
\begin_inset Formula $w^{L}$
\end_inset

 if and only if 
\begin_inset Formula $\sigma>1$
\end_inset

.
 However, as we will see below, if the effect of technology on 
\begin_inset Formula $H$
\end_inset

 and 
\begin_inset Formula $L$
\end_inset

 is taken into account, 
\begin_inset Formula $\sigma>1$
\end_inset

 is only necessary but not sufficient for 
\begin_inset Formula $w^{H}$
\end_inset

 to increase more than 
\begin_inset Formula $w^{L}$
\end_inset

.
 We now turn to individual labor supply.
\end_layout

\begin_layout Standard
Actually, reconsider
\begin_inset Formula 
\[
\tilde{w^{L}}\approx\frac{1-\alpha}{\sigma}\left(\tilde{H}-\tilde{L}+\tilde{A}\right).
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\tilde{w^{H}}\approx\frac{\alpha}{\sigma}\left(\tilde{L}-\tilde{H}\right)+\tilde{A}\left(1-\frac{\alpha}{\sigma}\right).
\]

\end_inset

Then
\begin_inset Formula 
\begin{align*}
\tilde{w^{H}}>\tilde{w^{L}}=\frac{\alpha}{\sigma}\left(\tilde{L}-\tilde{H}\right)+\tilde{A}\left(1-\frac{\alpha}{\sigma}\right)>-\frac{1-\alpha}{\sigma}\left(\tilde{L}-\tilde{H}\right)+\frac{1-\alpha}{\sigma}\tilde{A} & \Leftrightarrow\\
\frac{1}{\sigma}\left(\tilde{L}-\tilde{H}\right)>\frac{1-\sigma}{\sigma}\tilde{A} & \Leftrightarrow\\
\left(\tilde{L}-\tilde{H}\right)>\left(1-\sigma\right)\tilde{A} & \Leftrightarrow\\
\tilde{H}-\tilde{L}<\left(\sigma-1\right)\tilde{A}.
\end{align*}

\end_inset

Now, the question is whether we can unambiguously say that the LHS is positive.
 (In that case we would still obtain that 
\begin_inset Formula $\sigma>1$
\end_inset

 is only a necessary but not a sufficient condition.
 We have
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\tilde{h^{j}}\approx\varepsilon\left(-\tilde{t}+\tilde{w^{j}}\right).
\]

\end_inset


\begin_inset Formula 
\[
\tilde{L}\approx\varepsilon\left(-\tilde{t}+\tilde{w^{L}}\right)+\theta^{*}h_{\theta^{*}}^{L}f\left(\theta^{*}\right)\tilde{\theta^{*}}.
\]

\end_inset


\begin_inset Formula 
\[
\tilde{H}\approx\varepsilon\left(-\tilde{t}+\tilde{w^{H}}\right)-\theta^{*}h_{\theta^{*}}^{H}f\left(\theta^{*}\right)\tilde{\theta^{*}}.
\]

\end_inset

The condition for 
\begin_inset Formula $\tilde{H}>\tilde{L}$
\end_inset

 then is
\begin_inset Formula 
\begin{align*}
\varepsilon\left(-\tilde{t}+\tilde{w^{H}}\right)-\theta^{*}h_{\theta^{*}}^{H}f\left(\theta^{*}\right)\tilde{\theta^{*}}>\varepsilon\left(-\tilde{t}+\tilde{w^{L}}\right)+\theta^{*}h_{\theta^{*}}^{L}f\left(\theta^{*}\right)\tilde{\theta^{*}} & \Leftrightarrow\\
\varepsilon\left(\tilde{w^{H}-}\tilde{w^{L}}\right)>\left(h_{\theta^{*}}^{L}+h_{\theta^{*}}^{H}\right)\theta^{*}f\left(\theta^{*}\right)\tilde{\theta^{*}}.
\end{align*}

\end_inset

Hence, 
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none

\begin_inset Formula $\left(\tilde{w^{H}-}\tilde{w^{L}}\right)>0$
\end_inset

 is a necessary (but not sufficient) condition for 
\begin_inset Formula $\tilde{H}-\tilde{L}>0$
\end_inset

.
 It is probably better to keep the fully solved condition for 
\begin_inset Formula $\tilde{w^{H}}>\tilde{w^{L}}$
\end_inset

.
\end_layout

\begin_layout Subsection
Individual labor supply
\end_layout

\begin_layout Standard
Individual labor supply is given by 
\begin_inset Formula 
\[
h_{\theta}^{j}=\left[\theta\left(1-t\right)w^{j}\right]^{\varepsilon}.
\]

\end_inset

The relative change in labor supply can be approximate by
\begin_inset Formula 
\[
\tilde{h^{j}}\approx\varepsilon\left(-\tilde{t}+\tilde{w^{j}}\right).
\]

\end_inset


\end_layout

\begin_layout Subsection
Total sectoral labor supply
\end_layout

\begin_layout Standard
We approximate
\begin_inset Formula 
\[
\frac{dL}{L}=\frac{L-L^{*}}{L^{*}}
\]

\end_inset

by first decomposing the change in 
\begin_inset Formula $L$
\end_inset

 into a change due to a change in labor supply 
\begin_inset Formula $h^{L}$
\end_inset

, and a change due to a change in the threshold 
\begin_inset Formula $\theta^{*}$
\end_inset

.
\begin_inset Formula 
\begin{align*}
\tilde{L}\approx\frac{dL}{L}= & \frac{\int_{\underline{\theta}}^{\theta^{*}}\theta h_{\theta}^{L}dF\left(\theta\right)-\int_{\underline{\theta}}^{\theta^{*}}\theta h_{\theta}^{*L}dF\left(\theta\right)}{\int_{\underline{\theta}}^{\theta^{*}}\theta h_{\theta}^{*L}dF\left(\theta\right)}\\
+ & \frac{1}{L}\theta^{*}h_{\theta^{*}}^{L}f\left(\theta^{*}\right)\frac{\theta^{*}-\theta^{**}}{\theta^{**}}\\
= & \frac{\int_{\underline{\theta}}^{\theta^{*}}\theta\frac{dh_{\theta}^{L}}{h_{\theta}^{*L}}h_{\theta}^{*L}dF\left(\theta\right)}{\int_{\underline{\theta}}^{\theta^{*}}\theta h_{\theta}^{*L}dF\left(\theta\right)}\\
+ & \frac{1}{L}\theta^{*}h_{\theta^{*}}^{L}f\left(\theta^{*}\right)\frac{d\theta^{*}}{\theta^{**}}\\
\approx & \frac{\int_{\underline{\theta}}^{\theta^{*}}\theta\tilde{h}^{L}h_{\theta}^{*L}dF\left(\theta\right)}{\int_{\underline{\theta}}^{\theta^{*}}\theta h_{\theta}^{*L}dF\left(\theta\right)}\\
+ & \frac{1}{L}\theta^{*}h_{\theta^{*}}^{L}f\left(\theta^{*}\right)\tilde{\theta^{*}}\\
= & \tilde{h}^{L}\\
+ & \frac{1}{L}\theta^{*}h_{\theta^{*}}^{L}f\left(\theta^{*}\right)\tilde{\theta^{*}}.
\end{align*}

\end_inset

Where the last step uses that 
\begin_inset Formula $\tilde{h}^{L}$
\end_inset

 does not depend on 
\begin_inset Formula $\theta$
\end_inset

 anymore.
 Similarly, we have
\begin_inset Formula 
\[
\tilde{H}\approx\tilde{h}^{H}-\frac{1}{H}\theta^{*}h_{\theta^{*}}^{H}f\left(\theta^{*}\right)\tilde{\theta^{*}}.
\]

\end_inset


\end_layout

\begin_layout Subsection
Threshold
\end_layout

\begin_layout Standard
Next, we consider a relative change in threshold 
\begin_inset Formula $\theta^{*}$
\end_inset

.
 Recall that
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none

\begin_inset Formula 
\[
\theta^{*}=\left[\frac{\left(p-s\right)\left(1+\varepsilon\right)}{\left(1-t\right)^{1+\varepsilon}\left(\left(w^{H}\right)^{1+\varepsilon}-\left(w^{L}\right)^{1+\varepsilon}\right)}\right]^{\frac{1}{1+\varepsilon}}.
\]

\end_inset

Taking logs leads to
\begin_inset Formula 
\[
\ln\theta^{*}=\frac{1}{1+\varepsilon}\left[\ln\left(p-s\right)+\ln\left(1+\varepsilon\right)-\left(1+\varepsilon\right)\ln\left(1-t\right)-\ln\left(\left(w^{H}\right)^{1+\varepsilon}-\left(w^{L}\right)^{1+\varepsilon}\right)\right].
\]

\end_inset


\end_layout

\begin_layout Standard

\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
Now consider 
\begin_inset Formula $d\ln\theta^{*}$
\end_inset


\begin_inset Formula 
\begin{align*}
d\ln\theta^{*}= & \frac{1}{1+\varepsilon}\left[d\ln\left(p-s\right)+d\ln\left(1+\varepsilon\right)-\left(1+\varepsilon\right)d\ln\left(1-t\right)-d\ln\left(\left(w^{H}\right)^{1+\varepsilon}-\left(w^{L}\right)^{1+\varepsilon}\right)\right]
\end{align*}

\end_inset


\end_layout

\begin_layout Standard

\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
Next, we take a first-order Taylor approximation wrt.
 
\begin_inset Formula $1-t$
\end_inset

, 
\begin_inset Formula $s$
\end_inset

, 
\begin_inset Formula $w^{H}$
\end_inset

, and 
\begin_inset Formula $w^{L}$
\end_inset

 .
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align*}
d\ln\theta^{*}\approx & \frac{1}{1+\varepsilon}\left[\frac{d\left(p-s\right)}{\left(p-s\right)}-\left(1+\varepsilon\right)\left(-\tilde{t}\right)-\frac{\left(1+\varepsilon\right)\left(\left(w^{H}\right)^{\varepsilon}dw^{H}-\left(w^{L}\right)^{\varepsilon}dw^{L}\right)}{\left(\left(w^{H}\right)^{1+\varepsilon}-\left(w^{L}\right)^{1+\varepsilon}\right)}\right]
\end{align*}

\end_inset

Now we rewrite the expression such that wage changes are also expressed
 in relative terms
\begin_inset Formula 
\begin{align*}
d\ln\theta^{*}\approx\tilde{\theta^{*}}\approx & \frac{1}{1+\varepsilon}\left[\frac{d\left(p-s\right)}{\left(p-s\right)}-\left(1+\varepsilon\right)\left(-\tilde{t}\right)-\frac{\left(1+\varepsilon\right)\left(\left(w^{H}\right)^{\varepsilon}\frac{dw^{H}}{w^{H}}w^{H}-\left(w^{L}\right)^{\varepsilon}\frac{dw^{L}}{w^{L}}w^{L}\right)}{\left(\left(w^{H}\right)^{1+\varepsilon}-\left(w^{L}\right)^{1+\varepsilon}\right)}\right]\\
\approx & \frac{1}{1+\varepsilon}\left[\tilde{\left(p-s\right)}-\left(1+\varepsilon\right)\left(-\tilde{t}\right)-\frac{\left(1+\varepsilon\right)\left(\left(w^{H}\right)^{1+\varepsilon}\tilde{w^{H}}-\left(w^{L}\right)^{1+\varepsilon}\tilde{w^{L}}\right)}{\left(\left(w^{H}\right)^{1+\varepsilon}-\left(w^{L}\right)^{1+\varepsilon}\right)}\right]\\
= & \frac{1}{1+\varepsilon}\tilde{\left(p-s\right)}+\tilde{t}-\frac{\left(w^{H}\right)^{1+\varepsilon}}{\left(w^{H}\right)^{1+\varepsilon}-\left(w^{L}\right)^{1+\varepsilon}}\tilde{w^{H}}+\frac{\left(w^{L}\right)^{1+\varepsilon}}{\left(w^{H}\right)^{1+\varepsilon}-\left(w^{L}\right)^{1+\varepsilon}}\tilde{w^{L}}.
\end{align*}

\end_inset


\end_layout

\begin_layout Subsection
System of equations
\end_layout

\begin_layout Standard
We have now approximated relative changes in all endogenous variables, leading
 to the inhomogeneous linear system of seven equations and seven unknowns.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\tilde{w}^{L}+\frac{1-\alpha}{\sigma}\tilde{L}-\frac{1-\alpha}{\sigma}\tilde{H}=\frac{1-\alpha}{\sigma}\tilde{A},
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\tilde{w}^{H}-\frac{\alpha}{\sigma}\tilde{L}+\frac{\alpha}{\sigma}\tilde{H}=\left(1-\frac{\alpha}{\sigma}\right)\tilde{A},
\]

\end_inset


\begin_inset Formula 
\[
\tilde{h}^{L}-\varepsilon\tilde{w}^{L}=-\varepsilon\tilde{t},
\]

\end_inset


\begin_inset Formula 
\[
\tilde{h}^{H}-\varepsilon\tilde{w}^{H}=-\varepsilon\tilde{t},
\]

\end_inset


\begin_inset Formula 
\[
\tilde{L}-\tilde{h}^{L}-\frac{h_{\theta^{*}}^{L}}{L}\theta^{*}f\left(\theta^{*}\right)\tilde{\theta^{*}}=0,
\]

\end_inset


\begin_inset Formula 
\[
\tilde{H}-\tilde{h}^{H}+\frac{h_{\theta^{*}}^{H}}{H}\theta^{*}f\left(\theta^{*}\right)\tilde{\theta^{*}}=0,
\]

\end_inset


\begin_inset Formula 
\[
\tilde{\theta^{*}}\approx\frac{1}{1+\varepsilon}\tilde{\left(p-s\right)}+\tilde{t}-\frac{\left(w^{H}\right)^{1+\varepsilon}}{\left(w^{H}\right)^{1+\varepsilon}-\left(w^{L}\right)^{1+\varepsilon}}\tilde{w^{H}}+\frac{\left(w^{L}\right)^{1+\varepsilon}}{\left(w^{H}\right)^{1+\varepsilon}-\left(w^{L}\right)^{1+\varepsilon}}\tilde{w^{L}}.
\]

\end_inset

In order to simplify notation we define
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\rho\equiv1+\varepsilon
\]

\end_inset

and
\begin_inset Formula 
\[
\Delta_{w}\equiv\left(w^{H}\right)^{1+\varepsilon}-\left(w^{L}\right)^{1+\varepsilon}
\]

\end_inset

to write
\begin_inset Formula 
\[
\tilde{\theta^{*}}-\frac{\left(w^{L}\right)^{\rho}}{\Delta_{w}}\tilde{w^{L}}+\frac{\left(w^{H}\right)^{\rho}}{\Delta_{w}}\tilde{w^{H}}=\tilde{t}+\frac{1}{\rho}\tilde{\left(p-s\right)}.
\]

\end_inset

Moreover, we define
\begin_inset Formula 
\[
\kappa_{L}\equiv\theta^{*}\frac{h_{\theta^{*}}^{L}}{L}f\left(\theta^{*}\right),
\]

\end_inset

and
\begin_inset Formula 
\[
\kappa_{H}\equiv\theta^{*}\frac{h_{\theta^{*}}^{H}}{H}f\left(\theta^{*}\right).
\]

\end_inset

In matrix form the system reads 
\begin_inset Formula $Mx=b$
\end_inset

, with
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
M=\left[\begin{array}{ccccccc}
1 & 0 & 0 & 0 & \frac{1-\alpha}{\sigma} & -\frac{1-\alpha}{\sigma} & 0\\
0 & 1 & 0 & 0 & -\frac{\alpha}{\sigma} & \frac{\alpha}{\sigma} & 0\\
-\varepsilon & 0 & 1 & 0 & 0 & 0 & 0\\
0 & -\varepsilon & 0 & 1 & 0 & 0 & 0\\
0 & 0 & -1 & 0 & 1 & 0 & -\kappa_{L}\\
0 & 0 & 0 & -1 & 0 & 1 & \kappa_{H}\\
-\frac{\left(w^{L}\right)^{\rho}}{\Delta_{w}} & \frac{\left(w^{H}\right)^{\rho}}{\Delta_{w}} & 0 & 0 & 0 & 0 & 1
\end{array}\right],\ x=\left[\begin{array}{c}
\tilde{w^{L}}\\
\tilde{w^{H}}\\
\tilde{h^{L}}\\
\tilde{h^{H}}\\
\tilde{L}\\
\tilde{H}\\
\tilde{\theta^{*}}
\end{array}\right],\ b=\left[\begin{array}{c}
\frac{1-\alpha}{\sigma}\tilde{A}\\
\left(1-\frac{\alpha}{\sigma}\right)\tilde{A}\\
-\varepsilon\tilde{t}\\
-\varepsilon\tilde{t}\\
0\\
0\\
\frac{1}{\rho}\tilde{\left(p-s\right)}+\tilde{t}
\end{array}\right].
\]

\end_inset


\end_layout

\begin_layout Subsection
Solutions
\end_layout

\begin_layout Standard
The solutions are
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
\tilde{w}^{L}=\frac{(-1+\alpha)\left(-\left(\tilde{p-s}+\rho\tilde{t}\right)\Delta_{w}\left(\kappa_{H}+\kappa_{L}\right)+\rho\tilde{A}\left((1+\epsilon)\Delta_{w}+\left(w^{H}\right)^{\rho}\left(\kappa_{H}+\kappa_{L}\right)\right)\right)}{\rho\left(-(\epsilon+\sigma)\Delta_{w}+\left(\left(w^{L}\right)^{\rho}(-1+\alpha)-\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)\right)},\label{eq:wLtilde-solution}
\end{equation}

\end_inset


\begin_inset Formula 
\begin{equation}
\tilde{w}^{H}=\frac{\alpha\left(\tilde{p-s}+\rho\tilde{t}\right)\Delta_{w}\left(\kappa_{H}+\kappa_{L}\right)+\rho\tilde{A}\left((\epsilon-\alpha(1+\epsilon)+\sigma)\Delta_{w}-\left(w^{L}\right)^{\rho}(-1+\alpha)\left(\kappa_{H}+\kappa_{L}\right)\right)}{\rho\left((\epsilon+\sigma)\Delta_{w}+\left(-\left(w^{L}\right)^{\rho}(-1+\alpha)+\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)\right)},\label{eq:wHtilde-solution}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align}
\tilde{h}^{L}= & -\epsilon\Bigg[-(-1+\alpha)\tilde{p-s}\Delta_{w}\left(\kappa_{H}+\kappa_{L}\right)+(-1+\alpha)\rho\tilde{A}\left((1+\epsilon)\Delta_{w}+\left(w^{H}\right)^{\rho}\left(\kappa_{H}+\kappa_{L}\right)\right)\nonumber \\
+ & \rho\tilde{t}\left(\left(-\left(w^{L}\right)^{\rho}(-1+\alpha)+\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)+\Delta_{w}\left(\epsilon+\sigma+\kappa_{H}-\alpha\kappa_{H}+\kappa_{L}-\alpha\kappa_{L}\right)\right)\Bigg]\label{eq:hLtilde}\\
\cdot & \rho^{-1}\left((\epsilon+\sigma)\Delta_{w}+\left(-\left(w^{L}\right)^{\rho}(-1+\alpha)+\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)\right)^{-1},\nonumber 
\end{align}

\end_inset


\begin_inset Formula 
\begin{align}
\tilde{h}^{H}= & \epsilon\Bigg[\alpha\tilde{p-s}\Delta_{w}\left(\kappa_{H}+\kappa_{L}\right)+\rho\tilde{A}\left((\epsilon-\alpha(1+\epsilon)+\sigma)\Delta_{w}-\left(w^{L}\right)^{\rho}(-1+\alpha)\left(\kappa_{H}+\kappa_{L}\right)\right)\nonumber \\
- & \rho\tilde{t}\left(\left(-\left(w^{L}\right)^{\rho}(-1+\alpha)+\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)+\Delta_{w}\left(\epsilon+\sigma-\alpha\left(\kappa_{H}+\kappa_{L}\right)\right)\right)\Bigg]\label{eq:hHtilde}\\
\cdot & \rho^{-1}\left((\epsilon+\sigma)\Delta_{w}+\left(-\left(w^{L}\right)^{\rho}(-1+\alpha)+\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)\right)^{-1},\nonumber 
\end{align}

\end_inset


\begin_inset Formula 
\begin{align*}
\tilde{L}= & \Bigg[\tilde{p-s}\Delta_{w}\left((-1+\alpha)\epsilon\kappa_{H}+(\alpha\epsilon+\sigma)\kappa_{L}\right)+\\
+ & \rho\tilde{A}\left(-(-1+\alpha)(1+\epsilon)\left(\epsilon\Delta_{w}+\left(w^{L}\right)^{\rho}\kappa_{L}\right)+\left(w^{H}\right)^{\rho}\left((\epsilon-\alpha\epsilon)\kappa_{H}+(\alpha-\sigma)\kappa_{L}\right)\right)\\
+ & \rho\tilde{t}\left(\left(\left(w^{L}\right)^{\rho}(-1+\alpha)-\left(w^{H}\right)^{\rho}\alpha\right)\epsilon\left(\kappa_{H}+\kappa_{L}\right)+\Delta_{w}\left(-\epsilon(\epsilon+\sigma)+(-1+\alpha)\epsilon\kappa_{H}+(\alpha\epsilon+\sigma)\kappa_{L}\right)\right)\Bigg]\\
\cdot & \rho^{-1}\left((\epsilon+\sigma)\Delta_{w}+\left(-\left(w^{L}\right)^{\rho}(-1+\alpha)+\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)\right)^{-1},
\end{align*}

\end_inset


\begin_inset Formula 
\begin{align*}
\tilde{H}= & \Bigg[\tilde{p-s}\Delta_{w}\left(((-1+\alpha)\epsilon-\sigma)\kappa_{H}+\alpha\epsilon\kappa_{L}\right)\\
+ & \rho\tilde{A}\left(-(\alpha-\epsilon+\alpha\epsilon-\sigma)\left(\epsilon\Delta_{w}+\left(w^{H}\right)^{\rho}\kappa_{H}\right)+\left(w^{L}\right)^{\rho}(-1+\alpha)\left(\kappa_{H}-\epsilon\kappa_{L}\right)\right)\\
+ & \rho\tilde{t}\left(\left(\left(w^{L}\right)^{\rho}(-1+\alpha)-\left(w^{H}\right)^{\rho}\alpha\right)\epsilon\left(\kappa_{H}+\kappa_{L}\right)-\Delta_{w}\left((\epsilon-\alpha\epsilon+\sigma)\kappa_{H}+\epsilon\left(\epsilon+\sigma-\alpha\kappa_{L}\right)\right)\right)\Bigg]\\
\cdot & \rho^{-1}\left((\epsilon+\sigma)\Delta_{w}+\left(-\left(w^{L}\right)^{\rho}(-1+\alpha)+\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)\right)^{-1},
\end{align*}

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align}
\tilde{\theta}^{*}= & \frac{\rho\left(\left(w^{L}\right)^{\rho}(-1+\alpha)(1+\epsilon)+\left(w^{H}\right)^{\rho}(\epsilon-\alpha(1+\epsilon)+\sigma)\right)\tilde{A}-(\epsilon+\sigma)\left(\tilde{p-s}+\rho\tilde{t}\right)\Delta_{w}}{\rho\left(-(\epsilon+\sigma)\Delta_{w}+\left(\left(w^{L}\right)^{\rho}(-1+\alpha)-\left(w^{H}\right)^{\rho}\alpha\right)\left(\kappa_{H}+\kappa_{L}\right)\right)}.\label{eq:thetatilde}
\end{align}

\end_inset


\end_layout

\begin_layout Standard
We observe that 
\begin_inset Formula $\tilde{h}^{L}$
\end_inset

 and 
\begin_inset Formula $\tilde{h}^{H}$
\end_inset

 depend on 
\begin_inset Formula $\tilde{\left(p-s\right)}$
\end_inset

 hence intensive labor supply responds to a change in the subsidy - which
 can only be the case if individuals take general-equilibrium effects into
 account.
\end_layout

\end_body
\end_document
