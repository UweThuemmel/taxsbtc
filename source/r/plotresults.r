# File for plotting simulation results
library(ggplot2)
library(gridExtra)

file <- "2014-07-28--10-58.csv"
path <- "/media/data/Dropbox/PhD/projects/taxsbtc/source/mathematica/output/"

fullfile <- paste(file,path,sep="")
d<-read.csv(fullfile)

## Set working directory
setwd("/media/data/Dropbox/PhD/projects/taxsbtc/source/mathematica/output/plots")

## Specify footnote text
footnote <- paste(d$comment,file,sep=" : ")

# Wage rates
p <- ggplot(d,aes(AH)) +
  geom_line(aes(y=wH, linetype="wH")) + 
  geom_line(aes(y=wL, linetype="wL")) +
  xlab(expression(A^H)) +
  ylab(expression(A^H ~ "," ~ A^L)) +
  ## ggtitle("Wage rates") +
  scale_linetype_manual(values=c("dashed", "solid"),name="")

## add footnote
g <- arrangeGrob(p, 
                 sub = textGrob(footnote, x = 0, hjust = -0.1, vjust=0.1,
                     gp = gpar(fontface = "italic", fontsize = 10)))


ggsave(file="wLwH.pdf",g)


# Total sectoral labor supplies
p <- ggplot(d,aes(AH)) +
  geom_line(aes(y=L, linetype="dashed")) +
  geom_line(aes(y=H, linetype="solid")) + 
  xlab(expression(A^H)) +
  ylab("L, H") +
  ## ggtitle("Total sectoral labor supplies") +
  scale_linetype_manual(values=c("dashed", "solid"),name="",labels=c("L","H"))

g <- arrangeGrob(p, 
                 sub = textGrob(footnote, x = 0, hjust = -0.1, vjust=0.1,
                     gp = gpar(fontface = "italic", fontsize = 10)))

ggsave(file="HL.pdf",g)


# Thetastar
p <- ggplot(d,aes(AH)) +
  geom_line(aes(y=thetastar)) + 
  xlab(expression(A^H)) +
  ylab(expression(theta^"*")) 
  ## ggtitle(expression("Threshold"~theta^"*"))

g <- arrangeGrob(p, 
                 sub = textGrob(footnote, x = 0, hjust = -0.1, vjust=0.1,
                     gp = gpar(fontface = "italic", fontsize = 10)))

ggsave(file="thetastar.pdf",g)


# Optimal tax rate
p <- ggplot(d,aes(AH)) +
  geom_line(aes(y=t)) + 
  xlab(expression(A^H)) +
  ylab("t") 
  ## ggtitle("Optimal tax rate")

g <- arrangeGrob(p, 
                 sub = textGrob(footnote, x = 0, hjust = -0.1, vjust=0.1,
                     gp = gpar(fontface = "italic", fontsize = 10)))

ggsave(file="t.pdf",g)


# xi
p <- ggplot(d,aes(AH)) +
  geom_line(aes(y=xi)) + 
  xlab(expression(A^H)) +
  ylab(expression(xi)) 
  ## ggtitle(expression("Distributional characteristic"~xi))

g <- arrangeGrob(p, 
                 sub = textGrob(footnote, x = 0, hjust = -0.1, vjust=0.1,
                     gp = gpar(fontface = "italic", fontsize = 10)))

ggsave(file="xi.pdf",g)


# wedge
p <- ggplot(d,aes(AH)) +
  geom_line(aes(y=ddelta)) + 
  xlab(expression(A^H)) +
  ylab(expression(Delta)) 
  ## ggtitle(expression("Wedge"~Delta))

g <- arrangeGrob(p, 
                 sub = textGrob(footnote, x = 0, hjust = -0.1, vjust=0.1,
                     gp = gpar(fontface = "italic", fontsize = 10)))

ggsave(file="ddelta.pdf",g)


# weigths qL, qH
mylabs <- list(expression(q^L),expression(q^H))
p <- ggplot(d,aes(AH)) +
  geom_line(aes(y=qL,linetype="dashed")) + 
  geom_line(aes(y=qH,linetype="solid")) +
  xlab(expression(A^H)) +
  ylab(expression(q^L~","~q^H)) +
  ## ggtitle(expression("Weights"~q^L~","~q^H)) +
  scale_linetype_manual(values=c("dashed", "solid"),name="",labels=mylabs)

g <- arrangeGrob(p, 
                 sub = textGrob(footnote, x = 0, hjust = -0.1, vjust=0.1,
                     gp = gpar(fontface = "italic", fontsize = 10)))

ggsave(file="qLqH.pdf",g)


# incomes zLbar, zHbar
mylabs <- list(expression(bar(z)^L),expression(bar(z)^H))
p <- ggplot(d,aes(AH)) +
  geom_line(aes(y=zLbar,linetype="dashed")) + 
  geom_line(aes(y=zHbar,linetype="solid")) +
  xlab(expression(A^H)) +
  ylab(expression(bar(z)^L~","~bar(z)^H)) +
  ## ggtitle(expression("Sectoral incomes"~bar(z)^L~","~bar(z)^H)) +
  scale_linetype_manual(values=c("dashed","solid"),name="",labels=mylabs)

g <- arrangeGrob(p, 
                 sub = textGrob(footnote, x = 0, hjust = -0.1, vjust=0.1,
                     gp = gpar(fontface = "italic", fontsize = 10)))

ggsave(file="zLbarzHbar.pdf",g)


# alpha
p <- ggplot(d,aes(AH)) +
  geom_line(aes(y=alpha)) + 
  xlab(expression(A^H)) +
  ylab(expression(alpha)) 
  ## ggtitle(expression("Income share" ~alpha~"of high-school sector"))

g <- arrangeGrob(p, 
                 sub = textGrob(footnote, x = 0, hjust = -0.1, vjust=0.1,
                     gp = gpar(fontface = "italic", fontsize = 10)))

ggsave(file="alpha.pdf",g)


# total income
p <- ggplot(d,aes(AH)) +
  geom_line(aes(y=zbar)) + 
  xlab(expression(A^H)) +
  ylab(expression(bar(z))) 
  ## ggtitle(expression("Total income" ~bar(z)))

g <- arrangeGrob(p, 
                 sub = textGrob(footnote, x = 0, hjust = -0.1, vjust=0.1,
                     gp = gpar(fontface = "italic", fontsize = 10)))

ggsave(file="zbar.pdf",g)


# delta
p <- ggplot(d,aes(AH)) +
  geom_line(aes(y=delta)) + 
  xlab(expression(A^H)) +
  ylab(expression(delta)) 
  ## ggtitle(expression(delta))

g <- arrangeGrob(p, 
                 sub = textGrob(footnote, x = 0, hjust = -0.1, vjust=0.1,
                     gp = gpar(fontface = "italic", fontsize = 10)))

ggsave(file="delta.pdf",g)


# epsthetastart
p <- ggplot(d,aes(AH)) +
  geom_line(aes(y=epsthetastart)) + 
  xlab(expression(A^H)) +
  ylab(expression(epsilon^theta^"*,t")) 
  ## ggtitle(expression(delta))

g <- arrangeGrob(p, 
                 sub = textGrob(footnote, x = 0, hjust = -0.1, vjust=0.1,
                     gp = gpar(fontface = "italic", fontsize = 10)))

ggsave(file="epsthetastart.pdf",g)


# epsbarht
p <- ggplot(d,aes(AH)) +
  geom_line(aes(y=epsbarht)) + 
  xlab(expression(A^H)) +
  ylab(expression(bar(epsilon)^"h,t")) 
  ## ggtitle(expression(delta))

g <- arrangeGrob(p, 
                 sub = textGrob(footnote, x = 0, hjust = -0.1, vjust=0.1,
                     gp = gpar(fontface = "italic", fontsize = 10)))

ggsave(file="epsbarht.pdf",g)


# epshLt,epshHt
mylabs <- list(expression(epsilon^(h^H~",t")),expression(epsilon^(h^L~",t")))
p <- ggplot(d,aes(AH)) +
  geom_line(aes(y=epshLt,linetype="dashed")) + 
  geom_line(aes(y=epshHt,linetype="solid")) +
  xlab(expression(A^H)) +
  ylab(expression(epsilon^(h^H~",t")~","~epsilon^(h^L~",t"))) +
  ## ggtitle(expression("Sectoral incomes"~bar(z)^L~","~bar(z)^H)) +
  scale_linetype_manual(values=c("dashed","solid"),name="",labels=mylabs)

g <- arrangeGrob(p, 
                 sub = textGrob(footnote, x = 0, hjust = -0.1, vjust=0.1,
                     gp = gpar(fontface = "italic", fontsize = 10)))

ggsave(file="epshHtepshLt.pdf",g)

